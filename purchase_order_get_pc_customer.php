<?php 

    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $amh_pc = new AMH_PC();

    $arr_usrcus = $amh_pc->aut_user_customer($_REQUEST["user_name"], $_REQUEST["cus_id"]);

    $str_ret = json_encode($arr_usrcus);

    echo $str_ret;
<?php
require_once("include_function.php");
/*
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
*/	
require_once ("assets/classes/PHPExcel.php");
/** Include PHPExcel_IOFactory */
require_once ("assets/classes/PHPExcel/IOFactory.php");


class AMH_rp_sale_excel
{
	
	public function del_files($path)
	{
		
		$files = glob($path."*");
		$now   = time();

		foreach ($files as $file)
		if (is_file($file))
			if ($now - filemtime($file) >= 60 * 60 * 24 * 1) // 1 days
				unlink($file);
	}
	
	public function gen_excel_sale($arr_data, $excel_path, $pro_img_path,$start_date, $end_date,$report_type)
    {
		
		$this->del_files($excel_path);
		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		// Set document properties

		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
		
		// Create a first sheet, representing sales data
		$objPHPExcel->setActiveSheetIndex(0);
		if ($report_type == "SALE")
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SALE REPORT FOR PC');
		if ($report_type == "ORDER")
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ORDER REPORT FOR PC');
		
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Export Date :');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', PHPExcel_Shared_Date::PHPToExcel( gmmktime(0,0,0,date('m'),date('d'),date('Y')) ));
		$objPHPExcel->getActiveSheet()->getStyle('E1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
		
		// Rename first worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Sheet 1');
		$objPHPExcel->setActiveSheetIndex(0);
		
		// Save Excel 2007 file
		
		$callStartTime = microtime(true);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		
		
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'PC Name : '.$arr_data [0]["PC_NAME"]);
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Sale Date : '.$start_date.' to '.$end_date);
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "Pro. Code");
		$objPHPExcel->getActiveSheet()->setCellValue('B4', "Pro. Image");
		$objPHPExcel->getActiveSheet()->setCellValue('C4', "Product Name");
		$objPHPExcel->getActiveSheet()->setCellValue('D4', "Product Type");
		$objPHPExcel->getActiveSheet()->setCellValue('E4', "QTY");
		$objPHPExcel->getActiveSheet()->setCellValue('F4', "Amount Baht");
		
		$row_index = 5;
        foreach ($arr_data as $row)
        {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row_index, $row["PRODUCT_CODE"]);
			
			
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setPath($pro_img_path.$row['PRD_IMG']);
			$objDrawing->setCoordinates('B'.$row_index);
			
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row_index, $row["PRODUCT_NAME_TH"]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row_index, $row["TYPE_NAME_TH"]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row_index, $row["QTY"]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$row_index,str_replace(",","", $row["AMOUNT"]));
			
			
			$objDrawing->setHeight(72);
			$objDrawing->setOffsetX(5);
			$objDrawing->setOffsetY(5);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			
			// Format Row
			$objPHPExcel->getActiveSheet()->getRowDimension($row_index)->setRowHeight(60);
			$row_index++;
            
        }
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$row_index,'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$row_index, '=SUM(E5:E'.($row_index-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$row_index, '=SUM(F5:F'.($row_index-1).')');
		//****Configulation Format Table
		//Format Columb 'A'
		//Format Column 'B'
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(70);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(35);
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(28);
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(28);
		
		//Set Style
		$objPHPExcel->getActiveSheet()->getStyle('A4:F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('F5:F'.$row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle('A5:F'.$row_index)->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getStyle('A5:F'.$row_index)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$objPHPExcel->getActiveSheet()->getStyle('A1:F3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		
		//Set Font
		$objPHPExcel->getActiveSheet()->getStyle('A1:F3')->getFont()->setName('Tahoma');
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		$objPHPExcel->getActiveSheet()->getStyle('D1:E3')->getFont()->setSize(10);
		
		$objPHPExcel->getActiveSheet()->getStyle('A4:F'.$row_index)->getFont()->setName('Cordia New');
		$objPHPExcel->getActiveSheet()->getStyle('A4:F4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A4:F'.$row_index)->getFont()->setSize(14);
		$objPHPExcel->getActiveSheet()->getStyle("F4:F".$row_index)->getNumberFormat()->setFormatCode('#,##0.00');
		
		
		//Set Border
		$objPHPExcel->getActiveSheet()->getStyle('A4:F'.$row_index)->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
				)
			)
		);
		//Set Color Fill
		$objPHPExcel->getActiveSheet()->getStyle('A4:F4')->applyFromArray(
			array(
				'fill' => array( 
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'd1d1d1')
				)
			)
		);
		
		//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
		
		$objWriter->save(str_replace(__FILE__, $excel_path.iconv('UTF-8','UTF-8',$arr_data [0]["EXCEL_NAME"]), __FILE__));
		
		
		
    }
	
	public function gen_excel_sale_daily($arr_data, $excel_path, $pro_img_path,$start_date, $end_date,$report_type)
    {
		
		//$this->del_files($excel_path);
		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		// Set document properties

		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
		
		// Create a first sheet, representing sales data
		$objPHPExcel->setActiveSheetIndex(0);
		if ($report_type == "SALE")
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SALE REPORT FOR PC DAILY');
		if ($report_type == "ORDER")
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ORDER REPORT FOR PC DAILY');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Export Date :');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', PHPExcel_Shared_Date::PHPToExcel( gmmktime(0,0,0,date('m'),date('d'),date('Y')) ));
		$objPHPExcel->getActiveSheet()->getStyle('H1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
		
		// Rename first worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Sheet');
		$objPHPExcel->setActiveSheetIndex(0);
		
		// Save Excel 2007 file
		
		$callStartTime = microtime(true);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		
		
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'PC Name : '.$arr_data [0]["PC_NAME"]);
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Sale Date : '.$start_date.' to '.$end_date);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "Date");
		$objPHPExcel->getActiveSheet()->setCellValue('B4', "No. of PC");
		$objPHPExcel->getActiveSheet()->setCellValue('C4', "Pro. Code");
		$objPHPExcel->getActiveSheet()->setCellValue('D4', "Pro. Image");
		$objPHPExcel->getActiveSheet()->setCellValue('E4', "Product Name");
		$objPHPExcel->getActiveSheet()->setCellValue('F4', "Product Type");
		$objPHPExcel->getActiveSheet()->setCellValue('G4', "QTY");
		$objPHPExcel->getActiveSheet()->setCellValue('H4', "Amount Baht");
		
		$row_index = 5;
        foreach ($arr_data as $row)
        {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row_index, $row["ACTION_DATE"]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row_index, $row["NO_OF_PC"]);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row_index, $row["PRODUCT_CODE"]);
			
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setPath($pro_img_path.$row['PRD_IMG']);
			$objDrawing->setCoordinates('D'.$row_index);
			
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row_index, $row["PRODUCT_NAME_TH"]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$row_index, $row["TYPE_NAME_TH"]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$row_index, $row["QTY"]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$row_index, str_replace("-","",$row["AMOUNT"]));
			
			
			$objDrawing->setHeight(72);
			$objDrawing->setOffsetX(5);
			$objDrawing->setOffsetY(5);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			
			// Format Row
			$objPHPExcel->getActiveSheet()->getRowDimension($row_index)->setRowHeight(60);
			$row_index++;
            
        }
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$row_index,'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$row_index, '=SUM(G5:G'.($row_index-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$row_index, '=SUM(H5:H'.($row_index-1).')');
		//****Configulation Format Table
		//Format Columb 'A'
		//Format Column 'B'
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(70);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(35);
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(28);
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(28);
		
		//Set Style
		$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('H5:H'.$row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle('A5:H'.$row_index)->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getStyle('A5:H'.$row_index)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$objPHPExcel->getActiveSheet()->getStyle('A1:H3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		//Set Font
		$objPHPExcel->getActiveSheet()->getStyle('A1:H3')->getFont()->setName('Tahoma');
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		$objPHPExcel->getActiveSheet()->getStyle('D1:E3')->getFont()->setSize(10);
		
		$objPHPExcel->getActiveSheet()->getStyle('A4:H'.$row_index)->getFont()->setName('Cordia New');
		$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A4:H'.$row_index)->getFont()->setSize(14);
		$objPHPExcel->getActiveSheet()->getStyle("H4:H".$row_index)->getNumberFormat()->setFormatCode('#,##0.00');
		
		//Set Border
		$objPHPExcel->getActiveSheet()->getStyle('A4:H'.$row_index)->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
				)
			)
		);
		//Set Color Fill
		$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->applyFromArray(
			array(
				'fill' => array( 
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'd1d1d1')
				)
			)
		);
		
		$objWriter->save(str_replace(__FILE__, $excel_path.iconv('UTF-8','UTF-8',$arr_data [0]["EXCEL_NAME"]), __FILE__));
		
		//return $excel_path.'report_sale_pc_'.date('ymd_His').'.xlsx';
		
    }
	
	
	
}
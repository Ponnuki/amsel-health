<?php 

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

require_once('config.php');
require_once('class_amh_db.php');
require_once('class_amh_pc.php');

$the_date   = $_REQUEST["the_date"];

$amh_pc     = new AMH_PC();

if ($the_date != "")
{
    $the_date = $amh_pc->swap_date($the_date);
}

$arr_prd_pc = $amh_pc->get_product_pc($the_date, "");

$i_count = 0;
$str_crr_type = "";
$str_prd_list = "";

foreach ($arr_prd_pc as $prd_pc)
{
    $i_count++;
    if ($str_crr_type != $prd_pc["TYPE_NAME_EN"])
    {
        if ($i_count > 1) { $str_prd_list .= "</table>"; }
        $str_prd_list .= "<div class='ptype_title' style=''>".$prd_pc["TYPE_NAME_EN"]." (".$prd_pc["TYPE_NAME_TH"].")</div>";
        $str_prd_list .= "<table class='table table-bordered table-striped'>";
        $str_crr_type  = $prd_pc["TYPE_NAME_EN"];
    }

    $str_prd_list .= "<tr>";
    $str_prd_list .= "<td align='center' style='width:50px;'>";
    $str_prd_list .= "<input type='checkbox' id='chk_".$i_count."' onclick='swap_readonly(".$i_count.");'>";
    $str_prd_list .= "<input type='HIDDEN' id='prd_pc_id_".$i_count."' value='".$prd_pc["PRODUCT_PC_ID"]."'>";
    $str_prd_list .= "</td>";
    $str_prd_list .= "<td width='120'><img src='".$pro_img_path.$prd_pc["PRD_IMG"]."' style='width:100px; height:100px;'></td>";
    $str_prd_list .= "<td>";
    $str_prd_list .= "<div style='margin: 5px;'>";
    $str_prd_list .= "<div style='font-weight:bold; color:#009;'>".$prd_pc["PRODUCT_CODE"]."<span id='PO_NO_".$i_count."'></span></div>";
    $str_prd_list .= "<div>".$prd_pc["PRODUCT_NAME_TH"]."</div>";
    $str_prd_list .= "</div>";
    $str_prd_list .= "</td>";
    $str_prd_list .= "<td style='width:80px;'><input type='TEXT' id='QTY_".$i_count."' class='form-control' style='width:50px; text-align:right;' value='0' readonly></td>";
    $str_prd_list .= "</tr>";
}

if ($str_prd_list != "") { $str_prd_list .= "</table><input type='HIDDEN' id='num_all_item' name='num_all_item' value='".$i_count."'>"; }

echo $str_prd_list;
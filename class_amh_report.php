<?php
/* 
    Class name
    AMH_REPORT

    need to include 'class_amh_db.php' outside
    require_once('class_amh_db.php');
*/
class AMH_REPORT extends AMH_DB
{
    public function get_rpt_text($rpt_name)
    {
        $arr_ret = array();

        $sql_sel = " SELECT * FROM mst_report WHERE RPT_TYPE = '{$rpt_name}' ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_rpt_text error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret = $row_sel;
        }

        return $arr_ret;
    }

    public function get_po_main($po_id)
    {
        $arr_ret  = array();

        $sql_sel  = " SELECT POM.*, PRV.PROVINCE_NAME ";
        $sql_sel .= " FROM mst_province PRV, po_main POM ";
        $sql_sel .= " WHERE (POM.PO_ID = '{$po_id}') AND (POM.PROVINCE_CODE = PRV.PROVINCE_CODE) ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_po_main error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_po_dtl($po_id)
    {
        $arr_ret  = array();

        $sql_sel  = " SELECT DTL.*, MPD.PRODUCT_CODE ";
        $sql_sel .= " FROM po_dtl DTL ";
        $sql_sel .= " LEFT JOIN mst_product_pc MPC ON MPC.PRODUCT_PC_ID = DTL.PRODUCT_PC_ID ";
        $sql_sel .= " LEFT JOIN mst_product MPD    ON MPC.PRODUCT_ID    = MPD.PRODUCT_ID    ";
        $sql_sel .= " WHERE (DTL.PO_ID = '{$po_id}') ORDER BY DTL.QNO ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_po_dtl error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_do_main($do_id)
    {
        $arr_ret  = array();

        $sql_sel  = " SELECT DOM.*, PRV.PROVINCE_NAME, POM.CUS_PO_NO ";
        $sql_sel .= " FROM do_main DOM, mst_province PRV, po_main POM ";
        $sql_sel .= " WHERE (DOM.DO_ID = '{$do_id}') AND (DOM.PROVINCE_CODE = PRV.PROVINCE_CODE) AND (DOM.PO_ID = POM.PO_ID) ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_do_main error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }


    public function get_do_dtl($do_id)
    {
        $arr_ret  = array();

        $sql_sel  = " SELECT DTL.* ";
        $sql_sel .= " FROM do_dtl DTL ";
        $sql_sel .= " WHERE (DTL.DO_ID = '{$do_id}') ORDER BY DTL.QNO ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_do_dtl error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_inv_main($inv_id)
    {
        $arr_ret  = array();

        $sql_sel  = " SELECT IVM.*, PRV.PROVINCE_NAME, POM.CUS_PO_NO ";
        $sql_sel .= " FROM inv_main IVM, mst_province PRV, po_main POM ";
        $sql_sel .= " WHERE (IVM.INV_ID = '{$inv_id}') AND (IVM.PROVINCE_CODE = PRV.PROVINCE_CODE) AND (IVM.PO_ID = POM.PO_ID) ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_inv_main error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_inv_dtl($inv_id)
    {
        $arr_ret  = array();

        $sql_sel  = " SELECT DTL.* ";
        $sql_sel .= " FROM inv_dtl DTL ";
        $sql_sel .= " WHERE (DTL.INV_ID = '{$inv_id}') ORDER BY DTL.QNO ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_inv_dtl error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_district_name($dst_code, $prv_code)
    {
        $str_ret  = "";

        $sql_sel  = " SELECT * ";
        $sql_sel .= " FROM mst_district ";
        $sql_sel .= " WHERE (DISTRICT_CODE = '{$dst_code}') AND (PROVINCE_CODE = '{$prv_code}') ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_district_name error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $str_ret = $row_sel["DISTRICT_NAME"];
        }
        $res_sel->free();

        return $str_ret;
    }
}
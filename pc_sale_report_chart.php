<?php session_start();

error_reporting(E_ERROR);
ini_set('display_errors', 1);

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    $arr_order_dtl  = array();

    require_once('config.php');
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $amh_pc = new AMH_PC();

    $arr_pc = $amh_pc->get_aut_user(" AUT_UNAME = '{$user_name}' ");

    $aut_id = $arr_pc[0]["AUT_ID"];

    if ($_REQUEST["mm"] != "") { $this_month = $_REQUEST["mm"]; } else { $this_month = date("m"); }
    if ($_REQUEST["yy"] != "") { $this_year  = $_REQUEST["yy"]; } else { $this_year  = date("Y"); }

    $arr_report_dtl = $amh_pc->get_sale_report_detail($aut_id, $this_month, $this_year);
    $sales_target   = $amh_pc->get_sale_target_detail($aut_id, $this_month, $this_year);

    $number_detail  = count($arr_report_dtl);

    $num_amount = array();
    $sales_amount = 0;

    $num_amount[0] = 0;
    $num_amount[1] = 0;
    $num_amount[2] = 0;
    $num_amount[3] = 0;

    foreach ($arr_report_dtl as $report_dtl)
    {
        $sales_amount = $sales_amount + $report_dtl["LTP_PRICE"];
        $type_id = $report_dtl["PRODUCT_TYPE_ID"];
        $num_amount[$type_id] = $num_amount[$type_id] + $report_dtl["LTP_PRICE"];
    }

    $arr_month = array();
    $arr_month["01"] = "มกราคม";
    $arr_month["02"] = "กุมภาพันธ์";
    $arr_month["03"] = "มีนาคม";
    $arr_month["04"] = "มษายน";
    $arr_month["05"] = "พฤษภาคม";
    $arr_month["06"] = "มิถุนายน";
    $arr_month["07"] = "กรกฎาคม";
    $arr_month["08"] = "สิงหาคม";
    $arr_month["09"] = "กันยายน";
    $arr_month["10"] = "ตุลาคม";
    $arr_month["11"] = "พฤศจิกายน";
    $arr_month["12"] = "ธันวาคม";

    $option_month    = "";

    foreach ($arr_month as $month_val => $month_name)
    {
        if ($month_val == $this_month) { $str_selected = "selected"; } else { $str_selected = ""; }
        $option_month .= "<option value='{$month_val}' {$str_selected}>{$month_name}</option>\n";
    }

    $arr_year = array('2015','2016','2017','2018','2019','2020');

    $option_year  = "";

    foreach ($arr_year as $year_val)
    {
        if ($year_val == $this_year) { $str_selected = "selected"; } else { $str_selected = ""; }
        $option_year .= "<option value='{$year_val}' {$str_selected}>{$year_val}</option>\n";
    }

    $req_mm = $_REQUEST["mm"];
    if ($req_mm != "") { $str_mm_show = $arr_month[$req_mm]; }

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>PC Sales Target</title>
<script src="http://ccchart.com/js/ccchart.js" charset="utf-8"></script>
<script>
var the_target = <?php echo $sales_target; ?>;
var the_month  = <?php echo $this_month; ?>;
var str_month  = '<?php echo $arr_month[$this_month]; ?>';
var the_year   = <?php echo $this_year; ?>;
var sales_amount  = <?php echo $sales_amount; ?>;
var remain_amount = the_target - sales_amount;

var prd_type1  = <?php echo $num_amount[1]; ?>;
var prd_type2  = <?php echo $num_amount[2]; ?>;
var prd_type3  = <?php echo $num_amount[3]; ?>;

var the_chartdata1 = {

    "config": {
        "title": "ในเดือน"+str_month+" "+the_year,
        "subTitle": "ยอดขายที่ทำได้ "+sales_amount+"บาท จากเป้าหมาย "+the_target+"บาท",
        "type": "pie",
        "useVal": "yes",
        "pieDataIndex": 0,
        "colNameFont": "140 14pt 'Arial'",
        "pieRingWidth": 80,
        "pieHoleRadius": 60,
        "textColor": "#888",
        "bg": "#fff"
    },

    "data": [
        [sales_amount+"/"+the_target, str_month+" "+the_year],
        ["ยอดขายที่ยังเหลือ", remain_amount],
        ["ยอดขายที่ทำได้",  sales_amount]
    ]
};

<?php
    if (($num_amount[1] + $num_amount[2] + $num_amount[3]) > 0)
    {
?>
var the_chartdata2 = {

    "config": {
        "title": "ข้อมูลยอดขายแยกประเภท",
        "subTitle": "ในเดือน"+str_month+" "+the_year,
        "type": "pie",
        "useVal": "yes",
        "pieDataIndex": 0,
        "colNameFont": "140 14pt 'Arial'",
        "pieRingWidth": 80,
        "pieHoleRadius": 60,
        "textColor": "#888",
        "bg": "#fff"
    },

    "data": [
        ["ยอดขายแยกประเทภ", ""],
        ["เม็ด", prd_type1],
        ["ผง",  prd_type2],
        ["เซ็ต", prd_type3]
    ]
};
<?php
    }
?>

function start_page()
{
    ccchart.init('the_chart1', the_chartdata1);
    if (((prd_type1 * 1) + (prd_type2 * 1) + (prd_type3 * 1)) > 0)
    {
        ccchart.init('the_chart2', the_chartdata2);
        document.getElementById('footer_display').innerHTML = '';
        document.getElementById('the_chart2').style.display = 'block';
    }
    else
    {
        document.getElementById('footer_display').innerHTML = '<h3>ยังไม่มียอดขายในเดือนนี้</h3>';
        document.getElementById('the_chart2').style.display = 'none';
    }
}
</script>
<link rel="stylesheet" href="stylesheets/amh_pc.css">
<style type="text/css">
body
{
    background-color: #000;
    margin: 0;
    padding: 10px;
}

body,*
{
    font-family: 'Exo 2', sans-serif;
}

button
{
    background-color: #ccc;
    border: 1px solid #999;
    border-bottom: 5px solid #999;
    border-radius: 2px;
    color: #333;
    font-size: 24pt;
    font-weight: bold;
    padding: 5px 15px;
    vertical-align: middle;
}

button:hover
{
    background-color: #eee;
    border-bottom: 1px solid #999;
    padding: 9px 15px 5px;
}

.center_margin
{
    margin: 0 auto;
}

#footer_display
{
    color: #D33;
    font-size: 36px;
    font-weight: bold;
    text-align: center;
}

.link_white
{
    color:#FFF;
    text-decoration: none;
}

.link_white:hover
{
    color:#FF9;
}

.main_area
{
    background-color: #fff;
    padding: 20px 20px 100px 20px;
    width: 800px;
}

.main_label
{
    font-size: 18px;
    font-weight: bold;
}

.panel_search
{
    text-align: center;
}

.plus_item
{
    bottom: 0;
}

#the_chart1, #the_chart2
{
    margin: 15px auto;
}

.the_select
{
    border: 1px solid #333;
    border-radius: 6px 2px 2px 6px;
    font-size: 24pt;
    margin: 10px;
    padding: 10px;
    vertical-align: middle;
}
</style>
</head>
<body onload="start_page();">
    <div class="main_area center_margin">
    <div class="panel_search">
        <form action="pc_sale_report_chart.php" method="POST">
        <select id="mm" name="mm" class="the_select"><?php echo $option_month; ?></select>
        <select id="yy" name="yy" class="the_select"><?php echo $option_year;  ?></select>
        <button>Search</button>
        </form>
    </div>
    <canvas id="the_chart1"></canvas>
    <div id="footer_display"></div>
    <canvas id="the_chart2"></canvas>
    </div>
    <div class='plus_item fnt40 fnt_white'>
        <a href='pc_main.php' class='link_white'><img src='<? echo $app_img_path; ?>icon-navg-back100.png' border='0' style='vertical-align:middle;'> กลับหน้าหลัก</a>
    </div>
</body>
</html>
<?php
}
else
{
    echo "<h3>Session expired</h3>";
}
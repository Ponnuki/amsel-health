<?php 
	require("include_function.php");
	require("validatelogin.php");
 ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | PRODUCT PC</title>

	<?php $current_menu = "product_pc"; ?>
	<?php require("include_headtag.php"); ?>
    
</head> 

<body>    

<div  id="wrap" class="wrapper">

	<?php require("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">PRODUCT PC</h1>
			
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
	<script type="text/javascript">
    <!--
	function confirmDel(DelID)
	{
		if (confirm("Are you sure you want to delete this item."))
		{
			document.getElementById('hdDelID').value = DelID;
			document.getElementById('hdCancelID').value  = "";
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 function confirmCancel(CancelID)
	{
		if (confirm("Are you sure you want to cancel this item."))
		{
			document.getElementById('hdDelID').value = "";
			document.getElementById('hdCancelID').value = CancelID;
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
		<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		<?php 
		
			// **** Gen Product pc code
			$sql = "SELECT mp.PRODUCT_ID, mp.PRODUCT_CODE, concat(mp.PRODUCT_CODE,  ' : ', mp.PRODUCT_NAME_TH) as PCODE_PNAME
						FROM mst_product AS mp 
						WHERE mp.ACTIVE_FLAG = 'Y'
						ORDER BY mp.PRODUCT_CODE; ";
			$result_pro_sale_code =  mysql_query($sql);
			
			// **** Gen Customer Type
			$sql = "SELECT ct.CUS_TYPE_ID, ct.CUS_TYPE_NAME
						FROM customer_type AS ct
						ORDER BY ct.CUS_TYPE_ID; ";
			$res_cus_type =  mysql_query($sql);
			
			// **** Gen Product pc Table
			
			$sql = "SELECT DISTINCT mpc.PRODUCT_PC_ID, mp.PRODUCT_CODE, mpc.PRODUCT_ID, mp.PRODUCT_NAME_TH , mpt.TYPE_NAME_TH , ct.CUS_TYPE_NAME ,
								mpc.LTP_PER, DATE_FORMAT(mpc.EFFECTIVE_DATE,'%d-%m-%Y') as EFFECTIVE_DATE,
								mpc.STICKER_PRICE, mpc.CHEER_RATE, mpc.LTP_FOR_COMM, CASE WHEN IFNULL(od.PRODUCT_PC_ID,'') = '' THEN 1 ELSE 0 END AS CAN_DEL, mpc.ACTIVE_FLAG,
								mpt.ICON
						FROM mst_product_pc AS mpc
							INNER JOIN ( SELECT MAX(EFFECTIVE_DATE) AS MAX_EFF, PRODUCT_ID
												FROM mst_product_pc 
												WHERE EFFECTIVE_DATE <= STR_TO_DATE( DATE_FORMAT(NOW(),'%d/%m/%y'), '%d/%m/%Y')
												GROUP BY PRODUCT_ID ) max_effective 
								ON mpc.PRODUCT_ID = max_effective.PRODUCT_ID AND mpc.EFFECTIVE_DATE >= max_effective.MAX_EFF
							LEFT JOIN customer_type AS ct ON mpc.CUS_TYPE_ID = ct.CUS_TYPE_ID
							LEFT JOIN mst_product AS mp ON mpc.PRODUCT_ID = mp.PRODUCT_ID
							LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID
							LEFT JOIN  order_dtl AS od ON od.PRODUCT_PC_ID = mpc.PRODUCT_PC_ID 	";
				
			$where = " WHERE mpc.ACTIVE_FLAG = 'Y'  ";
			
			if ($_POST['ddlProCodeSearch']!="")
			{
				$where .= " AND mp.PRODUCT_CODE = '".$_POST['ddlProCodeSearch']."' ";
			}
			
			if ($_POST['ddlCusTypeSearch']!="")
			{
				$where .= " AND mpc.CUS_TYPE_ID = ".$_POST['ddlCusTypeSearch'];
			}
			
			$order = "	ORDER BY mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE ASC, mpc.EFFECTIVE_DATE DESC; ";
			$sql = $sql.$where.$order;
			$result_lpt_chart = mysql_query($sql);

			?>
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_condtion" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Product Sales Code / Product Name :</label>
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
												<select name="ddlProCodeSearch" id="ddlProCodeSearch" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
														<option value="" selected> - All Products - </option>
													<?php 
													while($row = mysql_fetch_array($result_pro_sale_code)) 
													{
														echo "<option value='".$row['PRODUCT_CODE']."' ";
														if ($row['PRODUCT_CODE'] == $_POST['ddlProCodeSearch'])
														{
															echo " selected ";
														}
														echo ">".$row['PCODE_PNAME']."</option>";
													}
													mysql_data_seek ($result_pro_sale_code , 0 );
													?>
												</select>
												
										</label>
										
									</div>
								</div>
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Customer Type :</label>
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
												<select name="ddlCusTypeSearch" id="ddlCusTypeSearch" class="selectpicker form-control" 
												data-live-search="false" title="Please select ...">
														<option value="" selected> - All Customer Type - </option>
													<?php 
													while($row = mysql_fetch_array($res_cus_type)) 
													{
														echo "<option value='".$row['CUS_TYPE_ID']."' ";
														if ($row['CUS_TYPE_ID'] == $_POST['ddlCusTypeSearch'])
														{
															echo " selected ";
														}
														echo ">".$row['CUS_TYPE_NAME']."</option>";
													}
													mysql_data_seek ($res_cus_type , 0 );
													?>
												</select>
												
										</label>
										
									</div>
								</div>
								<!--
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Effective Date :</label>
									<div class="col-lg-8" style="height: 35px">
										<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" id="txbDate" value="<?php //if (!isset($_POST['ddlProSaleCodeSearch'])) {echo date("d-m-Y"); } else {echo "All Day";} ?>">
											</label>
										
									</div>
									
								</div>
								-->
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button type="submit" class="btn-u"> Search </button> &nbsp;&nbsp;
										<!-- <button type="button" class="btn-u btn-u-default">Clear</button> -->
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

	




    <!--=== End Search Block Version 2 ===-->

    <div class="container content-sm"  style="padding:30px;">


		<div class="col-lg-offset col-lg-8" style="height:50px">
			<button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#product_sales_dtl_form" 
				onclick="ClearForm();"> <i class="fa fa-plus-square icon-color-white"></i> New Product PC </button> 
		
			<?php  require("product_pc_dtl_form.php");  ?>
		</div>

		
		<div class="table-search-v2 margin-bottom-30">
			<div class="table-responsive">
				<form method="POST" enctype="multipart/form-data" action="product_pc_model.php?form=frm_pc_tran_tb" name="frmSalesTranTb" onSubmit="return validate_form(this)"
					id="frmSalesTranTb" >
					<input type="hidden" name="hdDelID" id="hdDelID"  value="">
					<input type="hidden" name="hdCancelID" id="hdCancelID"  value="">
					<table class="table table-bordered table-striped">
						<thead>
							<tr valign="middle">
								<th> &nbsp; </th>
								<th>Pro. Code</th>
								<th class="hidden-sm">Product name</th>
								<th class="hidden-sm">Product Type</th>
								<th>Customer Type</th>
								<th>Effective Date</th>
								<th>LTP. %</th>
								<th>Sticker Price</th>
								<th>Cheer Rate</th>
								<th>LTP. for Commission</th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<?php 
								while($row=mysql_fetch_array($result_lpt_chart)) 
								{
									if($row["PRODUCT_CODE"] == $_REQUEST['focus'])
									{
										echo '<tr style = "background-color: #ffffbb">';
										echo '<script> history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'" ); </script>';
									}else
									{
										echo '<tr>';
									}
									
									echo '<td width="78px">
												<ul class="list-inline table-buttons">
													<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#product_sales_dtl_form" onclick="getDataAjax('.$row["PRODUCT_PC_ID"].')"><i class="fa fa-edit"></i> Edit</button></li>
												</ul>
											</td>';
									echo '<td width="90px"> <p>'.$row["PRODUCT_CODE"].'</p> </td>';
									echo '<td> <p>'.$row["PRODUCT_NAME_TH"].'</p> </td>';
									echo '<td width="90px"> '.$row["ICON"].$row["TYPE_NAME_TH"].' </td>';
									echo '<td width="90px"> '.$row["CUS_TYPE_NAME"].' </td>';
									echo '<td width="90px"> <p>'.$row["EFFECTIVE_DATE"].'</p> </td>';
									echo '<td width="85px"> <p>'.$row["LTP_PER"].'</p> </td>';
									echo '<td width="90px">'.$row["STICKER_PRICE"].'</td>';
									echo '<td width="90px"> <p>'.$row["CHEER_RATE"].'</p> </td>';
									echo '<td width="90px"> <p>'.$row["LTP_FOR_COMM"].'</p> </td>';
									
									echo '<td  width="84px">
													<ul class="list-inline table-buttons">
														<li>';
									
									if ($row["CAN_DEL"] == 1)
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["PRODUCT_PC_ID"] .')"><i class="fa fa-trash-o"></i> Delete</button></li>';
									}
									
									else if ($row["ACTIVE_FLAG"] == 'Y')
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-orange" onclick ="return confirmCancel('.$row["PRODUCT_PC_ID"] .')"> 
															<i class="fa fa-times-circle"></i> Cancel</button>';
									} else 
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-green" onclick ="return confirmCancel('.$row["PRODUCT_PC_ID"] .')"> 
															<i class="fa fa-check-circle"></i> Enable</button>';
									}
									
									echo '		</ul>
												</td>';
									
									echo '</tr>';
								}
							?>
						
						</tbody>
					</table>
				</form>
				
				
				<!-- End Test Form -->
			</div>    
		</div>    
        <!-- End Table Search v2 -->
		


        
    </div>
	<?php require("include_footer.php"); ?>
     
</div><!--/End Wrapepr-->

<?php require("include_js.php"); ?>

</body>
</html> 
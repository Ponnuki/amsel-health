<?php session_start();

    error_reporting(E_ERROR);
    ini_set('display_errors', 1);

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    require_once('config.php');
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');
    require_once('class_amh_customer.php');

    function write_day_option($the_date)
    {
        $str_ret = "";

        $date_this_shw = date('j/n/Y');
        $date_this_val = date('Y-m-d');

        $date_prev_shw = date('j/n/Y', strtotime("-1 day"));
        $date_prev_val = date('Y-m-d', strtotime("-1 day"));

        if ($the_date == $date_prev_val) { $selected_prev = "selected"; } else { $selected_prev = ""; }
        if ($the_date == $date_this_val) { $selected_this = "selected"; } else { $selected_this = ""; }
        if ($the_date == "") { $selected_this = "selected"; }

        $str_ret .= "<option value='".$date_prev_val."' ".$selected_prev.">".$date_prev_shw."</option>\n";
        $str_ret .= "<option value='".$date_this_val."' ".$selected_this.">".$date_this_shw."</option>\n";

        return $str_ret;
    }

    $amh_pc       = new AMH_PC();
    $amh_customer = new AMH_Customer();

    $str_pc_opt   = $amh_pc->create_pc_option();
    $cus_id       = $amh_pc->get_cus_id($user_name);

    $str_date_opt = write_day_option($_REQUEST["the_date"]);

    if ($_REQUEST["the_cus_id"] != "") { $the_cus_id = $_REQUEST["the_cus_id"]; } else { $the_cus_id = $cus_id; }

    $str_cus_opt  = "<option value='' style='text-align:center;'> - เลือกร้าน - </option>".$amh_customer->get_customer_option($the_cus_id);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Amsel Health - Sales Report</title>
<link rel="stylesheet" type="text/css" href="incl/jquery_ui/css/sunny/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" href="stylesheets/amh_pc.css">
<link href='https://fonts.googleapis.com/css?family=Exo+2:700,400' rel='stylesheet' type='text/css'>
<style type='text/css'>
body
{
    background-color: #000;
    font-family: Arial;
    margin: 0;
    padding: 10px;
}

body,*
{
    font-family: 'Exo 2', sans-serif;
}

.btn_gen
{
    background-color: #fff;
    border: 1px solid #999;
    border-bottom: 10px solid #999;
    border-radius: 5px;
    font-size: 36pt;
    font-weight: bold;
    padding: 5pt 15pt;
}

.link_white
{
    color:#FFF;
    text-decoration: none;
}

.link_white:hover
{
    color:#FF9;
}

.panel_add_pc
{
    border: 1px solid #ddd;
    border-radius: 5px;
    color:#fff;
    margin: 16pt;
    padding: 10pt;
}

.set_center
{
    text-align: center;
}

.the_select
{
    border: 1px solid #333;
    border-radius: 6px 2px 2px 6px;
    font-size: 32pt;
    margin: 20px 10px;
    padding: 10px 20px;
    width: 400px;
}

.pc_tag
{
    background-color: #fff;
    border-bottom: 5px solid #888;
    border-radius: 3px;
    color: #000;
    padding: 15px;
    cursor: pointer;
    text-align: center;
    margin: 15px 0;
}
</style>
<script type="text/javascript" src="incl/jquery_ui/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="incl/jquery_ui/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript">
function page_startup()
{
    /* start_calendar(); */
    loading_pc('load', '', '');
}

function loading_pc(the_action, adtn_name, adtn_id)
{
    user_name = $('#the_user_name').val();
    cus_id    = $('#cus_id').val();
    s_date    = $('#s_date').val();

    if (adtn_id == '')
    {
        adtn_id = $('#pc_id').val();
    }

    if (adtn_name == '')
    {
        adtn_name = $('#pc_id option:selected').html();
    }

    $.post( "pc_sale_report_adtn_pc.php", { action: the_action, cus_id: cus_id, user_name: user_name, s_date: s_date, adtn_id: adtn_id, adtn_name: adtn_name })
        .done(function(data)
        {
            $('#adtn_pc').html(data);
        });
}

function goto_url(the_url)
{
    the_form = document.getElementById("the_form");

    document.getElementById("the_date").value   = document.getElementById("s_date").value;
    document.getElementById("the_cus_id").value = document.getElementById("cus_id").value;

    the_form.action = the_url + ".php";
    the_form.submit();
}

/*
$('#pc_id option:selected').html()
*/

/*
function start_calendar()
{
    $('.gen_input_calen').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'd M yy'
    });

    $('#ui-datepicker-div').css("font-size","32px");
}
*/
</script>
</head>
<body onload='page_startup();'>
<div class='the_page'>
<div class='the_page_label'>SALE REPORT</div>
<form action='pc_sale_report_add.php' method='POST' id='sale_rpt_add_pc_form' name='sale_rpt_add_pc_form'>
<div class='panel_add_pc fnt36 set_center'>
    <table border='0' style='width:100%'>
        <tr><td style='text-align:right;'>Report date:</td>   <td style='text-align:left;'><select class="the_select" id="s_date" name="s_date" data-live-search="true" onchange="goto_url('pc_sale_report_add_pc');"><?php echo $str_date_opt; ?></select></td></tr>
        <tr><td style='text-align:right;'>Customer Shop:</td> <td style='text-align:left;'><select class="the_select" id="cus_id" name="cus_id" data-live-search="true" onchange="goto_url('pc_sale_report_add_pc');"><?php echo $str_cus_opt;  ?></select></td></tr>
    </table>
</div>
</form>
<table border='0' class='fnt36' style='margin: 0 auto;'>
    <tr><td style='text-align:right;'></td><td style='text-align:left;'><div class='fnt44' style='color:#ff0;'>Main PC: <?php echo $user_name; ?></div></td></tr>
    <tr><td style='text-align:right;'></td><td style='text-align:left;'><div class='fnt44' style='color:#ff0;' id='adtn_pc'></div></td></tr>
    <tr><td style='text-align:right; color:#fff;'>Additional PC:</td> <td style='text-align:left;'><select class="the_select" id="pc_id"  name="pc_id"  data-live-search="true"><?php echo $str_pc_opt;   ?></select><button type='BUTTON' class='btn_gen' onclick='loading_pc("add", "", "")'>+</button></td></tr>
</table>
</div><!-- end of the_page -->
<div class='plus_item fnt40 fnt_white'>
    <a href='pc_main.php' class='link_white'><img src='<? echo $app_img_path; ?>icon-navg-back100.png' border='0' style='vertical-align:middle;'> กลับหน้าหลัก</a>
</div>
<form id='the_form' action='' method='POST' target=''>
<input type='HIDDEN' id='the_date'   name='the_date'   value=''>
<input type='HIDDEN' id='the_cus_id' name='the_cus_id' value=''>
</form>
<input type='HIDDEN' id='the_user_name' value='<?php echo $user_name; ?>' >
</body>
</html>
<?php
}
else
{
    echo "<h2>Please login</h2>";
}
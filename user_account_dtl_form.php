	
	<script type="text/javascript">
    <!--
		function getEmpDdl(emp_id)
		{
			
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					
					document.getElementById("ddlEmp").innerHTML = "<option value='' selected> - Please Select - </option>";
					
					document.getElementById("ddlEmp").innerHTML += str;
					if (typeof(emp_id) !="undefined") $('#ddlEmp').selectpicker('val',emp_id);
					$('.selectpicker').selectpicker('refresh');
					//alert (str);
				}
			}
			params  = 'operate=get_ddl_emp';
			if (typeof(emp_id) !="undefined")  params += "&emp_id="+emp_id;
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","user_account_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
				
			}
		
		}
		
		function ClearForm()
		{
			document.getElementById('hdMode').value = "new";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-plus-square'></i>New User Accout";
			
			$("#ddlEmp").attr('disabled', false);
			$('#ddlEmp').selectpicker("setStyle", "");
			$('#ddlEmp').selectpicker('val','');
			$('#ddlRole').selectpicker("setStyle", "");
			$('#ddlRole').selectpicker('val','');
			document.getElementById('txbUName').value = "";
			document.getElementById('txbUName').style.backgroundColor ="";
			document.getElementById('txbUName').readOnly = false;
			document.getElementById("labErrUName").innerHTML = "";
			document.getElementById("labErrUName").style.display= "none";
			document.getElementById('txbNewPw').value = "";
			document.getElementById('txbConfirmPw').value = "";
			document.getElementById("labErrConfirmPw").innerHTML = "";
			document.getElementById("labErrConfirmPw").style.display= "none";
			document.getElementById("btSave").style.display = "none";
			document.getElementById("btSaveNew").style.display = "";
			getEmpDdl();
			
		}
		
		
		
		function getRole(emp_id)
		{
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					
					role_id= xmlhttp.responseText.replace("\r\n\t", "");
					
					$('#ddlRole').selectpicker('val',role_id);
				}
			}
			
			xmlhttp.open("GET","user_account_ajax.php?emp_id="+emp_id ,true);
			xmlhttp.send();
		
		}
		
		function chkDupData()
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					
					if (str == "")
					{
						document.getElementById("labErrUName").innerHTML = "";
						document.getElementById("labErrUName").style.display= "none";
						document.getElementById("btSubmit").click();
						
					}else
					{
						if (document.getElementById("hdMode").value == "new")
						{
							document.getElementById("labErrUName").innerHTML = "This User name duplicate. Please change User name !";
							document.getElementById("labErrUName").style.display= "";
						}else
						{
							document.getElementById("btSubmit").click();
						}
					}
					
				}
				
			}
			
			params  = 'operate=check_dup_user';
			params += "&uname="+document.getElementById('txbUName').value;
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","user_account_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
				
			}
			
			
			
		}
		
		
		function chkPwStrength(txtpass)
	   {
			if (txtpass.length < 6 && document.getElementById('hdMode').value  == "new")
			{
				return "Password Should be Minimum 6 Characters.";
				
			}
			else
			{
				return "";
			}

	   }
		
		function validate_form(obj)
		{
			var validateResult;
			validateResult = true;
			
			if (document.getElementById('txbNewPw').value != document.getElementById('txbConfirmPw').value )
			{
				document.getElementById("labErrConfirmPw").innerHTML = "Must match the previous entry.";
				document.getElementById("labErrConfirmPw").style.display= "";
				validateResult = false;
			}else if (chkPwStrength(document.getElementById('txbNewPw').value ) != "" )
			{
				document.getElementById("labErrConfirmPw").innerHTML = chkPwStrength(document.getElementById('txbNewPw').value );
				document.getElementById("labErrConfirmPw").style.display= "";
				validateResult = false;
			}else if (document.getElementById('txbNewPw').value  == "" &&
						document.getElementById('txbConfirmPw').value == "" &&
						document.getElementById('hdMode').value  == "new")
			{
				document.getElementById("labErrConfirmPw").innerHTML = "Please create a password.";
				document.getElementById("labErrConfirmPw").style.display= "";
				validateResult = false;
			}
			
			return validateResult;
		}
		
		function getDataAjax(aut_id, emp_id) 
		{
			document.getElementById('hdMode').value = "edit";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit User Account";
			
			
			$("#ddlEmp").attr('disabled', true);
			$('#ddlEmp').selectpicker('setStyle', 'btn-inverse');
			getEmpDdl(emp_id);
			$('#ddlRole').selectpicker("setStyle", "");
			$('#ddlRole').selectpicker('val','');
			document.getElementById('txbUName').value = "";
			document.getElementById('txbUName').readOnly = true;
			document.getElementById('txbUName').style.backgroundColor = "#f5f5f5";
			document.getElementById("labErrUName").innerHTML = "";
			document.getElementById("labErrUName").style.display= "none";
			document.getElementById('txbNewPw').value = "";
			document.getElementById('txbConfirmPw').value = "";
			document.getElementById("labErrConfirmPw").innerHTML = "";
			document.getElementById("labErrConfirmPw").style.display= "none";
			document.getElementById("btSave").style.display = "";
			document.getElementById("btSaveNew").style.display = "none";
			//alert(emp_id);
			
			//alert("Round2 = "+emp_id);
			
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp1=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp1=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp1.onreadystatechange=function() 
			{
				
				if (xmlhttp1.readyState==4 && xmlhttp1.status==200) 
				{
					
					var str = xmlhttp1.responseText.replace("\r\n\t", "");
					var result = JSON.parse(str);
					//alert("EMP_ID="+result[0]["EMP_ID"]);
					$('#ddlRole').selectpicker('val',result[0]["AUT_ROLE_ID"]);
					document.getElementById('txbUName').value = result[0]["AUT_UNAME"];
					if (result[0]["ACTIVE_FLAG"] == "Y") {
						document.getElementById('chkEnable').checked =true; 
					}else {
						document.getElementById('chkEnable').checked =false;
					}
					//alert ("Test");
					//$('#ddlEmp').selectpicker('val',result[0]["EMP_ID"]);
					$('.selectpicker').selectpicker('refresh');
				}
			}
			params  = 'operate=get_data';
			if (typeof(aut_id) !="undefined")  params += "&aut_id="+aut_id;
			posting = true;
			if (posting)
			{
				xmlhttp1.open("POST","user_account_ajax.php",true);
				xmlhttp1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp1.send(params);
				
			}
			
		
		}
		function redirec_java()
		{
			
		}
		
    //-->
    </script>
	<?php
		// **** Gen Position 
		
		$sql = "SELECT AUT_ROLE_ID, AUT_ROLE_NAME FROM mst_aut_role;  ";
		$result_role =  mysql_query($sql);
		
	?>
	
	<form method="POST" enctype="multipart/form-data" action="user_account_model.php?form=aut_dtl" name="frmAutDtl" 
		id="frmAutDtl"  data-toggle="validator" role="form" onsubmit="return validate_form(this);" autocomplete="off">
		<input type="hidden" name="hdMode" id="hdMode"  value="">
	
		<div class="modal fade" id="u_acc_dtl_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
			<div class="modal-dialog">
				<div id="divFormBody" class="modal-content">
				
					<div class="modal-header">
						<button id="btClosePop" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						
						<h4 class="modal-title" id="labMode"><i class="fa fa-plus-square"></i>New User Account</h4>
					</div>
					<div class="modal-body sky-form"  style="border-style:none">
						
						<!-- Product panal -->
						<div class="panel panel-grey margin-bottom-5 padding: 15px">
							<div id = "divEmpCode" class="row" style="padding-top: 17px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-4 control-label" style="text-align:right">Employee Code <font color="#ff0000">*</font></label>
									
									<div class="col-lg-8">
										<label class="select" >
											<select name="ddlEmp" id="ddlEmp" class="selectpicker form-control" 
											data-live-search="true" title="Please select ..." onchange="getRole(this.value);">
												
											</select>
											
										</label>
									</div>
									
								</div>
							</div>
						
							<div id = "divRole" class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									<label class="col-lg-4 control-label" style="text-align:right">Role Name <font color="#ff0000">*</font></label>
									<div class="col-lg-8">
										<label class="select" >
											<select name="ddlRole" id="ddlRole" class="selectpicker form-control" 
											 title="Please select ..." >
												<option value="" selected> - Please Select - </option>
												<?php 
												while($row = mysql_fetch_array($result_role)) 
												{
													echo "<option value='".$row['AUT_ROLE_ID']."' >".$row['AUT_ROLE_NAME']."</option>";
												}
												mysql_data_seek ($result_role, 0 );
												?>
											</select>
											
										</label>
									
									</div>
									
								</div>
							</div>
							
							<div id="divUname" class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-4 control-label" style="text-align:right">User Name<font color=#ff0000>*</font></label>
									<div class="col-lg-8">
										<label class="input">
											<i class="icon-append fa fa-user"></i>
											<input type="text" class="form-control" name="txbUName" id="txbUName" placeholder="" required>
											<label id="labErrUName" class="error" style="display:none"></label>
										</label>
									</div>
									
								</div>
							</div>
							
							<div class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-4 control-label" style="text-align:right">New Password<font color=#ff0000>*</font></label>
									<div class="col-lg-8">
										<label class="input">
											<i class="icon-append fa fa-asterisk"></i>
											<input type="password" class="form-control" name="txbNewPw" id="txbNewPw" placeholder="" autocomplete="off" onclick="this.value = '';">
										</label>
									</div>
									
								</div>
							</div>
							
							<div class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-4 control-label" style="text-align:right">Confirm Password<font color=#ff0000>*</font></label>
									<div class="col-lg-8">
										<label class="input">
											<i class="icon-append fa fa-asterisk"></i>
											<input type="password" class="form-control" name="txbConfirmPw" id="txbConfirmPw" placeholder="" autocomplete="off" onclick="this.value = '';">
											<label id="labErrConfirmPw" class="error" style="display:none"></label>
										</label>
									</div>
									
								</div>
							</div>
							
							<div id = "divEnable" class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-4 control-label" style="text-align:right"></label>
									<div class="col col-8">
										<label class="checkbox">
											<input type="checkbox" id="chkEnable" value="Y" onclick = "if (this.checked) {this.value = 'Y'; }else {this.value = 'N';} " checked>
											<i></i>
											Enable User
										</label>
									</div>
									
								</div>
							</div>
							
						</div><!-- End Product panal -->
						<label id="labError" class="error"></label>
						</div><!-- End "modal-body" -->
					<div class="modal-footer">
					<?php if ($current_menu == "reset_pw")  { ?>
						<button type="button" name="btBkHome" id="btBkHome" class="btn-u btn-u-default"  onclick="window.location='home.php';" data-dismiss="modal">Back to Home</button>
					<?php } else { ?>
						<button type="button" name="btClose" id="btClose" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
					<?php } ?>
						<button type="button" onclick="chkDupData('New')" class="btn-u btn-u-primary" name="btSaveNew" id="btSaveNew" >Save New</button>
						<button type="button" onclick="$('#ddlEmp').attr('disabled', false); document.getElementById('btSubmit').click();"  class="btn-u btn-u-primary" name="btSave" id="btSave">Save</button>
						<button type="submit" class="btn-u btn-u-primary" name="btSubmit" id="btSubmit" style="display:none">Submit</button>
					</div>
					
				</div><!-- END modal-body -->
			</div>
		</div>

	</form>
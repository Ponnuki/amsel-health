<?php
	require_once("class_user_account.php");
	require_once("include_function.php");
	
	$amh_user_acc = new AMH_user_acc();
	
	if ($_POST["operate"] == "get_ddl_aut_user")
	{	
		
		$aut_user_lab_list = json_decode( $amh_user_acc->get_aut_user_lab(),true);
		$ddlUserSearch = "";
		foreach ($aut_user_lab_list as $row)
		{
			$ddlUserSearch .= "<option value='".$row['AUT_UNAME']."'>".$row['EMP_NAME']."</option>";
		}
		echo $ddlUserSearch;
	}
	else if ($_POST["operate"] == "get_ddl_emp")
	{	
		$empId = "";
		if (isset($_POST["emp_id"])) $empId = $_POST["emp_id"];
		$emp_list = json_decode( $amh_user_acc->get_emp($empId),true);
		$ddlEmpHtml = "";
		$select = "";
		
		
		foreach ($emp_list as $row)
		{
			//if($_POST["emp_id"] == $row['EMP_ID']) $select = " selected disabled ";
			$ddlEmpHtml .= "<option value='".$row['EMP_ID']."' ".$select.">".$row['EMPLOYEE_NAME']."</option>";
		}
		echo $ddlEmpHtml;
		//echo "<option value='12'>test</option>";
	}
	else if ($_POST["operate"] == "get_grid_aut_user")
	{
		$aut_user_list = json_decode( $amh_user_acc->get_aut_user($_POST["aut_uname"]),true);
		$gridUserSearch = "";
		foreach ($aut_user_list as $row)
		{
			
			if($row["AUT_UNAME"] == $_POST['focus'])
			{
				$gridUserSearch .= '<tr style = "background-color: #ffffbb">';
				$gridUserSearch .= '<script> history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'" ); </script>';
			}else
			{
				$gridUserSearch .= '<tr>';
			}
			
			$gridUserSearch .= '<td width="78px">
						<ul class="list-inline table-buttons">
							<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#u_acc_dtl_form" onclick="getDataAjax('.$row["AUT_ID"].','.$row["EMP_ID"].')"><i class="fa fa-edit"></i> Edit</button></li>
						</ul>
					</td>';
			$gridUserSearch .= '<td width="110px"> <p>'.$row["AUT_UNAME"].'</p> </td>';
			$gridUserSearch .= '<td> <p>'.$row["EMP_CODE"]." : ".$row["EMP_NAME"].'</p> </td>';
			$gridUserSearch .= '<td> <p>'.$row["POSITION_NAME_EN"].'</p> </td>';
			$gridUserSearch .= '<td width="120px">'.$row["AUT_ROLE_NAME"].'</td>';
			
			$gridUserSearch .= '<td  width="84px">
							<ul class="list-inline table-buttons">
								<li>';
				
			if ($row["IS_DEL"] == 1)
			{
				$gridUserSearch .= '			<button type="submit" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["EMP_ID"] .')"> 
									<i class="fa fa-trash-o"></i> Delete </button>';
				
			}else if ($row["ACTIVE_FLAG"] == 'Y')
			{
				$gridUserSearch .= '			<button type="submit" class="btn-u btn-u-sm btn-u-orange" onclick ="return confirmCancel('.$row["EMP_ID"] .')"> 
									<i class="fa fa-times-circle"></i> Cancel</button>';
			} else 
			{
				$gridUserSearch .= '			<button type="submit" class="btn-u btn-u-sm btn-u-green" onclick ="return confirmCancel('.$row["EMP_ID"] .')"> 
									<i class="fa fa-check-circle"></i> Enable</button>';
			}
							
			$gridUserSearch .='			</li>
		   
							</ul>
						</td>
					</tr>';
		}
			
		
		echo $gridUserSearch;
	}
	else if ($_POST["operate"] == "get_data")
	{	
		echo $amh_user_acc->get_emp_dtl($_POST["aut_id"]);
		
	}
	else if ($_POST["operate"] == "check_dup_user")
	{	
		echo $amh_user_acc->check_dup_user($_POST["uname"]);
		
	}
	/*
	if (isset($_REQUEST['aut_id']))
	{
		// Select Detail TRN_LPT_CHART_ID
		$sql = sprintf("SELECT AUT_UNAME, AUT_PW, EMP_ID, AUT_ROLE_ID, ACTIVE_FLAG 
							FROM aut_user
							WHERE AUT_ID = %d; "
					, mysql_real_escape_string($_REQUEST['aut_id'])
					);
		$result_dtl =  mysql_query($sql);
		
		while($row = mysql_fetch_array($result_dtl)) 
		{
			echo $row['AUT_UNAME'].'|'.$row['AUT_PW'].'|'.$row['EMP_ID'].'|'.$row['AUT_ROLE_ID'].'|'.$row['ACTIVE_FLAG'];
		}
			
	}
	else if (isset($_REQUEST['aut_uname'])) 
	{ // Check Duplicate 
		$sql=sprintf("SELECT AUT_UNAME FROM aut_user WHERE AUT_UNAME = '%s'; "
							, mysql_real_escape_string($_REQUEST['aut_uname'])
							);
		$result_dup=  mysql_query($sql);
		
		//mysql_num_rows($result_dup)
		
		if (mysql_num_rows($result_dup) > 0)
		{
			while($row = mysql_fetch_array($result_dup)) 
			{
				echo $row['AUT_UNAME'];
			}
			//echo mysql_num_rows($result_dup);
			
		}else{
			echo "";
		}
		
	}*/
	
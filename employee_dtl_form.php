	<script type="text/javascript">
    <!--
		function ClearForm()
		{
			document.getElementById('hdMode').value = "new";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-plus-square'></i>New Employee";
			document.getElementById('txbEmpCode').value = "";
			document.getElementById('txbEmpCode').readOnly = false;
			document.getElementById("labErrEmpCode").innerHTML = "";
			document.getElementById("labErrEmpCode").style.display= "none";
			document.getElementById('ddlTitle').value = "";
			document.getElementById('txbNName').value = "";
			document.getElementById('txbFNameTh').value = "";
			document.getElementById('txbLNameTh').value = "";
			document.getElementById('txbFNameEn').value = "";
			document.getElementById('txbLNameEn').value = "";
			document.getElementById('txbTel').value = "";
			document.getElementById('txbEmail').value = "";
			$('#ddlPosition').selectpicker("setStyle", "");
			$('#ddlPosition').selectpicker('val','');
			document.getElementById('txbStartJobDate').value = "";
			document.getElementById("btSave").style.display = "none";
			document.getElementById("btSaveNew").style.display = "";
			
		}
		
		function chkDupData()
		{
			
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					var res = str.split("|");
					
					if (res[0] == "")
					{
						document.getElementById("labErrEmpCode").innerHTML = "";
						document.getElementById("labErrEmpCode").style.display= "none";
						document.getElementById("btSubmit").click();
						//document.forms["frmProSalesDtl"].submit();
					}else
					{
						if (document.getElementById("hdMode").value == "new")
						{
							document.getElementById("labErrEmpCode").innerHTML = "This data duplicate. Please chack Emloye code : ["+ res[0]+"]";
							document.getElementById("labErrEmpCode").style.display= "";
						}else
						{
							document.getElementById("btSubmit").click();
						}
					}
				}
				
			}
			
			emp_code = document.getElementById('txbEmpCode').value;
			fname_en = document.getElementById('txbFNameEn').value;
			lname_en = document.getElementById('txbLNameEn').value;
			
			params  = 'operate=chk_dup';
			params += "&emp_code="+emp_code;
			params += "&fname_en="+fname_en;
			params += "&lname_en="+lname_en;
			
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","employee_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
			}
			
			
		}
		
		function getDataAjax(empId) 
		{
			document.getElementById('hdMode').value = "edit";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit Employee Detail";
			document.getElementById('txbEmpCode').readOnly = true;
			document.getElementById("labErrEmpCode").innerHTML = "";
			document.getElementById("labErrEmpCode").style.display= "none";
			document.getElementById("btSave").style.display = "";
			document.getElementById("btSaveNew").style.display = "none";
			
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					var res = JSON.parse(str);
					
					document.getElementById('txbEmpCode').value = res[0]["EMP_CODE"];
					document.getElementById('txbFNameTh').value = res[0]["FNAME_TH"];
					document.getElementById('txbLNameTh').value = res[0]["LNAME_TH"];
					document.getElementById('txbFNameEn').value = res[0]["FNAME_EN"];
					document.getElementById('txbLNameEn').value = res[0]["LNAME_EN"];
					document.getElementById('txbNName').value = res[0]["NICK_NAME"];
					document.getElementById('txbStartJobDate').value  = res[0]["START_JOB_DATE"];
					document.getElementById('txbEmail').value = res[0]["EMAIL"];
					document.getElementById('txbTel').value = res[0]["TEL"];
					$('#ddlPosition').selectpicker("setStyle", "");
					$('#ddlPosition').selectpicker('val',res[0]["POSITION_ID"]);
					document.getElementById('ddlTitle').value = res[0]["TITLE_ID"];
					//$('#ddlTitle').selectpicker('val',res[0]["TITLE_ID"]);
					$('.selectpicker').selectpicker('refresh');
				}
			}
		
			params  = 'operate=get_emp_dtl';
			params += "&emp_id="+empId;
			
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","employee_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
			}
			
		}
    //-->
    </script>
	<?php
		// **** Gen Position 
		$sql = "SELECT POSITION_ID, CONCAT(mp.POSITION_NAME_EN, '/', mp.POSITION_NAME_TH) AS POSITION_NAME
					FROM mst_position AS mp 
					ORDER BY mp.POSITION_NAME_EN; ";
		$result_position =  mysql_query($sql);
		
		// **** Gen Title 
		$sql_title = "SELECT TITLE_ID, CONCAT(mt.TITLE_NAME_EN, '/', mt.TITLE_NAME_TH) AS TITLE_NAME
					FROM mst_title AS mt 
					WHERE mt.TITLE_TYPE IN ('M','F')
					ORDER BY mt.TITLE_NAME_EN; ";
		$result_title =  mysql_query($sql_title);
		
	?>
	
		<form method="POST" enctype="multipart/form-data" action="employee_model.php?form=employee_dtl" name="frmEmployeeDtl" 
		 id="frmEmployeeDtl"  data-toggle="validator" role="form">
			<input type="hidden" name="hdMode" id="hdMode"  value="">
		
			<div class="modal fade" id="employee_dtl_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog modal-lg">
					<div id="divFormBody" class="modal-content">
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							
							<h4 class="modal-title" id="labMode"><i class="fa fa-plus-square"></i>New Employee</h4>
						</div>
						<div class="modal-body sky-form"  style="border-style:none">
							
							<!-- Product panal -->
							<div class="panel panel-grey margin-bottom-15 padding: 15px">
							
								<div class="row" style="padding-top: 15px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Employee Code <font color="#ff0000">*</font></label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbEmpCode" id="txbEmpCode" placeholder="" required>
											<label id="labErrEmpCode" class="error" style="display:none"></label>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Nick Name <font color="#ff0000">*</font></label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa  fa-smile-o"></i>
												<input type="text" class="form-control" name="txbNName" id="txbNName" placeholder="" required>
											</label>
											<label id="labErrNName" class="error" style="display:none"></label>
										</div>
										
									</div>
									
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Title <font color=#ff0000>*</font></label>
										<div class="col-lg-4">
											<label class="select" >
												<select name="ddlTitle" id="ddlTitle" required>
													<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_title)) 
													{
														echo "<option value='".$row['TITLE_ID']."' >".$row['TITLE_NAME']."</option>";
													}
													mysql_data_seek ($result_title , 0 );
													?>
													
												</select>
												<i></i>
											</label>
											
										</div>
										<label class="col-lg-2 control-label" style="text-align:right"> </label>
										<div class="col-lg-4">
											<label class="input">
												
											</label>
										</div>
									</div>
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">First Name (TH) <font color=#ff0000>*</font></label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-user"></i>
												<input type="text" class="form-control" name="txbFNameTh" id="txbFNameTh" placeholder="" required>
											</label>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Last Name (TH) <font color=#ff0000>*</font></label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-user"></i>
												<input type="text" class="form-control" name="txbLNameTh" id="txbLNameTh" placeholder="" required>
											</label>
										</div>
									</div>
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">First Name (EN) <font color=#ff0000>*</font></label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-user"></i>
												<input type="text" class="form-control" name="txbFNameEn" id="txbFNameEn" placeholder="" required>
											</label>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Last Name (EN) <font color=#ff0000>*</font></label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-user"></i>
												<input type="text" class="form-control" name="txbLNameEn" id="txbLNameEn" placeholder="" required>
											</label>
										</div>
									</div>
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Tel. </label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-phone"></i>
												<input type="tel" class="form-control" name="txbTel" id="txbTel" placeholder="" >
											</label>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Email </label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-envelope"></i>
												<input type="mail" class="form-control" name="txbEmail" id="txbEmail" placeholder="" >
											</label>
										</div>
									</div>
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Position <font color=#ff0000>*</font></label>
										<div class="col-lg-4">
											<label class="select" >
												<select name="ddlPosition" id="ddlPosition" class="selectpicker form-control" 
												data-live-search="true" title="Please select ..." required>
													<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_position)) 
													{
														echo "<option value='".$row['POSITION_ID']."' >".$row['POSITION_NAME']."</option>";
													}
													mysql_data_seek ($result_position , 0 );
													?>
												</select>
												
											</label>
											
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Start Job Date </label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" name="txbStartJobDate" id="txbStartJobDate" >
											</label>
										</div>
									</div>
								</div>
								
								
								
							</div><!-- End Product panal -->
							<label id="labError" class="error"></label>
							

							
						</div><!-- End "modal-body" -->
						<div class="modal-footer">
							<button type="button" name="btClose" id="btClose" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
							<button type="button" onclick="chkDupData('New')" class="btn-u btn-u-primary" name="btSaveNew" id="btSaveNew" >Save New</button>
							<button type="button" onclick="chkDupData('Edit')"  class="btn-u btn-u-primary" name="btSave" id="btSave">Save</button>
							<button type="submit" class="btn-u btn-u-primary" name="btSubmit" id="btSubmit" style="display:none">Submit</button>
						</div>
						
					</div><!-- END modal-body -->
				</div>
			</div>

		</form>
<?php session_start();

error_reporting(E_ERROR);
ini_set('display_errors', 1);

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    require_once('config.php');
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $amh_pc = new AMH_PC();

    $arr_pc = $amh_pc->get_aut_user(" AUT_UNAME = '{$user_name}' ");

    $aut_id = $arr_pc[0]["AUT_ID"];

    $this_year  = date("Y");
    $this_month = date("m");

    //$sales_target   = $amh_pc->get_sale_target_detail($aut_id, $this_month, $this_year);

    $arr_year = array('2015','2016','2017','2018','2019','2020');
    $option_year  = "";
    foreach ($arr_year as $year_val)
    {
        if ($year_val == $this_year) { $str_selected = "selected"; } else { $str_selected = ""; }
        $option_year .= "<option value='{$year_val}' {$str_selected}>{$year_val}</option>\n";
    }

    $arr_month = array();
    $arr_month["01"] = "มกราคม";
    $arr_month["02"] = "กุมภาพันธ์";
    $arr_month["03"] = "มีนาคม";
    $arr_month["04"] = "มษายน";
    $arr_month["05"] = "พฤษภาคม";
    $arr_month["06"] = "มิถุนายน";
    $arr_month["07"] = "กรกฎาคม";
    $arr_month["08"] = "สิงหาคม";
    $arr_month["09"] = "กันยายน";
    $arr_month["10"] = "ตุลาคม";
    $arr_month["11"] = "พฤศจิกายน";
    $arr_month["12"] = "ธันวาคม";

    $option_month    = "";

    foreach ($arr_month as $month_val => $month_name)
    {
        if ($month_val == $this_month) { $str_selected = "selected"; } else { $str_selected = ""; }
        $option_month .= "<option value='{$month_val}' {$str_selected}>{$month_name}</option>\n";
    }

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Amsel Health - PC:<?php echo $user_name; ?></title>
<link rel="stylesheet" href="stylesheets/amh_pc.css">
<link href='https://fonts.googleapis.com/css?family=Exo+2:700,400' rel='stylesheet' type='text/css'>
<style type='text/css'>
html, body
{
    height: 90vh;
}

.bg_w_40
{
    background-color: rgba(240, 230, 200, 0.4);
    padding: 10px;
}

.bg_w_50
{
    background-color: rgba(240, 230, 200, 0.5);
    padding: 10px;
}

.bg_w_60
{
    background-color: rgba(240, 230, 200, 0.6);
    padding: 10px;
}

.bg_w_70
{
    background-color: rgba(240, 230, 200, 0.7);
    padding: 10px;
}

.bg_w_80
{
    background-color: rgba(240, 230, 200, 0.8);
    padding: 10px;
}

body
{
    background-color: #000;
    margin: 0;
    padding: 0;
}

.btn_logout
{
    color: #960;
    cursor: pointer;
    display: inline-block;
    font-family: 'Exo 2', sans-serif;
    font-weight: bold;
    margin: 5px 15px;
    padding: 10px 20px;
}

.btn_logout:hover
{
    border: 1px solid #ff0;
    border-radius: 3px;
    color: #C90;
}

.btn_logout a
{
    color: #960;
    text-decoration: none;
}

.btn_logout a:hover
{
    color: #C90;
}

.half_page_item
{
    border-radius: 2px;
    color: #A80;
    cursor: pointer;
    font-family: Impact;
    font-size: 36px;
    margin: 15px;
    height: 45%;
    padding: 10px;
    position: relative;
    opacity: 1;
}

.item_pc_order
{
    background: #FFF url('images/vitamins_01.png') no-repeat right center;
}

.item_sale_rep
{
    background: #FFF url('images/notebook_01.png') no-repeat right center;
}

.password_panel, .setting_panel
{
    background-color: #fff;
    border-radius: 3px;
    display: none;
    margin: 15px;
    padding: 20px;
}

.password_panel label, .setting_panel label
{
    display: inline-block;
    font-family: 'Exo 2', sans-serif;
    font-size: 24px;
    font-weight: bold;
    margin: 10px;
    text-align: right;
    width: 200px;
}

.password_panel input, .setting_panel input
{
    border: 2px solid #888;
    border-radius: 3px;
    font-family: 'Exo 2', sans-serif;
    font-size: 24px;
    margin: 10px 0;
    padding: 5px 10px;
    width: 300px;
}

.password_panel button, .setting_panel button
{
    background-color: #eee;
    border: 2px solid #888;
    border-radius: 3px;
    font-family: 'Exo 2', sans-serif;
    font-size: 24px;
    margin: 10px 0;
    padding: 5px 10px;
    width: 325px;
}

.password_panel button:hover, .setting_panel button:hover
{
    background-color: #fff;
}

.submenu1
{
    border-bottom:1px solid #ddd;
    margin: 0 0 0 40px;
}

.the_page
{
    height: 90%;
}

.the_select
{
    font-size: 24pt;
    margin: 10px 0;
    padding: 10px;
    vertical-align: middle;
}
</style>
<script type="text/javascript" src="incl/jquery_ui/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
function startup_the_page(aut_id, the_month, the_year)
{
    get_sales_target(aut_id, the_month, the_year);
}

function goto_url(the_url)
{
    the_form = document.getElementById("the_form");
    the_form.action = the_url + ".php";
    the_form.submit();
}

function change_password()
{
    $('#password_panel').toggle();
    //$('#password_panel').css('display','block');
}

function change_setting()
{
    $('#setting_panel').toggle();
    //$('#setting_panel').css('display','block');
}

function do_changing_pass()
{
    m_passw = document.getElementById("m_passw").value;
    c_passw = document.getElementById("c_passw").value;

    if (m_passw != c_passw)
    {
        alert('Password ทั้งสองช่องไม่ตรงกัน');
    }
    else
    {
        $('#password_panel').css('display','none');
        post_password_changing(m_passw);
    }
}

function post_password_changing(pass_word)
{
    $.post( "pc_change_passwd.php", { pass_word: pass_word })
        .done(function(data)
        {
            alert(data);
        });
}

function do_change_setting(aut_id)
{
    the_target = $('#the_target').val();
    the_month  = $('#t_month').val();
    the_year   = $('#t_year').val();

    $.post( "pc_sales_target_set.php", { aut_id: aut_id, the_month: the_month, the_year: the_year, the_target: the_target })
        .done(function(data)
        {
            alert('New Target has been set.');
        });
}

function get_sales_target(aut_id, the_month, the_year)
{
    $('#the_target').val('0');

    if (the_month == '') { the_month = $('#t_month').val(); }
    if (the_year  == '') { the_year  = $('#t_year' ).val(); }

    $.post( "pc_sales_target_get.php", { aut_id: aut_id, the_month: the_month, the_year: the_year })
        .done(function(data)
        {
            $('#the_target').val(data);
        });
}
</script>
</head>
<body onload='startup_the_page("<?=$aut_id;?>","<?=$this_month;?>","<?=$this_year;?>");'>
<div class='the_page'>
<div class='half_page_item item_pc_order' onclick='goto_url("pc_order");'>
    <div class='bg_w_80'>
    <div class='fnt40'>PC ORDER</div>
    <div class='fnt40'>สั่งสินค้าเพิ่มเติม</div>
    </div>
</div>
<div class='half_page_item item_sale_rep'>
    <div class='bg_w_80' onclick='goto_url("pc_sale_report");'>
    <div class='fnt40'>SALES REPORT</div>
    <div class='fnt40'>ส่งรายงานสินค้าที่ขายได้</div>
    </div>
    <div class='bg_w_40 submenu1' onclick='goto_url("pc_sale_report_add_pc");'>
    <div class='fnt32 fnt_green'>เพิ่ม PC ช่วยขาย</div>
    </div>
    <div class='bg_w_40 submenu1' onclick='goto_url("pc_sale_report_chart");'>
    <div class='fnt32 fnt_green'>ดูรายงานยอดขาย</div>
    </div>
</div>
<div class='password_panel' id='password_panel'>
    <div><label for='m_passw'>Password:</label><input type='PASSWORD' name='m_passw' id='m_passw' value=''></div>
    <div><label for='c_passw'>Confirm:</label><input type='PASSWORD' name='c_passw' id='c_passw' value=''></div>
    <div style='width:545px; text-align: right;'><button type='BUTTON' onclick='do_changing_pass();'>Change Password</button></div>
</div>
<div class='setting_panel' id='setting_panel'>
    <div><label for='t_year'>Year:</label><select id='t_year' name='t_year' class='the_select' onchange='get_sales_target("<?=$aut_id;?>", "", "");'><?php echo $option_year; ?></select></div>
    <div><label for='t_month'>Month:</label><select id='t_month' name='t_month' class='the_select' onchange='get_sales_target("<?=$aut_id;?>", "", "");'><?php echo $option_month; ?></select></div>
    <div><label for='the_target'>Target:</label><input type='TEXT' id='the_target' name='the_target' value='<?php echo $sales_target; ?>'></div>
    <div><label></label><button type='BUTTON' onclick='do_change_setting("<?=$aut_id;?>");'>SET</button></div>
</div>
<div class='btn_logout fnt40'><a href='logout.php'>LOGOUT</a></div>
<div class='btn_logout fnt40' onclick='change_password();'>CHANGE PASSWORD</div>
<div class='btn_logout fnt40' onclick='change_setting();'>SETTING</div>
</div>
<form id='the_form' action='' method='POST' target=''>
<input type='HIDDEN' id='the_page' value='pc_main'>
</form>
</body>
</html>
<?php
}
else
{
    echo "<h2>Please login</h2>";
}
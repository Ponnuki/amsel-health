<?php

$aut_id      = $_REQUEST["aut_id"];
$this_month  = $_REQUEST["the_month"];
$this_year   = $_REQUEST["the_year"];
$this_target = $_REQUEST["the_target"];

require_once('config.php');
require_once('class_amh_db.php');
require_once('class_amh_pc.php');

$amh_pc = new AMH_PC();

$amh_pc->set_sale_target_detail($aut_id, $this_month, $this_year, $this_target);
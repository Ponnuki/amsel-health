<?php
/* Class name
   AMH_PO

   public function 
   po_main($str_search) return array of purchase order info (example: )
*/
require_once('class_amh_db.php');

class AMH_PO extends AMH_DB
{
    public function po_main($po_num, $str_date, $end_date, $order_by, $asc_desc)
    {
        $arr_ret = array();

        $str_where = "1";
        if ($po_num   != "") { $str_where .= " AND (PO.PO_NO         = '".$po_num  ."') "."\r\n"; }
        if ($str_date != "") { $str_where .= " AND (PO.CREATED_DATE >= '".$str_date."') "."\r\n"; }
        if ($end_date != "") { $str_where .= " AND (PO.CREATED_DATE <= '".$end_date."') "."\r\n"; }

        $sql_sel  = " SELECT PO.*, CS.CUS_NAME as THE_NAME ";
        $sql_sel .= " FROM po_main as PO, mst_customer as CS ";
        $sql_sel .= " WHERE ".$str_where;
        $sql_sel .= " AND (PO.CUS_ID = CS.CUS_ID) ";
        $sql_sel .= " ORDER BY {$order_by} {$asc_desc} ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>po_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function po_main_like($po_num, $str_date, $end_date, $order_by, $asc_desc)
    {
        $arr_ret = array();

        $str_where = "1";
        if ($po_num   != "") { $str_where .= " AND (PO.PO_NO      LIKE '%".$po_num  ."%') "."\r\n"; }
        if ($str_date != "") { $str_where .= " AND (PO.CREATED_DATE >= '".$str_date."') "."\r\n"; }
        if ($end_date != "") { $str_where .= " AND (PO.CREATED_DATE <= '".$end_date."') "."\r\n"; }

        $sql_sel  = " SELECT PO.*, CS.CUS_NAME as THE_NAME ";
        $sql_sel .= " FROM po_main as PO, mst_customer as CS ";
        $sql_sel .= " WHERE ".$str_where;
        $sql_sel .= " AND (PO.CUS_ID = CS.CUS_ID) ";
        $sql_sel .= " ORDER BY {$order_by} {$asc_desc} ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>po_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_porsor($kor_sor)
    {
        $str_ret = '';

        if ($kor_sor != '')
        {
            $str_ret = $kor_sor + 543;
        }

        $str_ret = substr($str_ret, -2);

        return $str_ret;
    }

    public function get_cus_name($cus_code)
    {
        $str_ret = "";

        $sql_sel = " SELECT CUS_ID FROM mst_customer WHERE CUS_CODE = '{$cus_code}' ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_po_num error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $str_ret = $row_sel["CUS_ID"];
        }
        $res_sel->free();

        return $str_ret;
    }

    public function get_po_num()
    {
        $str_num = "";

        $this_porsor = $this->get_porsor(date('Y'));
        $this_month  = date('m');
        $this_day    = date('d');
        $po_format   = "PO".$this_porsor.$this_month.$this_day;

        $sql_sel = " SELECT PO_NO FROM po_main WHERE PO_NO LIKE '{$po_format}%' ORDER BY PO_NO DESC ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_po_num error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $po_num  = $row_sel["PO_NO"];
            $po_part = array();
            $po_part = explode("-", $po_num);
            $po_part[1]++;

            if (strlen($po_part[1]) == 1) { $po_part[1] = "00".$po_part[1]; }
            if (strlen($po_part[1]) == 2) { $po_part[1] = "0" .$po_part[1]; }

            $str_num = $po_format."-".$po_part[1];
        }
        else
        {
            $str_num = $po_format."-001";
        }

        return $str_num;
    }

    public function re_format_date($ddmmyyyy)
    {
        $str_ren  = "";
        if ($ddmmyyyy != '')
        {
            $arr_date = explode("-", $ddmmyyyy);
            $str_ren  = $arr_date[2]."-".$arr_date[1]."-".$arr_date[0];
        }
        return $str_ren;
    }

    public function thisday()
    {
        $str_ret = Date('Y-m-d');
        return $str_ret;
    }

    public function get_po_detail_json($PO_ID)
    {
        $str_ret = "";
        $arr_data = array();

        if ($PO_ID != "")
        {
            $sql_sel = " SELECT * FROM po_dtl WHERE PO_ID = '{$PO_ID}' ";
            $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_po_detail_json error:{$sql_sel}</div>");
            while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // can be MYSQLI_BOTH, MYSQLI_NUM
            {
                $arr_data[] = $row_sel;
            }
        }
        $res_sel->free();

        return json_encode($arr_data);
    }

    public function get_po_main_json($PO_ID)
    {
        $str_ret = "";

        if ($PO_ID != "")
        {
            $sql_sel = " SELECT * FROM po_main WHERE PO_ID = '{$PO_ID}' ";
            $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_po_main_json error:{$sql_sel}</div>");
            if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // can be MYSQLI_BOTH, MYSQLI_NUM
            {
                $row_sel["PO_DATE"]       = date('d-m-Y', strtotime($row_sel["PO_DATE"]));
                $row_sel["ORG_PO_DATE"]   = date('d-m-Y', strtotime($row_sel["ORG_PO_DATE"]));
                $row_sel["DELIVERY_DATE"] = date('d-m-Y', strtotime($row_sel["DELIVERY_DATE"]));
                
                $str_ret = json_encode($row_sel);
            }
        }
        $res_sel->free();

        return $str_ret;
    }

    public function del_po($po_id)
    {
        $sql_del_dtl = " DELETE FROM po_dtl WHERE PO_ID = '{$po_id}' ";
        $this->mysqli->query($sql_del_dtl) OR die("<div style='display:none;'>po_dtl error:{$sql_del_dtl}</div>");

        $sql_del_main = " DELETE FROM po_main WHERE PO_ID = '{$po_id}' ";
        $this->mysqli->query($sql_del_main) OR die("<div style='display:none;'>po_dtl error:{$sql_del_main}</div>");
    }

    public function upd_po($posting)
    {
        $str_po_no = $posting["po_num"];
        $thisday   = $this->thisday();

        $str_upd  = " UPDATE po_main SET ";
        $str_upd .= " CUS_PO_NO     = '".$posting["cpo_num" ]."' ,";
        $str_upd .= " PO_DATE       = '".$this->re_format_date($posting["po_date" ])."' ,";
        
        if ($posting["org_date"] != '')
        {
            $str_upd .= " ORG_PO_DATE   = '".$this->re_format_date($posting["org_date"])."' ,";
        }
        
        if ($posting["dlv_date"] != '')
        {
            $str_upd .= " DELIVERY_DATE = '".$this->re_format_date($posting["dlv_date"])."' ,";
        }
        
        $str_upd .= " OBJ_ID        = '".$posting["po_type" ]."' ,";
        $str_upd .= " REASON_DTL    = '".$posting["details" ]."' ,";
        $str_upd .= " CUS_ID        = '".$posting["cus_id"  ]."' ,";
        $str_upd .= " CUS_NAME      = '".$posting["cus_name"]."' ,";
        $str_upd .= " CUS_CONTACT   = '".$posting["cus_cnct"]."' ,";
        $str_upd .= " CUS_ADDRESS   = '".$posting["cus_addr"]."' ,";
        $str_upd .= " DISTRICT_CODE = '".$posting["district"]."' ,";
        $str_upd .= " PROVINCE_CODE = '".$posting["province"]."' ,";
        $str_upd .= " CUS_ZIP       = '".$posting["cus_zip" ]."' ,";
        $str_upd .= " CUS_TEL       = '".$posting["cus_tel" ]."' ,";
        $str_upd .= " CUS_FAX       = '".$posting["cus_fax" ]."' ,";
        $str_upd .= " TAX_NUMBER    = '".$posting["cus_tax" ]."' ,";
        $str_upd .= " PO_TOTAL      = '".$posting["po_total"]."' ,";
        $str_upd .= " DISCOUNT_PER  = '".$posting["disc_pcn"]."' ,";
        $str_upd .= " VAT_PER       = '".$posting["vat_pcn" ]."' ,";
        $str_upd .= " GRAND_TOTAL   = '".$posting["grand_tl"]."' ,";
        $str_upd .= " UPDATE_DATE   = '".$thisday  ."'  ";
        $str_upd .= " WHERE PO_NO   = '".$str_po_no."'  ";

        $this->mysqli->query($str_upd) OR die("<div style='display:none;'>add po main error:{$str_upd}</div>");

        $str_sel = " SELECT * FROM po_main WHERE PO_NO = '".$str_po_no."' ";
        $res_sel = $this->mysqli->query($str_sel) OR die("<div style='display:none;'>get po_id error:{$str_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $po_id   = $row_sel["PO_ID"];
            $created = $row_sel["CREATED_DATE"];
        }
        $res_sel->free();

        $str_del = " DELETE FROM po_dtl WHERE PO_ID = '".$po_id."' ";
        $this->mysqli->query($str_del) OR die("<div style='display:none;'>del po_dtl error:{$str_del}</div>");

        $str_dtl_ttl = "";
        $num_count   = 0;
        $arr_items = explode('^^^', $posting["po_detail"]);
        foreach ($arr_items as $item_detail)
        {
            $num_count++;
            $items_data = explode('vvv', $item_detail);

            $str_dtl  = " INSERT INTO po_dtl SET ";
            $str_dtl .= " PO_ID        = '".$po_id    ."' ,";
            $str_dtl .= " QNO          = '".$num_count."' ,";
            $str_dtl .= " CREATED_DATE = '".$created  ."' ,";
            $str_dtl .= " UPDATE_DATE  = '".$thisday  ."' ";

            foreach ($items_data as $data_dtl)
            {
                $ins_data = explode(':', $data_dtl);
                $str_dtl .= ",".$ins_data[0]." = '".$ins_data[1]."'";
            }
            $this->mysqli->query($str_dtl) OR die("<div style='display:none;'>add po dtl error:{$str_dtl}</div>");
            $str_dtl_ttl .= $str_dtl;
        }

        return $str_po_no;
    }

    public function add_po($posting)
    {
        $str_po_no = $this->get_po_num();
        $thisday   = $this->thisday();

        $str_ins  = " INSERT INTO po_main SET ";
        $str_ins .= " PO_NO         = '".$str_po_no."' ,";
        $str_ins .= " CUS_PO_NO     = '".$posting["cpo_num" ]."' ,";
        $str_ins .= " PO_DATE       = '".$this->re_format_date($posting["po_date" ])."' ,";
        
        if ($posting["org_date"] != '')
        {
            $str_ins .= " ORG_PO_DATE   = '".$this->re_format_date($posting["org_date"])."' ,";
        }
        
        if ($posting["dlv_date"] != '')
        {
            $str_ins .= " DELIVERY_DATE = '".$this->re_format_date($posting["dlv_date"])."' ,";
        }

        $str_ins .= " OBJ_ID        = '".$posting["po_type" ]."' ,";
        $str_ins .= " REASON_DTL    = '".$posting["details" ]."' ,";
        $str_ins .= " CUS_ID        = '".$posting["cus_id"  ]."' ,";
        $str_ins .= " CUS_NAME      = '".$posting["cus_name"]."' ,";
        $str_ins .= " CUS_CONTACT   = '".$posting["cus_cnct"]."' ,";
        $str_ins .= " CUS_ADDRESS   = '".$posting["cus_addr"]."' ,";
        $str_ins .= " DISTRICT_CODE = '".$posting["district"]."' ,";
        $str_ins .= " PROVINCE_CODE = '".$posting["province"]."' ,";
        $str_ins .= " CUS_ZIP       = '".$posting["cus_zip" ]."' ,";
        $str_ins .= " CUS_TEL       = '".$posting["cus_tel" ]."' ,";
        $str_ins .= " CUS_FAX       = '".$posting["cus_fax" ]."' ,";
        $str_ins .= " TAX_NUMBER    = '".$posting["cus_tax" ]."' ,";
        $str_ins .= " PO_TOTAL      = '".$posting["po_total"]."' ,";
        $str_ins .= " DISCOUNT_PER  = '".$posting["disc_pcn"]."' ,";
        $str_ins .= " VAT_PER       = '".$posting["vat_pcn" ]."' ,";
        $str_ins .= " GRAND_TOTAL   = '".$posting["grand_tl"]."' ,";
        $str_ins .= " CREATED_DATE  = '".$thisday    ."' ,";
        $str_ins .= " UPDATE_DATE   = '".$thisday    ."'  ";

        $this->mysqli->query($str_ins) OR die("<div style='display:none;'>add po main error:{$str_ins}</div>");

        $str_sel = " SELECT * FROM po_main ORDER BY PO_ID DESC ";
        $res_sel = $this->mysqli->query($str_sel) OR die("<div style='display:none;'>get po_id error:{$str_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $po_id = $row_sel["PO_ID"];
        }
        $res_sel->free();

        $str_dtl_ttl = "";
        $num_count   = 0;
        $arr_items = explode('^^^', $posting["po_detail"]);
        foreach ($arr_items as $item_detail)
        {
            $num_count++;
            $items_data = explode('vvv', $item_detail);

            $str_dtl  = " INSERT INTO po_dtl SET ";
            $str_dtl .= " PO_ID        = '".$po_id    ."' ,";
            $str_dtl .= " QNO          = '".$num_count."' ,";
            $str_dtl .= " CREATED_DATE = '".$thisday  ."' ,";
            $str_dtl .= " UPDATE_DATE  = '".$thisday  ."' ";

            foreach ($items_data as $data_dtl)
            {
                $ins_data = explode(':', $data_dtl);
                $str_dtl .= ",".$ins_data[0]." = '".$ins_data[1]."'";
            }
            $this->mysqli->query($str_dtl) OR die("<div style='display:none;'>add po dtl error:{$str_dtl}</div>");
            $str_dtl_ttl .= $str_dtl;
        }

        return $str_po_no;
    }

    public function convert_date($date_input)
    {
        if ($date_input == '')
        {
            $str_ret = '';
        }
        else
        {
            $arr_date = explode('-', $date_input);
            $str_ret  = $arr_date[2]."-".$arr_date[1]."-".$arr_date[0];
        }

        return $str_ret;
    }

    public function mst_product($product_id, $product_name)
    {
        $arr_ret = array();

        $str_where = "1";
        if ($product_id   != '') { $str_where .= " AND (PRD.PRODUCT_ID   >=   '{$product_id}'    ) "."\r\n"; }
        if ($product_name != '') { $str_where .= " AND (PRD.PRODUCT_NAME LIKE '%{$product_name}%') "."\r\n"; }

        $sql_sel  = " SELECT PRD.* ";
        $sql_sel .= " FROM mst_product as PRD ";
        $sql_sel .= " WHERE ".$str_where;
        $sql_sel .= " AND (ACTIVE_FLAG = 'Y') ";
        $sql_sel .= " ORDER BY PRODUCT_CODE ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_mst_product error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function mst_product_sale($product_id, $product_name)
    {
        $arr_ret = array();

        $str_where = "1";
        if ($product_id   != '') { $str_where .= " AND (PRD.PRODUCT_ID   >=   '{$product_id}'    ) "."\r\n"; }
        if ($product_name != '') { $str_where .= " AND (PRD.PRODUCT_NAME LIKE '%{$product_name}%') "."\r\n"; }

        $sql_sel  = " SELECT PRD.*, PDC.* ";
        $sql_sel .= " FROM mst_product_pc as PDC, mst_product as PRD ";
        $sql_sel .= " WHERE ".$str_where;
        $sql_sel .= " AND (PDC.ACTIVE_FLAG = 'Y') ";
        $sql_sel .= " AND (PRD.PRODUCT_ID  = PDC.PRODUCT_ID) ";
        $sql_sel .= " ORDER BY PRD.PRODUCT_CODE ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_mst_product error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function create_product_code_option($ltp_chart_id)
    {
        $str_retn = "";
        $arr_product = $this->mst_product_sale('','');
        
        foreach ($arr_product as $product)
        {
            $selected  = "";
            if ($ltp_chart_id == $product["LTP_CHART_ID"]) { $selected  = " selected"; }
            $str_retn .= "<option value='".$product["LTP_CHART_ID"]."'".$selected.">".$product["PRODUCT_SALE_CODE"]."</option>";
        }

        return $str_retn;
    }

    public function gen_temp_id()
    {
        //
    }

    public function exist_in_do($po_id)
    {
        $str_ret = "";

        if ($po_id == "")
        {
            $str_ret = "<div style='display:none;'>exist_in_do error: parameter is missing</div>";
        }
        else
        {
            $sql_sel = " SELECT * FROM do_main WHERE PO_ID = '{$po_id}' ";
            $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>exist_in_do error:{$sql_sel}</div>");
            if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
            {
                $str_ret = true;
            }
            else
            {
                $str_ret = false;
            }
            $res_sel->free();
        }

        return $str_ret;
    }

    public function get_po_id($po_num)
    {
        $po_id   = "";

        $str_sel = " SELECT * FROM po_main WHERE PO_NO = '{$po_num}' ";
        $res_sel = $this->mysqli->query($str_sel) OR die("<div style='display:none;'>get_po_id error:{$str_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $po_id = $row_sel["PO_ID"];
        }
        $res_sel->free();

        return $po_id;
    }
}
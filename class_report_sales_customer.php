<?php
require_once("class_amh_db.php");
require_once("class_report_sales_customer_excel.php");
require_once("include_function.php");
require_once("validatelogin.php");

class AMH_rp_sale_cus extends AMH_DB
{
	public function customer_list($cus_id = "")
    {
        $arr_ret = array();
		$where = "WHERE mc.ACTIVE_FLAG = 'Y' ";
		if ($aut_uname != "") $where .= " AND mc.CUS_ID ='".$aut_uname."' ";
        $sql_sel = " SELECT mc.CUS_ID, CONCAT( mc.CUS_NAME ,' ', '(' ,mc.CUS_CODE , ')') AS CUS_NAME FROM mst_customer AS mc  ";
		$order = "ORDER BY CONCAT( mc.CUS_NAME ,' ', '(' ,mc.CUS_CODE , ')') ";
        $res_sel = $this->mysqli->query($sql_sel.$where) OR die("<div style='display:none;'>mst_district error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }
	
	public function get_ddl_customer()
    {
        $str_retn = "";
		
        $arr = $this->customer_list();

        foreach ($arr as $row)
        {
            $selected  = "";
            //if ($aut_id == $row["alert(source);"]) { $selected  = " selected"; }
            $str_retn .= "<option value='".$row["CUS_ID"]."'".$selected.">".$row["CUS_NAME"]."</option>";
        }
        return $str_retn;
		
    }
	
	
	
	public function get_data_sale($cus_id="", $prd_type ="", $start_date ="", $end_date = "",$excel_path="",$pro_img_path="")
    {

        $where = " WHERE 1 ";
        if ($cus_id != "")
        {
			$where  .= " AND srm.CUS_ID = ".$cus_id;
		}/*
		if ($prd_type != "")
        {
			$where  .= " AND mp.PRODUCT_TYPE_ID = ".$prd_type;
		}
		*/
		if ($start_date !="" || $end_date != "")
		{
			$where .= " AND srm.SALE_REPORT_DATE BETWEEN STR_TO_DATE('".$start_date ."','%d-%m-%Y') AND  STR_TO_DATE('".$end_date."','%d-%m-%Y')  ";
        }
		$file_fname="report_sale_cus_";
		
		$arr_ret = array();
		$sql_main = "SELECT mp.PRODUCT_CODE,
								FORMAT(IFNULL(dtl.QTY,0),0) AS QTY ,
								FORMAT(IFNULL(dtl.AMOUNT,0),2) AS AMOUNT ,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								IFNULL(cus.CUS_NAME,'') as CUS_NAME,
								CONCAT('".$file_fname."', IFNULL(cus.CUS_NAME,''),'_".str_replace("-","",$start_date)."_".str_replace("-","",$end_date).".xlsx') as EXCEL_NAME
							FROM  mst_product AS mp 
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID
								LEFT OUTER JOIN ( ";
		$sql_dtl = "			SELECT pc.PRODUCT_ID, SUM(srd.QTY) as QTY, SUM( srd.QTY * LTP_PRICE) as AMOUNT			
									FROM  sale_report_main  AS srm
										LEFT JOIN sale_report_dtl  AS srd  ON srm.SALE_REPORT_ID = srd.SALE_REPORT_ID
										LEFT JOIN sale_report_pc_rel AS srpc ON srm.SALE_REPORT_ID = srpc.SALE_REPORT_ID
										LEFT JOIN mst_product_pc AS pc ON pc.PRODUCT_PC_ID = srd.PRODUCT_PC_ID 
										LEFT JOIN mst_customer AS mc ON srm.CUS_ID = mc.CUS_ID ";
						
		$group = " 			GROUP BY pc.PRODUCT_ID, mc.CUS_NAME, mc.CUS_CODE ";
		$sql_end = " ) dtl ON dtl.PRODUCT_ID = mp.PRODUCT_ID
						,(select CONCAT( c.CUS_NAME ,' ', '(' ,c.CUS_CODE , ')') AS CUS_NAME from mst_customer AS c where c.CUS_ID = ".$cus_id." ) cus
						WHERE mp.ACTIVE_FLAG = 'Y'
						ORDER BY mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
		$sql = $sql_main.$sql_dtl.$where.$group.$sql_end;
        $res_sel = $this->mysqli->query($sql) OR die("Function get_data_sale ERROR:{$sql}");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		$amh_rp_sale_cus_excel =new AMH_rp_sale_cus_excel();
		$amh_rp_sale_cus_excel->gen_excel_sale($arr_ret, $excel_path, $pro_img_path, $start_date, $end_date,"SALE");
		
        return json_encode($arr_ret);
    }
	
	
	public function get_data_sale_daily($cus_id="", $prd_type ="", $start_date ="", $end_date = "",$excel_path="",$pro_img_path="")
    {

        $where = " WHERE 1 ";
        if ($cus_id != "")
        {
			$where  .= " AND srm.CUS_ID = ".$cus_id;
		}/*
		if ($prd_type != "")
        {
			$where  .= " AND mp.PRODUCT_TYPE_ID = ".$prd_type;
		}
		*/
		if ($start_date !="" || $end_date != "")
		{
			$where .= " AND srm.SALE_REPORT_DATE BETWEEN STR_TO_DATE('".$start_date ."','%d-%c-%Y') AND  STR_TO_DATE('".$end_date."','%d-%c-%Y')  ";
        }
		$file_fname="report_sale_cus_daily_";
		
		$arr_ret = array();
		$sql_main = "SELECT 
								DATE_FORMAT(srm.SALE_REPORT_DATE,'%d-%m-%Y') AS ACTION_DATE, pc.PRODUCT_ID, 
								FORMAT(SUM(srd.QTY),0) as QTY, FORMAT(SUM( srd.QTY * LTP_PRICE),2) as AMOUNT,
								mp.PRODUCT_CODE,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								CONCAT( mc.CUS_NAME ,' ', '(' ,mc.CUS_CODE , ')') AS CUS_NAME,
								CONCAT('".$file_fname."',mc.CUS_NAME ,' ', '(' ,mc.CUS_CODE , ')' ,'_".str_replace("-","",$start_date)."_".str_replace("-","",$end_date).".xlsx') as EXCEL_NAME
							FROM   sale_report_main AS srm
								LEFT JOIN  sale_report_dtl AS srd ON srm.SALE_REPORT_ID = srd.SALE_REPORT_ID
								LEFT JOIN mst_product_pc AS pc ON  pc.PRODUCT_PC_ID = srd.PRODUCT_PC_ID 
								LEFT JOIN sale_report_pc_rel AS srpc ON srm.SALE_REPORT_ID = srpc.SALE_REPORT_ID
								LEFT JOIN mst_customer AS mc ON srm.CUS_ID = mc.CUS_ID
								LEFT JOIN mst_product AS mp  ON pc.PRODUCT_ID = mp.PRODUCT_ID
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID ";
						
		$group = " GROUP BY srm.SALE_REPORT_ID, srm.SALE_REPORT_DATE, pc.PRODUCT_ID, mc.CUS_NAME, mc.CUS_CODE,
							mp.PRODUCT_CODE,mp.PRODUCT_TYPE_ID,mp.PRODUCT_ID,mpt.TYPE_NAME_EN ,mpt.TYPE_NAME_TH ,mpt.ICON ,
							mp.PRODUCT_NAME_EN, mp.PRODUCT_NAME_TH,mp.PRD_IMG ";
		
		$sql_end = " 		ORDER BY srm.SALE_REPORT_DATE desc, mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
        $res_sel = $this->mysqli->query($sql_main.$where.$group.$sql_end) OR die("Function [get_data_sale_daily] ERROR:{$sql_main}");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		$amh_rp_sale_cus_excel =new AMH_rp_sale_cus_excel();
		$amh_rp_sale_cus_excel->gen_excel_sale_daily($arr_ret, $excel_path, $pro_img_path, $start_date, $end_date,"SALE");
		
        return json_encode($arr_ret);
    }
	
	
	public function get_data_order($cus_id="", $prd_type ="", $start_date ="", $end_date = "",$excel_path="",$pro_img_path="")
    {

        $where = " WHERE 1 ";
        if ($cus_id != "")
        {
			$where  .= " AND  o_dtl.CUS_ID = ".$cus_id;
		}
		/*
		if ($prd_type != "")
        {
			$where  .= " AND mp.PRODUCT_TYPE_ID = ".$prd_type;
		}*/
		if ($start_date !="" || $end_date != "")
		{
			$where .= " AND o_dtl.CREATED_DATE BETWEEN STR_TO_DATE('".$start_date ."','%d-%c-%Y') AND  STR_TO_DATE('".$end_date."','%d-%c-%Y')  ";
        }
		$file_fname="report_order_cus_";
		
		$arr_ret = array();
		$sql_main = "SELECT mp.PRODUCT_CODE,
								FORMAT(IFNULL(dtl.QTY,0),0) AS QTY ,
								FORMAT(IFNULL(dtl.AMOUNT,0),2) AS AMOUNT ,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								IFNULL(cus.CUS_NAME,'') as CUS_NAME,
								CONCAT('".$file_fname."',IFNULL(cus.CUS_NAME,''),'_".str_replace("-","",$start_date)."_".str_replace("-","",$end_date).".xlsx') as EXCEL_NAME
							FROM  mst_product AS mp 
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID
								LEFT OUTER JOIN
									(";
		$sql_dtl = "			SELECT pc.PRODUCT_ID, SUM(o_dtl.QTY) as QTY, SUM( o_dtl.QTY * pc.LTP_PRICE) as AMOUNT
									FROM   mst_product_pc AS pc 
										LEFT JOIN order_dtl AS o_dtl ON pc.PRODUCT_PC_ID = o_dtl.PRODUCT_PC_ID 
										LEFT JOIN mst_customer AS mc ON o_dtl.CUS_ID = mc.CUS_ID ";
						
		$group = "	GROUP BY pc.PRODUCT_ID, mc.CUS_NAME , mc.CUS_CODE ";
		$sql_end = " ) dtl ON dtl.PRODUCT_ID = mp.PRODUCT_ID
						,(select CONCAT( c.CUS_NAME ,' ', '(' ,c.CUS_CODE , ')') AS CUS_NAME from mst_customer AS c where c.CUS_ID = ".$cus_id." ) cus
						WHERE mp.ACTIVE_FLAG = 'Y' 
						ORDER BY mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
        $res_sel = $this->mysqli->query($sql_main.$sql_dtl.$where.$group.$sql_end) OR die("Function [get_data_order] ERROR:{$sql_main}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		$amh_rp_sale_cus_excel =new AMH_rp_sale_cus_excel();
		$amh_rp_sale_cus_excel->gen_excel_sale($arr_ret, $excel_path, $pro_img_path, $start_date, $end_date,"ORDER");
		
        return json_encode($arr_ret);
    }
	
	
	
	public function get_data_order_daily($cus_id="", $prd_type ="", $start_date ="", $end_date = "",$excel_path="",$pro_img_path="")
    {

        $where = " WHERE 1 ";
        if ($cus_id != "")
        {
			$where  .= " AND o_dtl.CUS_ID = ".$cus_id;
		}
		/*
		if ($prd_type != "")
        {
			$where  .= " AND mp.PRODUCT_TYPE_ID = ".$prd_type;
		}*/
		if ($start_date !="" || $end_date != "")
		{
			$where .= " AND o_dtl.CREATED_DATE BETWEEN STR_TO_DATE('".$start_date ."','%d-%c-%Y') AND  STR_TO_DATE('".$end_date."','%d-%c-%Y')  ";
        }
		
		$file_fname="report_order_cus_daily_";
		$arr_ret = array();
		$sql_main = "SELECT 
								DATE_FORMAT(o_dtl.CREATED_DATE,'%d-%m-%Y') AS ACTION_DATE, pc.PRODUCT_ID, 
								FORMAT(SUM(o_dtl.QTY),0) as QTY, FORMAT(SUM( o_dtl.QTY * LTP_PRICE),2) as AMOUNT,
								mp.PRODUCT_CODE,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								CONCAT( mc.CUS_NAME ,' ', '(' ,mc.CUS_CODE , ')') AS CUS_NAME,
								1 AS NO_OF_PC,
								CONCAT('".$file_fname."',mc.CUS_NAME ,' ', '(' ,mc.CUS_CODE , ')' ,'_".str_replace("-","",$start_date)."_".str_replace("-","",$end_date).".xlsx') as EXCEL_NAME
							FROM  order_dtl AS o_dtl
								LEFT JOIN mst_product_pc AS pc ON  pc.PRODUCT_PC_ID = o_dtl.PRODUCT_PC_ID 
								LEFT JOIN mst_customer AS mc ON o_dtl.CUS_ID = mc.CUS_ID
								left join mst_product AS mp  ON pc.PRODUCT_ID = mp.PRODUCT_ID
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID ";
						
		$group = "	GROUP BY o_dtl.CREATED_DATE, pc.PRODUCT_ID, mc.CUS_NAME , mc.CUS_CODE,
								mp.PRODUCT_CODE,mp.PRODUCT_TYPE_ID,mp.PRODUCT_ID,mpt.TYPE_NAME_EN ,mpt.TYPE_NAME_TH ,mpt.ICON ,
								mp.PRODUCT_NAME_EN, mp.PRODUCT_NAME_TH,mp.PRD_IMG ";
		$sql_end = " ORDER BY o_dtl.CREATED_DATE desc, mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
        $res_sel = $this->mysqli->query($sql_main.$where.$group.$sql_end) OR die("Function 'get_data_order_daily' ERROR:{$sql_main}");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		$amh_rp_sale_cus_excel =new AMH_rp_sale_cus_excel();
		$amh_rp_sale_cus_excel->gen_excel_sale_daily($arr_ret, $excel_path, $pro_img_path, $start_date, $end_date,"ORDER");
		
        return json_encode($arr_ret);
    }
	
	
	public function get_data_stock($cus_id="", $prd_type ="",$excel_path="",$pro_img_path="")
    {

        $where_od = " WHERE 1 ";
		$where_sr = " WHERE 1 ";
        if ($cus_id != "")
        {
			$where_od  .= " AND o_dtl.CUS_ID = ".$cus_id;
			$where_sr  .= " AND srm.CUS_ID = ".$cus_id;
			
		}/*
		if ($prd_type != "")
        {
			$where  .= " AND mp.PRODUCT_TYPE_ID = ".$prd_type;
		}*/
		$file_fname="report_stock_cus_";
		
		$arr_ret = array();
		$sql_main = "SELECT mp.PRODUCT_CODE,
								FORMAT(IFNULL(od_sum.QTY,0) - IFNULL(sr_sum.QTY,0),0) AS QTY ,
								FORMAT(IFNULL(od_sum.AMOUNT,0) - IFNULL(sr_sum.AMOUNT,0),2) AS AMOUNT ,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								IFNULL(od_mc.CUS_NAME,sr_mc.CUS_NAME) as CUS_NAME,
								CONCAT('".$file_fname."',IFNULL(od_mc.CUS_NAME,sr_mc.CUS_NAME) ,' ', '(' ,IFNULL(od_mc.CUS_CODE,sr_mc.CUS_CODE) , ')' ,'.xlsx') as EXCEL_NAME
							FROM  mst_product AS mp 
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID	";
		$sql_sub1 = "	LEFT OUTER JOIN
									(		
									SELECT pc.PRODUCT_ID, SUM(o_dtl.QTY) as QTY, SUM(o_dtl.QTY * pc.LTP_PRICE) as AMOUNT,
										o_dtl.CUS_ID
									FROM order_dtl AS o_dtl 
										LEFT JOIN mst_product_pc AS pc  ON pc.PRODUCT_PC_ID = o_dtl.PRODUCT_PC_ID  ".$where_od.
							" GROUP BY pc.PRODUCT_ID, o_dtl.CUS_ID
								) od_sum ON od_sum.PRODUCT_ID = mp.PRODUCT_ID ";
								
		$sql_sub2 = "	LEFT OUTER JOIN
									(		
									SELECT pc.PRODUCT_ID, SUM(srd.QTY) as QTY, SUM(srd.QTY * pc.LTP_PRICE) as AMOUNT,
										srm.CUS_ID
									FROM  sale_report_main  AS srm 
										LEFT JOIN sale_report_dtl  AS srd  ON srm.SALE_REPORT_ID = srd.SALE_REPORT_ID
										LEFT JOIN mst_product_pc AS pc  ON pc.PRODUCT_PC_ID = srd.PRODUCT_PC_ID  ".$where_sr.
							" GROUP BY pc.PRODUCT_ID, srm.CUS_ID
								) sr_sum ON sr_sum.PRODUCT_ID = mp.PRODUCT_ID ";				
		
		$sql_end = "	LEFT JOIN mst_customer AS od_mc ON od_sum.CUS_ID = od_mc.CUS_ID
							LEFT JOIN mst_customer AS sr_mc ON sr_sum.CUS_ID = sr_mc.CUS_ID
						WHERE mp.ACTIVE_FLAG = 'Y'
						ORDER BY mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
		$sql = $sql_main.$sql_sub1.$sql_sub2.$sql_end;
        $res_sel = $this->mysqli->query($sql) OR die("Function [get_data_stock] ERROR:{$sql}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		$amh_rp_sale_cus_excel =new AMH_rp_sale_cus_excel();
		$amh_rp_sale_cus_excel->gen_excel_sale($arr_ret, $excel_path, $pro_img_path, $start_date, $end_date,"STOCK");
		
        return json_encode($arr_ret);
    }
	
	public function get_prd_type()
    {
        $arr_ret = array();

        $sql = "SELECT *
						FROM mst_product_type
						ORDER BY PRODUCT_TYPE_ID";
		
        $res_sel = $this->mysqli->query($sql) OR die("<div style='color:red;'>Function [get_prd_type] ERROR:{$sql}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
        return json_encode($arr_ret);
    }
	
}
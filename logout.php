<?php
	ob_start();
	session_start();
	if(session_destroy()) // Destroying All Sessions
	{
		$querystr = "";
		if (isset($_REQUEST["msg"]))  $querystr ="?msg=".$_REQUEST["msg"]; 
		header("Location: login.php".$querystr); // Redirecting To Home Page
	}
	ob_end_flush();
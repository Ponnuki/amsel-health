    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	

	<!-- Favicon -->
    <!-- <link rel="shortcut icon" href="favicon.ipng"> -->
	
	 <link rel="icon" type="image/png" href="images/favicon.png" />

	 
    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="assets/css/style.css"> 

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">  
    <link rel="stylesheet" href="assets/css/footers/footer-me.css">

	<!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-colors/orange.css" /> 

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">

	
    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="assets/css/pages/page_search_inner_tables.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

	<style type="text/css">

      /* Sticky footer styles
      -------------------------------------------------- */

      html,
      body {
        height: 100%;
        /* The html and body elements cannot have any padding or margin. */
      }

      /* Wrapper for page content to push down footer */
      #wrap {
        min-height: 100%;
        height: auto !important;
        height: 100%;
        /* Negative indent footer by it's height */
        margin: 0 auto -85px;
      }
      
    </style>
	
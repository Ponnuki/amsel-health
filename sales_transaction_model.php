﻿<?php
	
	echo "Submit Form = ".$_GET["form"]."</br> hdMode=".$_POST['hdMode']."<br/>";

	require("connect.php");
	session_start();
	//***** Function Begin *******
	function replaseStockShop($ddlShopCodeID, $ddlSalesCodeID, $ddlProSaleCode, $txbQty)
	{
		$sql = sprintf("SELECT ss.STOCK_QTY 
								FROM stock_shop AS ss 
								WHERE ss.SHOP_ID = %d AND ss.SALES_EMP_ID =%d AND ss.PRODUCT_SALE_CODE = '%s' ",
					mysql_real_escape_string($ddlShopCodeID),
					mysql_real_escape_string($ddlSalesCodeID),
					mysql_real_escape_string($ddlProSaleCode)
					);
		$result_trn_sale_dtl =  mysql_query($sql);
		$num_rows = mysql_num_rows($result_trn_sale_dtl);
		echo "check Stock=".$sql."<br/>";
		if ($num_rows < 1)
		{
		
			$sql = sprintf("INSERT INTO stock_shop (SHOP_ID, SALES_EMP_ID, PRODUCT_SALE_CODE, PRODUCT_ID, STOCK_QTY, CREATED_BY, CREATED_DATE,  UPDATE_DATE)
										SELECT   tmp_tb.SHOP_ID, tmp_tb.SALES_EMP_ID,  tmp_tb.PRODUCT_SALE_CODE, tlc.PRODUCT_ID, 
										  tmp_tb.STOCK_QTY, '%s' AS CREATED_BY , NOW() AS CREATED_DATE , NOW() AS UPDATE_DATE
										FROM   (SELECT 
														%d AS SHOP_ID, 
														%d AS SALES_EMP_ID, 
														'%s' AS PRODUCT_SALE_CODE, 
														%f AS STOCK_QTY) AS tmp_tb 
										  LEFT JOIN trn_lpt_chart AS tlc ON tmp_tb.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE; "
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($ddlShopCodeID)
				, mysql_real_escape_string($ddlSalesCodeID)
				, mysql_real_escape_string($ddlProSaleCode)
				, mysql_real_escape_string($txbQty)
				);
			if($result=mysql_query($sql)){
				echo "INSERT SHOP STOCK OK ^.^=".$sql."<br/>";
				return true;
			}else{
				echo "SQL STOCK SHOP Error = ".$sql."<br/>";
				return false;
			}
		}else
		{
			$sql = sprintf("UPDATE stock_shop SET STOCK_QTY=%f , UPDATE_BY='%s', UPDATE_DATE=NOW()
						WHERE SHOP_ID = %d AND SALES_EMP_ID = %d AND PRODUCT_SALE_CODE = '%s'; "
				, mysql_real_escape_string($txbQty)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($ddlShopCodeID)
				, mysql_real_escape_string($ddlSalesCodeID)
				, mysql_real_escape_string($ddlProSaleCode)
				);
			if($result=mysql_query($sql)){
				echo "UPDATE STOCK SHOP OK ^.^ sql =".$sql."<br/>";
				return true;
			}else{
				echo "SQL STOCK SHOP = ".$sql."<br/>";
				return false;
			}
		}
	}
	
	
	//***** Function End ********
	
	
	
	if ($_GET["form"] == "sales_tran_dtl")
	{
		$hdMode = $_POST['hdMode'];
		//*** Data Field ****
		$txbTranNo =$_POST['txbTranNo'];
		$txbTranDate =$_POST['txbTranDate'];
		$ddlSalesCodeID =$_POST['ddlSalesCode'];
		$ddlProSaleCode =$_POST['ddlProSaleCode'];
		$ddlShopCodeID =$_POST['ddlShopCode'];
		$txbSalesIn =$_POST['txbSalesIn'];
		$txbSalesOut =$_POST['txbSalesOut'];
		$txbQty =$_POST['txbQty'];
		
		$query_str="";
		
		if ($hdMode=="new")
		{
			$sql="SELECT CAST( (DATE_FORMAT(Now(),'%y')*1000000) + (SUBSTR(IFNULL(MAX(TRN_SALES_NO),0) ,3,6)+1) AS CHAR) AS NEW_ID
				FROM trn_sales_dtl 
				WHERE TRN_SALES_NO LIKE CONCAT(DATE_FORMAT(Now(),'%y'),'%'); ";
			$result_new_id =  mysql_query($sql);
			
			while($row = mysql_fetch_array($result_new_id)) 
			{
				$txbTranNo = $row['NEW_ID'];
			}
			
			$sql = sprintf("INSERT INTO trn_sales_dtl(
						   TRN_SALES_NO
						  ,SALES_EMP_ID
						  ,PRODUCT_SALE_CODE
						  ,PRODUCT_ID
						  ,SALES_IN
						  ,SALES_OUT
						  ,SHOP_ID
						  ,TRN_SALES_DATE
						  ,CREATED_BY
						  ,CREATED_DATE
						  ,UPDATE_BY
						  ,UPDATE_DATE)
						VALUES (
						   '%s'
						  ,%d  
						  ,'%s' 
						  ,(SELECT PRODUCT_ID FROM trn_lpt_chart WHERE PRODUCT_SALE_CODE = '%s' LIMIT 1)
						  ,%f  
						  ,%f  
						  ,%d
						  ,STR_TO_DATE('%s', '%%d-%%c-%%Y')  
						  ,'%s'  
						  ,NOW()  
						  ,''  
						  ,NOW()  )"
				, mysql_real_escape_string($txbTranNo)
				, mysql_real_escape_string($ddlSalesCodeID)
				, mysql_real_escape_string($ddlProSaleCode)
				, mysql_real_escape_string($ddlProSaleCode)
				, mysql_real_escape_string($txbSalesIn)
				, mysql_real_escape_string($txbSalesOut)
				, mysql_real_escape_string($ddlShopCodeID)
				, mysql_real_escape_string($txbTranDate)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				);
			if($result=mysql_query($sql)){
				
				replaseStockShop($ddlShopCodeID, $ddlSalesCodeID, $ddlProSaleCode,$txbQty);
				if ($query_str == "") $query_str= "?new=1&focus=".$txbTranNo; else $query_str .= "&new=1&focus=".$txbTranNo;
				echo "Insert OK ^.^ <br/>";
				
			}else{
				echo "SQL Error = ".$sql."<br/>";
			}
			
			
		}else if ($hdMode=="edit")
		{
			$sql = sprintf("UPDATE trn_sales_dtl
								SET
								   SALES_EMP_ID = %d -- int(11)
									,PRODUCT_SALE_CODE = '%s' -- varchar(20)
									,PRODUCT_ID = (SELECT PRODUCT_ID FROM trn_lpt_chart WHERE PRODUCT_SALE_CODE = '%s' LIMIT 1)
									,SALES_IN = %f -- decimal(12,2)
									,SALES_OUT = %f -- decimal(12,2)
									,SHOP_ID = %d -- int(11)
									,TRN_SALES_DATE = STR_TO_DATE('%s', '%%d-%%c-%%Y') -- datetime						 
									,UPDATE_BY = '%s' -- varchar(20)
									,UPDATE_DATE = NOW() -- datetime
								WHERE TRN_SALES_NO = '%s' -- bigint(20)",
				mysql_real_escape_string($ddlSalesCodeID),
				mysql_real_escape_string($ddlProSaleCode),
				mysql_real_escape_string($ddlProSaleCode),
				mysql_real_escape_string($txbSalesIn),
				mysql_real_escape_string($txbSalesOut),
				mysql_real_escape_string($ddlShopCodeID),
				mysql_real_escape_string($txbTranDate),
				mysql_real_escape_string($_SESSION['aut_uname']),
				mysql_real_escape_string($txbTranNo)
				);
			
			if($result=mysql_query($sql)){
				echo "Update OK ^.^";
				
				replaseStockShop($ddlShopCodeID, $ddlSalesCodeID, $ddlProSaleCode,$txbQty);
			}else{
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
		

	} else if ($_GET["form"] == "sales_tran_table") 
	{
		$hdDelID = $_POST['hdDelID'];
		$sql = "    SELECT tsd.SHOP_ID, tsd.SALES_EMP_ID, tsd.PRODUCT_SALE_CODE, ss.STOCK_QTY - tsd.SALES_IN + tsd.SALES_OUT AS VAL
						  FROM trn_sales_dtl AS tsd
							LEFT JOIN stock_shop AS ss ON tsd.SHOP_ID = ss.SHOP_ID AND tsd.PRODUCT_SALE_CODE = ss.PRODUCT_SALE_CODE AND tsd.SALES_EMP_ID = ss.SALES_EMP_ID
						  WHERE tsd.SALES_DTL_ID = ".$hdDelID." ;";
		$result = mysql_query($sql);
		 while($row = mysql_fetch_array($result)) 
		 {
			 $ShopCodeID= $row["SHOP_ID"];
			 $SalesCodeID= $row["SALES_EMP_ID"];
			 $ProSaleCode= $row["PRODUCT_SALE_CODE"];
			 $Qty= $row["VAL"];
		}
		if (replaseStockShop($ShopCodeID, $SalesCodeID, $ProSaleCode,$Qty))
		{
			$sql = "DELETE FROM trn_sales_dtl
				WHERE SALES_DTL_ID = ".$hdDelID."; -- bigint(20)";
			
			if($result=mysql_query($sql)){
				echo "Delete OK ^.^ <br/>";
			}else{
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}else{
			echo "replaseStockShop Error";
		}
		
		echo $sql;
		
		
	}
		
	
	header("Location: sales_transaction.php".$query_str); 

	
?>
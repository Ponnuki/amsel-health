	<script type="text/javascript">
	
		
		function showDtlForm(mode, doNo)
		{
			
			document.getElementById("frmDoDtl").style.display= "";
			
			
			document.getElementById("txbDONo").value = "";
			document.getElementById("txbDONo").style.backgroundColor = "#f4f4f4";
			document.getElementById('txbDoDate').value = todayDDMMYYYY();
			
			var elmRsDotype = document.getElementsByName("rdDoType");
			var i;
			for (i = 0; i < elmRsDotype.length; i++) {
				 elmRsDotype[i].checked = false;
			}
			
			document.getElementById("txbCreditTerm").value = "";
			document.getElementById("txbNote").value = "";
			document.getElementById("txbCusName").value = "";
			document.getElementById("txbCusContact").value = "";
			document.getElementById("txbCusAddress").value = "";
			$('#ddlCusDistrict').selectpicker('val','');
			$('#ddlCusProvince').selectpicker('val','');
			document.getElementById("txbCusZip").value = "";
			document.getElementById("txbCusTel").value = "";
			document.getElementById("txbCusFax").value = "";
			document.getElementById("txbTaxNo").value = "";
			
			document.getElementById("txbTotal").value = "";
			document.getElementById("txbDisc").value = "";
			document.getElementById("txbTotalDisc").value = "";
			document.getElementById("txbVat").value = "";
			document.getElementById("txbTotalVat").value = "";
			document.getElementById("txbGrandTotal").value = "";
			
			document.getElementById("divTbResult").style.display= "none";
			document.getElementById("divSearch").style.display= "none";
			document.getElementById("divTbDo").innerHTML = "<table class='table table-bordered table-striped'>	   <thead>	<tr valign='middle'>	<th>  # </th>	<th>Pro. Sale Code</th> <th>Product Name</th>	<th>Description</th>	<th>Unit Price</th>	<th>Qty.</th>		<th>Total Amount</th>		<th>   </th> 	</tr>	</thead>		<tbody>	</tbody>		</table>";
			
			if (mode == "new")
			{
				document.getElementById('hdMode').value = mode;
				document.getElementById('labMode').innerHTML = "<i class='fa fa-plus-square'></i>New Delivery Order";
				getDdlPO();
				$('#ddlCus').selectpicker('val','');
				$('#ddlCus').attr("disabled", false); 
				$('#ddlPO').attr("disabled", false); 
				document.getElementById("btSave").style.display = "none";
				document.getElementById("btSaveNew").style.display = "";
				document.getElementById("btPrintForm").disabled = true;
				document.getElementById("btPrintForm").className = "btn-u btn-u-default";
				
			}else if (mode == "edit")
			{
				document.getElementById('hdMode').value = mode;
				document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit Delivery Order";
				$('#ddlCus').attr("disabled", true); 
				$('#ddlCus').selectpicker('setStyle', 'btn-inverse');
				$('#ddlPO').attr("disabled", true); 
				$('#ddlPO').selectpicker('setStyle', 'btn-inverse');
				//document.getElementById("ddlCus").style.backgroundColor = "#f4f4f4";
				document.getElementById("btSave").style.display = "";
				document.getElementById("btSaveNew").style.display = "none";
				getDoData(doNo);
				document.getElementById("btPrintForm").disabled = false;
				document.getElementById("btPrintForm").className = "btn-u btn-u-green";
			}
			
			
		}
		
		function hideDtlForm()
		{
			document.getElementById("frmDoDtl").style.display= "none";
			document.getElementById("divTbResult").style.display= "";
			document.getElementById("divSearch").style.display= "";
			getDoMain();
			
			
		}
		
		
		function reCalTotal()
		{
			var a_total = 0;
			var amount = 0;
			var elmAmount = document.getElementsByName("txbAmount");
			var i;
			for (i = 0; i < elmAmount.length; i++) {
				 
				amount = parseFloat((elmAmount[i].value).replace(",",""));
				a_total = a_total + amount;
				
			}
			document.getElementById("txbTotal").value = a_total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			
			var discPer = parseFloat(document.getElementById("txbDisc").value);
			var vatPer = parseFloat(document.getElementById("txbVat").value);
			var total = parseFloat(document.getElementById("txbTotal").value .replace(",",""));
			var totalDisc = total*(discPer/100);
			var totalVat = (total - totalDisc)*(vatPer/100);
			var grandTotal = total - totalDisc + totalVat;
			document.getElementById("txbTotalDisc").value = totalDisc.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			document.getElementById("txbTotalVat").value = totalVat.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			document.getElementById("txbGrandTotal").value = grandTotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
		}
		
		function getDdlPO(cus_id)
		{
			if (typeof(cus_id) ==="undefined")  cus_id = "";
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp2.onreadystatechange=function() 
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200) 
				{
					var str = xmlhttp2.responseText.replace("\r\n\t", "");
					document.getElementById("ddlPO").innerHTML = str;
					$('.selectpicker').selectpicker('refresh');
					
				}
			}
			
			xmlhttp2.open("GET","delivery_order_po_aj.php?po_cus_id="+cus_id ,true);
			xmlhttp2.send();
			
		}
		
		
		
		function getDdlDistrict(district_code, province_code)
		{
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp3=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp3=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp3.onreadystatechange=function() 
			{
				if (xmlhttp3.readyState==4 && xmlhttp3.status==200) 
				{
					var str = xmlhttp3.responseText.replace("\r\n\t", "");
					document.getElementById("ddlCusDistrict").innerHTML = str;
					$('.selectpicker').selectpicker('refresh');
					
				}
			}
			xmlhttp3.open("GET","district_get_option.php?pcode="+province_code+"&dcode="+district_code ,true);
			xmlhttp3.send();
		}
		
		function getCustomerDtl(cus_id)
		{
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t﻿﻿﻿﻿﻿﻿﻿﻿", "");
					var res = str.split("|");
					
					document.getElementById("txbCusName").value = res[1];
					document.getElementById('txbCusContact').value = res[2];
					document.getElementById('txbCusAddress').value = res[3];
					$('#ddlCusProvince').selectpicker('val',res[5]);
		
					getDdlDistrict(res[4], res[5]);
					
					document.getElementById('txbCusZip').value = res[6];
					document.getElementById('txbCusTel').value = res[7];
					document.getElementById('txbCusFax').value = res[8];
					document.getElementById('txbTaxNo').value = res[9];
				}
			}
			xmlhttp.open("GET","delivery_order_ajax.php?cus_id="+cus_id ,true);
			xmlhttp.send();
			
		}
		
		
		
		function getNewDoDtl(po_id)
		{
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					
					document.getElementById("divTbDo").innerHTML = str;
					$('#ddlCus').selectpicker('val',document.getElementById("hdCusID").value);
					$('.selectpicker').selectpicker('refresh');
					//getCustomerDtl(document.getElementById("hdCusID").value);
					document.getElementById("txbCusName").value = document.getElementById("hdCusName").value;
					document.getElementById('txbCusContact').value = document.getElementById("hdCusContact").value;
					document.getElementById('txbCusAddress').value = document.getElementById("hdCusAddrss").value;
					$('#ddlCusProvince').selectpicker('val',document.getElementById("hdProvinceCode").value);
		
					getDdlDistrict(document.getElementById("hdDistrictCode").value, document.getElementById("hdProvinceCode").value);
					
					document.getElementById('txbCusZip').value = document.getElementById("hdCusZip").value;
					document.getElementById('txbCusTel').value = document.getElementById("hdCusTel").value;
					document.getElementById('txbCusFax').value = document.getElementById("hdCusFax").value;
					document.getElementById('txbTaxNo').value = document.getElementById("hdTaxNumber").value;
					
					
					total = parseFloat(document.getElementById("hdTotal").value);
					document.getElementById("txbTotal").value = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
					document.getElementById("txbDisc").value = document.getElementById("hdDiscPer").value;
					document.getElementById("txbVat").value = document.getElementById("hdVatPer").value;
					reCalTotal();
				}
			}
			xmlhttp.open("GET","delivery_order_po_aj.php?po_id="+po_id ,true);
			xmlhttp.send();
		}
		
		
		
		function getDoData(doNo)
		{
			posting = false;
		
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp2.onreadystatechange=function() 
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200) 
				{
					
					var str = xmlhttp2.responseText.replace("\r\n\t", "");
					var res = JSON.parse(str);
					
					document.getElementById("ddlPO").innerHTML = "<option value='"+res[0]["PO_ID"]+"' selected> "+res[0]["PO_NO"]+"</option>";
				
					$('#ddlCus').selectpicker('val',res[0]["CUS_ID"]);
					$('#ddlPO').selectpicker('val',res[0]["PO_ID"]);
					
					document.getElementById("hdDoID").value = res[0]["DO_ID"];
					document.getElementById("txbDONo").value = res[0]["DO_NO"];
					document.getElementById('txbDoDate').value  = res[0]["DO_DATE"];
					
					var elmRsDotype = document.getElementsByName("rdDoType");
					var i;
					for (i = 0; i < elmRsDotype.length; i++) {
						if (elmRsDotype[i].value ==  res[0]["DO_TYPE_ID"])
						{
							elmRsDotype[i].checked = true;
						}else
						{
							elmRsDotype[i].checked = false;
						}
					}
					
					document.getElementById("txbCusName").value = res[0]["CUS_NAME"];
					document.getElementById("txbCusContact").value = res[0]["CUS_CONTACT"];
					document.getElementById("txbCreditTerm").value = res[0]["CREDIT_TERM"];
					document.getElementById("txbNote").value = res[0]["NOTE"];
					document.getElementById("txbCusAddress").value = res[0]["CUS_ADDRESS"];
					getDdlDistrict(res[0]["DISTRICT_CODE"], res[0]["PROVINCE_CODE"]);
					
					$('#ddlCusProvince').selectpicker('val',res[0]["PROVINCE_CODE"]);
					document.getElementById("txbCusZip").value = res[0]["CUS_ZIP"];
					document.getElementById("txbCusTel").value = res[0]["CUS_TEL"];
					document.getElementById("txbCusFax").value = res[0]["CUS_FAX"];
					document.getElementById("txbTaxNo").value = res[0]["TAX_NUMBER"];
					document.getElementById("txbDisc").value = res[0]["DISCOUNT_PER"];
					document.getElementById("txbVat").value = res[0]["VAT_PER"];
					
					getDoDtl(res[0]["DO_ID"]);
					
				}
			}
			
			params_dtl  = 'operate=get_do_data';
			if (typeof(doNo) !="undefined")  params_dtl += '&p_do_no=' + doNo;
			
			posting = true;
			
			if (posting)
			{
				xmlhttp2.open('POST','delivery_order_ajax.php',true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params_dtl);
			}
			
		}
		
		function getDoDtl(doID)
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					
					document.getElementById("divTbDo").innerHTML = str;
					total = parseFloat(document.getElementById("hdTotal").value);
					document.getElementById("txbTotal").value = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
					
					reCalTotal();
				}
			}
			params_dtl  = 'operate=get_do_dtl_data';
			if (typeof(doID) !="undefined")  params_dtl += '&p_do_id=' + doID;
			posting = true;
			
			if (posting)
			{
				xmlhttp.open('POST','delivery_order_ajax.php',true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params_dtl);
			}
		}
		
		
		function add_do_dtl(new_do_id, elm_no)
        {
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp2.onreadystatechange=function()
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200)
				{
					//var result_msg="";
					//result_length= xmlhttp2.responseText.length;
					result_msg= xmlhttp2.responseText.replace("\r\n\t", "");
					
					if (result_msg != "")
					{
						alert("Function [add_do_dtl ] Error :" + result_msg+"-"+txt_to_show);
						
					}
					/*else{
						hideDtlForm();
					}*/
				}
			}
			
			mode = document.getElementById('hdMode' ).value
			params_dtl  = 'operate='  + mode +'_do_dtl';
			
			params_dtl += '&p_do_id=' + new_do_id;
			params_dtl += '&p_qno=' + elm_no;
			params_dtl += '&p_pro_id=' + document.getElementsByName("hdProID")[elm_no].value;
			params_dtl += '&p_pro_sale_code=' + document.getElementsByName("txbProSaleCode")[elm_no].value;
			params_dtl += '&p_pro_name=' + document.getElementsByName("divProName")[elm_no].innerHTML;
			params_dtl += '&p_pro_desc=' + document.getElementsByName("divDesc")[elm_no].innerHTML ;
			params_dtl += '&p_unit_price=' + (document.getElementsByName("txbUnitPrice")[elm_no].value).replace(",","");
			params_dtl += '&p_do_qty=' + (document.getElementsByName("txbDoQty")[elm_no].value).replace(",","");
			params_dtl += '&p_amount=' + (document.getElementsByName("txbAmount")[elm_no].value).replace(",","");
			posting = true;
			
			if (posting)
			{
				xmlhttp2.open('POST','delivery_order_ajax.php',true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params_dtl);
			}
			
		}
		
		
		 function add_do_main()
        {
         
			posting = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var str_response = xmlhttp.responseText.replace("\r\n\t", "");
					var res = str_response.split("|");
					
					if (res[0] != '')
					{
						var elmIdDoDtl = document.getElementsByName("hdDoDtlId");
						var i;
						for (i = 0; i < elmIdDoDtl.length; i++) {
							
							add_do_dtl(res[1], i);
							
						}
						
						alert("Add delivery order compleat. DO Number = ["+res[0]+"]");
						document.getElementById("txbDONo").value = res[0];
						document.getElementById("hdDoID").value = res[1];
						
						//*** Change mode Edit ****//
						document.getElementById('hdMode').value = "edit";
						document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit Delivery Order";
						
						document.getElementById("btSave").style.display = "";
						document.getElementById("btSaveNew").style.display = "none";
						//getDoData(doNo);
						document.getElementById("btPrintForm").disabled = false;
						document.getElementById("btPrintForm").className = "btn-u btn-u-green";
						//*** End change
						
						
						//hideDtlForm();
					}
				}
			}
			
			mode = document.getElementById('hdMode' ).value
			
			params  = 'operate='  + mode;
				
			params += '&p_po_id=' + document.getElementById('ddlPO').value;
			params += '&p_cus_id=' + document.getElementById('ddlCus').value;
			params += '&p_do_date='  + document.getElementById('txbDoDate' ).value;
			var elements = document.getElementsByName('rdDoType');
			for (i=0;i<elements.length;i++) {
			  if(elements[i].checked) {
				params += '&p_do_type_id='  + elements[i].value;
			  }
			}
			params += '&p_credit_term=' + document.getElementById('txbCreditTerm' ).value;
			params += '&p_note='  + document.getElementById('txbNote' ).value;
			params += '&p_cus_name='  + document.getElementById('txbCusName' ).value;
			params += '&p_cus_contact='   + document.getElementById('txbCusContact'  ).value;
			params += '&p_cus_address=' + document.getElementById('txbCusAddress').value;
			params += '&p_district_code='   + document.getElementById('ddlCusDistrict'  ).value;
			params += '&p_province_code=' + document.getElementById('ddlCusProvince').value;
			params += '&p_cus_zip=' + document.getElementById('txbCusZip').value;
			params += '&p_cus_tel=' + document.getElementById('txbCusTel').value;
			params += '&p_cus_fax=' + document.getElementById('txbCusFax').value;
			params += '&p_tax_number=' + document.getElementById('txbTaxNo').value;
			params += '&p_do_total=' + (document.getElementById('txbTotal').value).replace(",","");
			
			params += '&p_discount_per=' + document.getElementById('txbDisc').value;
			params += '&p_vat_per=' + document.getElementById('txbVat').value;
			params += '&p_grand_total=' + (document.getElementById('txbGrandTotal').value).replace(",","");
			posting = true;
			
			if (posting)
			{
				xmlhttp.open('POST','delivery_order_ajax.php',true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
            }
			
        } 
		
		function update_do_dtl(elm_no)
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp2.onreadystatechange=function()
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200)
				{
					result_msg= xmlhttp2.responseText.replace("\r\n\t", "");
					
					if (result_msg != "")
					{
						alert("Function [update_do_dtl ] Error :" + result_msg);
						
					}
				}
			}
			
			mode = document.getElementById('hdMode' ).value
			params_dtl  = 'operate='  + mode +'_do_dtl';
			params_dtl += '&p_do_dtl_id=' + document.getElementsByName("hdDoDtlId")[elm_no].value;
			params_dtl += '&p_qno=' + elm_no;
			
			params_dtl += '&p_do_qty=' + (document.getElementsByName("txbDoQty")[elm_no].value).replace(",","");
			params_dtl += '&p_amount=' + (document.getElementsByName("txbAmount")[elm_no].value).replace(",","");
			posting = true;
			
			if (posting)
			{
				xmlhttp2.open('POST','delivery_order_ajax.php',true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params_dtl);
			}
		}
		
		function update_do_main()
		{
			posting = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var str_response = xmlhttp.responseText;
					var res = str_response.split("|");
					
					if (res[0] != '')
					{
						var elmIdDoDtl = document.getElementsByName("hdDoDtlId");
						var i;
						for (i = 0; i < elmIdDoDtl.length; i++) {
							
							update_do_dtl(i);
							
						}
						
						alert("Update delivery order compleat. DO Number = ["+res[0]+"]");
						
						//hideDtlForm();
					}
				}
			}
			
			mode = document.getElementById('hdMode' ).value
			
			params  = 'operate='  + mode;
			params += '&p_do_id=' + document.getElementById('hdDoID').value;
			params += '&p_do_no=' + document.getElementById('txbDONo').value;
			params += '&p_po_id=' + document.getElementById('ddlPO').value;
			params += '&p_cus_id=' + document.getElementById('ddlCus').value;
			params += '&p_do_date='  + document.getElementById('txbDoDate' ).value;
			var elements = document.getElementsByName('rdDoType');
			for (i=0;i<elements.length;i++) {
			  if(elements[i].checked) {
				params += '&p_do_type_id='  + elements[i].value;
			  }
			}
			params += '&p_credit_term=' + document.getElementById('txbCreditTerm' ).value;
			params += '&p_note='  + document.getElementById('txbNote' ).value;
			params += '&p_cus_name='  + document.getElementById('txbCusName' ).value;
			params += '&p_cus_contact='   + document.getElementById('txbCusContact'  ).value;
			params += '&p_cus_address=' + document.getElementById('txbCusAddress').value;
			params += '&p_district_code='   + document.getElementById('ddlCusDistrict'  ).value;
			params += '&p_province_code=' + document.getElementById('ddlCusProvince').value;
			params += '&p_cus_zip=' + document.getElementById('txbCusZip').value;
			params += '&p_cus_tel=' + document.getElementById('txbCusTel').value;
			params += '&p_cus_fax=' + document.getElementById('txbCusFax').value;
			params += '&p_tax_number=' + document.getElementById('txbTaxNo').value;
			params += '&p_do_total=' + (document.getElementById('txbTotal').value).replace(",","");
			
			params += '&p_discount_per=' + document.getElementById('txbDisc').value;
			params += '&p_vat_per=' + document.getElementById('txbVat').value;
			params += '&p_grand_total=' + (document.getElementById('txbGrandTotal').value).replace(",","");
			posting = true;
			
			if (posting)
			{
				xmlhttp.open('POST','delivery_order_ajax.php',true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
            }
		}
		
		function reCalDtl(elm_no)
		{
			
			var unit_price = "";
			var do_qty = "";
			unit_price =  (document.getElementsByName("txbUnitPrice")[elm_no].value).replace(",","");
			do_qty = (document.getElementsByName("txbDoQty")[elm_no].value).replace(",","");
			//alert((unit_price*do_qty).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
			if (unit_price != "" || do_qty!="")
			{
				document.getElementsByName("txbAmount")[elm_no].value = (unit_price*do_qty).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			}else
			{
				document.getElementsByName("txbDoQty")[elm_no].value = "0";
				document.getElementsByName("txbAmount")[elm_no].value = "0";
			}
			
			reCalTotal();
			
			//document.getElementById("txbTotal").value = a_total;
		}
		
		function confirmRemove(elm_no)
		{
			if( document.getElementsByName("trRow").length > 1)
			{
				if (confirm("Are you sure you want to remove this item."))
				{
					
					document.getElementsByName("trRow")[elm_no].remove();
					
					reCalTotal();
				}
			}else
			{
				alert("Must contain at least one item.");
			}
		 
		}
		
		function delDoDtl(doDtlId, em_no)
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp2.onreadystatechange=function()
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200)
				{
					result_msg= xmlhttp2.responseText.replace("\r\n\t", "");
					//alert ("delete result = " + result_msg);
					if (result_msg == "")
					{
						document.getElementsByName("trRow")[em_no].remove();
						
					}else
					{
						alert("Function [delDoDtl] Error :" + result_msg);
					}
				}
			}
			
			mode = document.getElementById('hdMode' ).value
			params_dtl  = 'operate=del_do_dtl';
			params_dtl += '&p_do_dtl_id=' + doDtlId;
			
			posting = true;
			
			if (posting)
			{
				xmlhttp2.open('POST','delivery_order_ajax.php',true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params_dtl);
			}
		}
		
		function confirmDtlDel(elm_no)
		{
			if( document.getElementsByName("trRow").length > 1)
			{
				if (confirm("Are you sure you want to delete this item."))
				{
					if(document.getElementById("hdMode").value == "edit")
					{
						delDoDtl(document.getElementsByName("hdDoDtlId")[elm_no].value, elm_no);
						
					}
					
					reCalTotal();
				}
			}else
			{
				alert("Must contain at least one item.");
			}
		 
		}
		
		function printForm()
		{
			//document.getElementById("hdDoID").value ;
			
			window.open("delivery_order_report.php?do_id="+ document.getElementById("hdDoID").value +"&media=PDF","_blank");
		}
    </script>
	
	<form method="POST" enctype="multipart/form-data" name="frmAutDtl" 
		id="frmDoDtl"  data-toggle="validator" role="form" onsubmit="return validate_form(this);" style="display:none;" >
		<input type="hidden" name="hdMode" id="hdMode"  value="">
	
		<div id="do_dtl_form" name = "do_dtl_form" >
			
			<div class="container content" style="padding:10px">
				
				<!-- <div id="divFormBody" class="modal-content"> -->
				
				<div class="modal-header">
					<h4 class="modal-title" id="labMode"><i class="fa fa-plus-square"></i>New Delivery Order</h4>
				</div>
					<div class="modal-body sky-form"  style="border-style:none">
						
						<!-- Purchase Order Referent-->
						<div class="panel panel-grey margin-bottom-5 padding: 15px">
							<header>Purchase Order Referent</header>
							<div class="row" style="padding-top: 17px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
								
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-3 control-label" style="text-align:right">Customer Code / Name <font color="#ff0000">*</font></label>
									
									<div class="col-lg-3">
										<label class="select" >
											<select name="ddlCus" id="ddlCus" class="selectpicker form-control" 
											data-live-search="true" title="Please select ..." onchange="getDdlPO(this.value);">
												<option value="" selected> - Please Select - </option>
												
												<?php
													foreach ($ddl_customer as $row)
													{
														echo "<option value='".$row["CUS_ID"]."' > ".$row["CUS_NAME"]."</option>";
													}
												
												?>
												
											</select>
											
										</label>
									</div>
									
									<label class="col-lg-3 control-label" style="text-align:right">PO Number <font color="#ff0000">*</font></label>
									
									<div class="col-lg-3">
										<label class="select" >
											<select name="ddlPO" id="ddlPO" class="selectpicker form-control" 
											data-live-search="true" title="Please select ..." onchange="getNewDoDtl(this.value);">
												<option value="" selected> - Please Select - </option>
												
												<?php
													foreach ($ddl_po as $row)
													{
														echo "<option value='".$row["PO_ID"]."' > ".$row["PO_NO"]."</option>";
													}
												
												?>
												
											</select>
											
										</label>
									</div>
									
								</div>
							</div>
						
							
						</div><!-- End Product panal -->
						<div class="margin-bottom-20"></div>
						<!-- Delivery Order Referent-->
						<div class="panel panel-grey margin-bottom-5 padding: 15px">
							<header>Delivery Order Information</header>
							
						
							<div class="row" style="padding-top: 17px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									<label class="col-lg-3 control-label" style="text-align:right">DO Number <font color="#ff0000">*</font></label>
									<div class="col-lg-3">
										<label class="input">
											<input type="hidden" name="hdDoID" id="hdDoID"  value="">
											<input type="text" class="form-control" name="txbDONo" id="txbDONo" placeholder="New DO Number" autocomplete="off" readOnly>
										</label>
									
									</div>
									
									<label class="col-lg-3 control-label" style="text-align:right">DO Date<font color=#ff0000>*</font></label>
									<div class="col-lg-3">
										<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" class="calendar" name="txbDoDate" id="txbDoDate" required>
											</label>
									</div>
										
									
								
								</div>
							</div>
							
							<div class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-3 control-label" style="text-align:right">DO Type<font color=#ff0000>*</font></label>
									<div class="col-lg-3">
										<div class="inline-group">
											<label class="radio">
												<input type="radio"  id="rdConsignment" name="rdDoType" value="1">
												<i class="rounded-x"></i>	Consignment
											</label>
											<label class="radio">
												<input type="radio"  id="rdCredit" name="rdDoType" value="2">
												<i class="rounded-x"></i>	Credit
											</label>
										</div>	
									</div>
									<label class="col-lg-3 control-label" style="text-align:right">Credit Term<font color=#ff0000>*</font></label>
									<div class="col-lg-2">
										<label class="input">
											<input type="input" class="form-control" name="txbCreditTerm" id="txbCreditTerm" placeholder="" autocomplete="off" >
										</label>
									</div>
									<div class="col-lg-1" style="float:left;padding-left:0px;">
										<label class="control-label" >
											Days
										</label>
									</div>
								</div>
							</div>
							
							
							<div class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-3 control-label" style="text-align:right">Note</label>
									<div class="col-lg-9">
										<label class="input">
											
											<input type="input" class="form-control" name="txbNote" id="txbNote" placeholder="" autocomplete="off" >
										</label>
									</div>
									
								</div>
							</div>
							
							
							
							
						</div><!-- End panal -->
						<div class="margin-bottom-20"></div>
						
						<!-- Customer Info.-->
						<div class="panel panel-grey margin-bottom-5 padding: 15px">
							<header>Customer Information</header>
							
							<div class="row" style="padding-top: 17px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-3 control-label" style="text-align:right">Customer Name<font color=#ff0000>*</font></label>
									<div class="col-lg-4">
										<label class="input">
											<input type="input" class="form-control" name="txbCusName" id="txbCusName" placeholder="" autocomplete="off" >
										</label>
									</div>
									
									<label class="col-lg-2 control-label" style="text-align:right">Contact Person</label>
									<div class="col-lg-3">
										<label class="input">
											<input type="input" class="form-control" name="txbCusContact" id="txbCusContact" placeholder="" autocomplete="off" >
										</label>
									</div>
									
								</div>
							</div>
							
							<div class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-3 control-label" style="text-align:right">Address<font color=#ff0000>*</font></label>
									<div class="col-lg-9">
										<label class="input">
											<input type="input" class="form-control" name="txbCusAddress" id="txbCusAddress" placeholder="" autocomplete="off" >
										</label>
									</div>
									
								</div>
							</div>
							
							<div class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-3 control-label" style="text-align:right">District<font color=#ff0000>*</font></label>
									<div class="col-lg-3">
										<label class="select" >
											<select name="ddlCusDistrict" id="ddlCusDistrict" class="selectpicker form-control" 
											data-live-search="true" title="Please select ..." onchange="">
												<option value="" selected> - Please Select - </option>
												
											</select>
											
										</label>
									</div>
									<label class="col-lg-3 control-label" style="text-align:right">Province<font color=#ff0000>*</font></label>
									<div class="col-lg-3">
										<label class="select" >
											<select name="ddlCusProvince" id="ddlCusProvince" class="selectpicker form-control" 
											data-live-search="true" title="Please select ..." onchange="getDdlDistrict('',this.value);">
												<option value="" selected> - Please Select - </option>
												<?php
													foreach ($ddl_province as $row)
													{
														echo "<option value='".$row["PROVINCE_CODE"]."' > ".$row["PROVINCE_NAME"]."</option>";
													}
												?>
											</select>
											
										</label>
									</div>
									
								</div>
							</div>
							
							<div class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-3 control-label" style="text-align:right">ZIP Code</label>
									<div class="col-lg-3">
										<label class="input">
											<input type="input" class="form-control" name="txbCusZip" id="txbCusZip" placeholder="" autocomplete="off" >
										</label>
									</div>
									
									<label class="col-lg-3 control-label" style="text-align:right">Tel.<font color=#ff0000>*</font></label>
									<div class="col-lg-3">
										<label class="input">
											<input type="input" class="form-control" name="txbCusTel" id="txbCusTel" placeholder="" autocomplete="off" >
										</label>
									</div>
									
								</div>
							</div>
							
							<div class="row" style="padding-bottom:15px; padding-right:20px;padding-left:20px;">
								<div class="form-group" style="vertical-align:bottom;">
									
									<label class="col-lg-3 control-label" style="text-align:right">FAX</label>
									<div class="col-lg-3">
										<label class="input">
											<input type="input" class="form-control" name="txbCusFax" id="txbCusFax" placeholder="" autocomplete="off" >
										</label>
									</div>
									<label class="col-lg-3 control-label" style="text-align:right">TAX ID</label>
									<div class="col-lg-3">
										<label class="input">
											<input type="input" class="form-control" name="txbTaxNo" id="txbTaxNo" placeholder="" autocomplete="off" >
										</label>
									</div>
									
								</div>
							</div>
							
						</div><!-- End panal -->
						<div class="margin-bottom-20"></div>
						
						<!-- DO Detail-->
						<!-- <div class="panel panel-grey margin-bottom-5 padding: 15px"> -->
						<header>Delivery Order Detail</header>
						
						<div id="divTbDo" class="row" style="padding-top: 17px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
							<table class="table table-bordered table-striped">
								<thead>
									<tr valign="middle">
										<th>  # </th>
										<th>Pro. Sale Code</th>
										<th>Product Name</th>
										<th>Description</th>
										<th>Unit Price</th>
										<th>Qty.</th>
										<th>Total Amount</th>
										<th>   </th>
									</tr>
								</thead>
								<tbody>
									
									
								</tbody>
							</table>
						</div>
						
						<div class="row" style="padding-top: 17px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
							<div class="form-group" style="vertical-align:bottom;">
								<div class="col-lg-7 control-label" style="text-align:right"> </div>
								<label class="col-lg-2 control-label" style="text-align:right">Total </label>
								<div class="col-lg-3">
									<input type="input" class="form-control" name="txbTotal" id="txbTotal" placeholder="" autocomplete="off" readOnly >
								</div>
								
							</div>
						</div>
						
						<div class="row" style="padding-top: 0px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
							<div class="form-group" style="vertical-align:bottom;">
								<div class="col-lg-7 control-label" style="text-align:right"> </div>
								<label class="col-lg-2 control-label" style="text-align:right; align:right">
									<table width="100%">
										<tr>
											<td>Discount </td>
											<td style="width:55px;padding-right:10px;padding-left:10px;" > 
												<input type="input" class="form-control" id="txbDisc" style="width:55px"> 
											</td> 
											<td style="width:15px;"> % </td>
										</tr>
									</table>
								</label>
								<div class="col-lg-3">
									<input type="input" class="form-control" name="txbTotalDisc" id="txbTotalDisc" placeholder="" autocomplete="off" readOnly>
								</div>
								
							</div>
						</div>
						
						<div class="row" style="padding-top: 0px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
							<div class="form-group" style="vertical-align:bottom;">
								<div class="col-lg-7 control-label" style="text-align:right"> </div>
								<label class="col-lg-2 control-label" style="text-align:right; align:right">
									<table width="100%">
										<tr>
											<td style="text-align:right">VAT </td>
											<td style="width:55px;padding-right:10px;padding-left:10px;" > 
												<input type="input" class="form-control" id="txbVat" style="width:55px"> 
											</td> 
											<td style="width:15px;"> % </td>
										</tr>
									</table>
								</label>
								<div class="col-lg-3">
									<input type="input" class="form-control" name="txbTotalVat" id="txbTotalVat" placeholder="" autocomplete="off"  readOnly>
								</div>
								
							</div>
						</div>
						
						<div class="row" style="padding-top: 0px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
							<div class="form-group" style="vertical-align:bottom;">
								<div class="col-lg-7 control-label" style="text-align:right"> </div>
								<label class="col-lg-2 control-label">
									<table width="100%" >
										<tr style="text-align:right">
											<td>Grand Total </td>
											
										</tr>
									</table>
								</label>
								
								<div class="col-lg-3">
									<input type="input" class="form-control" name="txbGrandTotal" id="txbGrandTotal" placeholder="" autocomplete="off" readOnly>
								</div>
								
							</div>
						</div>
						
						
						<!-- End panal -->
						
						<label id="labError" class="error"></label>
						
					</div><!-- End "modal-body" -->
					<div class="modal-footer">
						<button type="button" onclick="hideDtlForm();" name="btBack" id="btBack" class="btn-u btn-u-dark" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Back</button>
						<button type="button" onclick="printForm();" name="btPrintForm" id="btPrintForm" class="btn-u btn-u-default" data-dismiss="modal"
							disabled	>
							<i class="fa fa-print"></i> Print this Delivery Order
						</button>
						<button type="button" onclick="add_do_main();" class="btn-u btn-u-primary" name="btSaveNew" id="btSaveNew" ><i class="glyphicon glyphicon-floppy-save"></i> Save New</button>
						<button type="button" onclick="update_do_main();"  class="btn-u btn-u-primary" name="btSave" id="btSave"><i class="glyphicon glyphicon-floppy-save"></i> Save</button>
						<button type="submit" class="btn-u btn-u-primary" name="btSubmit" id="btSubmit" style="display:none">Submit</button>
					</div>
					
				<!-- </div> END modal-body -->
			</div>
		</div>

	</form>
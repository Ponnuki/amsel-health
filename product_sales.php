﻿<?php 
	
	require("include_function.php");
	require("connect.php");
	require('validatelogin.php'); 
	//echo "session =".$_SESSION['aut_uname'];
 ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | PRODUCT SALES</title>

	<?php $current_menu = "product_sales"; ?>
	<?php require("include_headtag.php"); ?>
    
</head> 

<body>    

<div  id="wrap" class="wrapper">

	<?php require("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">PRODUCT SALES</h1>
			
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
	<script type="text/javascript">
    <!--
	function confirmDel(DelID)
	{
		if (confirm("Are you sure you want to delete this item."))
		{
			document.getElementById('hdDelID').value = DelID;
			document.getElementById('hdCancelID').value  = "";
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 function confirmCancel(CancelID)
	{
		if (confirm("Are you sure you want to cancel this item."))
		{
			document.getElementById('hdDelID').value = "";
			document.getElementById('hdCancelID').value = CancelID;
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
		<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		<?php 
		
			// **** Gen Product sales code
			$sql = "SELECT tlc.LTP_CHART_ID, tlc.PRODUCT_SALE_CODE, concat(tlc.PRODUCT_SALE_CODE,  ' : ', mp.PRODUCT_NAME_TH) as PCODE_PNAME
						FROM mst_product_sale AS tlc
						  LEFT JOIN mst_product AS mp ON tlc.PRODUCT_ID = mp.PRODUCT_ID 
						ORDER BY PRODUCT_SALE_CODE; ";
			$result_pro_sale_code =  mysql_query($sql);
			
	
			
			// **** Gen Product sales code Table
			$sql = "SELECT tlc.LTP_CHART_ID, PRODUCT_SALE_CODE, tlc.PRODUCT_ID, mp.PRODUCT_NAME_TH, tlc.LTP,
							tlc.STICKER_PRICE, tlc.CHEER_RATE, tlc.LTP_FOR_COMM, tlc.CANCEL_FLAG,
							IFNULL((SELECT tsd.TRN_SALES_NO FROM trn_sales_dtl AS tsd WHERE tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE LIMIT 1), 1) as IS_DEL
						FROM mst_product_sale AS tlc 
							LEFT JOIN mst_product AS mp ON tlc.PRODUCT_ID = mp.PRODUCT_ID";
			
			$where = "";
			
			
			if ($_POST['ddlProSalesCodeSearch']!="")
			{
				if ($where == "") $where .= "WHERE "; else $where .=" AND ";
				$where .= " tsd.PRODUCT_SALE_CODE = '".
					mysql_real_escape_string($_POST['ddlProSalesCodeSearch'])
					."' ";
			}
			
			$order = "	ORDER BY PRODUCT_SALE_CODE ; ";
			$sql = $sql.$where.$order;
			$result_lpt_chart = mysql_query($sql);

			?>
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_condtion" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Product Sales Code / Product Name :</label>
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
												<select name="ddlProSalesCodeSearch" id="ddlProSalesCodeSearch" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
														<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_pro_sale_code)) 
													{
														echo "<option value='".$row['PRODUCT_SALE_CODE']."' ";
														if ($row['PRODUCT_SALE_CODE'] == $_POST['ddlProSaleCodeSearch'])
														{
															echo " selected ";
														}
														echo ">".$row['PCODE_PNAME']."</option>";
													}
													mysql_data_seek ($result_pro_sale_code , 0 );
													?>
												</select>
												
										</label>
									</div>
								</div>
									
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button type="submit" class="btn-u"> Search </button> &nbsp;&nbsp;
										<button type="button" class="btn-u btn-u-default">Clear</button>
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

	




    <!--=== End Search Block Version 2 ===-->

    <div class="container content-sm"  style="padding:30px;">


		<div class="col-lg-offset col-lg-8" style="height:50px">
			<button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#product_sales_dtl_form" 
				onclick="ClearForm();"> <i class="fa fa-plus-square icon-color-white"></i> New Product Sales </button> 
		
			<?php  require("product_sales_dtl_form.php");  ?>
		</div>

		
		<div class="table-search-v2 margin-bottom-30">
			<div class="table-responsive">
				<form method="POST" enctype="multipart/form-data" action="product_sales_model.php?form=frm_sales_tran_tb" name="frmSalesTranTb" onSubmit="return validate_form(this)"
					id="frmSalesTranTb" >
					<input type="hidden" name="hdDelID" id="hdDelID"  value="">
					<input type="hidden" name="hdCancelID" id="hdCancelID"  value="">
					<table class="table table-bordered table-striped">
						<thead>
							<tr valign="middle">
								<th> &nbsp; </th>
								<th>Pro. Sales Code</th>
								<th class="hidden-sm">Product name</th>
								<th>LTP.</th>
								<th>Sticker Price</th>
								<th>Cheer Rate</th>
								<th>LTP. for Commission</th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<?php 
								while($row=mysql_fetch_array($result_lpt_chart)) 
								{
									if($row["PRODUCT_SALE_CODE"] == $_REQUEST['focus'])
									{
										echo '<tr style = "background-color: #ffffbb">';
										echo '<script> history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'" ); </script>';
									}else
									{
										echo '<tr>';
									}
									
									echo '<td width="78px">
												<ul class="list-inline table-buttons">
													<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#product_sales_dtl_form" onclick="getDataAjax('.$row["LTP_CHART_ID"].')"><i class="fa fa-edit"></i> Edit</button></li>
												</ul>
											</td>';
									echo '<td width="120px"> <p>'.$row["PRODUCT_SALE_CODE"].'</p> </td>';
									echo '<td> <p>'.$row["PRODUCT_NAME_TH"].'</p> </td>';
									echo '<td width="102px"> <p>'.$row["LTP"].'</p> </td>';
									echo '<td width="100px">'.$row["STICKER_PRICE"].'</td>';
									echo '<td width="100px"> <p>'.$row["CHEER_RATE"].'</p> </td>';
									echo '<td width="100px"> <p>'.$row["LPT_FOR_COMM"].'</p> </td>';
									
									echo '<td  width="84px">
													<ul class="list-inline table-buttons">
														<li>';
									if ($row["IS_DEL"] == 1)
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["LTP_CHART_ID"] .')"><i class="fa fa-trash-o"></i> Delete</button></li>';
									}else if ($row["CANCEL_FLAG"] == 'N')
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-orange" onclick ="return confirmCancel('.$row["LTP_CHART_ID"] .')"> 
															<i class="fa fa-times-circle"></i> Cancel</button>';
									} else 
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-green" onclick ="return confirmCancel('.$row["LTP_CHART_ID"] .')"> 
															<i class="fa fa-check-circle"></i> Enable</button>';
									}
									echo '		</ul>
												</td>
											</tr>';
								}
							?>
						
						</tbody>
					</table>
				</form>
				
				
				<!-- End Test Form -->
			</div>    
		</div>    
        <!-- End Table Search v2 -->
		


        
    </div>
	<?php require("include_footer.php"); ?>
     
</div><!--/End Wrapepr-->

<?php require("include_js.php"); ?>

</body>
</html> 
<?php session_start();

/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    $arr_prd_pc = array();

    require_once('config.php');
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');
    require_once('class_amh_customer.php');

    $the_date = date('Y-m-d');

    $amh_pc       = new AMH_PC();
    $amh_customer = new AMH_Customer();
/*
    $cus_id = $amh_pc->get_cus_id($user_name);
*/
    $aut_id = $amh_pc->get_aut_user_id($user_name);

    $arr_cus_opt = $amh_customer->get_customer_pc_option($aut_id, $_REQUEST["cus_id"]);

    $type_id  = $_REQUEST["type_id"];

    if ($type_id == "") { $type_id = 1; }

    $cus_type_id = $amh_customer->get_customer_type($arr_cus_opt["cus_id"]);

    $arr_prd_pc = $amh_pc->get_product_pc($the_date, $type_id, $cus_type_id);

    $max_items  = count($arr_prd_pc);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Amsel Health - SALE REPORT ADD</title>
<link rel="stylesheet" href="stylesheets/amh_pc.css?v=2">
<link href='https://fonts.googleapis.com/css?family=Exo+2:700,400' rel='stylesheet' type='text/css'>
<style type='text/css'>
body
{
    background-color: #000;
    margin: 0;
    padding: 10px;
}

.link_white
{
    color:#FFF;
    text-decoration: none;
}

.link_white:hover
{
    color:#FF9;
}

.main_prd_selection
{
    background-color: #fff;
    border-radius: 2px;
    margin: 10px;
    text-align: center;
}

.main_prd_selection img
{
    vertical-align: middle;
}

.plus_item
{
    background-color: rgba(240,240,240,0.6);
    bottom: 0; left: 0;
    color: #fff;
    cursor: pointer;
    font-family: Impact;
    position: fixed;
    text-align: center;
    padding: 10px;
    width: 100%;
}

#popup_panel_body
{
    background-color: #fff;
    border: 1px solid #000;
    border-radius: 3px;
    color: #000;
    display: block;
    font-family: 'Arial';
    font-size: 30pt;
    font-weight: bold;
    left: 200px;
    padding: 15px;
    position: absolute;
    top: 200px;
    z-index: 5500;
}

#popup_panel_shadow
{
    background-color: #333;
    border-radius: 5px;
    display: block;
    height: 100%;
    left: 0;
    opacity: 0.5;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 5000;
}

.row_item
{
    background-color: #fff;
    border-radius: 2px;
    margin: 10px 0;
    min-height: 100px;
    padding: 10px;
}

.row_item .i_object
{
    background-color: rgba(240,240,240,0.6);
    border: 1px solid #ccc;
    display: inline-block;
    height: 30px;
    padding: 3px 5px;
    vertical-align: top;
}

.the_select
{
    font-size: 36pt;
    margin: 20px 10px;
    padding: 10px 20px;
}
</style>
<script type="text/javascript" src="incl/jquery_ui/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
var prd_pc_id      = [];
var prd_name_th    = [];
var prd_name_en    = [];
var the_img        = [];
var the_max        = [];

var img_index      = 1;
var img_max        = <? echo $max_items; ?>;
var the_group_num  = 1;

<?php
$int_cnt = 0;
foreach ($arr_prd_pc as $prd_pc)
{
    $int_cnt++;
    echo "the_img[".$int_cnt."]     = new Image();\r\n";
    echo "the_img[".$int_cnt."].src = '".$pro_img_path.$prd_pc["PRD_IMG"]."';\r\n";
    echo "prd_pc_id[".$int_cnt."]   = '".$prd_pc["PRODUCT_PC_ID"]  ."';\r\n";
    echo "prd_name_th[".$int_cnt."] = '".$prd_pc["PRODUCT_NAME_TH"]."-".$prd_pc["CUS_TYPE_NAME"]."';\r\n";
    echo "prd_name_en[".$int_cnt."] = '".$prd_pc["PRODUCT_NAME_EN"]."-".$prd_pc["CUS_TYPE_NAME"]."';\r\n";
    echo "\r\n";
}
?>

function page_start()
{
    $("#popup_panel_shadow").css('display','none');
    $("#popup_panel_body"  ).css('display','none');

    upd_image();
}

function upd_image()
{
    document.getElementById('prd_image').src = the_img[img_index].src;

    the_text  = '<div>'+prd_name_en[img_index]+'</div>\r\n';
    the_text += '<div>'+prd_name_th[img_index]+'</div>\r\n';

    document.getElementById('text_info').innerHTML = the_text;

    if (img_index == 1)
    {
        $('#btn_back').css('display','none');
        $('#btn_next').css('display','inline-block');
    }

    if ((img_index > 1) && (img_index < img_max))
    {
        $('#btn_back').css('display','inline-block');
        $('#btn_next').css('display','inline-block');
    }

    if (img_index == img_max)
    {
        $('#btn_back').css('display','inline-block');
        $('#btn_next').css('display','none');
    }
}

function prd_load(the_direction)
{
    if (the_direction == 'prev')
    {
        img_index--;
        if (img_index < 1) { img_index = 1; }
    }

    if (the_direction == 'next')
    {
        img_index++;
        if (img_index > img_max) { img_index = img_max; }
    }

    //document.getElementById('QTY').value = img_index;

    upd_image();
}

function calc_number(inc_dec)
{
    qty_value = document.getElementById('QTY').value;
    if (inc_dec == 'dec') { qty_value--; if (qty_value <= 0) { qty_value = 0; } }
    if (inc_dec == 'inc') { qty_value++; }
    document.getElementById('QTY').value = qty_value;
}

function add_report()
{
    the_qty  = document.getElementById('QTY').value;
    cus_id   = document.getElementById('cus_id').value;

    if ((the_qty > 0) && (cus_id != ''))
    {
        the_text = 'เพิ่มรายงานการขาย '+prd_name_th[img_index]+'-('+prd_pc_id[img_index]+')\n'+'จำนวน '+the_qty;
        if (confirm(the_text))
        {
            $.post( "pc_sale_rpt_upd.php", { action: "add", prd_pc_id: prd_pc_id[img_index], qty: the_qty, cus_id: cus_id })
                .done(function(data)
                {
                    alert('เพิ่ม '+prd_name_th[img_index]+' ในรายงาน  report เรียบร้อยแล้ว');
                    //window.location = 'pc_sale_report.php';
                });
        }
    }
    else
    {
        show_popup('ไม่สามารถส่ง report<br>กรุณาเลือกร้านและระบุจำนวน<br>โปรดตรวจสอบข้อมูลก่อนโพส');
    }
}

function prd_swap_group(type_id)
{
    this_obj = document.getElementById("frm_swap_group");
    document.getElementById("type_id").value = type_id;
    this_obj.submit();
}

function show_popup(the_msg)
{
    document.body.style.overflow = "hidden";
    
    obj_shadow = document.getElementById('popup_panel_shadow');
    obj_body   = document.getElementById('popup_panel_body'  );
    obj_body.innerHTML  = the_msg;
    
    $("#popup_panel_shadow").fadeIn(1000);
    $("#popup_panel_body"  ).fadeIn(1000);
    
    int_left = (obj_shadow.offsetWidth / 2) - (obj_body.offsetWidth / 2);
    obj_body.style.left = int_left + 'px';
}

function hide_popup()
{
    document.body.style.overflow = "visible";
    $("#popup_panel_shadow").fadeOut(500);
    $("#popup_panel_body"  ).fadeOut(500);
    $("#popup_panel_body"  ).html('');
}

function choose_pc_id(cus_id)
{
    window.location = 'pc_sale_report_add.php?type_id=<?=$_REQUEST["type_id"];?>&cus_id='+cus_id;
}
</script>
</head>
<body onload='page_start();'>
<div class='the_page'>
<div class='the_page_label'>
SALE REPORT
<div><select class="the_select" id="cus_id" name="cus_id" data-live-search="true" onchange="choose_pc_id(this.value);"><?=$arr_cus_opt["option"];?></select></div>
</div>
<table class='the_table' cellspacing='5'>
<tr>
<td><button onclick='prd_swap_group(1);'>เม็ด</button></td>
<td><button onclick='prd_swap_group(2);'>ผง</button></td>
<td><button onclick='prd_swap_group(3);'>เซ็ต</button></td>
</tr>
</table>
<table class='the_table_prd' cellspacing='5'>
<tr>
<td style='width:65px;'><img src='<? echo $app_img_path; ?>icon-goback-256.png' border='0' onclick='prd_load("prev");' id='btn_back'></td>
<td><div class='main_prd_selection'><img src='' id='prd_image'></div></td>
<td style='width:65px;'><img src='<? echo $app_img_path; ?>icon-gonext-256.png' border='0' onclick='prd_load("next");' id='btn_next'></td>
</tr>
</table>
<div style='font-size:40px; color:#fff; text-align:center;' id='text_info'></div>
<div style='min-height:120px;'>&nbsp;</div>
<div class='plus_item fnt36'>
    <div style='display:inline-block; margin-right:60px;'><a href='pc_sale_report.php' class='link_white'><img src='<? echo $app_img_path; ?>icon-navg-back100.png' border='0'>กลับ</a></div>
    <div style='display:inline-block; margin-right:60px;'>
        <img src='<? echo $app_img_path; ?>icon_left_100.png' border='0' onclick='calc_number("dec");' class='img_btn'>
        <input type='TEXT' class='the_input_text fnt44' id='QTY' value='0'>
        <img src='<? echo $app_img_path; ?>icon_right_100.png' border='0' onclick='calc_number("inc");' class='img_btn'>
    </div>
    <div style='display:inline-block;' onclick='add_report();'><img src='<? echo $app_img_path; ?>icon_send_doc100.png' border='0'> ส่งยอดขาย</div>
</div>
</div>
<div onclick="hide_popup();" id="popup_panel_shadow" style=""></div>
<div onclick="hide_popup();" id="popup_panel_body"   style="left: 617.5px;"></div>
<form id='frm_swap_group' action='pc_sale_report_add.php' method='POST'>
    <input type='HIDDEN' id='type_id' name='type_id' value=''>
</form>
</body>
</html>
<?
}
else
{
    //icon-navg-back100.png
    echo "<h2>Please login</h2>";
}
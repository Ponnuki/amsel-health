<?php
	ob_start();
	require("connect.php");
	session_start();
	
	if ($_GET["form"] == "employee_dtl")
	{
		$hdMode = $_POST['hdMode'];
		//*** Data Field ****
		
		$txbEmpCode=$_POST['txbEmpCode'];
		$txbNName=$_POST['txbNName'];
		$ddlTitle=$_POST['ddlTitle'];
		$txbFNameTh=$_POST['txbFNameTh'];
		$txbLNameTh=$_POST['txbLNameTh'];
		$txbFNameEn=$_POST['txbFNameEn'];
		$txbLNameEn=$_POST['txbLNameEn'];
		$txbTel=$_POST['txbTel'];
		$txbEmail=$_POST['txbEmail'];
		$ddlPosition=$_POST['ddlPosition'];
		$txbStartJobDate=$_POST['txbStartJobDate'];
		
		$query_str="";
		
		if ($hdMode=="new")
		{
			
			$sql = sprintf("INSERT INTO mst_employee(
								   EMP_CODE
								  ,TITLE_ID
								  ,FNAME_TH
								  ,LNAME_TH
								  ,FNAME_EN
								  ,LNAME_EN
								  ,NICK_NAME
								  ,START_JOB_DATE
								  ,EMAIL
								  ,TEL
								  ,POSITION_ID
								  ,CREATED_BY
								  ,CREATE_DATE
								  ,UPDATE_DATE
								) VALUES (
								  '%s' -- EMP_CODE - IN varchar(10)
								  ,%d
								  ,'%s' -- FNAME_TH - IN varchar(100)
								  ,'%s'  -- LNAME_TH - IN varchar(100)
								  ,'%s'  -- FNAME_EN - IN varchar(100)
								  ,'%s'  -- LNAME_EN - IN varchar(100)
								  ,'%s'  -- NICK_NAME - IN varchar(40)
								  ,STR_TO_DATE('%s', '%%d-%%c-%%Y') -- START_JOB_DATE - IN date
								  ,'%s'  -- EMAIL - IN varchar(128)
								  ,'%s'  -- TEL - IN varchar(50)
								  ,%d   -- POSITION_ID - IN int(11)
								  ,'%s'  -- CREATED_BY - IN varchar(20)
								  ,NOW()  -- CREATE_DATE - IN datetime
								  ,NOW()  -- UPDATE_DATE - IN datetime
								) "
				, mysql_real_escape_string($txbEmpCode)
				, mysql_real_escape_string($ddlTitle)
				, mysql_real_escape_string($txbFNameTh)
				, mysql_real_escape_string($txbLNameTh)
				, mysql_real_escape_string($txbFNameEn)
				, mysql_real_escape_string($txbLNameEn)
				, mysql_real_escape_string($txbNName)
				, mysql_real_escape_string($txbStartJobDate)
				, mysql_real_escape_string($txbEmail)
				, mysql_real_escape_string($txbTel)
				, mysql_real_escape_string($ddlPosition)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				);
				
			if($result=mysql_query($sql)){
				$query_str="?focus=".$txbEmpCode;
				echo "Insert OK ^.^";
			}else{
				$query_str="?error_ins=".$txbEmpCode;
				echo "SQL Error = ".$sql."<br/>";
			}
		}else if ($hdMode=="edit")
		{
			$sql = sprintf("UPDATE mst_employee
								SET
									TITLE_ID = %d
									,FNAME_TH = '%s' -- varchar(120)
									,LNAME_TH = '%s' -- varchar(120)
									,FNAME_EN = '%s' -- varchar(120)
									,LNAME_EN = '%s' -- varchar(120)
									,NICK_NAME = '%s' -- varchar(100)
									,START_JOB_DATE = STR_TO_DATE('%s', '%%d-%%c-%%Y')  -- date
									,EMAIL = '%s' -- varchar(128)
									,TEL = '%s' -- varchar(50)
									,POSITION_ID = %d -- int(11)
									,UPDATE_BY = '%s' -- varchar(20)
									,UPDATE_DATE = NOW() -- datetime
								WHERE EMP_CODE = '%s'; "
				, mysql_real_escape_string($ddlTitle)
				, mysql_real_escape_string($txbFNameTh)
				, mysql_real_escape_string($txbLNameTh)
				, mysql_real_escape_string($txbFNameEn)
				, mysql_real_escape_string($txbLNameEn)
				, mysql_real_escape_string($txbNName)
				, mysql_real_escape_string($txbStartJobDate)
				, mysql_real_escape_string($txbEmail)
				, mysql_real_escape_string($txbTel)
				, mysql_real_escape_string($ddlPosition)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($txbEmpCode)
				);
			
			if($result=mysql_query($sql)){
				$query_str="?focus=".$txbEmpCode;
				echo "Update OK ^.^=".$sql."<br/>";
			}else{
				$query_str="?error_ins=".$txbEmpCode;
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
		

	} else if ($_GET["form"] == "frmEmployeeTb") 
	{
		$hdDelID = $_POST['hdDelID'];
		$hdCancelID = $_POST['hdCancelID'];
		echo "</br>DelID=".$hdDelID;
		echo "</br>CancelID=".$hdCancelID;
		if ($_POST['hdDelID'] != "")
		{
			$sql = "DELETE FROM mst_employee
					WHERE EMP_ID = ".$hdDelID."; ";
			
			if($result=mysql_query($sql)){
				echo "Delete OK ^.^";
			}else{
				echo "SQL Error = ".$sql."<br/>";
			}
		} else if ($_POST['hdCancelID'] != "")
		{
			
			$sql = sprintf("UPDATE mst_employee
									SET ACTIVE_FLAG = CASE WHEN ACTIVE_FLAG = 'Y' THEN 'N' ELSE 'Y' END -- varchar(1)
										,UPDATE_BY = '%s' -- varchar(20)
										,UPDATE_DATE = Now() -- datetime
									WHERE EMP_ID = %d;"
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($hdCancelID)
				);
			if($result=mysql_query($sql))
			{
				$query_str="?focus=".$txbProCode;
				echo "Update OK ^.^=".$sql."<br/>";
			}else
			{
				$query_str="?error_ins=".$txbProCode;
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
	}
	header("Location: employee.php".$query_str); 
	ob_end_flush();
?>
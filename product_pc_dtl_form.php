	<script type="text/javascript">
    <!--
		function ClearForm()
		{
			document.getElementById('hdMode').value = "new";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-plus-square'></i>New Product PC";
			
			$("#ddlProCode").attr("disabled", false);
			$('#ddlProCode').selectpicker("setStyle", "");
			$('#ddlProCode').selectpicker('val','');
			$('#ddlCusType').selectpicker('val','');
			document.getElementById('txbDate').value = todayDDMMYYYY();
			document.getElementById("ddlProCode").style.backgroundColor = "";
		
			document.getElementById('txbLTPPer').value = "";
			document.getElementById('txbLTPPrice').value = "";
			document.getElementById('txbStickerPrice').value = "";
			document.getElementById('txbCheerRate').value = "";
			document.getElementById('txbLPTComm').value = "";
			document.getElementById('labError').innerHTML = "";
			document.getElementById("btSave").style.display = "none";
			document.getElementById("btSaveNew").style.display = "";
			
		}
		function getNewProCode(pro_id)
		{
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					document.getElementById("txbProSalesCode").value = xmlhttp.responseText.replace("\r\n\t", "");
					document.getElementById("txbProSalesCode").style.backgroundColor = "#ddffdd";
					document.getElementById("labProSalesCode").innerHTML = "Product Code <font color=#ff0000>*</font>: ";
				}
			}
			xmlhttp.open("GET","product_sales_ajax.php?pro_id="+pro_id ,true);
			xmlhttp.send();
		
		}
		
		function chkDupData()
		{
			//var dupResult;
			//dupResult = false;
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					var dupProSalesCode = "";
					dupProSalesCode = xmlhttp.responseText.replace("\r\n\t", "");
					
					if (dupProSalesCode == ""|| dupProSalesCode== null)
					{
						document.getElementById("labError").innerHTML = "";
						//alert (dupProSalesCode);
						document.getElementById("btSubmit").click();
						
						//document.forms["frmProSalesDtl"].submit();
					}else
					{
						if (document.getElementById("hdMode").value == "new")
						{
							document.getElementById("labError").innerHTML = "This data duplicate. Please chack Product code and Effective Date: ["+ dupProSalesCode+"].";
						}else
						{
							document.getElementById("btSubmit").click();
						}
					}
				}
				
			}
			
			
			params  = 'operate=chk_dup';
			proId = $('#ddlProCode').val();
			effective = document.getElementById('txbDate').value;
			cusTypeId = $('#ddlCusType').val();
			
			params += "&prd_id="+proId;
			params += "&effective="+effective;
			params += "&cus_type_id="+cusTypeId;
			
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","product_pc_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
			}
			
			
		}
		
		function getDataAjax(prd_pc_id) 
		{
			document.getElementById('hdMode').value = "edit";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit Product PC";
			document.getElementById("labError").innerHTML = "";
			document.getElementById("btSave").style.display = "";
			document.getElementById("btSaveNew").style.display = "none";
			$("#ddlProCode").attr('disabled', true);
			$('#ddlProCode').selectpicker('setStyle', 'btn-inverse');
			
			
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					var res = JSON.parse(str);
					
					document.getElementById('hdProPcId').value =res[0]["PRODUCT_PC_ID"];
					$('#ddlProCode').selectpicker('val',res[0]["PRODUCT_ID"]);
					//document.getElementById("ddlProCode").readOnly = true;
					document.getElementById('txbDate').value =res[0]["EFFECTIVE_DATE"];
					document.getElementById('txbLTPPer').value = res[0]["LTP_PER"];
					document.getElementById('txbLTPPrice').value = res[0]["LTP_PRICE"];
					document.getElementById('txbStickerPrice').value = res[0]["STICKER_PRICE"];
					document.getElementById('txbCheerRate').value = res[0]["CHEER_RATE"];
					document.getElementById('txbLPTComm').value = res[0]["LTP_FOR_COMM"];
					
				}
			}
			
			params  = 'operate=get_prd_pc_dtl';
			if (typeof(prd_pc_id) !="undefined")  params += "&prd_pc_id="+prd_pc_id;
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","product_pc_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
			}
			
			
		}
		
		function calLTP(item)
		{
			//alert ("id="+item.id);
			
			sticker_price = document.getElementById('txbStickerPrice').value ;
			ltp = document.getElementById('txbLTPPer').value;
			document.getElementById('txbLTPPrice').value  = (sticker_price - (ltp / 100 * sticker_price )).toFixed(2);
			
		}
    //-->
    </script>
	<?php
		// **** Gen Maser Product 
		$sql = "SELECT PRODUCT_ID, concat(PRODUCT_CODE,  ' : ', mp.PRODUCT_NAME_TH) as PCODE_PNAME
					FROM mst_product AS mp 
					ORDER BY PRODUCT_CODE; ";
		$result_pro =  mysql_query($sql);
		
		// **** Gen CUSTOMER TYPE
		$sql = "SELECT CUS_TYPE_ID, CUS_TYPE_NAME
					FROM customer_type
					ORDER BY CUS_TYPE_ID; ";
		$result_cus_type =  mysql_query($sql);
		
	?>
	
		<form method="POST" enctype="multipart/form-data" action="product_pc_model.php?form=pro_pc_dtl" name="frmProPCDtl" 
		 id="frmProSalesDtl"  data-toggle="validator" role="form">
			<input type="hidden" name="hdMode" id="hdMode"  value="">
			<input type="hidden" name="hdProPcId" id="hdProPcId"  value="">
		
			<div class="modal fade" id="product_sales_dtl_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog modal-lg">
					<div id="divFormSalesTran" class="modal-content">
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							
							<h4 class="modal-title" id="labMode"><i class="fa fa-plus-square"></i>New Product Sales</h4>
						</div>
						<div class="modal-body sky-form"  style="border-style:none">
							
							<!-- Product panal -->
							<div class="panel panel-grey margin-bottom-15 padding: 15px">
								<div class="row" style="padding-top: 15px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Product Code <font color=#ff0000>*</font>:</label>
										<div class="col-lg-4">
											<label class="select" >
												<select name="ddlProCode" id="ddlProCode" class="selectpicker form-control" 
												data-live-search="true" title="Please select ..." onchange="getNewProCode(this.value);">
													<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_pro)) 
													{
														echo "<option value='".$row['PRODUCT_ID']."' >".$row['PCODE_PNAME']."</option>";
													}
													mysql_data_seek ($result_pro_sale_code , 0 );
													?>
												</select>
												
											</label>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right"> </label>
										
										<div class="col-lg-4">
											
										</div>
										
									</div>
								</div>
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Effective Date <font color=#ff0000>*</font>:</label>
										<div class="col-lg-4" style="height: 35px">
											<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" id="txbDate" name="txbDate" value="">
											</label>
										
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Customer Type <font color=#ff0000>*</font>:</label>
										<div class="col-lg-4" style="height: 35px">
											<label class="select" >
												<select name="ddlCusType" id="ddlCusType" class="selectpicker form-control" 
												data-live-search="false" title="Please select ..." onchange="getNewProCode(this.value);">
													<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_cus_type)) 
													{
														echo "<option value='".$row['CUS_TYPE_ID']."' >".$row['CUS_TYPE_NAME']."</option>";
													}
													mysql_data_seek ($result_pro_sale_code , 0 );
													?>
												</select>
												
											</label>
										
										</div>
										
										
									</div>
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Sticker Price <font color=#ff0000>*</font>:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbStickerPrice" id="txbStickerPrice" onkeyup="calLTP(this);" placeholder="" required>
										</div>
										
										<label class="col-lg-2 control-label" style="text-align:right">LTP Percent <font color=#ff0000>*</font>:</label>
										<div class="col-lg-1" style ="padding-right:0px;">
											<input type="text" class="form-control"  name="txbLTPPer" id="txbLTPPer" placeholder="" onkeyup="calLTP(this);" required>
										</div>
										<div id="divLTPPrice" class="col-lg-1 control-label" style="text-align:right;padding-right:0px; padding-left:0px;">LTP Price : </div>
										<div class="col-lg-2">
											<input type="text" class="form-control" name="txbLTPPrice" id="txbLTPPrice" placeholder="" readonly>
										</div>
										
									</div>
								</div>
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Cheer Rate <font color=#ff0000>*</font>:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbCheerRate" id="txbCheerRate" placeholder="" required>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">LTP for Commission <font color=#ff0000>*</font>:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbLPTComm" id="txbLPTComm" placeholder="" required>
										</div>
									</div>
								</div>
								
							</div><!-- End Product panal -->
							<label id="labError" class="error"></label>
							

							
						</div><!-- End "modal-body" -->
						<div class="modal-footer">
							<button type="button" name="btClose" id="btClose" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
							<button type="button" onclick="chkDupData('New')" class="btn-u btn-u-primary" name="btSaveNew" id="btSaveNew" >Save New</button>
							<button type="button" onclick="chkDupData('Edit')"  class="btn-u btn-u-primary" name="btSave" id="btSave">Save</button>
							<button type="submit" class="btn-u btn-u-primary" name="btSubmit" id="btSubmit" style="display:none">Submit</button>
						</div>
						
					</div><!-- END modal-body -->
				</div>
			</div>

		</form>
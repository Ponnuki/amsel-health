<?php
	require_once("class_tax_invoice.php");
	$amh_inv = new AMH_inv();
	if (isset($_REQUEST['po_cus_id']))
	{
		$dll_po = $amh_inv->get_ddl_po($_REQUEST['cus_id']);
		echo "<option value='' selected> - Please Select - </option>";
		foreach ($dll_po as $row)
		{
			echo "<option value='".$row["PO_ID"]."' > ".$row["PO_NO"]."</option>";
			
		}
				
	}
	else if (isset($_REQUEST['po_id']) )
	{
		$inv_dtl = $amh_inv->get_inv_dtl_from_po_id($_REQUEST['po_id']);

		echo "<input type='hidden' name='hdCusID' id='hdCusID'  value='".$inv_dtl[0]["CUS_ID"]."'>";
		echo "<input type='hidden' name='hdCusName' id='hdCusName'  value='".$inv_dtl[0]["CUS_NAME"]."'>";
		echo "<input type='hidden' name='hdCusContact' id='hdCusContact'  value='".$inv_dtl[0]["CUS_CONTACT"]."'>";
		echo "<input type='hidden' name='hdCusAddrss' id='hdCusAddrss'  value='".$inv_dtl[0]["CUS_ADDRESS"]."'>";
		echo "<input type='hidden' name='hdProvinceCode' id='hdProvinceCode'  value='".$inv_dtl[0]["PROVINCE_CODE"]."'>";
		echo "<input type='hidden' name='hdDistrictCode' id='hdDistrictCode'  value='".$inv_dtl[0]["DISTRICT_CODE"]."'>";
		echo "<input type='hidden' name='hdCusZip' id='hdCusZip'  value='".$inv_dtl[0]["CUS_ZIP"]."'>";
		echo "<input type='hidden' name='hdCusTel' id='hdCusTel'  value='".$inv_dtl[0]["CUS_TEL"]."'>";
		echo "<input type='hidden' name='hdCusFax' id='hdCusFax'  value='".$inv_dtl[0]["CUS_FAX"]."'>";
		echo "<input type='hidden' name='hdTaxNumber' id='hdTaxNumber'  value='".$inv_dtl[0]["TAX_NUMBER"]."'>";
		echo "<table class='table table-bordered table-striped'>
					<thead>
						<tr valign='middle'>
							<th>  # </th>
							<th>Pro. Sale Code</th>
							<th>Product Name</th>
							<th>Description</th>
							<th>Unit Price</th>
							<th>Qty.</th>
							<th>Total Amount</th>
							<th>   </th>
						</tr>
					</thead>
					<tbody>";
					
		$i_no = 1;
		$total = 0;
		foreach ($inv_dtl as $row)
		{
			echo "	
						<tr name='trRow'>
							<td width='45px'>
								<ul class='list-inline table-buttons'>
								<li><div name='divQno'>".$i_no."</div><input type='hidden' name='hdInvDtlId' value=''>
								</li>
								</ul>
							</td>
							<td width='90px'>
								<input type='hidden' name='hdProID' value='".$row["PRODUCT_ID"]."'>
								<p><input type='text' class='form-control' name='txbProSaleCode' value='".$row["PRODUCT_SALE_CODE"]."' readonly></p>
							</td>
							<td width='200px'>
								<input type='hidden' name='hdProName' value='".$row["PRODUCT_NAME"]."'>
								<div name='divProName'>".$row["PRODUCT_NAME"]."</div>
							</td>
							<td>
								<input type='hidden' name='hdProDesc' value='".$row["DESCRIPTION"]."'>
								<div name='divDesc'>".$row["DESCRIPTION"]."</div>
							</td>
							<td width='100px'>
								<p><input type='text' class='form-control' name='txbUnitPrice' value='".number_format($row["UNIT_PRICE"],2)."' readonly></p>
							</td>
							<td width='75px'>
								<p><input type='text' class='form-control' name='txbInvQty' value='".number_format($row["INV_QTY"],0)."' onkeyup='reCalDtl(".($i_no-1).");'></p>
							</td>
							<td width='140px'>
								<p><input type='text' class='form-control' name='txbAmount' value='".number_format($row["AMOUNT"],2)."' readonly></p>
							</td>
							<td width='84px'>
								<ul class='list-inline table-buttons'>
								<li>
								<button class='btn-u btn-u-sm btn-u-orange' onclick='confirmRemove(".($i_no-1).")' type='button'>
									<i class='fa fa-arrow-right'></i> Remove
								</button>
								</li>
								</ul>
							</td>
						</tr>
						";
			$total = $total + $row["AMOUNT"];
			$i_no++;
		}						
		
		echo "	</tbody>
				</table>";
		echo "<input type='hidden' name='hdTotal' id='hdTotal'  value='".$total."'>";
		echo "<input type='hidden' name='hdDiscPer' id='hdDiscPer'  value='".$inv_dtl[0]["DISCOUNT_PER"]."'>";
		echo "<input type='hidden' name='hdVatPer' id='hdVatPer'  value='".$inv_dtl[0]["VAT_PER"]."'>";
		
		
	}
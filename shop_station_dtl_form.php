﻿	<script type="text/javascript">
    <!--
		function ClearForm()
		{
			document.getElementById('hdMode').value = "new";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-plus-square'></i>New Shop Station";
			document.getElementById('txbShopCode').value = "";
			document.getElementById('txbShopCode').readOnly = false;
			document.getElementById('labErrShopCode').value = "";
			document.getElementById("labErrShopCode").style.display = "none";
			
			document.getElementById('txbShopName').value = "";
			document.getElementById('txbConName').value = "";
			document.getElementById('txbTel').value = "";
			
			document.getElementById("btSave").style.display = "none";
			document.getElementById("btSaveNew").style.display = "";
			
		}
		function getNewProCode(pro_id)
		{
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					document.getElementById("txbProSalesCode").value = xmlhttp.responseText.replace("\r\n\t", "");
					document.getElementById("txbProSalesCode").style.backgroundColor = "#ddffdd";
					document.getElementById("labProSalesCode").innerHTML = "New Product Sales Code <font color=#ff0000>*</font>: ";
				}
			}
			xmlhttp.open("GET","product_sales_ajax.php?pro_id="+pro_id ,true);
			xmlhttp.send();
		
		}
		
		function chkDupData()
		{
			//var dupResult;
			//dupResult = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					var res = str.split("|");
					
					if (res[0] == "")
					{
						document.getElementById("labErrShopCode").innerHTML = "";
						document.getElementById("labErrShopCode").style.display= "none";
						document.getElementById("btSubmit").click();
						//document.forms["frmProSalesDtl"].submit();
					}else
					{
						if (document.getElementById("hdMode").value == "new")
						{
							document.getElementById("labErrShopCode").innerHTML = "This data duplicate. Please chack Shop code : ["+ res[0]+"]";
							document.getElementById("labErrShopCode").style.display= "";
						}else
						{
							document.getElementById("btSubmit").click();
						}
					}
				}
				
			}
			
			shop_code = document.getElementById('txbShopCode').value;
			shop_name = document.getElementById('txbShopName').value;
			
			xmlhttp.open("GET","shop_station_ajax.php?shop_code="+shop_code+"&shop_name="+shop_name ,true);
			xmlhttp.send();
			
			//return dupResult;
			
			
		}
		
		function getDataAjax(int) 
		{
			document.getElementById('hdMode').value = "edit";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit Shop Station";
			document.getElementById('txbShopCode').readOnly = true;
			document.getElementById("labErrShopCode").innerHTML = "";
			document.getElementById("labErrShopCode").style.display= "none";
			document.getElementById("btSave").style.display = "";
			document.getElementById("btSaveNew").style.display = "none";
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					var res = str.split("|");
					
					document.getElementById('txbShopCode').value = res[1];
					document.getElementById('txbShopName').value = res[2];
					document.getElementById('txbConName').value = res[3];
					document.getElementById('txbTel').value = res[4];
					
				}
			}
			
			xmlhttp.open("GET","shop_station_ajax.php?shop_id="+int,true);
			xmlhttp.send();
		}
    //-->
    </script>
	<?php
		// **** Gen Position 
		$sql = "SELECT POSITION_ID, CONCAT(mp.POSITION_NAME_EN, '/', mp.POSITION_NAME_TH) AS POSITION_NAME
					FROM mst_position AS mp 
					ORDER BY mp.POSITION_NAME_EN; ";
		$result_position =  mysql_query($sql);
	?>
	
		<form method="POST" enctype="multipart/form-data" action="shop_station_model.php?form=frmShopDtl" name="frmShopDtl" 
		 id="frmShopDtl"  data-toggle="validator" role="form">
			<input type="hidden" name="hdMode" id="hdMode"  value="">
		
			<div class="modal fade" id="shop_dtl_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog modal-lg">
					<div id="divFormBody" class="modal-content">
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							
							<h4 class="modal-title" id="labMode"><i class="fa fa-plus-square"></i>New Shop Station</h4>
						</div>
						<div class="modal-body sky-form"  style="border-style:none">
							
							<!-- Product panal -->
							<div class="panel panel-grey margin-bottom-15 padding: 15px">
							
								
								
								<div class="row" style="padding-top: 15px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Shop Code <font color="#ff0000">*</font></label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbShopCode" id="txbShopCode" placeholder="" required>
											<label id="labErrShopCode" class="error" style="display:none"></label>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Shop Name <font color="#ff0000">*</font></label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa  fa-smile-o"></i>
												<input type="text" class="form-control" name="txbShopName" id="txbShopName" placeholder="" required>
											</label>
											<label id="labErrNName" class="error" style="display:none"></label>
										</div>
									</div>
									
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Contact Name <font color=#ff0000>*</font></label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-user"></i>
												<input type="text" class="form-control" name="txbConName" id="txbConName" placeholder="" required>
											</label>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Tel. </label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-phone"></i>
												<input type="tel" class="form-control" name="txbTel" id="txbTel" placeholder="" >
											</label>
										</div>
									</div>
								</div>
								
								
							</div><!-- End Product panal -->
							<label id="labError" class="error"></label>
							

							
						</div><!-- End "modal-body" -->
						<div class="modal-footer">
							<button type="button" name="btClose" id="btClose" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
							<button type="button" onclick="chkDupData('New')" class="btn-u btn-u-primary" name="btSaveNew" id="btSaveNew" >Save New</button>
							<button type="button" onclick="chkDupData('Edit')"  class="btn-u btn-u-primary" name="btSave" id="btSave">Save</button>
							<button type="submit" class="btn-u btn-u-primary" name="btSubmit" id="btSubmit" style="display:none">Submit</button>
						</div>
						
					</div><!-- END modal-body -->
				</div>
			</div>

		</form>
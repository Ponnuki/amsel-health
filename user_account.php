<?php 
	require("include_function.php");
	require('validatelogin.php');
 ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | USER ACCOUNT</title>
	<?php $current_menu = "user_account"; ?>
	<?php require("include_headtag.php"); ?>
    
</head> 

<body>    

	<div  id="wrap" class="wrapper">

		<?php require("include_header.php"); ?>

		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs breadcrumbs-dark">
			<div class="container">
				<h1 class="pull-left">USER ACCOUNT</h1>
				
			</div>
		</div>
		<!--=== End Breadcrumbs ===-->
		<script type="text/javascript">
		<!--
		function confirmDel(DelID)
		{
			if (confirm("Are you sure you want to delete this item."))
			{
				document.getElementById('hdDelID').value = DelID;
				document.getElementById('hdCancelID').value  = "";
				return true;
			}else{
				return false;
			}
		 
		}
		
		function confirmCancel(CancelID)
		{
			if (confirm("Are you sure you want to cancel this item."))
			{
				document.getElementById('hdDelID').value = "";
				document.getElementById('hdCancelID').value = CancelID;
				return true;
			}else{
				return false;
			}
		 
		}
		 
		 function getDdlUserSearch()
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					document.getElementById("ddlUserSearch").innerHTML += str;
					$('.selectpicker').selectpicker('refresh');
					
				}
			}
			params  = 'operate=get_ddl_aut_user';
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","user_account_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
				
			}
		}
		
		function getGridAutUser()
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp2.onreadystatechange=function() 
			{
				
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200) 
				{
					var str = xmlhttp2.responseText.replace("\r\n\t", "");
					//alert str;
					document.getElementById("gridTb").innerHTML = str;
					//$('.selectpicker').selectpicker('refresh');
					
				}
			}
			
			params  = 'operate=get_grid_aut_user';
			params += "&aut_uname="+document.getElementById("ddlUserSearch").value;
			posting = true;
			if (posting)
			{
				xmlhttp2.open("POST","user_account_ajax.php",true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params);
				
			}
		}
		 
		 //-->
		</script>
		<!--=== Search Block Version 2 ===-->
	   
			<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
				<div class="container">
					<div class="col-md-6 col-md-offset-3">
						<h2>CONDITION</h2>
						<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
							
							<div class="sky-form" style="border-style:none">                                                      
								<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_condtion" onSubmit="return validate_form(this)" >
									<div class="form-group">
										<label for="inputSalesCode" class="col-lg-4 control-label">User ID / Employee Name </label>
										<div class="col-lg-8" style="height: 35px">
											<label class="select" >
													<select name="ddlUserSearch" id="ddlUserSearch" class="selectpicker form-control" 
													data-live-search="true" title="Please select ...">
															<option value="" selected> - All User - </option>
														
													</select>
													
											</label>
										</div>
									</div>
										
									 
									<div class="form-group">
										<div class="col-lg-offset-4 col-lg-8">
											<button type="button" onclick="getGridAutUser();" class="btn-u"> Search </button> &nbsp;&nbsp;
											
										</div>
									</div>

								</form>

							</div>
						</div>

					</div>
				</div>    
				
			</div><!--/container--> 

		
<!--=== End Search Block Version 2 ===-->

		<div class="container content-sm"  style="padding:30px;">


			<div class="col-lg-offset col-lg-8" style="height:50px">
				<button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#u_acc_dtl_form" 
					onclick="ClearForm();"> <i class="fa fa-plus-square icon-color-white"></i> New User Account </button> 
			
				<?php  require("user_account_dtl_form.php");  ?>
			</div>

			
			<div class="table-search-v2 margin-bottom-30">
				<div class="table-responsive">
					<form method="POST" enctype="multipart/form-data" action="user_account_model.php?form=frmUsAccTb" name="frmUsAccTb" onSubmit="return validate_form(this)"
						id="frmUsAccTb" >
						<input type="hidden" name="hdDelID" id="hdDelID"  value="">
						<input type="hidden" name="hdCancelID" id="hdCancelID"  value="">
						<table class="table table-bordered table-striped">
							<thead>
								<tr valign="middle">
									<th> &nbsp; </th>
									<th>User Name</th>
									<th class="hidden-sm">Name / Nick Name </th>
									<th>Position</th>
									<th>Application Role</th>

									<th> &nbsp; </th>
								</tr>
							</thead>
							<tbody id = "gridTb">
								
								<script type="text/javascript">
									getDdlUserSearch();
									getGridAutUser();
								</script>
							</tbody>
						</table>
					</form>
					
					
					<!-- End Test Form -->
				</div>    
			</div>    
			<!-- End Table Search v2 -->
			


			
		</div>
		<?php require("include_footer.php"); ?>
		 
	</div><!--/End Wrapepr-->

	<?php require("include_js.php"); ?>


</body>
</html> 
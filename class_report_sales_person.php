<?php
require_once("class_amh_db.php");
require_once("class_report_sales_person_excel.php");
require_once("include_function.php");
require_once("validatelogin.php");

/*
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
*/
require_once ("assets/classes/PHPExcel.php");
/** Include PHPExcel_IOFactory */
require_once ("assets/classes/PHPExcel/IOFactory.php");

//$amh_rp_sale_excel = new AMH_rp_sale_excel();

class AMH_rp_sale extends AMH_DB
{
	
	public function pc_user($aut_uname = "")
    {
        $arr_ret = array();
		$where = "WHERE au.AUT_ROLE_ID = 4";
		if ($aut_uname != "") $where .= " AND au.AUT_UNAME ='".$aut_uname."' ";
        $sql_sel = "SELECT au.AUT_ID, au.AUT_UNAME, EMP_CODE, CONCAT(emp.EMP_CODE,' : ',FNAME_TH,' ',LNAME_TH,'(',NICK_NAME,')') AS PC_NAME  
						FROM aut_user AS au
							LEFT JOIN mst_employee AS emp ON au.EMP_ID = emp.EMP_ID
						  ";
		$order = "ORDER BY EMP_CODE";
        $res_sel = $this->mysqli->query($sql_sel.$where) OR die("<div style='display:none;'>mst_district error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }
	
	public function get_ddl_pc()
    {
        $str_retn = "";
		
        $arr = $this->pc_user();

        foreach ($arr as $row)
        {
            $selected  = "";
            //if ($aut_id == $row["alert(source);"]) { $selected  = " selected"; }
            $str_retn .= "<option value='".$row["AUT_ID"]."'".$selected.">".$row["PC_NAME"]."</option>";
        }
        return $str_retn;
		
    }
	
	
	public function get_data_sale($aut_id="", $prd_type ="", $start_date ="", $end_date = "",$excel_path="",$pro_img_path="")
    {

        $where = " WHERE 1 ";
        if ($aut_id != "")
        {
			$where  .= " AND srpc.AUT_ID = ".$aut_id;
		}
		
		if ($start_date !="" || $end_date != "")
		{
			$where .= " AND srm.SALE_REPORT_DATE BETWEEN STR_TO_DATE('".$start_date ."','%d-%m-%Y') AND  STR_TO_DATE('".$end_date."','%d-%m-%Y')  ";
        }
		$file_fname="report_sale_pc_";
		$arr_ret = array();
		$sql_main = "SELECT mp.PRODUCT_CODE,
								FORMAT(IFNULL(dtl.QTY,0),0) AS QTY ,
								FORMAT(IFNULL(dtl.AMOUNT,0),2) AS AMOUNT ,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								IFNULL(PC_NAME,'') as PC_NAME,
								CONCAT('".$file_fname."',IFNULL(PC_NAME,''),'_".str_replace("-","",$start_date)."_".str_replace("-","",$end_date).".xlsx') as EXCEL_NAME
							FROM  mst_product AS mp 
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID
								LEFT OUTER JOIN
									(";
		$sql_dtl = "			SELECT pc.PRODUCT_ID, SUM(srd.QTY) as QTY, SUM( srd.QTY * LTP_PRICE) as AMOUNT,			
										CONCAT( em.FNAME_TH ,' ',em.LNAME_TH , '(' , IFNULL(em.NICK_NAME,'') , ')') AS PC_NAME
									FROM   mst_product_pc AS pc 
										LEFT JOIN  sale_report_dtl AS srd ON pc.PRODUCT_PC_ID = srd.PRODUCT_PC_ID 
										LEFT JOIN sale_report_main AS srm ON srm.SALE_REPORT_ID = srd.SALE_REPORT_ID
										LEFT JOIN sale_report_pc_rel AS srpc ON srm.SALE_REPORT_ID = srpc.SALE_REPORT_ID
										LEFT JOIN aut_user AS au ON srpc.AUT_ID = au.AUT_ID
										LEFT JOIN mst_employee AS em ON au.EMP_ID = em.EMP_ID ";
						
		$group = " 			GROUP BY pc.PRODUCT_ID, em.FNAME_TH,em.LNAME_TH, em.NICK_NAME ";
		$sql_end = " ) dtl ON dtl.PRODUCT_ID = mp.PRODUCT_ID
						WHERE mp.ACTIVE_FLAG = 'Y'
						ORDER BY mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
        $res_sel = $this->mysqli->query($sql_main.$sql_dtl.$where.$group.$sql_end) OR die("<div style='color:red;'>do_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		$amh_rp_sale_excel =new AMH_rp_sale_excel();
		$amh_rp_sale_excel->gen_excel_sale($arr_ret, $excel_path, $pro_img_path, $start_date, $end_date,"SALE");
		
        return json_encode($arr_ret);
    }
	
	public function get_data_order($aut_id="", $prd_type ="", $start_date ="", $end_date = "",$excel_path="",$pro_img_path="")
    {

        $where = " WHERE 1 ";
        if ($aut_id != "")
        {
			$where  .= " AND au.AUT_ID = ".$aut_id;
		}
		/*
		if ($prd_type != "")
        {
			$where  .= " AND mp.PRODUCT_TYPE_ID = ".$prd_type;
		}*/
		if ($start_date !="" || $end_date != "")
		{
			$where .= " AND o_dtl.CREATED_DATE BETWEEN STR_TO_DATE('".$start_date ."','%d-%c-%Y') AND  STR_TO_DATE('".$end_date."','%d-%c-%Y')  ";
        }
		$file_fname="report_order_pc_";
		$arr_ret = array();
		$sql_main = "SELECT mp.PRODUCT_CODE,
								FORMAT(IFNULL(dtl.QTY,0),0) AS QTY ,
								FORMAT(IFNULL(dtl.AMOUNT,0),2) AS AMOUNT ,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								IFNULL(PC_NAME,'') as PC_NAME,
								CONCAT('".$file_fname."',IFNULL(PC_NAME,''),'_".str_replace("-","",$start_date)."_".str_replace("-","",$end_date).".xlsx') as EXCEL_NAME
							FROM  mst_product AS mp 
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID
								LEFT OUTER JOIN
									(";
		$sql_dtl = "			SELECT pc.PRODUCT_ID, SUM(o_dtl.QTY) as QTY, SUM( o_dtl.QTY * pc.LTP_PRICE) as AMOUNT,
										CONCAT( em.FNAME_TH ,' ',em.LNAME_TH , ' (' , IFNULL(em.NICK_NAME,'') , ')') AS PC_NAME
									FROM   mst_product_pc AS pc 
										LEFT JOIN order_dtl AS o_dtl ON pc.PRODUCT_PC_ID = o_dtl.PRODUCT_PC_ID 
										LEFT JOIN aut_user AS au ON o_dtl.CREATED_BY = au.AUT_UNAME
										LEFT JOIN mst_employee AS em ON au.EMP_ID = em.EMP_ID ";
						
		$group = "	GROUP BY pc.PRODUCT_ID, em.FNAME_TH,em.LNAME_TH, em.NICK_NAME ";
		$sql_end = " ) dtl ON dtl.PRODUCT_ID = mp.PRODUCT_ID
						WHERE mp.ACTIVE_FLAG = 'Y' 
						ORDER BY mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
        $res_sel = $this->mysqli->query($sql_main.$sql_dtl.$where.$group.$sql_end) OR die("<div style='color:red;'>do_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		$amh_rp_sale_excel =new AMH_rp_sale_excel();
		$amh_rp_sale_excel->gen_excel_sale($arr_ret, $excel_path, $pro_img_path, $start_date, $end_date,"ORDER");
		
        return json_encode($arr_ret);
    }
	
	public function get_data_sale_daily($aut_id="", $prd_type ="", $start_date ="", $end_date = "",$excel_path="",$pro_img_path="")
    {

        $where = " WHERE 1 ";
        if ($aut_id != "")
        {
			$where  .= " AND srpc.AUT_ID = ".$aut_id;
		}/*
		if ($prd_type != "")
        {
			$where  .= " AND mp.PRODUCT_TYPE_ID = ".$prd_type;
		}
		*/
		if ($start_date !="" || $end_date != "")
		{
			$where .= " AND srm.SALE_REPORT_DATE BETWEEN STR_TO_DATE('".$start_date ."','%d-%c-%Y') AND  STR_TO_DATE('".$end_date."','%d-%c-%Y')  ";
        }
	
		$file_fname="report_sale_pc_daily_";
		$arr_ret = array();
		$sql_main = "SELECT 
								DATE_FORMAT(srm.SALE_REPORT_DATE,'%d-%m-%Y') AS ACTION_DATE, pc.PRODUCT_ID, 
								FORMAT(SUM(srd.QTY),0) as QTY, FORMAT(SUM( srd.QTY * LTP_PRICE),2) as AMOUNT,
								mp.PRODUCT_CODE,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								CONCAT( em.FNAME_TH ,' ',em.LNAME_TH , '(' , IFNULL(em.NICK_NAME,'') , ')') AS PC_NAME,
								(SELECT COUNT(s.AUT_ID) FROM sale_report_pc_rel AS s WHERE s.SALE_REPORT_ID = srm.SALE_REPORT_ID) AS NO_OF_PC,
								CONCAT('".$file_fname."',em.FNAME_TH ,' ',em.LNAME_TH , '(' , IFNULL(em.NICK_NAME,'') , ')','_".str_replace("-","",$start_date)."_".str_replace("-","",$end_date).".xlsx') as EXCEL_NAME
							FROM   sale_report_main AS srm

								LEFT JOIN  sale_report_dtl AS srd ON srm.SALE_REPORT_ID = srd.SALE_REPORT_ID
								LEFT JOIN mst_product_pc AS pc ON  pc.PRODUCT_PC_ID = srd.PRODUCT_PC_ID 
								LEFT JOIN sale_report_pc_rel AS srpc ON srm.SALE_REPORT_ID = srpc.SALE_REPORT_ID
								LEFT JOIN aut_user AS au ON srpc.AUT_ID = au.AUT_ID
								LEFT JOIN mst_employee AS em ON au.EMP_ID = em.EMP_ID
								LEFT JOIN mst_product AS mp  ON pc.PRODUCT_ID = mp.PRODUCT_ID
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID";
						
		$group = " GROUP BY srm.SALE_REPORT_ID, srm.SALE_REPORT_DATE, pc.PRODUCT_ID, em.FNAME_TH,em.LNAME_TH, em.NICK_NAME,
							mp.PRODUCT_CODE,mp.PRODUCT_TYPE_ID,mp.PRODUCT_ID,mpt.TYPE_NAME_EN ,mpt.TYPE_NAME_TH ,mpt.ICON ,
							mp.PRODUCT_NAME_EN, mp.PRODUCT_NAME_TH,mp.PRD_IMG ";
		
		$sql_end = " 		ORDER BY srm.SALE_REPORT_DATE desc, mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
        $res_sel = $this->mysqli->query($sql_main.$where.$group.$sql_end) OR die("<div style='color:red;'>do_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		$amh_rp_sale_excel =new AMH_rp_sale_excel();
		$amh_rp_sale_excel->gen_excel_sale_daily($arr_ret, $excel_path, $pro_img_path, $start_date, $end_date,"SALE");
		
		
        return json_encode($arr_ret);
    }
	
	public function get_data_order_daily($aut_id="", $prd_type ="", $start_date ="", $end_date = "",$excel_path="",$pro_img_path="")
    {

        $where = " WHERE 1 ";
        if ($aut_id != "")
        {
			$where  .= " AND au.AUT_ID = ".$aut_id;
		}
		/*
		if ($prd_type != "")
        {
			$where  .= " AND mp.PRODUCT_TYPE_ID = ".$prd_type;
		}*/
		if ($start_date !="" || $end_date != "")
		{
			$where .= " AND o_dtl.CREATED_DATE BETWEEN STR_TO_DATE('".$start_date ."','%d-%c-%Y') AND  STR_TO_DATE('".$end_date."','%d-%c-%Y')  ";
        }
		
		$arr_ret = array();
		$sql_main = "SELECT 
								DATE_FORMAT(o_dtl.CREATED_DATE,'%d-%m-%Y') AS ACTION_DATE, pc.PRODUCT_ID, 
								FORMAT(SUM(o_dtl.QTY),0) as QTY, FORMAT(SUM( o_dtl.QTY * LTP_PRICE),2) as AMOUNT,
								mp.PRODUCT_CODE,
								mp.PRODUCT_TYPE_ID,
								mp.PRODUCT_ID,
								mpt.TYPE_NAME_EN ,
								mpt.TYPE_NAME_TH ,
								mpt.ICON ,
								mp.PRODUCT_NAME_EN, 
								mp.PRODUCT_NAME_TH,
								mp.PRD_IMG,
								CONCAT( em.FNAME_TH ,' ',em.LNAME_TH , '(' , IFNULL(em.NICK_NAME,'') , ')') AS PC_NAME,
								1 AS NO_OF_PC
							FROM  order_dtl AS o_dtl
								LEFT JOIN mst_product_pc AS pc ON  pc.PRODUCT_PC_ID = o_dtl.PRODUCT_PC_ID 
								LEFT JOIN aut_user AS au ON o_dtl.CREATED_BY = au.AUT_UNAME
								LEFT JOIN mst_employee AS em ON au.EMP_ID = em.EMP_ID
								left join mst_product AS mp  ON pc.PRODUCT_ID = mp.PRODUCT_ID
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID ";
						
		$group = "	GROUP BY o_dtl.CREATED_DATE, pc.PRODUCT_ID, em.FNAME_TH,em.LNAME_TH, em.NICK_NAME,
								mp.PRODUCT_CODE,mp.PRODUCT_TYPE_ID,mp.PRODUCT_ID,mpt.TYPE_NAME_EN ,mpt.TYPE_NAME_TH ,mpt.ICON ,
								mp.PRODUCT_NAME_EN, mp.PRODUCT_NAME_TH,mp.PRD_IMG";
		$sql_end = " ORDER BY o_dtl.CREATED_DATE desc, mp.PRODUCT_TYPE_ID, mp.PRODUCT_CODE; ";
        $res_sel = $this->mysqli->query($sql_main.$where.$group.$sql_end) OR die("<div style='color:red;'>do_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		
        return json_encode($arr_ret);
    }
	
	
	public function get_prd_type()
    {
        $arr_ret = array();

        $sql = "SELECT *
						FROM mst_product_type
						ORDER BY PRODUCT_TYPE_ID";
		
        $res_sel = $this->mysqli->query($sql) OR die("<div style='color:red;'>do_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
        return json_encode($arr_ret);
    }
	
}
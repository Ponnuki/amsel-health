<?php

require_once('class_amh_db.php');
require_once('class_amh_report.php');
require_once("./mpdf60/mpdf.php");

$amh_rpt = new AMH_REPORT();

$arr_inside = $amh_rpt->get_rpt_text('INV');
$arr_ivmain = $amh_rpt->get_inv_main($_REQUEST["inv_id"]);
$arr_iv_dtl = $amh_rpt->get_inv_dtl($_REQUEST["inv_id"]);

$iv_date = Date("j M Y", strtotime($arr_ivmain["INV_DATE"]));

$str_this_page  = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
$str_this_page .= '<html xmlns="http://www.w3.org/1999/xhtml">';
$str_this_page .= '<head>';
$str_this_page .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
$str_this_page .= '<title>Tax Invoice - Report</title>';
$str_this_page .= '</head>';
$str_this_page .= '<body style="padding:0; margin:0;">';
$str_this_page .= '<div id="the_paper" style="margin:0 auto; width:1200px; background-color:#fff; height: 1600px; overflow: hidden;">';
$str_this_page .= '<table style="border:0; width:100%">';
$str_this_page .= '<tr>';
$str_this_page .= '<td style="font-size:10px; text-align:left;  " width="25%">'.$arr_inside["ADDR_TH"].'</td>';
$str_this_page .= '<td style="font-size:10px; text-align:center;" width="50%"><img src="'.$arr_inside["LOGO_IMAGE"].'" border="0"></td>';
$str_this_page .= '<td style="font-size:10px; text-align:right; " width="25%">'.$arr_inside["ADDR_EN"].'</td>';
$str_this_page .= '</tr>';
$str_this_page .= '<tr>';
$str_this_page .= '<td style="font-size:10px; text-align:left;   padding:10px 0;"></td>';
$str_this_page .= '<td style="font-size:10px; text-align:center; padding:10px 0;"></td>';
$str_this_page .= '<td style="font-size:10px; text-align:right;  padding:10px 0;">'.$arr_inside["TAX_NUMBER"].'</td>';
$str_this_page .= '</tr>';
$str_this_page .= '<tr>';
$str_this_page .= '<td style="font-size:9px; text-align:left;   padding:10px 0; vertical-align:top;">'.$arr_inside["BILL_TYPE"].'</td>';
$str_this_page .= '<td style="font-size:9px; text-align:center; padding:10px 0; vertical-align:top;"></td>';
$str_this_page .= '<td style="font-size:9px; text-align:right;  padding:10px 0; vertical-align:top;">'.$arr_inside["COPY_TYPE"].'</td>';
$str_this_page .= '</tr>';
$str_this_page .= '</table>';
$str_this_page .= '<div style="border-bottom: 1px solid #000;"></div>';
$str_this_page .= '<table style="border:0; width:100%;">';
$str_this_page .= '<tr>';
$str_this_page .= '<td style="vertical-align:top; width:800px;">';
$str_this_page .= '<table style="border:none; vertical-align:top;">';
$str_this_page .= '<tr>';
$str_this_page .= '<td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" width="160"><div>CUSTOMER NAME</div><div>ชื่อลูกค้า</div></td>';
$str_this_page .= '<td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" width="340"><div>'.$arr_ivmain["CUS_NAME"].'</div><div>'.$arr_ivmain["CUS_CONTACT"].'</div></td>';
$str_this_page .= '</tr>';
$str_this_page .= '<tr>';
$str_this_page .= '<td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;"><div>ADDRESS</div><div>ที่อยู่</div></td>';
$str_this_page .= '<td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;"><div>'.$arr_ivmain["CUS_ADDRESS"].' '.$arr_ivmain["PROVINCE_NAME"].'</div></td>';
$str_this_page .= '</tr>';
$str_this_page .= '</table>';
$str_this_page .= '</td>';
$str_this_page .= '<td style="vertical-align:top; width:400px; padding: 10px 0px;">';
$str_this_page .= '<table style="border:2px solid #000; border-radius:3px; vertical-align:top; width:100%;">';
$str_this_page .= '<tr>';
$str_this_page .= '    <td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" width="160">NO/เลขที่</td>';
$str_this_page .= '    <td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" >'.$arr_ivmain["INV_NO"].'</td>';
$str_this_page .= '</tr>';
$str_this_page .= '<tr>';
$str_this_page .= '    <td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" ><div>CUSTOMER PO</div><div>ใบสั่งซื้อ</div></td>';
$str_this_page .= '    <td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" >'.$arr_ivmain["CUS_PO_NO"].'</td>';
$str_this_page .= '</tr>';
$str_this_page .= '<tr>';
$str_this_page .= '    <td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" >DATE/วันที่</div></td>';
$str_this_page .= '    <td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" >'.$iv_date.'</td>';
$str_this_page .= '</tr>';
/*
$str_this_page .= '<tr>';
$str_this_page .= '    <td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" >CREDIT TERM</div></td>';
$str_this_page .= '    <td style="font-size:11pt; text-align:left; padding:10px; vertical-align:top;" >'.$arr_ivmain["CREDIT_TERM"].'วัน</td>';
$str_this_page .= '</tr>';
*/
$str_this_page .= '</table>';
$str_this_page .= '</td>';
$str_this_page .= '</tr>';
$str_this_page .= '</table>';
$str_this_page .= '<div style="border-bottom: 1px solid #000;"></div>';
$str_this_page .= '<table border="0" cellspacing="0" width="100%">';
$str_this_page .= '    <thead>';
$str_this_page .= '    <tr>';
$str_this_page .= '        <td style="font-size:9px; font-weight:bold; border-bottom:1px solid #666; vertical-align:middle; text-align:center; padding:5px;">'.$arr_inside["COLUMN01"].'</td>';
$str_this_page .= '        <td style="font-size:9px; font-weight:bold; border-bottom:1px solid #666; vertical-align:middle; text-align:center; padding:5px;">'.$arr_inside["COLUMN02"].'</td>';
$str_this_page .= '        <td style="font-size:9px; font-weight:bold; border-bottom:1px solid #666; vertical-align:middle; text-align:left;   padding:5px;">'.$arr_inside["COLUMN03"].'</td>';
$str_this_page .= '        <td style="font-size:9px; font-weight:bold; border-bottom:1px solid #666; vertical-align:middle; text-align:center; padding:5px;">'.$arr_inside["COLUMN04"].'</td>';
$str_this_page .= '        <td style="font-size:9px; font-weight:bold; border-bottom:1px solid #666; vertical-align:middle; text-align:center; padding:5px;">'.$arr_inside["COLUMN05"].'</td>';
$str_this_page .= '        <td style="font-size:9px; font-weight:bold; border-bottom:1px solid #666; vertical-align:middle; text-align:center; padding:5px;">'.$arr_inside["COLUMN06"].'</td>';
$str_this_page .= '        <td style="font-size:9px; font-weight:bold; border-bottom:1px solid #666; vertical-align:middle; text-align:center; padding:5px;">'.$arr_inside["COLUMN07"].'</td>';
$str_this_page .= '    </tr>';
$str_this_page .= '    </thead>';
$str_this_page .= '    <tbody>';
foreach ($arr_iv_dtl as $do_detail)
{
    $str_this_page .= '    <tr>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">'.($do_detail["QNO"] + 1)          .'</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:left;   padding:5px;">'.($do_detail["PRODUCT_NAME"])     .'</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:left;   padding:5px;">'.($do_detail["PRODUCT_SALE_CODE"]).'</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:left;   padding:5px;">'.($do_detail["DESCRIPTION"])      .'</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:right;  padding:5px;">'. ($do_detail["UNIT_PRICE"])           .'&nbsp;</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">'. number_format($do_detail["QTY"]*1,0) .'</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:right;  padding:5px;">'. ($do_detail["AMOUNT"])               .'&nbsp;</td>';
    $str_this_page .= '    </tr>';
}
$the_target = 24 - count($arr_iv_dtl);
for ($i_cnt = 1; $i_cnt <= $the_target; $i_cnt++)
{
    $str_this_page .= '    <tr>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">&nbsp;</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:left;   padding:5px;">&nbsp;</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:left;   padding:5px;">&nbsp;</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:left;   padding:5px;">&nbsp;</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">&nbsp;</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">&nbsp;</td>';
    $str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">&nbsp;</td>';
    $str_this_page .= '    </tr>';
}
$str_this_page .= '    <tr>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:right;  border-top:1px solid #000; padding:5px;" colspan="2" rowspan="4"><div>Grand Total</div><div>จำนวนเงินรวม</div></td>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; border-top:1px solid #000; padding:5px;" colspan="2" rowspan="4"><div style="display:inline-block; text-align:right; width:60px;"></div></td>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:right;  border-top:1px solid #000; padding:5px;" colspan="2">VALUE AMOUNT</td>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; border-top:1px solid #000; padding:5px;"><div style="display:inline-block; text-align:right; width:60px;">'.number_format($arr_ivmain["INV_TOTAL"], 2).'</div></td>';
$str_this_page .= '    </tr>';
$str_this_page .= '    <tr>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:right;  padding:5px;" colspan="2">DISCOUNT</td>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;"><div style="display:inline-block; text-align:right; width:60px;">'.number_format($arr_ivmain["DISCOUNT_PER"], 1).'</div></td>';
$str_this_page .= '    </tr>';
$str_this_page .= '    <tr>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:right;  padding:5px;" colspan="2">VAT 7%</td>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;"><div style="display:inline-block; text-align:right; width:60px;">'.number_format($arr_ivmain["VAT_PER"], 1).'</div></td>';
$str_this_page .= '    </tr>';
$str_this_page .= '    <tr>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:right;  padding:5px;" colspan="2">GRAND TOTAL</td>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;"><div style="display:inline-block; text-align:right; width:60px;">'.number_format($arr_ivmain["GRAND_TOTAL"], 2).'</div></td>';
$str_this_page .= '    </tr>';
$str_this_page .= '    </tbody>';
$str_this_page .= '</table>';
$str_this_page .= '<div style="border-bottom: 1px solid #000;"></div>';
$str_this_page .= '<div style="padding:20px;"></div>';
$str_this_page .= '<table border="0" cellspacing="0" width="100%">';
$str_this_page .= '   <tr>';
$str_this_page .= '       <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px; border-top:1px solid #000;">AUTHORIZED BY/ON ผู้มีอำนาจ</td>';
$str_this_page .= '       <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px; border-top:1px solid #000;">DELIVERED BY/ON ผู้ส่งสินค้า</td>';
$str_this_page .= '       <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px; border-top:1px solid #000;">RECEIVED BY/ON ผู้รับสินค้า</td>';
$str_this_page .= '   </tr>';
$str_this_page .= '    <tr>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">................................................</td>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">................................................</td>';
$str_this_page .= '        <td style="font-size:10px; font-weight:normal; text-align:center; padding:5px;">................................................</td>';
$str_this_page .= '    </tr>';
$str_this_page .= '</table>';
$str_this_page .= '</div>';
$str_this_page .= '</body>';
$str_this_page .= '</html>';

if ($_REQUEST["media"] == 'SCREEN')
{
    echo $str_this_page;
}

if ($_REQUEST["media"] == 'PDF')
{
    $mpdf=new mPDF('utf-8','A4','','Garuda', 8, 8, 8, 8, 0, 0);
    $mpdf->WriteHTML($str_this_page);
    //$mpdf->Output();
    $mpdf->Output('invoice_'.$_REQUEST["inv_id"].'.pdf', 'I');
}
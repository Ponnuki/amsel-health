<?php 
	require("include_function.php");
	require('validatelogin.php'); 
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | EMPLOYEE</title>

	<?php $current_menu = "employee"; ?>
	<?php require("include_headtag.php"); ?>
    
</head> 

<body>    

<div id="wrap"  class="wrapper">

	<?php require("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">EMPLOYEE</h1>
			
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
	<script type="text/javascript">
    <!--
	function confirmDel(DelID)
	{
		if (confirm("Are you sure you want to delete this item."))
		{
			document.getElementById('hdDelID').value = DelID;
			document.getElementById('hdCancelID').value  = "";
			return true;
		}else{
			return false;
		}
	 
	}
	
	function confirmCancel(CancelID)
	{
		if (confirm("Are you sure you want to cancel this item."))
		{
			document.getElementById('hdDelID').value = "";
			document.getElementById('hdCancelID').value = CancelID;
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
		<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		<?php 
		
			// **** Gen employee DDL
			$sql = "SELECT EMP_ID, EMP_CODE, CONCAT(EMP_CODE, ' : ' ,FNAME_TH ,' ', LNAME_TH , '(' ,NICK_NAME , ')') AS EMPLOYEE_NAME
						FROM mst_employee AS me 
						ORDER BY EMP_CODE; ";
			$result_employee_search=  mysql_query($sql);
			
			// **** Gen employee Table
			$sql = "SELECT EMP_ID, EMP_CODE, CONCAT(FNAME_TH, ' &nbsp;',LNAME_TH, '(',NICK_NAME,')' ) AS NAME_TH,
							CONCAT(FNAME_EN, ' &nbsp;',LNAME_EN) AS NAME_EN , DATE_FORMAT(START_JOB_DATE, '%d-%m-%Y') as START_JOB_DATE,
							EMAIL, TEL, mp.POSITION_NAME_EN,me.ACTIVE_FLAG,
							IFNULL((SELECT au.AUT_UNAME FROM aut_user AS au WHERE au.EMP_ID = me.EMP_ID LIMIT 1), '1') AS IS_DEL
						FROM mst_employee AS me
							LEFT JOIN mst_position AS mp ON me.POSITION_ID = mp.POSITION_ID	";
			
			$where = "";
			
			if ($_POST['ddlEmployeeSearch']!="")
			{
				if ($where == "") $where .= "WHERE "; else $where .=" AND ";
				$where .= " me.EMP_CODE = '".
					mysql_real_escape_string($_POST['ddlEmployeeSearch'])
					."' ";
			}
			
			$order = "	ORDER BY EMP_CODE ; ";
			$sql = $sql.$where.$order;
			$result_employee = mysql_query($sql);

			?>
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_condtion" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Employee Code / Employee Name </label>
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
												<select name="ddlEmployeeSearch" id="ddlEmployeeSearch" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
														<option value="" selected> - All Employee - </option>
													<?php 
													while($row = mysql_fetch_array($result_employee_search)) 
													{
														echo "<option value='".$row['EMP_CODE']."' ";
														if ($row['EMP_CODE'] == $_POST['ddlEmployeeSearch'])
														{
															echo " selected ";
														}
														echo ">".$row['EMPLOYEE_NAME']."</option>";
													}
													mysql_data_seek ($result_employee_search , 0 );
													?>
												</select>
												
										</label>
									</div>
								</div>
									
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button type="submit" class="btn-u"> Search </button> &nbsp;&nbsp;
										
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

	




    <!--=== End Search Block Version 2 ===-->

    <div class="container content-sm"  style="padding:30px;">


		<div class="col-lg-offset col-lg-8" style="height:50px">
			<button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#employee_dtl_form" 
				onclick="ClearForm();"> <i class="fa fa-plus-square icon-color-white"></i> New Employee </button> 
		
			<?php  require("employee_dtl_form.php");  ?>
		</div>

		
		<div class="table-search-v2 margin-bottom-30">
			<div class="table-responsive">
				<form method="POST" enctype="multipart/form-data" action="employee_model.php?form=frmEmployeeTb" name="frmEmployeeTb" onSubmit="return validate_form(this)"
					id="frmEmployeeTb" >
					<input type="hidden" name="hdDelID" id="hdDelID"  value="">
					<input type="hidden" name="hdCancelID" id="hdCancelID"  value="">
					<table class="table table-bordered table-striped">
						<thead>
							<tr valign="middle">
								<th> &nbsp; </th>
								<th>Employee Code</th>
								<th class="hidden-sm">Name / Nick Name (TH)</th>
								<th>Name (EN)</th>
								<th>Position</th>
								<th>Start Job Date</th>
								
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<?php 
								while($row=mysql_fetch_array($result_employee)) 
								{
									
									if($row["EMP_CODE"] == $_REQUEST['focus'])
									{
										echo '<tr style = "background-color: #ffffbb">';
										echo '<script> history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'" ); </script>';
									}else
									{
										echo '<tr>';
									}
									
									
									echo '<td width="78px">
												<ul class="list-inline table-buttons">
													<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#employee_dtl_form" onclick="getDataAjax('.$row["EMP_ID"].')"><i class="fa fa-edit"></i> Edit</button></li>
												</ul>
											</td>';
									echo '<td width="110px"> <p>'.$row["EMP_CODE"].'</p> </td>';
									echo '<td> <p>'.$row["NAME_TH"].'</p> </td>';
									echo '<td> <p>'.$row["NAME_EN"].'</p> </td>';
									echo '<td width="120px">'.$row["POSITION_NAME_EN"].'</td>';
									echo '<td width="120px"> <p>'.$row["START_JOB_DATE"].'</p> </td>';
									
									
									
									echo '<td  width="84px">
													<ul class="list-inline table-buttons">
														<li>';
										
														if ($row["IS_DEL"] == 1)
														{
															echo '			<button type="submit" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["EMP_ID"] .')"> 
																				<i class="fa fa-trash-o"></i> Delete </button>';
															
														}else if ($row["ACTIVE_FLAG"] == 'Y')
														{
															echo '			<button type="submit" class="btn-u btn-u-sm btn-u-orange" onclick ="return confirmCancel('.$row["EMP_ID"] .')"> 
																				<i class="fa fa-times-circle"></i> Cancel</button>';
														} else 
														{
															echo '			<button type="submit" class="btn-u btn-u-sm btn-u-green" onclick ="return confirmCancel('.$row["EMP_ID"] .')"> 
																				<i class="fa fa-check-circle"></i> Enable</button>';
														}
													
									echo'			</li>
								   
													</ul>
												</td>
											</tr>';
								}
							?>
						
						</tbody>
					</table>
				</form>
				
				
				<!-- End Test Form -->
			</div>    
		</div>    
        <!-- End Table Search v2 -->
		


        
    </div>
	<?php require("include_footer.php"); ?>
     
</div><!--/End Wrapepr-->

<?php require("include_js.php"); ?>

</body>
</html> 
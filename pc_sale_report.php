<?php session_start();

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    $arr_order_dtl  = array();

    require_once('config.php');
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $date_fr   = $_REQUEST["date_fr"];
    $date_to   = $_REQUEST["date_to"];
    $yest_date = date('Y-m-d', strtotime("-1 day"));
    $this_date = date('Y-m-d');
    $this_time = date('Hi');

    $str_time  = $this_time;

    (int) $this_time;

    if ($date_fr == "") { $date_fr = date('Y-m-d'); } else {  $date_fr = date('Y-m-d', strtotime($date_fr)); }
    if ($date_to == "") { $date_to = date('Y-m-d'); } else {  $date_to = date('Y-m-d', strtotime($date_to)); }

    $amh_pc   = new AMH_PC();

    $arr_rpt_dtl  = $amh_pc->get_sale_rpt($user_name, $date_fr, $date_to);

    //$day_of_week = date('D', strtotime('2015-01-21'));
    $show_date_fr = date('j M Y', strtotime($date_fr));
    $show_date_to = date('j M Y', strtotime($date_to));

    $show_each_item = "";

    foreach ($arr_rpt_dtl as $rpt_dtl)
    {
        $created_time    = date('j M Y H:i', strtotime($rpt_dtl["UPDATE_DATE"]));
        $the_quantiy     = round($rpt_dtl["QTY"]);

        if (($rpt_dtl["UPD_DATE"] == $this_date) || (($rpt_dtl["UPD_DATE"] == $yest_date) && ($this_time < 1100)))
        {
            $po_status    = "<span class='fnt_green2'> - รายการของวันนี้</span>";
            $read_only    = "";
            $delete_icon  = "<img src='".$app_img_path."icon_delete64.png' border='0' onclick='del_item({$rpt_dtl["SALE_REPORT_DTL_ID"]});'><img src='".$app_img_path."icon_checked48.png' border='0' style='margin-left:40px;'>";
            $qty_field    = "<input type='TEXT' class='gen_input_txt fnt50 w120px' id='QTY_{$rpt_dtl["SALE_REPORT_DTL_ID"]}' name='QTY_{$rpt_dtl["SALE_REPORT_DTL_ID"]}' value='{$the_quantiy}' onblur='upd_item({$rpt_dtl["SALE_REPORT_DTL_ID"]}, this);' {$read_only}>";
            $bg_color    = "#ffffff";
        }
        else
        {
            $po_status   = "<span class='fnt_red'> - รายการนี้ข้ามวันไปแล้ว</span>";
            $read_only   = "readonly";
            $delete_icon = "";
            $qty_field   = $the_quantiy;
            $bg_color    = "#baffba";
        }

        $show_each_item .= "<div class='row_item fnt20' style='background: {$bg_color} url(\"".$pro_img_path.$rpt_dtl["PRD_IMG"]."\") no-repeat right center; background-size: 170px 170px;'>\r\n";
        $show_each_item .= "    <div class='fnt24 fnt_blue'>{$created_time} <span class='fnt_green2'>({$rpt_dtl["CUS_NAME"]})<input type='HIDDEN' name='CUSID_{$rpt_dtl["SALE_REPORT_DTL_ID"]}' id='CUSID_{$rpt_dtl["SALE_REPORT_DTL_ID"]}' value='{$rpt_dtl["CUS_ID"]}'></span>{$po_status}</div>\r\n";
        $show_each_item .= "    <div>\r\n";
        //$show_each_item .= "        <div class='i_object w60 fnt36'><input type='TEXT' class='gen_input_txt fnt32 w80' id='PRODUCT_PC_ID-{$rpt_dtl["ORDER_DTL_ID"]}' value='{$rpt_dtl["PRODUCT_NAME_EN"]}' {$read_only}></div>\r\n";
        $show_each_item .= "        <div class='i_object w60 fnt36'>{$rpt_dtl["PRODUCT_NAME_EN"]} - {$rpt_dtl["CUS_TYPE_NAME"]}</div>\r\n";
        $show_each_item .= "        <div class='i_object w20 fnt36'>จำนวน {$qty_field}</div>\r\n";
        $show_each_item .= "    </div>\r\n";
        $show_each_item .= "    <div class='panel_icon'>{$delete_icon}</div>\r\n";
        $show_each_item .= "</div>";
    }

    if ($show_each_item == "") { $show_each_item = "<div class='fnt20 fnt_yellow'>No item on these date</div>"; }
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Amsel Health - Sales Report</title>
<link rel="stylesheet" type="text/css" href="incl/jquery_ui/css/sunny/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" href="stylesheets/amh_pc.css">
<link href='https://fonts.googleapis.com/css?family=Exo+2:700,400' rel='stylesheet' type='text/css'>
<style type='text/css'>
body
{
    background-color: #000;
    font-family: Arial;
    margin: 0;
    padding: 10px;
}
.link_white
{
    color:#FFF;
    text-decoration: none;
}

.link_white:hover
{
    color:#FF9;
}
</style>
<script type="text/javascript" src="incl/jquery_ui/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="incl/jquery_ui/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript">
function page_startup()
{
    start_calendar();
}

function start_calendar()
{
    $('.gen_input_calen').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'd M yy'
    });

    $('#ui-datepicker-div').css("font-size","32px");
}

function del_item(sale_report_dtl_id)
{
    if (confirm('ยืนยันลบรายการ'))
    {
        $.post( "pc_sale_rpt_upd.php", { action: "del", sale_report_dtl_id: sale_report_dtl_id })
            .done(function(data)
            {
                alert('ทำการลบ'+data);
                submit_date();
            });
    }
}

function upd_item(sale_report_dtl_id, the_obj)
{
    the_text = the_obj.id;
    arr_text = the_text.split("_");

    the_field = arr_text[0];
    the_value = the_obj.value;

    $.post( "pc_sale_rpt_upd.php", { action: "upd", sale_report_dtl_id: sale_report_dtl_id, field: the_field, value: the_value })
        .done(function(data)
        {
            //submit_date();
        });
}

function submit_date()
{
    document.getElementById('sale_rpt_date_form').submit();
}
</script>
</head>
<body onload='page_startup();'>
<div class='the_page'>
<div class='the_page_label'>SALE REPORT</div>
<form action='pc_sale_report.php' method='POST' id='sale_rpt_date_form' name='sale_rpt_date_form'>
<div class='order_info fnt36pt' style='text-align:center;'>
Report from: <input type='TEXT' class='gen_input_calen fnt36pt w300px' readonly id='date_fr' name='date_fr' value='<? echo $show_date_fr; ?>'>
to: <input type='TEXT' class='gen_input_calen fnt36pt w300px' readonly id='date_to' name='date_to' value='<? echo $show_date_to; ?>'>
<div style='text-align:center; margin:10pt 0 0;'><button type='BUTTON' class='gen_btn_silver fnt36pt fnt_black' onclick='submit_date();'>Refresh</button></div>
</div>
</form>
<? echo $show_each_item; ?>
<div style='min-height:120px;'>&nbsp;</div>
<div class='plus_item fnt40 fnt_white'>
    <a href='pc_main.php' class='link_white'><img src='<? echo $app_img_path; ?>icon-navg-back100.png' border='0' style='vertical-align:middle;'> กลับหน้าหลัก</a>
    <a href='pc_sale_report_add.php' class='link_white'><img src='<? echo $app_img_path; ?>plus100.png' border='0' style='vertical-align:middle; margin-left:50px;'> เพิ่ม report</a>
</div>
</div>
<div style='display:none;'>
<div class='fnt16 fnt_white'>from: <? echo date('Y-m-d', strtotime($show_date_fr)); ?></div>
<div class='fnt16 fnt_white'>to: <? echo date('Y-m-d', strtotime($show_date_to)); ?></div>
</div>
</body>
</html>
<?php
}
else
{
    echo "<h2>Please login</h2>";
}
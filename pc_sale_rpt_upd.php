<?php 

error_reporting(E_ALL);
ini_set('display_errors', 1);
 
session_start();

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $amh_pc  = new AMH_PC();
    $str_ret = "";
    $aut_id  = $amh_pc->get_aut_user(" AUT_UNAME = '{$user_name}' ");

    if ($_REQUEST["action"] == "add")
    {
        $sale_report_id = $amh_pc->add_sale_rpt_dtl($user_name, $_POST, "", $_REQUEST["cus_id"]);
        $amh_pc->add_sale_rpt_rel($aut_id[0]["AUT_ID"], $sale_report_id, $user_name);
    }

    if ($_REQUEST["action"] == "del")
    {
        $str_ret = $amh_pc->del_sale_rpt_dtl($user_name, $_REQUEST["sale_report_dtl_id"]);
    }

    if ($_REQUEST["action"] == "upd")
    {
        $str_ret = $amh_pc->upd_sale_rpt_dtl($user_name, $_REQUEST["sale_report_dtl_id"], $_REQUEST["field"], $_REQUEST["value"] );
    }
}
echo $str_ret;
<?php
require_once("class_amh_db.php");
require_once("include_function.php");
//require_once("validatelogin.php");

class AMH_user_acc extends AMH_DB
{
	public function get_aut_user_lab()
    {
        $arr_ret = array();
		
		$sql = "SELECT AUT_ID, AUT_UNAME, me.EMP_CODE, CONCAT(AUT_UNAME,' : ',me.FNAME_TH,' ',me.LNAME_TH,' (',me.NICK_NAME ,') ') AS EMP_NAME
							FROM  aut_user AS au
								LEFT JOIN mst_employee AS me ON au.EMP_ID = me.EMP_ID
								ORDER BY EMP_CODE; ";
        $result = $this->mysqli->query($sql) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
        while ($row = $result->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row;
        }
        $result->free();
		
        return json_encode($arr_ret);
	}
	
	public function get_aut_user($aut_uname = "")
    {
        $arr_ret = array();
		$where = "";
		if ($aut_uname != "")
		{
			$where .= "WHERE au.AUT_UNAME = '".$aut_uname."' ";
			
		}
				
		$sql = "SELECT AUT_ID, AUT_UNAME,  au.EMP_ID, me.EMP_CODE, CONCAT(me.FNAME_TH,' ',me.LNAME_TH,' (',me.NICK_NAME ,')') AS EMP_NAME, mp.POSITION_NAME_EN
								,ar.AUT_ROLE_NAME
							FROM  aut_user AS au
							  LEFT JOIN mst_employee AS me ON au.EMP_ID = me.EMP_ID
							  LEFT JOIN mst_position AS mp ON me.POSITION_ID = mp.POSITION_ID
							  LEFT JOIN mst_aut_role AS ar ON au.AUT_ROLE_ID = ar.AUT_ROLE_ID ";
		$order = "	ORDER BY AUT_UNAME ; ";					  
        $result = $this->mysqli->query($sql.$where.$order) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
        while ($row = $result->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row;
        }
        $result->free();
		
        return json_encode($arr_ret);
	}
	
	public function get_emp($emp_id="")
	{
		$arr_ret = array();
		$sql = "SELECT EMP_ID, EMP_CODE, CONCAT(EMP_CODE, ' : ' ,FNAME_TH ,' ', LNAME_TH , '(' ,NICK_NAME , ')') AS EMPLOYEE_NAME
					FROM mst_employee AS me ";
					
		$where = "";
		if ($emp_id == "")
		{
			$where = " WHERE me.EMP_ID NOT IN (SELECT EMP_ID FROM aut_user) ";
		}else
		{
			$where = " WHERE me.EMP_ID =".$emp_id." ";
		}
		
		$order = "	ORDER BY EMP_CODE;  ";
		
        $result = $this->mysqli->query($sql.$where.$order) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
        while ($row = $result->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row;
        }
        $result->free();
		
        return json_encode($arr_ret);
		
	}
	
	public function get_emp_dtl($aut_id="")
	{
		$arr_ret = array();
		// Select Detail TRN_LPT_CHART_ID
		$sql = "SELECT AUT_UNAME, AUT_PW, EMP_ID, AUT_ROLE_ID, ACTIVE_FLAG 
							FROM aut_user	 ";
		
		$where = " WHERE AUT_ID = ".$aut_id ;
		
        $result = $this->mysqli->query($sql.$where) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
        while ($row = $result->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row;
        }
        $result->free();
		
        return json_encode($arr_ret);
		
	}
	public function check_dup_user($uname="")
	{
		
		$sql="SELECT AUT_UNAME FROM aut_user WHERE AUT_UNAME = '".$uname."'; ";
						
		$result = $this->mysqli->query($sql.$where) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
		
		if ($result->num_rows > 0)
		{
			while ($row = $result->fetch_array(MYSQLI_BOTH))
			{
				echo $row['AUT_UNAME'];
				
			}
		}else{
			echo "";
		}
	}
}

	
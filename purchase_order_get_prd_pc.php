<?php 

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

require_once('class_amh_db.php');
require_once('class_amh_pc.php');

$the_date   = $_REQUEST["the_date"];

$amh_pc     = new AMH_PC();

if ($the_date != "")
{
    $the_date = $amh_pc->swap_date($the_date);
}
else
{
    $the_date = date('Y-m-d');
}

$arr_prd_pc = $amh_pc->get_product_pc($the_date, "");

echo json_encode($arr_prd_pc);
<?php 
    require("include_function.php");
    require('validatelogin.php'); 
    require('config.php');


    /* [start] for Chart, added by Nattapong Dns. 2016-07-20 */

    $arr_order_dtl = array();
    $arr_month     = array();

    $arr_month["01"] = "มกราคม";
    $arr_month["02"] = "กุมภาพันธ์";
    $arr_month["03"] = "มีนาคม";
    $arr_month["04"] = "มษายน";
    $arr_month["05"] = "พฤษภาคม";
    $arr_month["06"] = "มิถุนายน";
    $arr_month["07"] = "กรกฎาคม";
    $arr_month["08"] = "สิงหาคม";
    $arr_month["09"] = "กันยายน";
    $arr_month["10"] = "ตุลาคม";
    $arr_month["11"] = "พฤศจิกายน";
    $arr_month["12"] = "ธันวาคม";

    $this_year  = date(Y);
    $this_month = date(m);
    $name_month = $arr_month[$this_month];

    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $amh_pc = new AMH_PC();

    $arr_report_dtl = $amh_pc->get_sale_report_month($this_month, $this_year);

    $num_amount = array();
    $sales_amount = 0;

    $num_amount[0] = 0;
    $num_amount[1] = 0;
    $num_amount[2] = 0;
    $num_amount[3] = 0;

    $num_qty[0] = 0;
    $num_qty[1] = 0;
    $num_qty[2] = 0;
    $num_qty[3] = 0;

    foreach ($arr_report_dtl as $report_dtl)
    {
        $sales_amount = $sales_amount + $report_dtl["LTP_PRICE"];
        $type_id = $report_dtl["PRODUCT_TYPE_ID"];
        $num_amount[$type_id] = $num_amount[$type_id] + $report_dtl["LTP_PRICE"];
        $num_qty[$type_id] = $num_qty[$type_id] + $report_dtl["QTY"];
    }

    /* [end] for Chart, added by Nattapong Dns. 2016-07-20 */


?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMESEL NUTRACEUTICAL SYSTEM | HOME</title>
    <script src="assets/js/ccchart.js" charset="utf-8"></script>
    <?php $current_menu = "home"; ?>
    <!--=== End Breadcrumbs ===-->
        
    <?php require("include_headtag.php"); ?>
    
</head> 
<body>    
    <script type="text/javascript">

    var name_month = '<?php echo $name_month;?>';
    var this_year  = '<?php echo $this_year;?>';
    var prd_type1  = <?php echo $num_amount[1]; ?>;
    var prd_type2  = <?php echo $num_amount[2]; ?>;
    var prd_type3  = <?php echo $num_amount[3]; ?>;
    var qty_type1  = <?php echo $num_qty[1]; ?>;
    var qty_type2  = <?php echo $num_qty[2]; ?>;
    var qty_type3  = <?php echo $num_qty[3]; ?>;

        function genChart()
        {
            var chartdata79 = {

                "config": {
                "title": "Summary Sales Amount Pie Chart",
                "subTitle": "แสดงยอดขาย (บาท)",
                "type": "pie",
                "useVal": "yes",
                "pieDataIndex": 0,
                "colNameFont": "100 18px 'Arial'",
                "pieRingWidth": 280,
                "pieHoleRadius": 60,
                "textColor": "#888",
                "bg": "#fff"
                },

                "data": [
                    ["ประเภท",name_month+" "+this_year],
                    ["เม็ด", prd_type1],
                    ["ผง" , prd_type2],
                    ["เซต" , prd_type3]
                ]
            };

            ccchart.init('hoge', chartdata79);
        }
        
        function genChart2()
        {
            var chartdata79 = {

                "config": {
                "title": "Summary Sales QTY Pie Chart",
                "subTitle": "แสดงยอดขาย (หน่วย)",
                "type": "pie",
                "useVal": "yes",
                "pieDataIndex": 0,
                "colNameFont": "100 18px 'Arial'",
                "pieRingWidth": 280,
                "pieHoleRadius": 60,
                "textColor": "#888",
                "bg": "#fff"
                },

                "data": [
                    ["ประเภท",name_month+" "+this_year],
                    ["เม็ด", qty_type1],
                    ["ผง" , qty_type2],
                    ["เซต" , qty_type3]
                ]
            };

            ccchart.init('hoge2', chartdata79);
        }
         
         
    </script>
    <div id="wrap"  class="wrapper">

        <?php require("include_header.php"); ?>

        <!--=== Breadcrumbs ===-->
        <div class="breadcrumbs breadcrumbs-dark">
            <div class="container">
                <h1 class="pull-left">WELCOME TO AMSEL HEALTH SYSTEM</h1>
                
            </div>
        </div>
        <div id="divDispChart" style="" class="box-content;text" align="center" >
            <div class="row">
                <div class="col-sm-6">
                    <div class="thumbnails-v1">
                        <canvas id="hoge"></canvas>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="thumbnails-v1">
                        <canvas id="hoge2"></canvas>
                    </div>
                </div>
            </div>
            
        </div>
        
        <script>
            genChart();
            genChart2();
            
        </script>
        
         <?php 
        require("include_footer.php"); 
         ?>
    </div><!--/End Wrapepr-->
    
<?php require("include_js.php"); ?>



</body>
</html> 
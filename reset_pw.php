<?php 
	require("include_function.php");
	require('validatelogin.php'); 
	require('config.php'); 
 ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMESEL HEALTH SELECT | RESET PASSWORD</title>
	<?php $current_menu = "reset_pw"; ?>
	<!--=== End Breadcrumbs ===-->
		
	<?php require("include_headtag.php"); ?>
	
</head> 

<body>    
	
	<div id="wrap"  class="wrapper">

		<?php require("include_header.php"); ?>

		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs breadcrumbs-dark">
			<div class="container">
				<h1 class="pull-left">RESET PASSWORD</h1>
				
			</div>
		</div>
		<?php require("user_account_dtl_form.php"); ?>
		
		<script type="text/javascript">
			document.getElementById('hdMode').value = "reset_pw";
			document.getElementById('labMode').innerHTML = "<i class='fa  fa-key'></i>Reset Password";
			document.getElementById("btClosePop").style.display= "none";
			document.getElementById('u_acc_dtl_form').className  = "form-horizontal";
			document.getElementById('txbUName').value = "<?php echo $_SESSION["aut_uname"]; ?>";
			document.getElementById('txbUName').readOnly = true;
			document.getElementById('txbUName').style.backgroundColor = "#f5f5f5";
			document.getElementById('txbNewPw').value = "";
			document.getElementById('txbConfirmPw').value = "";
			document.getElementById("divEmpCode").style.display= "none";
			document.getElementById("divUname").style.paddingTop = "17px";
			document.getElementById("divRole").style.display= "none";
			document.getElementById("divEnable").style.display= "none";
			document.getElementById("btSave").style.display = "";
			document.getElementById("btSaveNew").style.display = "none";

		</script>
		 <?php require("include_footer.php"); ?>
	</div><!--/End Wrapepr-->

	<?php require("include_js.php"); ?>

</body>
</html> 
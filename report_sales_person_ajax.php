<?php
	require_once("class_report_sales_person.php");
	require_once("include_function.php");
	require_once("config.php");
	
	$amh_rp_sale = new AMH_rp_sale();
	
	
	if ($_POST["operate"] == "get_ddl_pc")
	{
		echo $amh_rp_sale->get_ddl_pc();
	}
	else if ($_POST["operate"] == "get_prd_type")
	{
		echo $amh_rp_sale->get_prd_type();
	}
	else if ($_POST["operate"] == "get_data_person")
	{
		if ($_POST["data_type"] == "sale")
		{
			echo $amh_rp_sale->get_data_sale($_POST["aut_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"],$temp_excel_path,$pro_img_path);
			
		}else if ($_POST["data_type"] == "order")
		{
			echo $amh_rp_sale->get_data_order($_POST["aut_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"],$temp_excel_path,$pro_img_path);
			
		}
		
		
	}
	else if ($_POST["operate"] == "get_data_person_daily")
	{
		if ($_POST["data_type"] == "sale")
		{
			echo $amh_rp_sale->get_data_sale_daily($_POST["aut_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"],$temp_excel_path,$pro_img_path);
			
		}else if ($_POST["data_type"] == "order")
		{
			echo $amh_rp_sale->get_data_order_daily($_POST["aut_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"],$temp_excel_path,$pro_img_path);
			
		}
		
		
	}
	
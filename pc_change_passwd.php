<?php  session_start();

error_reporting(E_ERROR);
ini_set('display_errors', 1);

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $amh_pc  = new AMH_PC();
    $str_ret = $amh_pc->update_password($user_name, $_REQUEST["pass_word"]);
    echo $str_ret;
}
else
{
    echo "Please re-login!";
}
<!--=== Header ===-->    
    <div class="header">
        <div class="container">
            <!-- Logo -->
			
			<a class="logo" href="index.php">
                <img src="images/logo.png" alt="Logo">
            </a>
            <!-- End Logo -->
            
            <!-- Topbar -->
            <div class="topbar">
                <ul class="loginbar pull-right">
                    <li>Welcome :  <?php echo $_SESSION["user_name"]; ?></li>  
                    <li class="topbar-devider"></li>   
                    <li><a href="reset_pw.php"><font color="#ff884d"><b><u>Reset Password</u></b></font></a></li>  
                    <li class="topbar-devider"></li>   
                    <li><a href="logout.php">Logout</a></li>   
                </ul>
            </div>
            <!-- End Topbar -->

            <!-- Toggle get grouped for better mobile display -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
            </button>
            <!-- End Toggle -->
        </div><!--/end container-->
		<div style="height:25px" ></div>
        <!-- Collect the nav links, forms, and other content for toggling -->
		<?php if ($current_menu != "reset_pw") { ?>
        <div id="divMenu" class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
            <div class="container">
                <ul class="nav navbar-nav">
                    <!-- Home -->
                    <li class="<?php if ($current_menu == "home") { echo "active"; } else { echo ""; } ?>">
                        <a href="home.php" class="dropdown-toggle" >
                            Home
                        </a>
				
                    </li>
                    <!-- End Home -->
					<!-- Data Entry-->
					<li class="dropdown <?php if ($current_menu == "pc_backend" || $current_menu == "sales_report"  ) { echo "active"; } else { echo ""; } ?>">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            Data Entry
                        </a>
						<ul class="dropdown-menu">
                            <li class="<?php  if ($current_menu == "pc_backend") { echo "active"; } else { echo ""; } ?>">
                                <a href="pc_back.php">Order from PC</a>
                            </li>
							
							<li class="<?php  if ($current_menu == "sales_report" ) { echo "active"; } else { echo ""; } ?>">
                                <a href="sales_report.php">Daily Sales form PC</a>
                            </li>
                                                       
                        </ul>
                    </li>
					
					<!--End DataEntry-->
					
					<!-- Sales transaction    
				
                    <li class="<?php //if ($current_menu == "sales_transaction") { echo "active"; } else { echo ""; } ?>">
                        <a href="sales_transaction.php" class="dropdown-toggle" >
                            Sales transaction
                        </a>
                       
                    </li> -->

                    <!-- Product -->                        
                    <li class="dropdown <?php if ($current_menu == "product" || $current_menu == "product_sales" || $current_menu == "product_pc" ) { echo "active"; } else { echo ""; } ?>">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            Product
                        </a>
						<ul class="dropdown-menu">
                            <li class="<?php  if ($current_menu == "product") { echo "active"; } else { echo ""; } ?>">
                                <a href="product_master.php">Product Master</a>
                            </li>
							<!--
                            <li class="<?php  //if ($current_menu == "product_sales" ) { echo "active"; } else { echo ""; } ?>">
                                <a href="product_sales.php">Product Sales</a>
                            </li>-->
							<li class="<?php  if ($current_menu == "product_pc" ) { echo "active"; } else { echo ""; } ?>">
                                <a href="product_pc.php">Product PC</a>
                            </li>
                                                       
                        </ul>
                       
                    </li>
                    <!-- End Pages -->
					
					<!-- Purchase Order -->                        
                    <li class="<?php if ($current_menu == "purchase_order") { echo "active"; } else { echo ""; } ?>">
                        <a href="purchase_order.php" class="dropdown-toggle" >
                            Purchase Order
                        </a>
                       
                    </li>
                    <!-- End Purchase Order -->
				
					<!-- DO & Tax Invoice 
					<li class="dropdown <?php //if ($current_menu == "delivery_order" || $current_menu == "tax_invoice" ) { echo "active"; } else { echo ""; } ?>">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            DO & Tax Invoice
                        </a>
						<ul class="dropdown-menu">
                            <li class="<?php  //if ($current_menu == "delivery_order") { echo "active"; } else { echo ""; } ?>">
                                <a href="delivery_order.php">Delivery Order</a>
                            </li>
                            <li class="<?php // if ($current_menu == "tax_invoice" ) { echo "active"; } else { echo ""; } ?>">
                                <a href="tax_invoice.php">Tax Invoice</a>
                            </li>
                                                       
                        </ul>
                     </li>
					< End DO & Tax  -->
				
					<!-- Purchase Order          
                    <li class="<?php //if ($current_menu == "consignment") { echo "active"; } else { echo ""; } ?>">
                        <a href="consignment.php" class="dropdown-toggle" >
                            Consignment
                        </a>
                       
                    </li>-->
                    <!-- End Purchase Order -->

                    <!-- Features -->
                    <li class="dropdown <?php if ($current_menu == "report_sale_person" || $current_menu == "report_sale_customer") { echo "active"; } else { echo ""; } ?>">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            Report
                        </a>
                        <ul class="dropdown-menu">
                            <li class="<?php  if ($current_menu == "report_sale_person") { echo "active"; } else { echo ""; } ?>">
								<a href="report_sales_person.php">Sales Report for PC</a>
                            </li>                        
                            <li class="<?php  if ($current_menu == "report_sale_customer") { echo "active"; } else { echo ""; } ?>">
                                <a href="report_sales_customer.php">Sales Report for Customer and Stock</a>
                            </li>
                        </ul>
                    </li>                    
                    <!-- End Features -->
					 <!-- Blog -->
                    <li class="dropdown <?php if ($current_menu == "employee" || $current_menu == "user_account" || $current_menu == "customer") { echo "active"; } else { echo ""; } ?>">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-cogs"  style = "font-size: 18px;"  title="Configuration"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li <?php if ($current_menu == "employee") { echo "class='active'"; } else { echo ""; } ?>>
                                <a href="employee.php" >Employee </a>
                            </li>
							<li <?php if ($current_menu == "user_account") { echo "class='active'"; } else { echo ""; } ?>>
                                <a href="user_account.php">User Account</a>
                            </li>
							<!--
                            <li <?php //if ($current_menu == "shop_station") { echo "class='active'"; } else { echo ""; } ?>>
                                <a href="shop_station.php">Shop Station</a>
                            </li> -->
							<li <?php if ($current_menu == "customer") { echo "class='active'"; } else { echo ""; } ?>>
                                <a href="customer.php">Customer</a>
                            </li>
                                                       
                        </ul>
                    </li>
                    <!-- End Blog -->
                   

                    <!-- Search Block 
                    <li>
                        <i class="search fa fa-search search-btn"></i>
                        <div class="search-open">
                            <div class="input-group animated fadeInDown">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn-u" type="button">Go</button>
                                </span>
                            </div>
                        </div>    
                    </li>
                    End Search Block -->
                </ul>
            </div><!--/end container-->
        </div><!--/navbar-collapse-->
		<?php } ?>
    </div>
    <!--=== End Header ===-->
<?php
	ob_start();
	require("connect.php");
	session_start();
	$query_str="";
	if ($_GET["form"] == "aut_dtl")
	{
		$hdMode = $_POST['hdMode'];
		//*** Data Field ****
		
		$ddlEmp=$_POST['ddlEmp'];
		$ddlRole=$_POST['ddlRole'];
		$txbUName=$_POST['txbUName'];
		$txbNewPw=$_POST['txbNewPw'];
		$chkEnable=$_POST['chkEnable'];
		echo "Post ddlEmp=".$_POST['ddlEmp']."<br/>";
		echo "Post ddlRole=".$_POST['ddlRole']."<br/>";
		echo "Post txbUName=".$_POST['txbUName']."<br/>";
		
		if ($hdMode=="new")
		{
			
			$sql = sprintf("INSERT INTO aut_user(
									AUT_UNAME
									,AUT_PW
									,EMP_ID
									,AUT_ROLE_ID
									,CREATED_BY
									,CREATED_DATE
									,UPDATE_DATE
									,ACTIVE_FLAG
								) VALUES (
									'%s'  -- AUT_UNAME - IN varchar(20)
									,'%s'  -- AUT_PW - IN varchar(256)
									,%d   -- EMP_ID - IN int(11)
									,%d   -- AUT_ROLE_ID - IN int(11)
									,'%s'  -- CREATED_BY - IN varchar(20)
									,NOW()  -- CREATED_DATE - IN datetime
									,NOW()  -- UPDATE_DATE - IN datetime
									,'Y'  -- ACTIVE_FLAG - IN varchar(1)
								); "
				, mysql_real_escape_string($txbUName)
				, mysql_real_escape_string(hash('sha256',$txbNewPw))
				, mysql_real_escape_string($ddlEmp)
				, mysql_real_escape_string($ddlRole)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				);
				
			if($result=mysql_query($sql)){
				$query_str="?focus=".$txbUName;
				echo "Insert OK ^.^";
			}else{
				$query_str="?error_ins=".$txbUName;
				echo "SQL Error = ".$sql."<br/>";
			}
		}else if ($hdMode=="edit")
		{
			if ($txbNewPw== "")
			{
				$sql = sprintf("UPDATE aut_user
								SET
									AUT_ROLE_ID = %d 
									,UPDATE_BY = '%s' 
									,UPDATE_DATE = NOW() 
									,ACTIVE_FLAG = '%s' 
								WHERE EMP_ID = %d ; "
				, mysql_real_escape_string($ddlRole)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($chkEnable)
				, mysql_real_escape_string($ddlEmp)
				);
				
			}else
			{
				$sql = sprintf("UPDATE aut_user
								SET
									AUT_ROLE_ID = %d
									,AUT_PW = '%s' 
									,UPDATE_BY = '%s' 
									,UPDATE_DATE = NOW() 
									,ACTIVE_FLAG = '%s'
								WHERE EMP_ID = %d ; "
				, mysql_real_escape_string($ddlRole)
				, mysql_real_escape_string(hash('sha256',$txbNewPw))
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($chkEnable)
				, mysql_real_escape_string($ddlEmp)
				);
			}
			//echo "SQL Update = ".$sql."<br/>";
		}else if ($hdMode=="reset_pw")
		{
			$sql = sprintf("UPDATE aut_user
								SET
									AUT_PW = '%s' 
									,UPDATE_BY = '%s' 
									,UPDATE_DATE = NOW() 
								WHERE AUT_UNAME = '%s' ; "
				, mysql_real_escape_string(hash('sha256',$txbNewPw))
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($txbUName)
				);

		}
		
		
		if($result=mysql_query($sql)){
			$query_str="?focus=".$txbUName;
			echo "Update aut_user OK ^.^<br/>SQL = ".$sql;
		}else{
			$query_str="?error_ins=".$txbUName;
			echo "SQL Error = ".$sql."<br/>";
		}
		

	} else if ($_GET["form"] == "frmEmployeeTb") 
	{
		$hdDelID = $_POST['hdDelID'];
		$hdCancelID = $_POST['hdCancelID'];
		echo "</br>DelID=".$hdDelID;
		echo "</br>CancelID=".$hdCancelID;
		if ($_POST['hdDelID'] != "")
		{
			$sql = "DELETE FROM mst_employee
					WHERE EMP_ID = ".$hdDelID."; ";
			
			if($result=mysql_query($sql)){
				echo "Delete OK ^.^";
			}else{
				echo "SQL Error = ".$sql."<br/>";
			}
		} else if ($_POST['hdCancelID'] != "")
		{
			
			$sql = sprintf("UPDATE mst_employee
									SET ACTIVE_FLAG = CASE WHEN ACTIVE_FLAG = 'Y' THEN 'N' ELSE 'Y' END 
										,UPDATE_BY = '%s' 
										,UPDATE_DATE = Now() 
									WHERE EMP_ID = %d;"
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($hdCancelID)
				);
			if($result=mysql_query($sql))
			{
				$query_str="?focus=".$txbProCode;
				echo "Update OK ^.^=".$sql."<br/>";
			}else
			{
				$query_str="?error_ins=".$txbProCode;
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
		
	}
	if ($hdMode=="reset_pw")
	{
		header("Location: home.php".$query_str); 
	}else {
		header("Location: user_account.php".$query_str); 
	}
	ob_end_flush();
?>
<?php
/* Class name
   AMH_Customer

   public function 
   mst_customer($str_search) return array of customer info (example: )
*/
require_once("class_amh_db.php");
require_once("include_function.php");
//require_once("validatelogin.php");

class AMH_inv extends AMH_DB
{
    public function inv_main($inv_no="", $start_date ="", $end_date = "")
    {
        $arr_ret = array();

        $str_where = "1";
        if ($inv_no != "")
        {
			$str_where  .= " AND  (INV_NO   LIKE '%".$inv_no."%') ";
		}
		if ($start_date !="")
        {
			//if ($str_where !="1" ) $str_where .= " AND ";
			$str_where .= " AND (INV_DATE BETWEEN '".printDate($start_date)."' AND '".printDate($end_date)."') ";
        }

		$arr_ret = array();
		
        $sql_sel = " SELECT ivm.INV_ID, ivm.INV_NO, DATE_FORMAT(ivm.INV_DATE, '%d-%m-%Y') AS INV_DATE,
							ivm.PO_ID, ivm.CREDIT_TERM, ivm.NOTE, ivm.CUS_ID, ivm.CUS_NAME, ivm.CUS_CONTACT, ivm.CUS_ADDRESS,
							ivm.DISTRICT_CODE, ivm.PROVINCE_CODE, ivm.CUS_ZIP, ivm.CUS_TEL, ivm.CUS_FAX, ivm.TAX_NUMBER, ivm.INV_TOTAL, ivm.DISCOUNT_PER, 
							ivm.VAT_PER, ivm.GRAND_TOTAL, pm.PO_NO
						FROM inv_main AS ivm 
							LEFT JOIN po_main AS pm ON ivm.PO_ID = pm.PO_ID 
						WHERE ".$str_where." ORDER BY ivm.INV_NO DESC; ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>inv_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();
		
		
        return json_encode($arr_ret);
    }
	
	public function get_ddl_customer()
    {
        $arr_ret = array();

        $sql_sel = "SELECT CUS_ID, CUS_NAME 
						FROM mst_customer
						WHERE ACTIVE_FLAG = 'Y'
						ORDER BY CUS_NAME ;  ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }
	
	public function get_ddl_po($cus_id="")
    {
		
        $arr_ret = array();
		$where = "";
		if ($cus_id != "") 
		{
			$where = " AND pm.CUS_ID =".$cus_id;
		}
			
		//*** Select กรณี PO ที่ยังไม่ถูกออก Inv. หมดแล้วเท่านั้น
        $sql_sel = "SELECT po_sum.PO_ID, po_sum.PO_NO, po_sum.PO_QTY, IFNULL(inv_sum.INV_QTY,0) AS INV_QTY
						FROM
							(SELECT pm.PO_ID, pm.PO_NO, SUM(pd.QTY) AS PO_QTY
							FROM po_main AS pm
								INNER JOIN po_dtl AS pd ON pm.PO_ID = pd.PO_ID
							WHERE pm.ACTIVE_FLAG = 'Y' AND pm.OBJ_ID = 1
							GROUP BY pm.PO_ID, pm.PO_NO) po_sum
							LEFT JOIN 
								(SELECT inv.PO_ID, SUM(ivd.QTY) AS INV_QTY
								FROM inv_main AS inv
									INNER JOIN inv_dtl AS ivd ON inv.INV_ID = ivd.INV_ID
								WHERE inv.ACTIVE_FLAG = 'Y' 
								GROUP BY inv.PO_ID ) inv_sum ON
								po_sum.PO_ID = inv_sum.PO_ID 
						WHERE po_sum.PO_QTY > IFNULL(inv_sum.INV_QTY,0) ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }
	
	public function get_customer_dtl($cus_id)
	{
		$arr_ret = array();
		$where = "";
		if ($cus_id != "") 
		{
			$where = " AND CUS_ID =".$cus_id;
		}
		
        $sql_sel = "SELECT *
						FROM mst_customer 
						WHERE ACTIVE_FLAG = 'Y' ".$where;
						
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
	}
	public function get_inv_dtl_from_po_id($po_id)
	{
		$arr_ret = array();

        $sql_sel = "SELECT po_sum.PO_ID, po_sum.PO_NO, po_sum.CUS_ID,
							po_sum.CUS_NAME	,IFNULL (po_sum.CUS_CONTACT,'') as CUS_CONTACT, po_sum.CUS_ADDRESS,po_sum.PROVINCE_CODE,po_sum.DISTRICT_CODE,po_sum.CUS_ZIP,po_sum.CUS_TEL,po_sum.CUS_FAX,po_sum.TAX_NUMBER,
							po_sum.QNO, po_sum.PRODUCT_ID, po_sum.PRODUCT_SALE_CODE, po_sum.PRODUCT_NAME, po_sum.DESCRIPTION, 
							po_sum.UNIT_PRICE, po_sum.PO_QTY - IFNULL(inv_sum.INV_QTY,0) AS INV_QTY, 
							po_sum.UNIT_PRICE * (po_sum.PO_QTY - IFNULL(inv_sum.INV_QTY,0) ) AS AMOUNT,
							po_sum.DISCOUNT_PER, po_sum.VAT_PER
						FROM
							(SELECT pm.PO_ID, pm.PO_NO, pm.CUS_ID
								,pm.CUS_NAME	,pm.CUS_CONTACT,pm.CUS_ADDRESS,pm.PROVINCE_CODE,pm.DISTRICT_CODE,pm.CUS_ZIP,pm.CUS_TEL,pm.CUS_FAX,pm.TAX_NUMBER
								,pd.QNO, pd.PRODUCT_ID, pd.PRODUCT_SALE_CODE, pd.PRODUCT_NAME,pd.DESCRIPTION, 
								pd.UNIT_PRICE, SUM(pd.QTY) AS PO_QTY, pm.DISCOUNT_PER, pm.VAT_PER
							FROM po_main AS pm
								INNER JOIN po_dtl AS pd ON pm.PO_ID = pd.PO_ID
							WHERE pm.ACTIVE_FLAG = 'Y' AND pm.OBJ_ID = 1 AND pm.PO_ID = ".$po_id."
							GROUP BY pm.PO_ID, pm.PO_NO, pm.CUS_ID,
								pm.CUS_NAME	,pm.CUS_CONTACT,pm.CUS_ADDRESS,pm.PROVINCE_CODE,pm.DISTRICT_CODE,pm.CUS_ZIP,pm.CUS_TEL,pm.CUS_FAX,pm.TAX_NUMBER,
								pd.QNO, pd.PRODUCT_ID, pd.PRODUCT_SALE_CODE, pd.PRODUCT_NAME, pd.DESCRIPTION, pd.UNIT_PRICE, pm.DISCOUNT_PER, pm.VAT_PER
							) po_sum
							LEFT JOIN 
								(SELECT iv.PO_ID, ivd.PRODUCT_ID, ivd.PRODUCT_SALE_CODE, SUM(ivd.QTY) AS INV_QTY
								FROM inv_main AS iv
									INNER JOIN inv_dtl AS ivd ON iv.INV_ID = ivd.INV_ID
								WHERE iv.ACTIVE_FLAG = 'Y'  AND iv.PO_ID = ".$po_id."
								GROUP BY iv.PO_ID, ivd.PRODUCT_ID, ivd.PRODUCT_SALE_CODE ) inv_sum ON
								po_sum.PO_ID = inv_sum.PO_ID and po_sum.PRODUCT_SALE_CODE = inv_sum.PRODUCT_SALE_CODE
						ORDER BY QNO;  ";
						
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
		
	}
	
	
	private function get_max_inv_no()
    {
		
        $arr_ret = array();

        $sql_sel = "SELECT CONCAT('IV',(DATE_FORMAT(DATE_ADD(Now(),INTERVAL 543 YEAR),'%y%m%d')),'-',
													LPAD((SUBSTR(IFNULL(MAX(INV_NO),'IVYYMMDD-000') ,10,3)+1),3,'0') ) AS NEW_INV_NO
						FROM inv_main 
						WHERE INV_NO LIKE CONCAT('IV',DATE_FORMAT(DATE_ADD(Now(),INTERVAL 543 YEAR),'%y%m%d'),'%'); ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
		
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret[0]["NEW_INV_NO"];

    }
	
	public function get_inv_id($inv_no)
    {
		$arr_ret = array();

        $sql_sel = "SELECT INV_ID
						FROM inv_main 
						WHERE INV_NO = '".$inv_no."'; ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
		
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret[0]["INV_ID"];
		
	}
	
	public function add_inv_dtl($posting)
	{
		
		$sql_add  = " INSERT INTO inv_dtl SET   ";
		$sql_add .= "  	INV_ID = ".$posting["p_inv_id"];
		$sql_add .= "  	,QNO = ".$posting["p_qno"];
		$sql_add .= "  	,PRODUCT_ID = ".$posting["p_pro_id"]."  ";
		$sql_add .= "  	,PRODUCT_SALE_CODE = '".$posting["p_pro_sale_code"]."'  ";
		$sql_add .= "  	,PRODUCT_NAME = '".$posting["p_pro_name"]."'  ";
		$sql_add .= "  	,DESCRIPTION = '".$posting["p_pro_desc"]."'  ";
		$sql_add .= "  	,UNIT_PRICE = ".$posting["p_unit_price"];
		$sql_add .= "  	,QTY = ".$posting["p_inv_qty"];
		$sql_add .= "  	,AMOUNT = ".$posting["p_amount"];
		$sql_add .= "  	,CREATED_BY = '".$_SESSION["aut_uname"]."'  ";
		$sql_add .= "  	,CREATED_DATE = Now()  ";
		$sql_add .= "  	,UPDATE_DATE = Now() ";


        $this->mysqli->query($sql_add) OR die("Function add_inv_dtl error:{$sql_add}");
		return "";
		
	}
	
	public function add_inv_main($posting)
    {
		
		$new_inv_no = $this ->get_max_inv_no();
        $sql_add  = " INSERT INTO inv_main SET   ";
		$sql_add .= "   INV_NO  = '".$new_inv_no."' ,";
		$sql_add .= "   INV_DATE = '".printDate($posting["p_inv_date"])."' ,";
		$sql_add .= "   PO_ID = ".$posting["p_po_id"]." ,";
		$sql_add .= "   CREDIT_TERM = ".$posting["p_credit_term"]." ,";
		$sql_add .= "   NOTE = '".$posting["p_note"]."' ,";
		$sql_add .= "   CUS_ID = ".$posting["p_cus_id"]." ,";
		$sql_add .= "   CUS_NAME = '".$posting["p_cus_name"]."' ,";
		$sql_add .= "   CUS_CONTACT = '".$posting["p_cus_contact"]."' ,";
		$sql_add .= "   CUS_ADDRESS = '".$posting["p_cus_address"]."' ,";
		$sql_add .= "   DISTRICT_CODE = ".$posting["p_district_code"].", ";
		$sql_add .= "   PROVINCE_CODE = ".$posting["p_province_code"].", ";
		$sql_add .= "   CUS_ZIP = '".$posting["p_cus_zip"]."', ";
		$sql_add .= "   CUS_TEL = '".$posting["p_cus_tel"]."', ";
		$sql_add .= "   CUS_FAX = '".$posting["p_cus_fax"]."', ";
		$sql_add .= "   TAX_NUMBER = '".$posting["p_tex_number"]."', ";
		$sql_add .= "   INV_TOTAL =".$posting["p_inv_total"].", ";
		$sql_add .= "   DISCOUNT_PER =".$posting["p_discount_per"].", ";
		$sql_add .= "   VAT_PER =".$posting["p_vat_per"].", ";
		$sql_add .= "   GRAND_TOTAL =".$posting["p_grand_total"].",";
		$sql_add .= "   CREATED_BY ='".$_SESSION["aut_uname"]."', ";
		$sql_add .= "   CREATED_DATE =Now(),";
		$sql_add .= "   UPDATE_DATE = Now() ";
		
        
		//echo " SQL = ".$sql_add;
        $this->mysqli->query($sql_add) OR die("<div style='display:none;'>add_inv_main error:{$sql_add}</div>");
		
		return $new_inv_no;
    }
	public function get_inv_dtl($inv_id)
	{
		$arr_ret = array();

        $sql_sel = "SELECT INV_DTL_ID, INV_ID, QNO, PRODUCT_ID, PRODUCT_SALE_CODE, PRODUCT_NAME, DESCRIPTION, 
							UNIT_PRICE, QTY, AMOUNT
						FROM inv_dtl
						WHERE INV_ID = ".$inv_id."; ";
						
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
		
	}
	public function edit_inv_main($posting)
    {
		
        $sql_add  = " UPDATE inv_main SET   ";
		$sql_add .= "   INV_DATE = '".printDate($posting["p_inv_date"])."', ";
		$sql_add .= "   CREDIT_TERM = ".$posting["p_credit_term"].", ";
		$sql_add .= "   NOTE = '".$posting["p_note"]."', ";
		$sql_add .= "   CUS_NAME = '".$posting["p_cus_name"]."', ";
		$sql_add .= "   CUS_CONTACT = '".$posting["p_cus_contact"]."', ";
		$sql_add .= "   CUS_ADDRESS = '".$posting["p_cus_address"]."' ,";
		$sql_add .= "   DISTRICT_CODE = ".$posting["p_district_code"].", ";
		$sql_add .= "   PROVINCE_CODE = ".$posting["p_province_code"].", ";
		$sql_add .= "   CUS_ZIP = '".$posting["p_cus_zip"]."', ";
		$sql_add .= "   CUS_TEL = '".$posting["p_cus_tel"]."', ";
		$sql_add .= "   CUS_FAX = '".$posting["p_cus_fax"]."', ";
		$sql_add .= "   TAX_NUMBER = '".$posting["p_tex_number"]."', ";
		$sql_add .= "   INV_TOTAL =".$posting["p_inv_total"].", ";
		$sql_add .= "   DISCOUNT_PER =".$posting["p_discount_per"].", ";
		$sql_add .= "   VAT_PER =".$posting["p_vat_per"].", ";
		$sql_add .= "   GRAND_TOTAL =".$posting["p_grand_total"].", ";
		$sql_add .= "   UPDATE_BY ='".$_SESSION["aut_uname"]."', ";
		$sql_add .= "   UPDATE_DATE = Now() ";
        $sql_add .= "WHERE INV_ID = ".$posting["p_inv_id"]." ;";
		//echo " SQL = ".$sql_add;
        $this->mysqli->query($sql_add) OR die("<div style='display:none;'>edit_inv_main error:{$sql_add}</div>");
		
		return $posting["p_inv_no"];
    }
	public function edit_inv_dtl($posting)
    {
		
        $sql_add  = " UPDATE inv_dtl SET   ";
		$sql_add .= "  	QNO = ".$posting["p_qno"];
		$sql_add .= "  	,QTY = ".$posting["p_inv_qty"];
		$sql_add .= "  	,AMOUNT = ".$posting["p_amount"];
		$sql_add .= "  	,UPDATE_BY = '".$_SESSION["aut_uname"]."'  ";
		$sql_add .= "  	,UPDATE_DATE = Now() ";
        $sql_add .= " WHERE INV_DTL_ID = ".$posting["p_inv_dtl_id"]." ;";
		//echo " SQL = ".$sql_add;
        $this->mysqli->query($sql_add) OR die("<div style='display:none;'>edit_inv_dtl error:{$sql_add}</div>");
		
		return $posting["p_inv_no"];
    }
	public function del_inv_dtl($posting)
    {
		
        $sql_add  = " DELETE FROM inv_dtl   ";
        $sql_add .= " WHERE INV_DTL_ID = ".$posting["p_inv_dtl_id"]." ;";
		
        $this->mysqli->query($sql_add) OR die("del_inv_dtl error:{$sql_add}");
		return "";
    }
	public function del_inv_main($posting)
    {
		//$this->mysqli->autocommit(false);
		
		$sql_add  = " DELETE FROM inv_dtl   ";
		$sql_add .= " WHERE INV_ID = ".$posting["p_inv_id"]." ;";
		$this->mysqli->query($sql_add) OR die("del_inv_main error:{$sql_add}");
		
		$sql_add = " DELETE FROM inv_main   ";
		$sql_add .= " WHERE INV_ID = ".$posting["p_inv_id"]." ;";
		$this->mysqli->query($sql_add) OR die("del_inv_main error:{$sql_add}");
		
		//if (!$mysqli->commit()) 
		//{
			//return ("Transaction commit failed\n");
			//exit();
		//}
		//$this->mysqli->autocommit(true);
		
		return "";
    }
}


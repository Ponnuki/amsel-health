﻿<?php 
	
	require("include_function.php");
	require("connect.php");
	require('validatelogin.php'); 
	//echo "session =".$_SESSION['aut_uname'];
 ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | SHOP STATION</title>

	<?php $current_menu = "master"; ?>
	<?php require("include_headtag.php"); ?>
    
</head> 

<body>    

<div id="wrap" class="wrapper">

	<?php require("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">SHOP STATION</h1>
			
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
	<script type="text/javascript">
    <!--
	function confirmDel(DelID)
	{
		if (confirm("Are you sure you want to delete this item."))
		{
			document.getElementById('hdDelID').value = DelID;
			document.getElementById('hdCancelID').value  = "";
			return true;
		}else{
			return false;
		}
	 
	}
	
	function confirmCancel(CancelID)
	{
		if (confirm("Are you sure you want to cancel this item."))
		{
			document.getElementById('hdDelID').value = "";
			document.getElementById('hdCancelID').value = CancelID;
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
		<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		<?php 
		
			// **** Gen Shop DDL
			$sql = "SELECT SHOP_ID, SHOP_CODE, CONCAT(SHOP_CODE, ' : ' ,SHOP_NAME) AS SHOP_NAME
						FROM mst_shop AS ms
						ORDER BY SHOP_CODE; ";
			$result_shop_search=  mysql_query($sql);
			
			// **** Gen Shop Table
			$sql = " SELECT SHOP_ID, SHOP_CODE, SHOP_NAME,
							CONTACT_NAME, SHOP_TEL, CANCEL_FLAG,
							IFNULL((SELECT tsd.TRN_SALES_NO FROM trn_sales_dtl AS tsd WHERE tsd.SHOP_ID = ms.SHOP_ID LIMIT 1), '1') AS IS_DEL
						FROM mst_shop AS ms	";
			
			$where = "";
			
			if ($_POST['ddlShopSearch']!="")
			{
				if ($where == "") $where .= "WHERE "; else $where .=" AND ";
				$where .= " ms.SHOP_CODE = '".
					mysql_real_escape_string($_POST['ddlShopSearch'])
					."' ";
			}
			
			$order = "	ORDER BY SHOP_CODE ; ";
			$sql = $sql.$where.$order;
			$result_shop = mysql_query($sql);

			?>
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_condtion" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Shop Code <br/>/ Shop Name </label>
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
												<select name="ddlShopSearch" id="ddlShopSearch" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
														<option value="" selected> - All Shop - </option>
													<?php 
													while($row = mysql_fetch_array($result_shop_search)) 
													{
														echo "<option value='".$row['SHOP_CODE']."' ";
														if ($row['SHOP_CODE'] == $_POST['ddlShopSearch'])
														{
															echo " selected ";
														}
														echo ">".$row['SHOP_NAME']."</option>";
													}
													mysql_data_seek ($result_shop_search , 0 );
													?>
												</select>
												
										</label>
									</div>
								</div>
									
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button type="submit" class="btn-u"> Search </button> &nbsp;&nbsp;
										
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

	




    <!--=== End Search Block Version 2 ===-->

    <div class="container content-sm"  style="padding:30px;">


		<div class="col-lg-offset col-lg-8" style="height:50px">
			<button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#shop_dtl_form" 
				onclick="ClearForm();"> <i class="fa fa-plus-square icon-color-white"></i> New Shop Station </button> 
		
			<?php  require("shop_station_dtl_form.php");  ?>
		</div>

		
		<div class="table-search-v2 margin-bottom-30">
			<div class="table-responsive">
				<form method="POST" enctype="multipart/form-data" action="shop_model.php?form=frmShopTb" name="frmShopTb" onSubmit="return validate_form(this)"
					id="frmShopTb" >
					<input type="hidden" name="hdDelID" id="hdDelID"  value="">
					<input type="hidden" name="hdCancelID" id="hdCancelID"  value="">
					<table class="table table-bordered table-striped">
						<thead>
							<tr valign="middle">
								<th> &nbsp; </th>
								<th>Shop Code</th>
								<th class="hidden-sm">Shop Name</th>
								<th>Contact Name</th>
								<th>Telephone No.</th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<?php 
								while($row=mysql_fetch_array($result_shop)) 
								{
									
									if($row["SHOP_CODE"] == $_REQUEST['focus'])
									{
										echo '<tr style = "background-color: #ffffbb">';
										echo '<script> history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'" ); </script>';
									}else
									{
										echo '<tr>';
									}
									
									echo '<td width="78px">
												<ul class="list-inline table-buttons">
													<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#shop_dtl_form" onclick="getDataAjax('.$row["SHOP_ID"].')"><i class="fa fa-edit"></i> Edit</button></li>
												</ul>
											</td>';
									echo '<td width="110px"> <p>'.$row["SHOP_CODE"].'</p> </td>';
									echo '<td> <p>'.$row["SHOP_NAME"].'</p> </td>';
									echo '<td width="150px">'.$row["CONTACT_NAME"].'</td>';
									echo '<td width="120px"> <p>'.$row["SHOP_TEL"].'</p> </td>';
									echo '<td  width="84px">
													<ul class="list-inline table-buttons">
														<li>';
										
														if ($row["IS_DEL"] == 1)
														{
															echo '			<button type="submit" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["SHOP_ID"] .')"> 
																				<i class="fa fa-trash-o"></i> Delete </button>';
															
														}else if ($row["CANCEL_FLAG"] == 'N')
														{
															echo '			<button type="submit" class="btn-u btn-u-sm btn-u-orange" onclick ="return confirmCancel('.$row["SHOP_ID"] .')"> 
																				<i class="fa fa-times-circle"></i> Cancel</button>';
														} else 
														{
															echo '			<button type="submit" class="btn-u btn-u-sm btn-u-green" onclick ="return confirmCancel('.$row["SHOP_ID"] .')"> 
																				<i class="fa fa-check-circle"></i> Enable</button>';
														}
													
									echo'			</li>
													</ul>
												</td>
											</tr>';
								}
							?>
						
						</tbody>
					</table>
				</form>
				
				
				<!-- End Test Form -->
			</div>    
		</div>    
        <!-- End Table Search v2 -->
		


        
    </div>
	<?php require("include_footer.php"); ?>
     
</div><!--/End Wrapepr-->

<?php require("include_js.php"); ?>

</body>
</html> 
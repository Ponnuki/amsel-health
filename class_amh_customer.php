<?php
/* Class name
   AMH_Customer

   public function 
   mst_customer($str_search) return array of customer info (example: )
*/
require_once('class_amh_db.php');

class AMH_Customer extends AMH_DB
{
    public function mst_customer($str_search, $page_num = "", $rows_per_page = "30")
    {
        $arr_ret = array();

        $sql_page = "";
        if ($page_num != "")
        {
            $rows_num = ($page_num - 1) * $rows_per_page;
            $sql_page = " LIMIT ".$rows_num.", ".$rows_per_page." ";
        }

        $str_where = "1";
        if ($str_search != '')
        {
           $str_where  = "    (CUS.CUS_NAME    LIKE '%".$str_search."%') ";
           $str_where .= " OR (CUS.CUS_CONTACT LIKE '%".$str_search."%') ";
           $str_where .= " OR (CUS.CUS_EMAIL   LIKE '%".$str_search."%') ";
           $str_where .= " OR (CUS.CUS_ADDRESS LIKE '%".$str_search."%') ";
           $str_where .= " OR (CUS.CUS_TEL     LIKE '%".$str_search."%') ";
           $str_where .= " OR (CUS.CUS_FAX     LIKE '%".$str_search."%') ";
           $str_where .= " OR (CUS.TAX_NUMBER  LIKE '%".$str_search."%') ";
        }

        /*
        $sql_sel = " SELECT * FROM mst_customer WHERE ".$str_where." ORDER BY cus_id ";
        */
        $sql_sel  = " SELECT USR.AUT_ID, CUS.* ";
        $sql_sel .= " FROM mst_customer CUS ";
        $sql_sel .= " LEFT JOIN aut_user_customer AUC ON AUC.CUS_ID = CUS.CUS_ID ";
        $sql_sel .= " LEFT JOIN aut_user USR ON AUC.AUT_ID = USR.AUT_ID ";
        $sql_sel .= " WHERE ".$str_where." ORDER BY CUS.cus_id ".$sql_page;

        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>mst_customer error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_customer_label()
    {
        $arr_label = array();
        $arr_label["c_id"]    = "Customer ID";
        $arr_label["c_code"]  = "Customer Code";
        $arr_label["c_name"]  = "Customer Name";
        $arr_label["c_cntc"]  = "Contact Name";
        $arr_label["c_phone"] = "Tel";
        $arr_label["c_fax"]   = "Fax";
        $arr_label["c_email"] = "E-Mail";
        $arr_label["c_addr"]  = "Address";
        $arr_label["c_dist"]  = "District";
        $arr_label["c_prov"]  = "Province";
        $arr_label["c_ctry"]  = "Country";
        $arr_label["c_zip"]   = "Zip Code";
        $arr_label["tax_num"] = "Tax Number";
        $arr_label["active"]  = "Active";

        return $arr_label;
    }

    public function get_customer_pc_option($AUT_ID, $CUS_ID = "")
    {
        $arr_ret = array();
        $num_row = 0;

        $sql_sel  = " SELECT MCS.* ";
        $sql_sel .= " FROM mst_customer MCS ";
        $sql_sel .= " LEFT JOIN aut_user_customer ACS ON ACS.CUS_ID = MCS.CUS_ID ";
        $sql_sel .= " WHERE (MCS.ACTIVE_FLAG = 'Y') AND (ACS.AUT_ID = '{$AUT_ID}') ";
        $sql_sel .= " ORDER BY MCS.CUS_CODE ";

        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_customer_option error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // can be MYSQLI_BOTH, MYSQLI_NUM
        {
            $num_row++;
            $opt_selected = "";

            if ($num_row == 1) { $arr_ret["cus_id"] = $row_sel["CUS_ID"]; }

            if (($CUS_ID != "") && ($CUS_ID == $row_sel["CUS_ID"]))
            {
                $arr_ret["cus_id"] = $row_sel["CUS_ID"];
                $opt_selected = "selected";
            }

            $arr_ret["option"] .= "<option value='".$row_sel["CUS_ID"]."' {$opt_selected}>".$row_sel["CUS_CODE"]." - ".$row_sel["CUS_NAME"]."</option>"."\r\n";
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_customer_type($CUS_ID) //CUS_TYPE_ID
    {
        $str_ret = "";

        $sql_sel = " SELECT * FROM mst_customer WHERE CUS_ID = '{$CUS_ID}' ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_customer_type error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // can be MYSQLI_BOTH, MYSQLI_NUM
        {
            $str_ret = $row_sel["CUS_TYPE_ID"];
        }
        $res_sel->free();

        return $str_ret;
    }

    public function get_customer_option($CUS_ID)
    {
        $str_ret = "";

        $sql_sel = " SELECT * FROM mst_customer WHERE (ACTIVE_FLAG = 'Y') ORDER BY CUS_CODE ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_customer_option error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // can be MYSQLI_BOTH, MYSQLI_NUM
        {
          $selected = "";
          if ($row_sel["CUS_ID"] == $CUS_ID) { $selected = " selected"; }

          $str_ret .= "<option value='".$row_sel["CUS_ID"]."'".$selected.">".$row_sel["CUS_CODE"]." - ".$row_sel["CUS_NAME"]."</option>"."\r\n";
        }
        $res_sel->free();

        return $str_ret;
    }

    public function get_customer_detail_json($CUS_ID)
    {
        $str_ret = "";

        if ($CUS_ID != "")
        {
            $sql_sel = " SELECT * FROM mst_customer WHERE CUS_ID = '{$CUS_ID}' ";
            $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_customer_option error:{$sql_sel}</div>");
            if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // can be MYSQLI_BOTH, MYSQLI_NUM
            {
                $str_ret = json_encode($row_sel);
            }
        }
        $res_sel->free();

        return $str_ret;
    }

    public function add_customer($posting)
    {
        $sql_add  = " INSERT INTO mst_customer SET   ";
        $sql_add .= " CUS_NAME     = '".$posting["c_name" ]."' ,";
        $sql_add .= " CUS_CODE     = '".$posting["c_code" ]."' ,";
        $sql_add .= " CUS_CONTACT  = '".$posting["c_cntc" ]."' ,";
        $sql_add .= " CUS_EMAIL    = '".$posting["c_email"]."' ,";
        $sql_add .= " CUS_ADDRESS  = '".$posting["c_addr" ]."' ,";
        $sql_add .= " DISTRICT_CODE = '".$posting["c_dist" ]."' ,";
        $sql_add .= " PROVINCE_CODE = '".$posting["c_prov" ]."' ,";
        $sql_add .= " CUS_COUNTRY  = '".$posting["c_ctry" ]."' ,";
        $sql_add .= " CUS_ZIP      = '".$posting["c_zip"  ]."' ,";
        $sql_add .= " CUS_TEL      = '".$posting["c_phone"]."' ,";
        $sql_add .= " CUS_FAX      = '".$posting["c_fax"  ]."' ,";
        $sql_add .= " TAX_NUMBER   = '".$posting["tax_num"]."'  ";

        $this->mysqli->query($sql_add) OR die("<div style='display:none;'>add_customer error:{$sql_add}</div>");
    }

    public function upd_customer($posting)
    {
        $sql_upd  = " UPDATE mst_customer SET   ";
        $sql_upd .= " CUS_CODE     = '".$posting["c_code" ]  ."' ,";
        $sql_upd .= " CUS_NAME     = '".$posting["c_name" ]  ."' ,";
        $sql_upd .= " CUS_TYPE_ID  = '".$posting["cus_type" ]."' ,";
        $sql_upd .= " CUS_CONTACT  = '".$posting["c_cntc" ]  ."' ,";
        $sql_upd .= " CUS_EMAIL    = '".$posting["c_email"]  ."' ,";
        $sql_upd .= " CUS_ADDRESS  = '".$posting["c_addr" ]  ."' ,";
        $sql_upd .= " DISTRICT_CODE = '".$posting["c_dist" ] ."' ,";
        $sql_upd .= " PROVINCE_CODE = '".$posting["c_prov" ] ."' ,";
        $sql_upd .= " CUS_COUNTRY  = '".$posting["c_ctry" ]  ."' ,";
        $sql_upd .= " CUS_ZIP      = '".$posting["c_zip"  ]  ."' ,";
        $sql_upd .= " CUS_TEL      = '".$posting["c_phone"]  ."' ,";
        $sql_upd .= " CUS_FAX      = '".$posting["c_fax"  ]  ."' ,";
        $sql_upd .= " TAX_NUMBER   = '".$posting["tax_num"]  ."' ,";
        $sql_upd .= " ACTIVE_FLAG  = '".$posting["active"]   ."' ,";
        $sql_upd .= " AREA_CODE    = '".$posting["area_code"]."' ,";
        $sql_upd .= " AREA_NAME    = '".$posting["area_name"]."'  ";
        $sql_upd .= " WHERE CUS_ID = '".$posting["cus_id"]   ."'  ";

        $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>upd_customer error:{$sql_upd}</div>");

        $this->upd_aut_user_customer($posting["cus_id"], $posting["aut_id"]);
    }

    public function del_customer($posting)
    {
        $sql_del  = " DELETE FROM mst_customer WHERE CUS_ID = '".$posting["cus_id"]."' ";

        $this->mysqli->query($sql_del) OR die("<div style='display:none;'>del_customer error:{$sql_del}</div>");
    }

    public function upd_aut_user_customer($cus_id, $aut_id)
    {
        $existing_autid = "";
        $existing_found = false;
        $sql_sel = " SELECT * FROM aut_user_customer WHERE CUS_ID = '{$cus_id}' ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>upd_aut_user_customer sel existing error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // can be MYSQLI_BOTH, MYSQLI_NUM
        {
            $existing_found = true;
            $existing_autid = $row_sel["AUT_ID"];
        }
        $res_sel->free();

        if ($existing_found)
        {
            $sql_upd = " UPDATE aut_user_customer SET AUT_ID = '".$aut_id."' WHERE CUS_ID = '".$cus_id."' ";
            $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>upd_aut_user_customer upd error:{$sql_upd}</div>");
        }
        else
        {
            $sql_upd = " INSERT INTO aut_user_customer SET AUT_ID = '".$aut_id."', CUS_ID = '".$cus_id."' ";
            $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>upd_aut_user_customer ins error:{$sql_upd}</div>");
        }
    }

    public function get_aut_user_array($str_where)
    {
        $arr_ret = array();

        if ($str_where == '')
        {
            $str_where = ' USR.AUT_ROLE_ID = 4 ';
        }

        $sql_sel = " SELECT USR.* FROM aut_user USR WHERE {$str_where} ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_aut_user_array error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function write_pc_option($the_array, $aut_id)
    {
        $str_ret = "";

        foreach ($the_array as $pc_usr)
        {
            $str_selected = "";
            if ($aut_id == $pc_usr["AUT_ID"]) { $str_selected = "selected"; }
            $str_ret .= "<option value='".$pc_usr["AUT_ID"]."' ".$str_selected.">".$pc_usr["AUT_UNAME"]."</option>\r\n";
        }

        return $str_ret;
    }

    public function write_cus_type_option($cus_type_id)
    {
        $str_ret = "";

        $sql_sel = " SELECT * FROM `customer_type` WHERE 1 ORDER BY CUS_TYPE_NAME ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>write_cus_type_option error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC))
        {
            if ($cus_type_id == $row_sel["CUS_TYPE_ID"]) { $str_selected = "selected"; } else { $str_selected = ""; }

            $str_ret .= "<option value='".$row_sel["CUS_TYPE_ID"]."' ".$str_selected.">".$row_sel["CUS_TYPE_NAME"]."</option>\n";
        }
        $res_sel->free();

        return $str_ret;
    }
}
<?php
require_once("class_amh_db.php");
require_once("include_function.php");
//require_once("validatelogin.php");

class AMH_prd_pc extends AMH_DB
{
	public function get_prd_pc_dtl($prd_pc_id = "")
    {
        $arr_ret = array();
		
		$sql = "SELECT mpc.PRODUCT_PC_ID, mp.PRODUCT_CODE, mpc.PRODUCT_ID, mp.PRODUCT_NAME_TH , mp.PRODUCT_TYPE_ID ,  DATE_FORMAT(mpc.EFFECTIVE_DATE,'%d-%m-%Y') as EFFECTIVE_DATE,
								mpc.LTP_PER, mpc.LTP_PRICE, mpc.STICKER_PRICE, mpc.CHEER_RATE, mpc.LTP_FOR_COMM
							FROM mst_product_pc AS mpc
								LEFT JOIN mst_product AS mp ON mpc.PRODUCT_ID = mp.PRODUCT_ID
								LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID
					WHERE mpc.PRODUCT_PC_ID = ".$prd_pc_id.";  ";
        $result = $this->mysqli->query($sql) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
        while ($row = $result->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row;
        }
        $result->free();
		
        return json_encode($arr_ret);
	}
	
	public function chk_dup($prd_id = "",$effective ="",$cus_type_id = "")
    {
        $arr_ret = array();
		
		$sql = "SELECT mpc.PRODUCT_ID, mpc.EFFECTIVE_DATE,  mp.PRODUCT_CODE	
							FROM mst_product_pc AS mpc
								LEFT JOIN mst_product AS mp ON mpc.PRODUCT_ID = mp.PRODUCT_ID
			WHERE mpc.PRODUCT_ID = ".$prd_id." AND mpc.EFFECTIVE_DATE = '".printDate($effective)."' AND mpc.CUS_TYPE_ID = ".$cus_type_id.";  ";
        $result = $this->mysqli->query($sql) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
      
		if ($result->num_rows > 0)
		{
			while ($row = $result->fetch_array(MYSQLI_BOTH))
			{
				echo $row['PRODUCT_CODE'];
			}
		}else{
			echo "";
		}
        $result->free();
		
	}
	
}

	
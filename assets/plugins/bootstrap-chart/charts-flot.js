

$(document).ready(function(){
	
	
	/* ---------- My Chart with points ---------- */
	if($("#myLineChart").length)
	{
		var val1 = [], val2 = [];
		val1.push([0, 0]);
		val2.push([0,0.1]);
		val1.push([0.5, -0.1]);
		val2.push([0.5,0.2]);
		val1.push([1, -0.2]);
		val2.push([1,0.3]);
		val1.push([1.5, -0.3]);
		val2.push([1.5,0.4]);
		val1.push([2, -0.4]);
		val2.push([2,0.5]);
		val1.push([3, 0]);
		val2.push([3,0.1]);
		val1.push([3.5, -0.1]);
		val2.push([3.5,0.2]);
		val1.push([4, -0.2]);
		val2.push([4,0.3]);
		val1.push([4.5, -0.3]);
		val2.push([4.5,0.4]);
		val1.push([5, -0.4]);
		val2.push([5,0.5]);
		val1.push([5.5, -0.4]);
		val2.push([5.5,0.5]);
		val1.push([6, -0.4]);
		val2.push([6,0.5]);
		val1.push([6.5, -0.1]);
		val2.push([6.5,0.2]);
		val1.push([7, -0.2]);
		val2.push([7,0.3]);
		val1.push([7.5, -0.3]);
		val2.push([7.5,0.4]);
		val1.push([8, -0.4]);
		val2.push([8,0.5]);
		val1.push([8.5,-0.3]);
		val2.push([8.5,0.4]);
		val1.push([9, -0.4]);
		val2.push([9,0.5]);
		val1.push([9.5, -0.1]);
		val2.push([9.5,0.2]);
		val1.push([10, -0.2]);
		val2.push([10,0.3]);
		val1.push([10.5, -0.3]);
		val2.push([10.5,0.4]);
		val1.push([11, -0.4]);
		val2.push([11,0.5]);
		val1.push([11.5, -0.3]);
		val2.push([11.5,0.4]);
		val1.push([12, -0.4]);
		val2.push([12,0.5]);
		val1.push([12.5, -0.3]);
		val2.push([12.5,0.4]);
		val1.push([13, -0.4]);
		val2.push([13,0.5]);
		val1.push([13.5, -0.3]);
		val2.push([13.5,0.4]);
		val1.push([14, -0.4]);
		val2.push([14,2.5]);
		val1.push([14.5, -0.9]);
		val2.push([14.5,1.5]);
		
		
		var plot = $.plot($("#myLineChart"),
			   [ { data: val1, label: "Line1"}, { data: val2, label: "Line2" } ], {
				   series: {
					   lines: { show: true,
								lineWidth: 2,
							 },
					   points: { show: true },
					   shadowSize: 2
				   },
				   xaxis: {
						ticks: [0,[1,"M1"], [2,"M2"],[3,"M3"], [4,"M4"],[5,"M5"], [6,"M6"],[7,"M7"], [8,"M8"],[9,"M9"], [10,"M10"],[11,"M11"], [12,"M12"],[13,"M13"], [14,"M14"]]
					},
				   grid: { hoverable: true, 
						   clickable: true, 
						   tickColor: "#dddddd",
						   borderWidth: 0 
						 },
				   yaxis: { min: -1.2, max: 2.5 },
				   colors: ["#11FF11", "#2FABE9"]
				 });

		function showTooltipMe(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css( {
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 5,
				border: '1px solid #fdd',
				padding: '2px',
				'background-color': '#dfeffc',
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#myLineChart").bind("plothover", function (event, pos, item) {
			$("#x2").text(pos.x.toFixed(2));
			$("#y2").text(pos.y.toFixed(2));

				if (item) {
					if (previousPoint != item.dataIndex) {
						previousPoint = item.dataIndex;

						$("#tooltip").remove();
						var x = item.datapoint[0].toFixed(2),
							y = item.datapoint[1].toFixed(2);

						showTooltipMe(item.pageX, item.pageY,
									item.series.label + " of " + x + " = " + y);
					}
				}
				else {
					$("#tooltip").remove();
					previousPoint = null;
				}
		});
		


		$("#myLineChart").bind("plotclick", function (event, pos, item) {
			if (item) {
				$("#clickdata2").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
				plot.highlight(item.series, item.datapoint);
			}
		});
	}
	
	
	
	
});
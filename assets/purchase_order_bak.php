﻿<?php 
	require("include_function.php");
	require("connect.php");
	require('validatelogin.php');
 ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | TEMPLATE</title>
	<?php $current_menu = "user_mag"; ?>
	<?php require("include_headtag.php"); ?>
</head> 

<body>    

<div id="wrap"  class="wrapper">
    <?php require("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">PURCHASE ORDER</h1>
        </div>
    </div>

    <!-- Search Section -->
    <div style="padding: 18px; padding-bottom: 0px;" class="search-block">
        <div class="container">
            <div class="col-md-6 col-md-offset-3">
                <h2>CONDITION</h2>

                <div style="padding: 18px;" class="panel panel-grey margin-bottom-40">
                    <div style="border-style:none" class="sky-form">
                    	<form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="inputTranNo">PO Number <span style="color:#f00;">*</span> :</label>
                            <div class="col-lg-8">
                                <input type="text" value="" placeholder="PO Number" id="txbTranNum" name="txbTranNum" class="form-control" style="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="inputTranDate">Create Date <span style="color:#f00;">*</span> :</label>
                            <section style="height: 16px" class="col col-lg-4">
                            <label class="input">
                                <i class="icon-append fa fa-calendar"></i>
                                <input type="text" value="" placeholder="Start date" id="txbTranDateStart" name="txbTranDateStart" class="form-control hasDatepicker">
                            </label>
                            </section>

                            <section style="height: 16px" class="col col-lg-4">
                            <label class="input">
                                <i class="icon-append fa fa-calendar"></i>
                                <input type="text" value="" placeholder="End date" id="txbTranDateEnd" name="txbTranDateEnd" class="form-control hasDatepicker">
                            </label>
                            </section>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-4 col-lg-8">
                                <button type="button" class="btn-u"> Search </button>
                                <button type="button" class="btn-u btn-u-default">Clear</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>    
    </div>
    <!-- Search Section -->
     
</div><!--/End Wrapepr-->

    <!-- List items Section -->
    <div style="padding:30px;" class="container content-sm">

		<div style="height:50px" class="col-lg-offset col-lg-8">
			<button onclick="ClearSalesTranForm();" data-target="#sales_transaction_dtl_form" data-toggle="modal" class="btn-u btn-u-green" type="button"> <i class="fa fa-plus-square icon-color-white"></i> New Purchase Order </button>
			&#65279;</div>

		<div class="table-search-v2 margin-bottom-30">
			<div class="table-responsive">
				<form id="formSalesTranTable" onsubmit="return validate_form(this)" name="formSalesTranTable" action="sales_transaction_model.php?form=sales_tran_table" enctype="multipart/form-data" method="POST">
					<input type="hidden" value="" id="hdDelID" name="hdDelID">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="42px"> &nbsp; </th>
								<th width="100px">Create Date</th>
								<th width="120px">PO NO</th>
								<th>Customer Name</th>
								<th width="120px">Total Amount</th>
								<th width="80px">Type</th>
								<th width="42px"> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<tr>
                                <td>
							    <ul class="list-inline table-buttons">
                                <li><button data-target="#sales_transaction_dtl_form" data-toggle="modal" class="btn-u btn-u-sm btn-u-blue" type="button"><i class="fa fa-edit"></i> Edit</button></li>
                                </ul>
                                </td>
                                <td>29-06-2015</td>
                                <td>PW001</td>
                                <td>Watson สีลม</td>
                                <td>20,000.50</td>
                                <td>For Invoice</td>
                                <td>
                                    <ul class="list-inline table-buttons">
                                    <li><button class="btn-u btn-u-sm btn-u-red" type="submit"><i class="fa fa-trash-o"></i> Delete</button></li>
                                    </ul>
                                </td>
                            </tr>						
							<tr>
                                <td>
							    <ul class="list-inline table-buttons">
                                <li><button data-target="#sales_transaction_dtl_form" data-toggle="modal" class="btn-u btn-u-sm btn-u-blue" type="button"><i class="fa fa-edit"></i> Edit</button></li>
                                </ul>
                                </td>
                                <td>29-06-2015</td>
                                <td>PW001</td>
                                <td>Watson สีลม</td>
                                <td>20,000.50</td>
                                <td>For Invoice</td>
                                <td>
                                    <ul class="list-inline table-buttons">
                                    <li><button class="btn-u btn-u-sm btn-u-red" type="submit"><i class="fa fa-trash-o"></i> Delete</button></li>
                                    </ul>
                                </td>
                            </tr>						
						</tbody>
					</table>
				</form>
			</div>    
		</div>  
    </div>
    <!-- List items Section -->

<?php require_once("include_footer.php"); ?>
<?php require_once("include_js.php"); ?>

</body>
</html> 
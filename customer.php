<?php

require_once('validatelogin.php');
require_once('class_amh_customer.php');
require_once('class_amh_province.php');

$amh_customer = new AMH_Customer();
$amh_province = new AMH_Province();

$arr_label = $amh_customer->get_customer_label();

/*
$str_show = "";
foreach ($mst_product as $tbl_product)
{
    $str_show .= "<div>".$tbl_product["PRODUCT_NAME_EN"]."</div>";
}
*/
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Customer Manage</title>

    <?php $current_menu = "customer"; ?>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-me.css">

    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    
    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-colors/blue.css" /> 
    
    <style type="text/css">
        .add_line_top
        {
            border: 1px solid #999;
            border-style: none none solid none;
        }

        .add_line_btm
        {
            border: 1px solid #999;
            border-style: solid none none none;
        }

        #form_add
        {
            display: none;
            padding:15px;
        }

        .input-group-addon
        {
            background-color: #666;
            color: #fff;
            font-family: Verdana, Geneva, sans-serif;
            font-size: 10pt;
            font-weight: bold;
            min-width: 100px;
        }

        .table_this
        {
            width: 100%;
        }

        .table_this, .table_this td
        {
            font-weight: normal;
            border: none;
            padding: 5px 10px;
        }

        .table_this_thead_tr
        {
            background-color: #ddd;
            font-weight: normal;
            border: none;
            padding: 5px 10px;
        }

        .table_this_thead_tr:hover
        {
            background-color: #eee;
        }

        .table_this_thead_tr td
        {
            border-bottom: 1px solid #ccc;
            padding: 5px;
        }

        .table_this_thead_tr button
        {
            width: 100%;
        }

        .table_input
        {
            border:1px solid #aac;
            font-size: 12px;
            font-weight: normal;
            width:100%;
        }

        .table_btn
        {
            width: 90%;
        }

        .table_label
        {
            display: inline-block;
        }

        .title_search
        {
            display: inline-block;
            font-family: "Trebuchet MS";
            font-size: 18px;
            font-weight: normal;
            margin: 0 10px;
        }
    </style>
    <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        str_expand_id = '';

        function startup()
        {
            //$('.selectpicker').selectpicker();
            get_customer_list(1);
        }

        function expanding_detail(id_to_expand)
        {
            hiding_detail();
            str_expand_id = id_to_expand;
            $("#tbody_"+str_expand_id).show(400);
            /* document.getElementById("tbody_"+str_expand_id).style.display = 'table-row-group'; */
        }

        function hiding_detail()
        {
            if (str_expand_id != '')
            { $("#tbody_"+str_expand_id).hide(600); }
            //{ document.getElementById("tbody_"+str_expand_id).style.display = 'none'; }
        }

        function get_customer_list(page_num)
        {
            if (page_num == 0)
            {
                page_num = $('#this_page_num').val();
            }
            if (window.XMLHttpRequest) { xmlHTTPcus=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                  else { xmlHTTPcus=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

            xmlHTTPcus.onreadystatechange=function()
            {
                if (xmlHTTPcus.readyState==4 && xmlHTTPcus.status==200)
                {
                    str_response = xmlHTTPcus.responseText;
                    if (str_response != '')
                    {
                        document.getElementById("list_customer").innerHTML = str_response;
                        if (str_expand_id != '') { $("#tbody_"+str_expand_id).show(400); /* document.getElementById("tbody_"+str_expand_id).style.display = 'table-row-group'; */ }
                    }
                }
            }

            //str_param = '?wo_p_id='+wo_id;
            document.getElementById("list_customer").innerHTML = "<table style='width:100%'><tr><td align='center' style='font-weight:bold; padding: 3px 5px;'>Loading customer list . . .</td></tr></table>";

            str_param = '?str_search='+document.getElementById('s_search').value+'&page_num='+page_num;

            xmlHTTPcus.open('GET','customer_list.php'+str_param, true);
            xmlHTTPcus.send();
        }

        function get_district_option(province_code, obj_id)
        {
            if (window.XMLHttpRequest) { xmlHTTPopt=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                  else { xmlHTTPopt=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

            xmlHTTPopt.onreadystatechange=function()
            {
                if (xmlHTTPopt.readyState==4 && xmlHTTPopt.status==200)
                {
                    str_response = xmlHTTPopt.responseText;
                    if (str_response != '')
                    {
                        document.getElementById(obj_id).innerHTML = str_response;
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            }

            str_param = '?pcode='+province_code;

            xmlHTTPopt.open('GET','district_get_option.php'+str_param, true);
            xmlHTTPopt.send();
        }

        function show_block(show_obj_id, hide_obj_id)
        {
            if (show_obj_id != '') { $("#"+show_obj_id).show(400); /* document.getElementById(show_obj_id).style.display = "block"; */ }
            if (hide_obj_id != '') { $("#"+hide_obj_id).hide(600); /* document.getElementById(hide_obj_id).style.display = "none";  */ }
        }

        function form_clear()
        {
            document.getElementById('c_name' ).value = "";
            document.getElementById('c_cntc').value = "";
            document.getElementById('c_email').value = "";
            document.getElementById('c_addr' ).value = "";
            document.getElementById('c_dist' ).value = "";
            document.getElementById('c_prov' ).value = "";
            document.getElementById('c_ctry' ).value = "";
            document.getElementById('c_zip'  ).value = "";
            document.getElementById('c_phone').value = "";
            document.getElementById('c_fax'  ).value = "";
            document.getElementById('tax_num').value = "";
        }

        function form_hide()
        {
            form_clear();
            show_block('btn_new_customer', 'form_add');
        }

        function form_validation()
        {
            bln_return = true;

            msg_alert  = '';

            if (document.getElementById('c_phone' ).value == '') { msg_alert = 'กรุณาใส่เบอร์โทร'; }
            if (document.getElementById('c_name'  ).value == '') { msg_alert = 'กรุณาใส่ชื่อลูกค้า'; }

            if (msg_alert != '')
            {
                alert(msg_alert);
                bln_return = false;
            }

            return bln_return;
        }

        function form_post(cus_id, operate)
        {
            if (operate == 'add') { validation = form_validation(); } else { validation = true; }

            posting = false;

            if (validation)
            {
                if (window.XMLHttpRequest) { xmlHTTPcus=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                      else { xmlHTTPcus=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5
    
                xmlHTTPcus.onreadystatechange=function()
                {
                    if (xmlHTTPcus.readyState==4 && xmlHTTPcus.status==200)
                    {
                        str_response = xmlHTTPcus.responseText;
                        if (str_response != '')
                        {
                            alert(str_response);

                            this_page_num = $('#this_page_num').val();
                            get_customer_list(this_page_num);
                            form_hide();
                        }
                    }
                }
    
                //str_param = '?wo_p_id='+wo_id;
                params  = 'operate='  + operate;

                if (operate == 'add')
                {
                    params += '&c_code='  + document.getElementById('c_code' ).value;
                    params += '&c_name='  + document.getElementById('c_name' ).value;
                    params += '&c_cntc='  + document.getElementById('c_cntc').value;
                    params += '&c_email=' + document.getElementById('c_email').value;
                    params += '&c_addr='  + document.getElementById('c_addr' ).value;
                    params += '&c_dist='  + document.getElementById('c_dist' ).value;
                    params += '&c_prov='  + document.getElementById('c_prov' ).value;
                    params += '&c_ctry='  + document.getElementById('c_ctry' ).value;
                    params += '&c_zip='   + document.getElementById('c_zip'  ).value;
                    params += '&c_phone=' + document.getElementById('c_phone').value;
                    params += '&c_fax='   + document.getElementById('c_fax'  ).value;
                    params += '&tax_num=' + document.getElementById('tax_num').value;

                    posting = true;
                }

                if (operate == 'upd')
                {
                    params += '&c_name='    + document.getElementById('cus_name_'     + cus_id).value;
                    params += '&c_cntc='    + document.getElementById('cus_cntc_'     + cus_id).value;
                    params += '&c_email='   + document.getElementById('cus_email_'    + cus_id).value;
                    params += '&c_addr='    + document.getElementById('cus_address_'  + cus_id).value;
                    params += '&c_dist='    + document.getElementById('cus_district_' + cus_id).value;
                    params += '&c_prov='    + document.getElementById('cus_province_' + cus_id).value;
                    params += '&c_ctry='    + document.getElementById('cus_country_'  + cus_id).value;
                    params += '&c_zip='     + document.getElementById('cus_zip_'      + cus_id).value;
                    params += '&c_phone='   + document.getElementById('cus_tel_'      + cus_id).value;
                    params += '&c_fax='     + document.getElementById('cus_fax_'      + cus_id).value;
                    params += '&tax_num='   + document.getElementById('tax_number_'   + cus_id).value;
                    params += '&c_code='    + document.getElementById('cus_code_'     + cus_id).value;
                    params += '&active='    + document.getElementById('active_'       + cus_id).value;
                    params += '&aut_id='    + document.getElementById('cus_pc_'       + cus_id).value;
                    params += '&area_code=' + document.getElementById('cus_area_code_'+ cus_id).value;
                    params += '&area_name=' + document.getElementById('cus_area_name_'+ cus_id).value;
                    params += '&cus_type='  + document.getElementById('cus_type_'     + cus_id).value;
                    params += '&cus_id='    + cus_id;

                    posting = true;
                }

                if (operate == 'del')
                {
                    if (confirm('ต้องการลบข้อมูลลูกค้าคนนี้หรือไม่'))
                    {
                        params += '&cus_id='  + cus_id;
                        posting = true;
                    }
                }

                if (posting)
                {
                    xmlHTTPcus.open('POST','customer_execdb.php',true);
                    xmlHTTPcus.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlHTTPcus.send(params);
                }
            }
        } /* function form_post */

        function clear_search()
        {
            document.getElementById('s_search').value = "";

            //this_page_num = $('#this_page_num').val();
            get_customer_list(1);
        }
    </script>
</head>
<body onload="startup();">
<div id="wrap" class="wrapper">
    <?php require("include_header.php"); ?>
    <div class="header">
        <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
        <div class="container"></div>
        </div>
        </div>
    <!--=== End Header ===-->

    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
        <h1 class="pull-left">CUSTOMER</h1>
        </div>
    </div>

    <!--=== Content Part ===-->
    <div class="breadcrumbs">
    <div class="container content">
    <div class="panel panel-grey col-md-8 col-md-offset-2 form-horizontal" id="form_add">
                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_code"];?>
                          </span>
                          <input type="TEXT" id="c_code"  name="c_code"  value="" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_name"];?>
                          </span>
                          <input type="TEXT" id="c_name"  name="c_name"  value="" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_cntc"];?>
                          </span>
                          <input type="TEXT" id="c_cntc" name="c_cntc" value="" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_email"];?>
                          </span>
                          <input type="TEXT" id="c_email" name="c_email" value="" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["tax_num"];?>
                          </span>
                          <input type="TEXT" id="tax_num" name="tax_num" value="" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_addr"];?>
                          </span>
                          <input type="TEXT" id="c_addr" name="c_addr" value="" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_dist"];?>
                          </span>
                          <select id="c_dist" name="c_dist" class="selectpicker" data-live-search="true">
                              <option value=''>เลือกจังหวัดก่อน</option>
                          </select>
                          <!--
                          <input type="TEXT" id="c_dist" name="c_dist" value="" class="form-control" />
                          -->
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_prov"];?>
                          </span>
                          <select id="c_prov" name="c_prov" class="selectpicker" data-live-search="true" onchange="get_district_option(this.value, 'c_dist');">
                              <option value=''>เลือกจังหวัด</option>
                              <?php echo $amh_province->create_province_option(''); ?>
                          </select>
                          <!--
                          <input type="TEXT" id="c_prov" name="c_prov" value="" class="form-control" />
                          -->
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_zip"];?>
                          </span>
                          <input type="TEXT" id="c_zip" name="c_zip" value="" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <input type="HIDDEN" id="c_ctry" name="c_ctry" value="THA">
                        <!--
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_ctry"];?>
                          </span>
                          <select id="c_ctry" name="c_ctry" class="selectpicker" data-live-search="true">
                              <option value="THA">THAILAND</option>
                              <option value="USA">USA</option>
                              <option value="AUS">AUSTRALIA</option>
                              <option value="JPN">JAPAN</option>
                              <option value="CHN">CHAINA</option>
                              <option value="TWN">TAIWAN</option>
                          </select>
                        </div>
                        -->
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_phone"];?>
                          </span>
                          <input type="TEXT" id="c_phone" name="c_phone" value="" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group margin-bottom-20">
                          <span class="input-group-addon">
                              <?=$arr_label["c_fax"];?>
                          </span>
                          <input type="TEXT" id="c_fax" name="c_fax" value="" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-sm-4"><button type="button" class="btn btn-primary btn-block" onclick="form_post('','add');"><span class="glyphicon glyphicon-plus-sign"> </span> Add</button></div>
                    <div class="col-md-4 col-sm-4"><button type="button" class="btn btn-info    btn-block" onclick="form_clear();"><span class="glyphicon glyphicon-ban-circle"> </span> Clear</button></div>
                    <div class="col-md-4 col-sm-4"><button type="button" class="btn btn-info    btn-block" onclick="form_hide();"><span class="glyphicon glyphicon-remove-sign"> </span> Close</button></div>
                </div>
    </div>

    <div class="col-sm-6 col-sm-offset-3" style="background-color:#ccc; padding:10px;">
        <span class="title_search">CONDITION</span>
        <input type="TEXT" class="table_input" style="width:160px;" id="s_search">
        <button class="btn-u btn-u-sm btn-u-blue" onclick="get_customer_list(1);">Search</button>
        <button class="btn-u btn-u-sm btn-u-default" onclick="clear_search();">Clear</button>
    </div>

    </div><!--/container-->
    </div>
    <div style="padding:5px;"></div>
    <div class="col-sm-6 col-sm-offset-1" style="margin-bottom:20px;">
        <button type="button" class="btn-u btn-u-green" id="btn_new_customer" onclick="show_block('form_add', this.id);"><span class="glyphicon glyphicon-plus-sign"> </span> New Customer</button>
    </div>

<div class="container content-sm" style="padding:30px;">
<div class="container" id="list_customer" style="margin-top: 20px; border:1px solid #999; padding:0;">123</div>
</div>

</div><!--/wrapper-->
<?php require("include_footer.php"); ?>
</body>
</html>
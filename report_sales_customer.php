<?php 
	require("include_function.php");
	require('validatelogin.php'); 
	require('config.php'); 
	//echo "session =".$_SESSION['aut_uname'];
 ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMESEL HEALTH SELECT | SALES REPORT FOR CUSTOMER</title>
	<script src="assets/js/ccchart.js" charset="utf-8"></script>
	<?php $current_menu = "report_sale_customer"; ?>
	<!--=== End Breadcrumbs ===-->
		
	<?php require("include_headtag.php"); ?>
	
</head> 

<body>    
	<script type="text/javascript">
		
		function genChart(chartObj, yScale, amont,qty,pcName,typeNameTh,typeNameEn,prdName,barColor,aPrdImg)
		{
			//มีแก้ไขใน assets/css/app.css เพื่อแก้ไขกราฟแสดง Point 4 เหลี่ยม
			
			var arrData = [];
			
			arrData[0] = ["รหัสสินค้า"];
			for (i=0; i<yScale.length; i++)
				arrData[0].push (yScale[i]);
			
			arrData[1] =["ยอดเงิน"];
			for (i=0; i<amont.length; i++)
				arrData[1] .push (amont[i]);
			
			arrData[2] =["จำนวนสินค้า"];
			for (i=0; i<qty.length; i++)
				arrData[2] .push (qty[i]);
			
			arrData[3] =["Product Name"];
			for (i=0; i<prdName.length; i++)
				arrData[3] .push (prdName[i]);
			
			var  barW = 26;
			var bgFrom = "#687478";
			var bgTo = "#222222";
			
			if (document.getElementById("hdDataType").value == "sale" )
			{
				bgFrom = "#687478";
				bgTo = "#222222";
			}else if(document.getElementById("hdDataType").value == "order" )
			{
				bgFrom = "#4c6cb3";
				bgTo = "#222222";
			}else if (document.getElementById("hdDataType").value == "stock" )
			{
				bgFrom = "#2d8659";
				bgTo = "#222222";
			}
			
			var xScaleRotate = 0;
			var xScaleYOffset = 20;
			var paddingBottom = 40;
			if (yScale.length > 12)
			{
				xScaleRotate = -90;
				xScaleYOffset = 45;
				paddingBottom = 90;
			}
			
			
			var chartdata77 = {

				"config": {
					"title": "Summary "+ typeNameEn+ " chart",
					"subTitle": "กราฟแท่ง แสดงยอดเงินที่ขายได้",
					"type": "stacked",
					"minY": 0,
					"roundDigit":2,
					"hanreiMarkerStyle": "rect",
					"unit": "มูลค่า",
					"barWidth": barW,
					"barPadding": (284/yScale.length)-(barW/2),
					"colorSet": [barColor],
					"width":800,//defoult 600*400
					"height":600,
					
					
				},

				"data": [
						arrData[0],
						arrData[1] 
					]
			};
			
			var chartdata77b= {
			"config": {
				"title": "Summary "+ typeNameEn+ " Chart",
				"subTitle": "กราฟสรุปรายงานการขายสินค้าประเภท["+typeNameTh+"]",
				"type": "line",
				"minY": 0,
				"roundDigit":0,
				"unit":"หน่วย",
				"useMarker":"css-ring",
				"borderWidth": 4,
				"markerWidth": 15,
				"hanreiMarkerStyle": 'arc',
				"colorSet": ["#ccc","#888"],
				"width":800,//defoult 600*400
				"height":600,
				"xScaleRotate": xScaleRotate,
				"xScaleYOffset": xScaleYOffset,
				"paddingBottom": paddingBottom,
				"bgGradient": {
					"direction":"vertical",
					"from":bgFrom,
					"to":bgTo
				 }
				
				//,"xScaleRotate": -90
				//,"xScaleYOffset": 40
				//,"paddingTop": 90
				//,"paddingBottom": 85
				
			},
			"data": [
					arrData[0],
					arrData[2]
				]

			}

			ccchart
				.init("canvas"+chartObj, chartdata77b)
				.add(chartdata77);
				
			document.getElementById("tb"+chartObj).style.width = "800px";
			
			//for (i=0; i<prdName.length; i++) document.getElementById("tr"+chartObj).innerHTML += "<td>"+ prdName[i]+ "</td>";
			var imgSize = (572 / yScale.length) ;
			document.getElementById("tr"+chartObj).innerHTML += "<td style='width:70px'></td>";
			for (i=0; i<yScale.length; i++) {
				document.getElementById("tr"+chartObj).innerHTML += "<td style='border :1px solid #bbbbbb'>" +
				 "<img id='img"+yScale[i]+
					"' width='"+imgSize+"' height='"+imgSize+"' title='"+ prdName[i] +"' src='<?php echo $pro_img_path ?>"+aPrdImg[i]+"' name='imgPrd' style='margin-bottom: 0px; margin-top: 0px;'></td>";
				
			}
			document.getElementById("tr"+chartObj).innerHTML += "<td style='width:158px'></td>";
		}
		
		function validate_form(obj)
		{
			result = true;
			if (document.getElementById('ddlCustomerSearch').value == "")
			{
				document.getElementById('labVddlSalesCodeSearch').innerHTML = "Please Select Sales Code.";
				result = false;
			}else
			{
				document.getElementById('labVddlSalesCodeSearch').innerHTML = "";
				result = true;
			}
		
			return result;
			
		}
		
		function getDdlCustomer()
		{
			
			posting = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					
					var ddlCustomerSearch = document.getElementById("ddlCustomerSearch").innerHTML;
					
					document.getElementById("ddlCustomerSearch").innerHTML = ddlCustomerSearch + " " +str;
					$('.selectpicker').selectpicker('refresh');
					
				}
			}
			params  = 'operate=get_ddl_customer';
			
			//if (typeof(aut_id) !="undefined")  params += "&aut_id="+aut_id;
			
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","report_sales_customer_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
				
			}
			
		}
		
		
		
		
		function resetCondition()
		{
			
			document.getElementById('txbStart').value = todayDDMMYYYY(1);
			document.getElementById("txbFinish").value = todayDDMMYYYY();
			
			
		}
		
		function getDtlData(canvObj,typeId,typeNameTh,typeNameEn)
		{
			
			posting = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp2.onreadystatechange=function() 
			{
				
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200) 
				{
					
					var str = xmlhttp2.responseText.replace("\r\n\t", "");
					if (str.indexOf("ERROR") > 0) alert(str);
					var result = JSON.parse(str);
					var len = result.length;
					
					var barColor = ["#ff6600","#0066cc","#ffff10","#008000"];
					var t_len = typeId.length;
					var trHtml = "";
					//***Loop Product Type***
					for (a=0; a < t_len; a++){
						var prd = "";
						var amount = "";
						var qty = "";
						var prdName = "";
						var prdImg = "";
						var totalQty = 0;
						var totalAmount=0;
						
						for (i=0; i < len; i++){
							
							if (typeId[a] == result[i]["PRODUCT_TYPE_ID"])
							{
								var am = result[i]["AMOUNT"];
								//alert (am.toFixed(2));
								if  (prd == "") prd +=result[i]["PRODUCT_CODE"]; else  prd +="|"+result[i]["PRODUCT_CODE"];
								if  (amount == "") amount +=am; else  amount +="|"+am;
								if  (qty == "") qty +=result[i]["QTY"]; else  qty +="|"+result[i]["QTY"];
								if  (prdName == "") prdName +=result[i]["PRODUCT_NAME_TH"]; else  prdName +="|"+result[i]["PRODUCT_NAME_TH"];
								if  (prdImg == "") prdImg +=result[i]["PRD_IMG"]; else  prdImg +="|"+result[i]["PRD_IMG"];
							}
							//Render tbBody
							
							if (a == 0)
							{
								totalQty += parseFloat(result[i]["QTY"].replace(",",""));
								totalAmount += parseFloat(result[i]["AMOUNT"].replace(",",""));
								trHtml += '<tr>';
								trHtml += '	<td width="100px"> <p>'+result[i]["PRODUCT_CODE"]+"</p> </td>";
								trHtml += '	<td width="110px">  <img id="im'+result[i]["PRODUCT_CODE"]+
												' width="110px" height="110px" src="<?php echo $pro_img_path ?>'+result[i]["PRD_IMG"]+'"  style="margin-bottom: 0px; margin-top: 0px;"> </td>';
								trHtml += '	<td > <p>'+ result[i]["PRODUCT_NAME_TH"]+'</p> </td>';
								trHtml += '	<td width="100px"> '+ result[i]["ICON"]+ result[i]["TYPE_NAME_TH"] +' </td>';
								trHtml += '	<td width="120px"><p>'+result[i]["QTY"] + '</p></td>';
								trHtml += '	<td width="120px"> <p>'+result[i]["AMOUNT"]+ '</p> </td>';
								trHtml += '</tr>';
							}
						}
						if (a == 0)
						{
							var tAmount = totalAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
							//$('span.number').number( true, 2 );
							trHtml += '<tr>';
							trHtml += '	<td></td>';
							trHtml += '	<td></td>';
							trHtml += '	<td></td>';
							trHtml += '	<td><p><b>Total</b></p></td>';
							trHtml += '	<td ><b>'+totalQty+'</b></td>';
							trHtml += '	<td ><b>'+tAmount+'</b></td>';
							trHtml += '<tr>';
						}
						var arrPrd = prd.split("|");
						var arrAmount = amount.split("|");
						var arrQty = qty.split("|");
						var arrPrdName = prdName.split("|");
						var arrPrdImg = prdImg.split("|");
						//alert(canvObj[a] +", Prd="+ arrPrd.length+",Amount="+arrAmount.length+",Qty="+arrQty.length);
						
						genChart(canvObj[a],arrPrd, arrAmount, arrQty,result[0]["CUS_NAME"],typeNameTh[a],typeNameEn[a],arrPrdName,barColor[a],arrPrdImg);
						
					}
					
					if (result.length > 0)
					{
						document.getElementById("divCusName").innerHTML = "<h4>Customer Name :" + result[0]["CUS_NAME"] + "</h4>";
						document.getElementById("tbBody").innerHTML  = trHtml;
						document.getElementById("btExpExcel").onclick = function() { window.location='<?php echo $temp_excel_path; ?>'+result[0]["EXCEL_NAME"]; };
						document.getElementById("btExpExcel").className = "btn-u rounded btn-u-green ";
					} else
					{
						document.getElementById("divCusName").innerHTML = "<h4>Customer Name :</h4>";
						document.getElementById("tbBody").innerHTML  = "";
						document.getElementById("btExpExcel").onclick = "";
						document.getElementById("btExpExcel").className = "btn rounded btn-info disabled ";
					}
					
				}
			}
			params  = 'operate=get_data_customer';
			cusId = document.getElementById("ddlCustomerSearch").value;
			
			startDate = document.getElementById("txbStart").value;
			endDate = document.getElementById("txbFinish").value;
			
			dataType = document.getElementById("hdDataType").value;
			if (typeof(cusId) !="undefined" || cusId=="")  params += "&cus_id=" + cusId;
			
			params += "&prd_type=";
			
			if (typeof(startDate) !="undefined" || startDate == "")  params += "&start_date=" +startDate;
			if (typeof(endDate) !="undefined" || endDate == "")  params += "&end_date=" + endDate;			
			if (typeof(dataType) !="undefined" || dataType == "")  params += "&data_type=" + dataType;
			
			
			posting = true;
			if (posting)
			{
				//alert(params);
				xmlhttp2.open("POST","report_sales_customer_ajax.php",true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params);
				
			}
			
		}
		
		function getDtlDataDaily(canvObj,typeId,typeNameTh,typeNameEn)
		{
			
			posting = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					
					if (str.indexOf("ERROR") > 0) alert(str);
					var result = JSON.parse(str);
					var len = result.length;
					
					var t_len = typeId.length;
					var trHtml = "";
					//***Loop Product Type***
					for (a=0; a < t_len; a++){
						var prd = "";
						var amount = "";
						var qty = "";
						var prdName = "";
						var prdImg = "";
						var totalQty = 0;
						var totalAmount=0;
						
						for (i=0; i < len; i++){
							
							//Render tbBody
							
							if (a == 0)
							{
								totalQty += parseFloat(result[i]["QTY"].replace(",",""));
								totalAmount += parseFloat(result[i]["AMOUNT"].replace(",",""));
								trHtml += '<tr>';
								trHtml += '	<td width="90px"> <p>'+result[i]["ACTION_DATE"]+'</p> </td>';
								trHtml += '	<td width="100px"> <p>'+result[i]["PRODUCT_CODE"]+"</p> </td>";
								trHtml += '	<td width="110px">  <img id="im'+result[i]["PRODUCT_CODE"]+
												' width="110px" height="110px" src="<?php echo $pro_img_path ?>'+result[i]["PRD_IMG"]+'"  style="margin-bottom: 0px; margin-top: 0px;"> </td>';
								trHtml += '	<td > <p>'+ result[i]["PRODUCT_NAME_TH"]+'</p> </td>';
								trHtml += '	<td width="100px"> '+ result[i]["ICON"]+ result[i]["TYPE_NAME_TH"] +' </td>';
								trHtml += '	<td width="80px"><p>'+result[i]["QTY"] + '</p></td>';
								trHtml += '	<td width="100px"> <p>'+result[i]["AMOUNT"]+ '</p> </td>';
								trHtml += '</tr>';
							}
						}
						if (a == 0)
						{
							var tAmount = totalAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
							
							trHtml += '<tr>';
							trHtml += '	<td></td>';
							trHtml += '	<td></td>';
							trHtml += '	<td></td>';
							trHtml += '	<td></td>';
							trHtml += '	<td><p><b>Total</b></p></td>';
							trHtml += '	<td ><b>'+totalQty+'</b></td>';
							trHtml += '	<td ><b>'+tAmount+'</b></td>';
							trHtml += '<tr>';
						}
						
					}
					//alert(result.length);
					
					if (result.length > 0)
					{
						document.getElementById("tbDaily").innerHTML  = trHtml;
						document.getElementById("btExpExcelDaily").onclick = function() { window.location='<?php echo $temp_excel_path; ?>'+result[0]["EXCEL_NAME"]; };
						document.getElementById("btExpExcelDaily").className = "btn-u rounded btn-u-green ";
					} else
					{
						
						document.getElementById("tbDaily").innerHTML  = "";
						document.getElementById("btExpExcelDaily").onclick = "";
						document.getElementById("btExpExcelDaily").className = "btn rounded btn-info disabled ";
					}
					
					
					
				}
			}
			params  = 'operate=get_data_customer_daily';
			cusId = document.getElementById("ddlCustomerSearch").value;
			//mounthSearcg = document.getElementById("ddlMonthSearch").value;
			//yearSearch = document.getElementById("ddlYearSearch").value;
			startDate = document.getElementById("txbStart").value;
			endDate = document.getElementById("txbFinish").value;
			dataType = document.getElementById("hdDataType").value;
			if (typeof(cusId) !="undefined" || cusId=="")  params += "&cus_id=" + cusId;
			
			params += "&prd_type=";
			
			if (typeof(startDate) !="undefined" || startDate == "")  params += "&start_date=" +startDate;
			if (typeof(endDate) !="undefined" || endDate == "")  params += "&end_date=" + endDate;			
			if (typeof(dataType) !="undefined" || dataType == "")  params += "&data_type=" + dataType;
			
			
			posting = true;
			if (posting)
			{
				
				xmlhttp.open("POST","report_sales_customer_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
				
			}
			
		}
		
		function getData(dataType)
		{
			if (typeof(dataType) !="undefined" || dataType == "")
			{
				document.getElementById("hdDataType").value = dataType;
			} 
			
			if (document.getElementById("hdDataType").value == "sale" )
			{
				document.getElementById("divSubject").innerHTML =  "<h2><i class='fa fa-btc'></i><span class='break'></span>Sales Report Chart</h2>";
				//document.getElementById("divConDate").style.visibility = "visible";
				document.getElementById("divConDate").style.display = "";
				document.getElementById("liDaily").style.visibility = "visible";
				
			}else if(document.getElementById("hdDataType").value == "order" )
			{
				document.getElementById("divSubject").innerHTML =  "<h2><i class='fa fa-bell-o'></i><span class='break'></span>Order Report Chart</h2>";
				//document.getElementById("divConDate").style.visibility = "visible";
				document.getElementById("divConDate").style.display = "";
				document.getElementById("liDaily").style.visibility = "visible";
			}
			else if (document.getElementById("hdDataType").value == "stock" )
			{
				document.getElementById("divSubject").innerHTML =  "<h2><i class='fa fa-database'></i><span class='break'></span>Stock Report Chart</h2>";
				//document.getElementById("divConDate").style.visibility = "hidden";
				document.getElementById("divConDate").style.display = "none";
				document.getElementById("liDaily").style.visibility = "hidden";
			}
			
			posting = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp2.onreadystatechange=function() 
			{
				
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200) 
				{
					
					var source = "";
					var str = xmlhttp2.responseText.replace("\r\n\t", "");
					var resolt = JSON.parse(str);
					//alert(resolt.length);
					var len = resolt.length;
					var canvObj = [];
					var typeId = [];
					var typeNameTh = [];
					var typeNameEn = [];
					// Create Object for Chart and  pust array data
					for (i=0; i < len; i++) { 
						source += "<canvas id ='canvas"+ resolt[i]["TYPE_NAME_EN"].replace(" ", "") +"'></canvas> ";
						source += "<table id='tb"+resolt[i]["TYPE_NAME_EN"].replace(" ", "") +"' ><tr id='tr"+ resolt[i]["TYPE_NAME_EN"].replace(" ", "") +"' ></tr></table>";
						source += "<br/>";
						canvObj.push(resolt[i]["TYPE_NAME_EN"].replace(" ", ""));
						typeId.push(resolt[i]["PRODUCT_TYPE_ID"]);
						typeNameTh.push(resolt[i]["TYPE_NAME_TH"]);
						typeNameEn.push(resolt[i]["TYPE_NAME_EN"]);
					}

					document.getElementById("divDispChart").innerHTML = source;
					
					getDtlData(canvObj,typeId,typeNameTh,typeNameEn);
					if (document.getElementById("hdDataType").value != "stock" )
					{
						getDtlDataDaily(canvObj,typeId,typeNameTh,typeNameEn);
					}
					
				}
			}
			params  = 'operate=get_prd_type';
			//params  += '&data_type=' + dataType ;
			//alert(params);
			posting = true;
			if (posting)
			{
				xmlhttp2.open("POST","report_sales_customer_ajax.php",true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params);
				
			}
			
		}
		 
		 
	</script>
	<div id="wrap"  class="wrapper">

		<?php require("include_header.php"); ?>

		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs breadcrumbs-dark">
			<div class="container">
				<h1 class="pull-left">SALES REPORT FOR CUSTOMER AND STOCK</h1>
				
			</div>
		</div>
		
		<div id="divTbResult" class="container content-sm"  style="padding:30px;">
			<div class="col-sm-12">
				<input type="hidden" name="hdDataType" id="hdDataType"  value="sale" />
				
				<!-- Tabs -->
				<div class="tab-v1">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tabReport" data-toggle="tab" onclick="getData('sale');">Sale Report</a></li>
						<li><a href="#tabReport" data-toggle="tab" onclick="getData('order');">Order Report</a></li>
						<li><a href="#tabReport" data-toggle="tab" onclick="getData('stock');">Stock Report</a></li> 
					</ul>                
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tabReport">
							<!--=== Search Block Version 2 ===-->
	   
							<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
								
								<div class="container">
									<div class="col-md-6 col-md-offset-2">
										<h2>CONDITION</h2>
										<div class="panel panel-grey margin-bottom-20" style="padding: 12px;">
											
											<div class="sky-form" style="border-style:none">                                                      
												<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="frm_rep_amount_stock" 
													id="frm_rep_amount_stock" onSubmit="return validate_form(this)" >
													<div class="form-group">
														<label for="inputSalesCode" class="col-lg-4 control-label">CUSTOMER <font color="#ff0000">*</font></label>
														
														<div class="col-lg-8" style="height: 35px">
															<label class="select" >
															
																<select name="ddlCustomerSearch" id="ddlCustomerSearch"  class="selectpicker form-control"  
																data-live-search="true"  title="Please select ...">
																	<option value="" selected> - Please Select - </option>
																	
																</select >
																	
															</label>
															
														</div>
													</div>
													
													<div id="divConDate" class="form-group">
														<label for="inputSalesCode" class="col-lg-4 control-label">Date<font color="#ff0000">*</font></label>
														<section class="col col-lg-4" style="height: 16px">
															<label class="input">
																<i class="icon-append fa fa-calendar"></i>
																<input type="text" class="form-control" name="txbStart" id="txbStart" placeholder="Start date" value="<?php echo $_POST['txbStart']; ?>">
															</label>
														</section>

														<section class="col col-lg-4" style="height: 16px">
															<label class="input">
																<i class="icon-append fa fa-calendar"></i>
																<input type="text" class="form-control" name="txbFinish" id="txbFinish" placeholder="End date" value="<?php echo $_POST['txbFinish']; ?>">
															</label>
														</section>
														
														
													</div>
													
													<div class="form-group">
														<div class="col-lg-offset-4 col-lg-8">
															<button  type="button" name="btSubmit" id="btSubmit" class="btn-u" onclick="getData(document.getElementById('hdDataType').value);"> 
																Search </button> &nbsp;&nbsp;
															<button type="button" class="btn-u btn-u-default">Clear</button>
														</div>
													</div>

												</form>

											</div>
										</div>

									</div>
									
								</div>    
								
							</div><!--/container--> 
							<div class="box">
								<div class="box-header">
									<div id = "divSubject"><h2><i class="fa fa-btc"></i><span class="break"></span>Sales Report Chart</h2></div>
									<div class="box-icon" id="divCusName">
										<h4>Customer Name</h4>
									</div>
								</div>
						
								<div id="divDispChart" style="" class="box-content;text" align="center" >
									
								</div>
								
								
								<br/><br/>
								
								<div class="tab-v1" style="padding:30px;">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tabSummary" data-toggle="tab" onclick="">Summary</a></li>
										<li id="liDaily"><a href="#tabDaily" data-toggle="tab" onclick="">Daily</a></li>
									</ul>                
									<div class="tab-content">
										<div class="tab-pane fade in active" id="tabSummary">
											<div style="padding:10px;">
												<button class="btn rounded btn-info disabled" type="button" id="btExpExcel"  name="btExpExcel" onclick="">
													<font size="3"><i class="fa fa-file-excel-o"></i></font>&nbsp; Export Excel
												</button>
											</div>
									
											<!-- Sales Detail Table -->
											<table class="table table-bordered table-striped">
												<thead>
													<tr>
														
														<th>Pro. Code</th>
														<th class="hidden-sm">Pro. Image</th>
														<th>Product Name</th>
														<th>Product Type</th>
														<th>QTY</th>
														<th>Amount Baht</th>
														
													</tr>
												</thead>
												<tbody id="tbBody" name="tbBody">
													
												</tbody>
											</table>
										</div>
											
										
										<div class="tab-pane fade" id="tabDaily">
											<div style="padding:10px;">
												<button class="btn rounded btn-info disabled" type="button" id="btExpExcelDaily"  name="btExpExcelDaily" onclick="">
													<font size="3"><i class="fa fa-file-excel-o"></i></font>&nbsp; Export Excel
												</button>
											</div>
											<!-- Sales Detail Table -->
											<table class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>Date</th>
														<th>Pro. Code</th>
														<th class="hidden-sm">Pro. Image</th>
														<th>Product Name</th>
														<th>Product Type</th>
														<th>QTY</th>
														<th>Amount Baht</th>
														
													</tr>
												</thead>
												<tbody id="tbDaily" name="tbDaily">
													
												</tbody>
											</table>
										</div>
										
									</div>
								</div>
							
							<!-- END Sales Detail Table -->
							</div>
						</div>
					</div>
				</div><!--/col-->
			
			</div>
		</div>
		
		
		<?php require("include_footer.php"); ?>
		 
	</div><!--/End Wrapepr-->

<?php require("include_js.php"); ?>
<script type="text/javascript">
									
	getDdlCustomer();
	resetCondition();
	//getDdlYear();
	
	//getData();
	
</script>


</body>
</html> 
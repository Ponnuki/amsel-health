<?php 

error_reporting(E_ERROR);
ini_set('display_errors', 1);

require_once('class_amh_db.php');
require_once('class_amh_pc.php');

$amh_pc = new AMH_PC();

$str_ret = "";

$arr_params = json_decode($_REQUEST["params"], 1);

$arr_aut  = $amh_pc->get_aut_user(" AUT_ID = '".$_REQUEST["aut_id"]."' ");
$aut_name = $arr_aut[0]["AUT_UNAME"];
$the_date = $amh_pc->swap_date($_REQUEST["the_date"]);

$str_del = $amh_pc->del_order_dtl_backend($the_date, $aut_name, $_REQUEST["cus_id"]);

foreach ($arr_params as $num => $params)
{
    $amh_pc->add_order_dtl($aut_name, $params, $the_date, "no add", $_REQUEST["cus_id"]);
    //$str_ret .= "--------<br>\n";
    $str_ret .= $num." prd_pc_id:".$params["prd_pc_id"]." qty:".$params["qty"]."\n";
}

echo "Updated ".count($arr_params)." item(s)\n".$aut_name."\n".$the_date."\n".$str_ret."\n".$str_del;
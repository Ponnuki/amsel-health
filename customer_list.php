<?php

require_once('class_amh_customer.php');
require_once('class_amh_province.php');
require_once('class_amh_district.php');

$amh_customer = new AMH_Customer();
$amh_province = new AMH_Province();
$amh_district = new AMH_District();

if ($_REQUEST["page_num"] == "")
{
    $this_page_num = 1;
}
else
{
    $this_page_num = $_REQUEST["page_num"];
}

$rows_per_page = "20";
$num_rows      = count($amh_customer->mst_customer($_REQUEST["str_search"], "", $rows_per_page));
$all_pages     = ceil($num_rows / $rows_per_page);

$mst_customer = $amh_customer->mst_customer($_REQUEST["str_search"], $this_page_num, $rows_per_page);
$arr_label    = $amh_customer->get_customer_label();

$rowstart = ($this_page_num * $rows_per_page) - $rows_per_page + 1;
$rowstop  = ($this_page_num * $rows_per_page);

if ($rowstop > $num_rows) { $rowstop = $num_rows; }

$str_list .= "<div style='border-bottom: 1px dotted #999' align='center'>";
$str_list .= "<ul class='pagination'>";

for($i_page = 1; $i_page <= $all_pages; $i_page++)
{
    if ($i_page == $this_page_num) { $page_active = "class='active'"; } else { $page_active = ""; }
    $str_list .= "<li onclick='get_customer_list(".$i_page.")' ".$page_active."><a href='#'>".$i_page."</a></li>\n";
}
$str_list .= "</ul>";
$str_list .= "<input type='HIDDEN' id='this_page_num' name='this_page_num' value='".$this_page_num."'>";
$str_list .= "<div style='margin-bottom:15px;'>Show customer from ".$rowstart." to ".$rowstop." (all ".$num_rows.")</div>";
$str_list .= "</div>";

$arr_pc_usr   = $amh_customer->get_aut_user_array("");

$num_row   = 0;
$str_list .= "<table class='table_this'>";
$str_list .= "<tbody>";
$str_list .= "<tr>";
$str_list .= "<td width='10%' style='border-bottom:1px solid #aaa; font-weight:bold;'></td>";
$str_list .= "<td width='15%' style='border-bottom:1px solid #aaa; font-weight:bold;'>".$arr_label["c_code"] ."</td>";
$str_list .= "<td width='18%' style='border-bottom:1px solid #aaa; font-weight:bold;'>".$arr_label["c_name"] ."</td>";
$str_list .= "<td width='18%' style='border-bottom:1px solid #aaa; font-weight:bold;'>".$arr_label["c_cntc"] ."</td>";
$str_list .= "<td width='15%' style='border-bottom:1px solid #aaa; font-weight:bold;'>".$arr_label["c_phone"]."</td>";
$str_list .= "<td width='14%' style='border-bottom:1px solid #aaa; font-weight:bold;'>".$arr_label["tax_num"]."</td>";
$str_list .= "<td width='10%' style='border-bottom:1px solid #aaa; font-weight:bold;'></td>";
$str_list .= "</tr>";
$str_list .= "</tbody>";
$str_list .= "</table>";

foreach ($mst_customer as $customer)
{
    $num_row++;

    $shw_cus_id = $customer["CUS_ID"] + 100000;

    if ($customer["ACTIVE_FLAG"] == 'Y') { $y_selected = " selected"; $n_selected = ""; }
                                    else { $n_selected = " selected"; $y_selected = ""; }

    $the_opt = $amh_customer->write_pc_option($arr_pc_usr, $customer["AUT_ID"]);
    $cus_opt = $amh_customer->write_cus_type_option($customer["CUS_TYPE_ID"]);

    $str_list .= "<table class='table_this'>";
    $str_list .= "<thead>";
    $str_list .= "<tr class='table_this_thead_tr'>";
    $str_list .= "<td width='10%'><button type='BUTTON' class='btn-u btn-u-sm btn-u-blue' onclick='expanding_detail(\"".$customer["CUS_ID"]."\")'><i class='fa fa-edit'></i>&nbsp;edit</button></td>";
    $str_list .= "<td width='15%'>".$customer["CUS_CODE"]  ."</td>";
    $str_list .= "<td width='18%'>".$customer["CUS_NAME"]  ."</td>";
    $str_list .= "<td width='18%'>".$customer["CUS_CONTACT"]."</td>";
    $str_list .= "<td width='15%'>".$customer["CUS_TEL"]   ."</td>";
    $str_list .= "<td width='14%'>".$customer["TAX_NUMBER"]."</td>";
    $str_list .= "<td width='10%'><button type='BUTTON' class='btn-u btn-u-sm btn-u-red' onclick='form_post(".$customer["CUS_ID"].",\"del\");'><i class='fa fa-trash-o'></i>&nbsp;delete</button></td>";
    $str_list .= "</tr>";
    $str_list .= "</thead>\n";
    $str_list .= "<tbody id='tbody_".$customer["CUS_ID"]."' style='display:none;'>";
    $str_list .= "<tr>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_code"] ."</div><input type='TEXT' class='table_input' id='cus_code_"  .$customer["CUS_ID"]."' value='".$customer["CUS_CODE"]   ."'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_name"] ."</div><input type='TEXT' class='table_input' id='cus_name_"  .$customer["CUS_ID"]."' value='".$customer["CUS_NAME"]   ."'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_cntc"] ."</div><input type='TEXT' class='table_input' id='cus_cntc_"  .$customer["CUS_ID"]."' value='".$customer["CUS_CONTACT"]."'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_phone"]."</div><input type='TEXT' class='table_input' id='cus_tel_"   .$customer["CUS_ID"]."' value='".$customer["CUS_TEL"]    ."'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_fax"]  ."</div><input type='TEXT' class='table_input' id='cus_fax_"   .$customer["CUS_ID"]."' value='".$customer["CUS_FAX"]    ."'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_email"]."</div><input type='TEXT' class='table_input' id='cus_email_" .$customer["CUS_ID"]."' value='".$customer["CUS_EMAIL"]  ."'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'>";
    $str_list .= "<div class='table_label'>".$arr_label["c_zip"]  ."</div>";
    $str_list .= "<input type='TEXT'   class='table_input' id='cus_zip_"     .$customer["CUS_ID"]."' value='".$customer["CUS_ZIP"]     ."'>";
    $str_list .= "<input type='HIDDEN' class='table_input' id='cus_country_" .$customer["CUS_ID"]."' value='".$customer["CUS_COUNTRY"] ."'>";
    $str_list .= "</td>";
    $str_list .= "</tr>\n";

    $str_list .= "<tr>";
    $str_list .= "<td colspan='3' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_addr"] ."</div><input type='TEXT' class='table_input' id='cus_address_" .$customer["CUS_ID"]."' value='".$customer["CUS_ADDRESS"] ."'></td>";
    
    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_dist"] ."</div>";
    $str_list .= "<select class='table_input' id='cus_district_".$customer["CUS_ID"]."'>".$amh_district->create_district_option($customer["DISTRICT_CODE"], $customer["PROVINCE_CODE"])."</select>";
    $str_list .= "</td>";
    //$str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_dist"] ."</div><input type='TEXT' class='table_input' id='cus_district_".$customer["CUS_ID"]."' value='".$customer["CUS_DISTRICT"]."'></td>";

    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_prov"] ."</div>";
    $str_list .= "<select class='table_input' id='cus_province_".$customer["CUS_ID"]."' onchange='get_district_option(this.value, \"cus_district_".$customer["CUS_ID"]."\");'>".$amh_province->create_province_option($customer["PROVINCE_CODE"])."</select>";
    $str_list .= "</td>";
    //$str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["c_prov"] ."</div><input type='TEXT' class='table_input' id='cus_province_".$customer["CUS_ID"]."' value='".$customer["CUS_PROVINCE"]."'></td>";

    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["tax_num"]."</div><input type='TEXT' class='table_input' id='tax_number_".$customer["CUS_ID"]."' value='".$customer["TAX_NUMBER"] ."'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'><div class='table_label'>".$arr_label["active"] ."</div>";
    $str_list .= "<div>";
    $str_list .= "<select id='active_".$customer["CUS_ID"]."' style='border:1px solid #999; padding:2px; border-radius:2px;'>";
    $str_list .= "<option value='Y'{$y_selected}>Yes</option>";
    $str_list .= "<option value='N'{$n_selected}>No</option>";
    $str_list .= "</select>";
    $str_list .= "</div>";
    $str_list .= "</td>";

    $str_list .= "</tr>\n";

    $str_list .= "<tr>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'>";
    $str_list .= "<div class='table_label'>PC Person</div>";
    $str_list .= "<div><select id='cus_pc_".$customer["CUS_ID"]."'><option value=''>--------</option>".$the_opt."</select></div>";
    $str_list .= "</td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'>";
    $str_list .= "<div class='table_label'>Customer Type</div>";
    $str_list .= "<div><select id='cus_type_".$customer["CUS_ID"]."'><option value=''>--------</option>".$cus_opt."</select></div>";
    $str_list .= "</td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'>";
    $str_list .= "<div class='table_label'>Area Code</div>";
    $str_list .= "<input type='TEXT' class='table_input' id='cus_area_code_".$customer["CUS_ID"]."' value='".$customer["AREA_CODE"]."'>";
    $str_list .= "</td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'>";
    $str_list .= "<div class='table_label'>Area Name</div>";
    $str_list .= "<input type='TEXT' class='table_input' id='cus_area_name_".$customer["CUS_ID"]."' value='".$customer["AREA_NAME"]."'>";
    $str_list .= "</td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'></td>";
    $str_list .= "<td colspan='1' style='background-color:#eee;'></td>";
    $str_list .= "</tr>\n";

    $str_list .= "<tr>";
    $str_list .= "<td colspan='7' align='right' style='background-color:#ccc; padding:10px 5px;'>";
    $str_list .= "<button type='BUTTON' class='btn-u btn-u-sm btn-u-blue' style='margin:0 5px;' onclick='form_post(".$customer["CUS_ID"].",\"upd\");'><span class='glyphicon glyphicon-plus-sign'> </span> Save</button>";
    $str_list .= "<button type='BUTTON' class='btn-u btn-u-sm btn-u-red'  style='margin:0 5px;' onclick='hiding_detail();'><span class='glyphicon glyphicon-plus-sign'> </span> Cancel</button>";
    $str_list .= "</tr>\n";

    $str_list .= "<tr><td colspan='7' style='border-bottom:5px solid #999; padding:0;'><div></div></td></tr>\n";
    $str_list .= "<tr><td colspan='7' style='border-bottom:20px solid #fff; padding:0;'><div></div></td></tr>\n";
    $str_list .= "</tbody>";
    $str_list .= "</table>";

    //$str_list .= "<div style='min-height: 10px;'></div>\n";
}

//echo "<table class='table_this'>{$str_head}<tbody>{$str_list}</tbody></table>";
echo $str_list;
<?php 

error_reporting(E_ALL);
ini_set('display_errors', 1);

$rpt_id   = $_REQUEST["sale_rpt_id"];
$aut_id   = $_REQUEST["aut_id"];

$str_ret  = "";

if (($rpt_id != "") AND ($aut_id != ""))
{
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $amh_pc   = new AMH_PC();

    $arr_pc_rel = $amh_pc->get_sale_rpt_rel($rpt_id, $aut_id);

    $str_ret = json_encode($arr_pc_rel);
}

echo $str_ret;
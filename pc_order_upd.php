<?php session_start();

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $amh_pc  = new AMH_PC();
    $str_ret = "";

    if ($_REQUEST["action"] == "add")
    {
        $str_ret = $amh_pc->add_order_dtl($user_name, $_POST, "", "", $_REQUEST["cus_id"]);
    }

    if ($_REQUEST["action"] == "del")
    {
        $str_ret = $amh_pc->del_order_dtl($user_name, $_REQUEST["order_dtl_id"]);
    }

    if ($_REQUEST["action"] == "upd")
    {
        $str_ret = $amh_pc->upd_order_dtl($user_name, $_REQUEST["order_dtl_id"], $_REQUEST["field"], $_REQUEST["value"] );
    }
}
echo $str_ret;
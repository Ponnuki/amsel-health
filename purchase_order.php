<?php 

    require_once("include_function.php");
    require_once("connect.php");
    require_once('validatelogin.php');

    require_once('class_amh_customer.php');
    require_once('class_amh_province.php');
    //require_once('class_amh_po.php');
    require_once('class_amh_pc.php');

    $amh_customer = new AMH_Customer();
    $amh_province = new AMH_Province();
    //$amh_po       = new AMH_PO();
    $amh_pc       = new AMH_PC();

    $this_date    = Date('d-m-Y');
    $last_7day    = Date('d-m-Y', strtotime("-7 days"));

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<title>AMSEL HEALTH SELECT | PURCHASE ORDER</title>
<?php
    $current_menu = "purchase_order";
    require("include_headtag.php");
?>
<link href="stylesheets/uploadfile.css" rel="stylesheet">
<style type='text/css'>
.box_qty
{
    text-align: center;
}

.txt_red
{
    color: #e22;
}

.txt_bold
{
    font-weight: bold;
}

.waiting_text
{
    color: #3B3;
    font-size: 24px;
    font-weight: bold;
    text-align: center;
}
</style>

<script type="text/javascript">

    var num_items = 0;
    var arr_items = new Array();
    var arr_lists = new Array();
    var arr_pcoid = new Array();
    var arr_label = {
        waiting_text: "เรียกดูข้อมูลรายการ PO"
    };

<?php

    //$arr_prd = $amh_po->mst_product_sale('','');

    $the_date = date('Y-m-d');
/*
    $arr_prd = $amh_pc->get_product_pc($the_date, $type_id);

    $str_javas = '';
    echo "//".count($arr_prd)."\r\n";

    foreach ($arr_prd as $prd)
    {
        $str_javas .= "        ";
        $str_javas .= "arr_items["     .$prd["PRODUCT_PC_ID"]    ."] = { ";
        $str_javas .= "product_code: '".$prd["PRODUCT_CODE"]     ."',";
        $str_javas .= "name_en:      '".$prd["PRODUCT_NAME_EN"]  ."',";
        $str_javas .= "name_th:      '".$prd["PRODUCT_NAME_TH"]  ."',";
        $str_javas .= "sale_code:    '".$prd["PRODUCT_CODE"]     ."',";
        $str_javas .= "sticker_price:'".$prd["STICKER_PRICE"]    ."',";
        $str_javas .= "ltp:          '".$prd["LTP_PRICE"]        ."' ";
        $str_javas .= "};"."\r\n";
    }

    echo $str_javas;
*/
?>
    function start_up()
    {
        reload_po_list('','','');
        reload_prd_pc();
        $('.selectpicker').selectpicker();

        /*
        $(document).ready(function() {
            $("#fileuploader").uploadFile({
                url:"purchase_order_upload.php",
                fileName:"myfile"
            });
        });
        */
    }
        function reload_prd_pc()
        {
            the_date = $('#txbTranDateEnd').val();

            $.post( "purchase_order_get_prd_pc.php", { the_date: the_date })
                .done(function(data)
                {
                    var arr_prd_pc = jQuery.parseJSON(data);
                    arr_items = [];

                    for (i_prd = 0; i_prd < arr_prd_pc.length; i_prd++)
                    {
                        prd_pc_id = arr_prd_pc[i_prd]["PRODUCT_PC_ID"];

                        arr_items[prd_pc_id] = {
                            product_code:  arr_prd_pc[i_prd]["PRODUCT_CODE"],
                            name_en:       arr_prd_pc[i_prd]["PRODUCT_NAME_EN"],
                            name_th:       arr_prd_pc[i_prd]["PRODUCT_NAME_TH"],
                            sale_code:     arr_prd_pc[i_prd]["PRODUCT_CODE"],
                            sticker_price: arr_prd_pc[i_prd]["STICKER_PRICE"],
                            ltp:           arr_prd_pc[i_prd]["LTP_PRICE"]
                        }
                    }
                });
        }

        function refresh_po_items_list()
        {
            $('#item_detail_area').html('');

            for (i_cnt = 1; i_cnt <= num_items; i_cnt++)
            {
                the_qty = arr_lists[i_cnt].unit_quan * 1;
                the_qty = the_qty.toFixed(0);
                arr_lists[i_cnt].unit_quan = the_qty;

                str_add  = "<div class='form-group'>";
                str_add += "<div class='col-lg-2'>";
                str_add += i_cnt+"&nbsp;<input type='TEXT' class='form-control' style='width:125px; display:inline-block;' id='sale_id_show_"+i_cnt+"' value='' readonly='readonly'>";
                str_add += "<select class='form-control selectpicker' style='display:none;' name='sale_id_"+i_cnt+"' id='sale_id_"+i_cnt+"' onchange='change_prd_id("+i_cnt+");' style='width:80%; display:inline-block;'><option value=''>PRODUCT CODE</option><?php echo $amh_pc->create_product_code_option('',$the_date); ?></select>";
                str_add += "</div>";
                str_add += "<div class='col-lg-2'><input type='text' class='form-control' name='prdt_name_"+i_cnt+"' id='prdt_name_"+i_cnt+"' value='"+arr_lists[i_cnt].prdt_name+"' onchange='upd_item_to_array("+i_cnt+");'></div>";
                /*
                str_add += "<div class='col-lg-2'><input type='text' class='form-control' name='unit_prce_"+i_cnt+"' id='unit_prce_"+i_cnt+"' value='"+arr_lists[i_cnt].unit_prce+"' onchange='recalc_itm_total("+i_cnt+");'></div>";
                */
                str_add += "<div class='col-lg-2'><input type='text' class='form-control' name='unit_prce_"+i_cnt+"' id='unit_prce_"+i_cnt+"' value='"+arr_lists[i_cnt].unit_prce+"' readonly></div>";
                str_add += "<div class='col-lg-1'><input type='text' class='form-control' name='unit_quan_"+i_cnt+"' id='unit_quan_"+i_cnt+"' value='"+arr_lists[i_cnt].unit_quan+"' onchange='recalc_itm_total("+i_cnt+");'></div>";
                str_add += "<div class='col-lg-2'><input type='text' class='form-control' name='unti_totl_"+i_cnt+"' id='unti_totl_"+i_cnt+"' value='"+arr_lists[i_cnt].unti_totl+"' readonly></div>";
                str_add += "<div class='col-lg-2'><input type='text' class='form-control' name='prdt_desc_"+i_cnt+"' id='prdt_desc_"+i_cnt+"' value='"+arr_lists[i_cnt].prdt_desc+"' onchange='upd_item_to_array("+i_cnt+");''></div>";
                str_add += "<div class='col-lg-1'><button type='button' class='btn-u btn-u-sm btn-u-red' onclick='del_po_item("+i_cnt+")'>Delete</button></div>";
                str_add += "</div>";

                $('#item_detail_area').append(str_add);
                the_id = arr_lists[i_cnt].sale_id;
                document.getElementById("sale_id_"+i_cnt).value = the_id;
                document.getElementById("sale_id_show_"+i_cnt).value = arr_items[the_id].product_code;
            }
        }

        function clear_adding_row()
        {
            $('#txbPrdCode').val('');
            $('#txbPrdCode').selectpicker('refresh');

            $('#txbPrdName').val('');
            $('#txbUntPrce').val(0);
            $('#txbUntQuan').val(1);
            $('#txbTotal'  ).val(0);
            $('#txbDescrip').val('');
        }

        function dup_item()
        {
            int_ret = 0;

            this_item_id = $("#txbPrdCode").val();
            for (i_cnt = 1; i_cnt <= num_items; i_cnt++)
            {
                each_item_id = $("#sale_id_"+i_cnt).val();
                if (this_item_id == each_item_id) { int_ret = i_cnt; }
            }

            return int_ret;
        }

        function add_po_item(pco_id)
        {
            num_dup = dup_item();

            if (num_dup == 0)
            {
                num_items++;
                add_item_to_array(num_items, pco_id);
                clear_adding_row();
                refresh_po_items_list();
                calc_grand_total();
            }
            else
            {
                alert('ซ้ำรายการที่ '+num_dup);
            }
        }

        function del_po_item(row_id)
        {
            if (confirm('ยืนยันลบรายการที่'+row_id))
            {
                num_items = num_items-1;
                arr_lists.splice(row_id, 1);
                arr_pcoid.splice(row_id, 1);
                refresh_po_items_list();
                calc_grand_total();
            }
        }

        function calc_grand_total()
        {
            dis_percent = $('#txbDscntPercent').val();
            vat_percent = $('#txbVATPercent'  ).val();

            grd_total   = 0;
            num_total   = 0;
            vat_total   = 0;
            dis_amount  = 0;
            
            for (i_cnt =1; i_cnt <= num_items; i_cnt++)
            {
                num_total = (num_total * 1) + (document.getElementById('unti_totl_'+i_cnt).value * 1);
            }

            dis_amount = (num_total/100) * dis_percent;
            grd_total  = num_total - dis_amount;
            vat_total  = (grd_total / 100) * vat_percent;
            grd_total  = (grd_total * 1) + (vat_total * 1);

            document.getElementById('txbPOTotal'    ).value = num_total.toFixed(2);
            document.getElementById('txbDscntAmount').value = dis_amount.toFixed(2);
            document.getElementById('txbVATAmount'  ).value = vat_total.toFixed(2);
            document.getElementById('txbGrandTotal' ).value = grd_total.toFixed(2);
        }

        function recalc_add_total()
        {
            num_total = $('#txbUntPrce').val() * $('#txbUntQuan').val();
            document.getElementById("txbTotal").value = num_total.toFixed(2);
        }

        function recalc_itm_total(row_id)
        {
            num_total = $('#unit_prce_'+row_id).val() * $('#unit_quan_'+row_id).val();
            document.getElementById('unti_totl_'+row_id).value = num_total.toFixed(2);
            upd_item_to_array(row_id);
            calc_grand_total();
        }

        function fill_prd_detail(prd_id)
        {
            //alert(prd_id);
            num_total = arr_items[prd_id].ltp * $('#txbUntQuan').val();
            document.getElementById("txbPrdName").value = arr_items[prd_id].name_th;
            document.getElementById("txbUntPrce").value = arr_items[prd_id].ltp;
            document.getElementById("txbTotal"  ).value = num_total.toFixed(2);
        }

        function change_prd_id(row_id)
        {
            prd_id    = document.getElementById('sale_id_'+row_id).value;

            num_total = arr_items[prd_id].ltp * document.getElementById('unit_quan_'+row_id).value;
            document.getElementById("prdt_name_"+row_id).value = arr_items[prd_id].name_th;
            document.getElementById("unit_prce_"+row_id).value = arr_items[prd_id].ltp;
            document.getElementById("unti_totl_"+row_id).value = num_total.toFixed(2);

            upd_item_to_array(row_id);

            calc_grand_total();
        }

        function testing()
        {
            $('#txbPoDate').val('09-03-2016');
        }

        function add_item_to_array(row_id, pco_id)
        {
            sale_id   = $('#txbPrdCode').val();
            sale_code = sale_id;
            //sale_code = $("#txbPrdCode option[value='"+sale_id+"']").text();
            prdt_name = $('#txbPrdName').val();
            unit_prce = $('#txbUntPrce').val();
            unit_quan = $('#txbUntQuan').val();
            unti_totl = $('#txbTotal'  ).val();
            prdt_desc = $('#txbDescrip').val();

            arr_lists[row_id] = {
                sale_id: sale_id,
                sale_code: sale_code,
                prdt_name: prdt_name,
                unit_prce: unit_prce,
                unit_quan: unit_quan,
                unti_totl: unti_totl,
                prdt_desc: prdt_desc
            };

            arr_pcoid[row_id] = pco_id;
        }

        function upd_item_to_array(row_id)
        {
            prd_id = document.getElementById('sale_id_'+row_id).value;

            arr_lists[row_id].sale_id   = prd_id;
            arr_lists[row_id].sale_code = arr_items[prd_id].sale_code;
            arr_lists[row_id].prdt_name = document.getElementById('prdt_name_'+row_id).value;
            arr_lists[row_id].unit_prce = document.getElementById("unit_prce_"+row_id).value;
            arr_lists[row_id].unit_quan = document.getElementById('unit_quan_'+row_id).value;
            arr_lists[row_id].unti_totl = document.getElementById("unti_totl_"+row_id).value;
            arr_lists[row_id].prdt_desc = document.getElementById("prdt_desc_"+row_id).value;
        }

        function do_searching()
        {
            po_num   = document.getElementById("txbTranNum").value;
            str_date = document.getElementById("txbTranDateStart").value;
            end_date = document.getElementById("txbTranDateEnd").value;

            reload_po_list(po_num, str_date, end_date);
        }

        function reload_po_list(po_num, str_date, end_date)
        {
            document.getElementById('po_list_item').innerHTML = "<div class='waiting_text'>"+arr_label["waiting_text"]+"</div>";

            if (window.XMLHttpRequest) { xmlHTTPpo=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                  else { xmlHTTPpo=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

            xmlHTTPpo.onreadystatechange=function()
            {
                if (xmlHTTPpo.readyState==4 && xmlHTTPpo.status==200)
                {
                    str_response = xmlHTTPpo.responseText;
                    if (str_response != '')
                    {
                        document.getElementById('po_list_item').innerHTML = str_response;
                    }
                }
            }

            str_param = '?po_num='+po_num+'&str_date='+str_date+'&end_date='+end_date;

            xmlHTTPpo.open('GET','purchase_order_list_item.php'+str_param, true);
            xmlHTTPpo.send();
        }

        function show_form_adding()
        {
            document.getElementById("lower_area"    ).style.display = 'none';
            document.getElementById("searching_area").style.display = 'none';
            document.getElementById("form_importing").style.display = 'none';
            document.getElementById("form_adding"   ).style.display = 'block';
            window.scroll(0, findPos(document.getElementById("form_adding")));
        }

        function show_form_import()
        {
            get_pc_order_list();
            document.getElementById("lower_area"    ).style.display = 'none';
            document.getElementById("searching_area").style.display = 'none';
            document.getElementById("form_adding"   ).style.display = 'none';
            document.getElementById("form_importing").style.display = 'block';
            window.scroll(0, findPos(document.getElementById("form_importing")));
        }

        function go_ready_stage()
        {
            var t_date = new Date();
            var yyyy = t_date.getFullYear().toString();
            var mm   = (t_date.getMonth()+1).toString();
            var dd   = t_date.getDate().toString();

            if (mm.length < 2) { the_mth = '0'+mm; } else { the_mth = mm; }
            if (dd.length < 2) { the_day = '0'+dd; } else { the_day = dd; }

            this_date = the_day + '-' + the_mth + '-' + yyyy;

            document.getElementById("txbPoNum"    ).value = '';
            document.getElementById("txbPoDate"   ).value = this_date;
            document.getElementById("txbCusPoNum" ).value = '';
            document.getElementById("txbOrgPoDate").value = '';
            document.getElementById("rdoPoTypeDO" ).checked = false;
            document.getElementById("rdoPoTypeINV").checked = false;
            document.getElementById("txbDlvrDate" ).value = '';
            document.getElementById("txaReasonDtl").value = '';
            document.getElementById("selCusID"    ).value = '';
            document.getElementById("txbCusName"  ).value = '';
            document.getElementById("txbCntPrsn"  ).value = '';
            document.getElementById("txaAddr"     ).value = '';
            
            document.getElementById("selDistric"  ).innerHTML = "<option value=''>เลือกจังหวัดก่อน</option>";
            $('.selectpicker').selectpicker('refresh');

            document.getElementById("selProvince" ).value = '';

            document.getElementById("txbZip").value = '';
            document.getElementById("txbTel").value = '';
            document.getElementById("txbFax").value = '';
            document.getElementById("txbTax").value = '';

            document.getElementById("txbPrdCode").value = '';
            document.getElementById("txbPrdName").value = '';
            document.getElementById("txbUntPrce").value = '0';
            document.getElementById("txbUntQuan").value = '1';
            document.getElementById("txbTotal"  ).value = '0';
            document.getElementById("txbDescrip").value = '';

            document.getElementById("txbPOTotal"     ).value = '0';
            document.getElementById("txbDscntPercent").value = '0';
            document.getElementById("txbDscntAmount" ).value = '0';
            document.getElementById("txbVATPercent"  ).value = '7';
            document.getElementById("txbVATAmount"   ).value = '0';
            document.getElementById("txbGrandTotal"  ).value = '0';

            num_items = 0;
            arr_lists = new Array();
            refresh_po_items_list();

            document.getElementById("lower_area"    ).style.display = 'block';
            document.getElementById("searching_area").style.display = 'block';
            document.getElementById("form_adding"   ).style.display = 'none';
            document.getElementById("form_importing").style.display = 'none';

            $('#THE_PO_ID').val('');
            $('#btn_print').css('display','none');
        }

        function do_save_po()
        {
            po_type  = '0';
            if (document.getElementById("rdoPoTypeDO" ).checked) { po_type = '2'; }
            if (document.getElementById("rdoPoTypeINV").checked) { po_type = '1'; }
            if (po_type  == '0')
            {
                alert('Please choose PO type');
            }
            else
            {
                po_num   = document.getElementById('txbPoNum'    ).value;
                cpo_num  = document.getElementById('txbCusPoNum' ).value;
                po_date  = document.getElementById('txbPoDate'   ).value;
                org_date = document.getElementById('txbOrgPoDate').value;
                dlv_date = document.getElementById('txbDlvrDate' ).value;

                po_actv  = document.getElementById('selActive'   ).value;
                details  = document.getElementById('txaReasonDtl').value;

                cus_id   = document.getElementById('selCusID'   ).value;
                cus_name = document.getElementById('txbCusName' ).value;
                cus_cnct = document.getElementById('txbCntPrsn' ).value;
                cus_cnct = document.getElementById('txbCntPrsn' ).value;
                cus_addr = document.getElementById('txaAddr'    ).value;
                district = document.getElementById('selDistric' ).value;
                province = document.getElementById('selProvince').value;
                cus_zip  = document.getElementById('txbZip'     ).value;
                cus_tel  = document.getElementById('txbTel'     ).value;
                cus_fax  = document.getElementById('txbFax'     ).value;
                cus_tax  = document.getElementById('txbTax'     ).value;

                po_total = document.getElementById('txbPOTotal'     ).value;
                disc_pcn = document.getElementById('txbDscntPercent').value;
                vat_pcn  = document.getElementById('txbVATPercent'  ).value;
                grand_tl = document.getElementById('txbGrandTotal'  ).value;

                po_detail = '';
                for (i_loop = 1; i_loop <= num_items; i_loop++)
                {
                    if (i_loop > 1) { po_detail += '^^^'; }
                    po_detail += 'PRODUCT_ID:'        + arr_lists[i_loop].sale_id   + 'vvv';
                    po_detail += 'PRODUCT_PC_ID:'     + arr_lists[i_loop].sale_code + 'vvv';
                    po_detail += 'PRODUCT_NAME:'      + arr_lists[i_loop].prdt_name + 'vvv';
                    po_detail += 'UNIT_PRICE:'        + arr_lists[i_loop].unit_prce + 'vvv';
                    po_detail += 'QTY:'               + arr_lists[i_loop].unit_quan + 'vvv';
                    po_detail += 'AMOUNT:'            + arr_lists[i_loop].unti_totl + 'vvv';
                    po_detail += 'DESCRIPTION:'       + arr_lists[i_loop].prdt_desc;
                }

                if (po_detail != '')
                {
                    go_ready_stage();

                    if (po_num != '') { str_params = 'operate=upd'; str_msg = 'แก้ไข'; }
                                 else { str_params = 'operate=add'; str_msg = 'เพิ่ม'; }

                    str_params +=    '&po_num=' + po_num;
                    str_params +=   '&cpo_num=' + cpo_num;
                    str_params +=   '&po_date=' + po_date;
                    str_params +=  '&org_date=' + org_date;
                    str_params +=  '&dlv_date=' + dlv_date;
                    str_params +=   '&po_type=' + po_type;
                    str_params +=   '&po_actv=' + po_actv;
                    str_params +=   '&details=' + details;

                    str_params +=    '&cus_id=' + cus_id;
                    str_params +=  '&cus_name=' + cus_name;
                    str_params +=  '&cus_cnct=' + cus_cnct;
                    str_params +=  '&cus_addr=' + cus_addr;
                    str_params +=  '&district=' + district;
                    str_params +=  '&province=' + province;
                    str_params +=   '&cus_zip=' + cus_zip;
                    str_params +=   '&cus_tel=' + cus_tel;
                    str_params +=   '&cus_fax=' + cus_fax;
                    str_params +=   '&cus_tax=' + cus_tax;

                    str_params += '&num_items=' + num_items;
                    str_params +=  '&po_total=' + po_total;
                    str_params +=  '&disc_pcn=' + disc_pcn;
                    str_params +=   '&vat_pcn=' + vat_pcn;
                    str_params +=  '&grand_tl=' + grand_tl;

                    str_params += '&po_detail=' + po_detail;

                    if (window.XMLHttpRequest) { xmlHTTPadd=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                          else { xmlHTTPadd=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

                    xmlHTTPadd.onreadystatechange=function()
                    {
                        if (xmlHTTPadd.readyState==4 && xmlHTTPadd.status==200)
                        {
                            str_response = xmlHTTPadd.responseText;
                            if (str_response != '')
                            {
                                alert(str_msg+' PO\n'+str_response+'\nในรายการ');

                                date_start = $("#txbTranDateStart").val();
                                date_end   = $("#txbTranDateEnd").val();
                                reload_po_list('', date_start, date_end);

                                if (po_num == '') { update_poid_order_detail(str_response); }
                            }
                        }
                    }

                    xmlHTTPadd.open('POST','purchase_order_exec.php',true);
                    xmlHTTPadd.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlHTTPadd.send(str_params);
                } /* if (po_detail != '') */
                else
                {
                    alert('ยังไม่ได้ทำรายการ\n'+num_items+'\n'+i_loop+'\n'+po_detail);
                }
            }
        }

        function findPos(obj)
        {
            var curtop = 0;
            if (obj.offsetParent)
            {
                do { curtop += obj.offsetTop; }
                while (obj = obj.offsetParent);
                return [curtop];
            }
        }

        function get_district_option(province_code, district_code, obj_id)
        {
            if (window.XMLHttpRequest) { xmlHTTPopt=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                  else { xmlHTTPopt=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

            xmlHTTPopt.onreadystatechange=function()
            {
                if (xmlHTTPopt.readyState==4 && xmlHTTPopt.status==200)
                {
                    str_response = xmlHTTPopt.responseText;
                    if (str_response != '')
                    {
                        document.getElementById(obj_id).innerHTML = str_response;
                        $('#'+obj_id).selectpicker('refresh');
                    }
                }
            }

            str_param = '?pcode='+province_code+'&dcode='+district_code;

            xmlHTTPopt.open('GET','district_get_option.php'+str_param, true);
            xmlHTTPopt.send();
        }

        function get_customer_detail(cus_id)
        {
            if (window.XMLHttpRequest) { xmlHTTPcus=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                  else { xmlHTTPcus=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

            xmlHTTPcus.onreadystatechange=function()
            {
                if (xmlHTTPcus.readyState==4 && xmlHTTPcus.status==200)
                {
                    str_response = xmlHTTPcus.responseText;
                    if (str_response != '')
                    {
                        var customer = JSON.parse(str_response);

                        document.getElementById("txbCusName" ).value = customer["CUS_NAME"];
                        document.getElementById("txbCntPrsn" ).value = customer["CUS_CONTACT"];
                        document.getElementById("txaAddr"    ).value = customer["CUS_ADDRESS"];
                        document.getElementById("txbZip"     ).value = customer["CUS_ZIP"];
                        document.getElementById("txbTel"     ).value = customer["CUS_TEL"];
                        document.getElementById("txbFax"     ).value = customer["CUS_FAX"];
                        document.getElementById("txbTax"     ).value = customer["TAX_NUMBER"];

                        get_district_option(customer["PROVINCE_CODE"], customer["DISTRICT_CODE"], 'selDistric');

                        document.getElementById("selProvince").value = customer["PROVINCE_CODE"];

                        $('#selProvince').selectpicker('refresh');
                    }
                }
            }

            str_param = 'cus_id='+cus_id;

            xmlHTTPcus.open('POST','purchase_order_get_cust_detail.php',true);
            xmlHTTPcus.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlHTTPcus.send(str_param);
        }

        function get_po_detail(po_id)
        {
            if (window.XMLHttpRequest) { xmlHTTPdtl=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                  else { xmlHTTPdtl=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

            xmlHTTPdtl.onreadystatechange=function()
            {
                if (xmlHTTPdtl.readyState==4 && xmlHTTPdtl.status==200)
                {
                    str_response = xmlHTTPdtl.responseText;
                    if (str_response != '')
                    {
                        var arr_po_dtl = JSON.parse(str_response);

                        arr_lists = new Array();

                        num_items = arr_po_dtl.length;

                        for (i_cnt = 1; i_cnt <= num_items; i_cnt++)
                        {
                            arr_lists[i_cnt] = {
                                sale_id:   arr_po_dtl[i_cnt-1].PRODUCT_ID,
                                sale_code: arr_po_dtl[i_cnt-1].PRODUCT_PC_ID,
                                prdt_name: arr_po_dtl[i_cnt-1].PRODUCT_NAME,
                                unit_prce: arr_po_dtl[i_cnt-1].UNIT_PRICE,
                                unit_quan: arr_po_dtl[i_cnt-1].QTY,
                                unti_totl: arr_po_dtl[i_cnt-1].AMOUNT,
                                prdt_desc: arr_po_dtl[i_cnt-1].DESCRIPTION
                            };
                        }

                        refresh_po_items_list();
                        calc_grand_total();
                    }
                }
            }

            str_param = "po_id="+po_id;

            xmlHTTPdtl.open('POST','purchase_order_get_po_detail.php',true);
            xmlHTTPdtl.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlHTTPdtl.send(str_param);
        }

        function edit_po(po_id)
        {
            if (window.XMLHttpRequest) { xmlHTTPget=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                  else { xmlHTTPget=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

            xmlHTTPget.onreadystatechange=function()
            {
                if (xmlHTTPget.readyState==4 && xmlHTTPget.status==200)
                {
                    str_response = xmlHTTPget.responseText;
                    if (str_response != '')
                    {
                        var po_data = JSON.parse(str_response);

                        show_form_adding();

                        document.getElementById("txbPoNum"    ).value = po_data["PO_NO"];
                        document.getElementById("txbPoDate"   ).value = po_data["PO_DATE"];
                        document.getElementById("txbCusPoNum" ).value = po_data["CUS_PO_NO"];
                        document.getElementById("txbOrgPoDate").value = po_data["ORG_PO_DATE"];
                        document.getElementById("txbDlvrDate" ).value = po_data["DELIVERY_DATE"];
                        document.getElementById("txaReasonDtl").value = po_data["REASON_DTL"];

                        if (po_data["OBJ_ID"] == '1') { document.getElementById("rdoPoTypeINV").checked = true; }
                        if (po_data["OBJ_ID"] == '2') { document.getElementById("rdoPoTypeDO" ).checked = true; }

                        document.getElementById("selCusID"  ).value = po_data["CUS_ID"];
                        document.getElementById("txbCusName").value = po_data["CUS_NAME"];
                        document.getElementById("txbCntPrsn").value = po_data["CUS_CONTACT"];
                        document.getElementById("txaAddr"   ).value = po_data["CUS_ADDRESS"];

                        document.getElementById("selProvince" ).value = po_data["PROVINCE_CODE"];
                        get_district_option(po_data["PROVINCE_CODE"], po_data["DISTRICT_CODE"], "selDistric");

                        document.getElementById("txbZip").value = po_data["CUS_ZIP"];
                        document.getElementById("txbTel").value = po_data["CUS_TEL"];
                        document.getElementById("txbFax").value = po_data["CUS_FAX"];
                        document.getElementById("txbTax").value = po_data["TAX_NUMBER"];

                        document.getElementById("txbDscntPercent").value = po_data["DISCOUNT_PER"];
                        document.getElementById("txbVATPercent"  ).value = po_data["VAT_PER"];

                        get_po_detail(po_data["PO_ID"]);
                        show_print_po(po_id);
                    }
                }
            }

            str_param = "po_id="+po_id;

            xmlHTTPget.open('POST','purchase_order_get_po_main.php',true);
            xmlHTTPget.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlHTTPget.send(str_param);
        }

        function delete_po(po_id, po_num)
        {
            if (confirm('ยืนยันลบ PO '+po_num))
            {
                if (window.XMLHttpRequest) { xmlHTTPdel=new XMLHttpRequest(); } // code for IE7+, Firefox, Chrome, Opera, Safari
                                      else { xmlHTTPdel=new ActiveXObject("Microsoft.XMLHTTP"); } // code for IE6, IE5

                xmlHTTPdel.onreadystatechange=function()
                {
                    if (xmlHTTPdel.readyState==4 && xmlHTTPdel.status==200)
                    {
                        str_response = xmlHTTPdel.responseText;
                        if (str_response != '')
                        {
                            alert(str_response);
                        }
                        alert('ลบ PO '+po_num+' เรียบร้อยแล้ว');
                        date_start = $("#txbTranDateStart").val();
                        date_end   = $("#txbTranDateEnd").val();
                        reload_po_list('', date_start, date_end);
                    }
                }

                str_param = "po_id="+po_id+"&operate=del";

                xmlHTTPdel.open('POST','purchase_order_exec.php',true);
                xmlHTTPdel.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlHTTPdel.send(str_param);
            }
        }

        function import_pc_order(user_name, cus_id)
        {
            go_ready_stage();

            $.post( "purchase_order_get_pc_customer.php", { user_name: user_name, cus_id: cus_id })
                .done(function(data)
                {
                    if (data != "[]")
                    {
                        var arr_response = jQuery.parseJSON(data);
                        $('#selCusID').val(arr_response[0]["CUS_ID"]);
    
                        document.getElementById("txbCusName" ).value = arr_response[0]["CUS_NAME"];
                        document.getElementById("txbCntPrsn" ).value = arr_response[0]["CUS_CONTACT"];
                        document.getElementById("txaAddr"    ).value = arr_response[0]["CUS_ADDRESS"];
                        document.getElementById("txbZip"     ).value = arr_response[0]["CUS_ZIP"];
                        document.getElementById("txbTel"     ).value = arr_response[0]["CUS_TEL"];
                        document.getElementById("txbFax"     ).value = arr_response[0]["CUS_FAX"];
                        document.getElementById("txbTax"     ).value = arr_response[0]["TAX_NUMBER"];
    
                        get_district_option(arr_response[0]["PROVINCE_CODE"], arr_response[0]["DISTRICT_CODE"], 'selDistric');
    
                        document.getElementById("selProvince").value = arr_response[0]["PROVINCE_CODE"];
    
                        $('#selProvince').selectpicker('refresh');
                    }

                    import_pc_order_list(user_name, cus_id);

                    $('#rdoPoTypeDO').prop('checked', true);
                });
        }

        function import_pc_order_list(user_name, cus_id)
        {
            $.post( "purchase_order_get_pc_order.php", { user_name: user_name, cus_id: cus_id })
                .done(function(data)
                {
                    var txt_response = '';
                    var obj_response = jQuery.parseJSON(data);

                    show_form_adding();
                    for (int_cnt = 0; int_cnt < obj_response.length; int_cnt++)
                    {
                        the_value = obj_response[int_cnt]["PRODUCT_PC_ID"];
                        
                        $('#txbPrdCode').val(the_value);
                        fill_prd_detail(the_value);

                        $('#txbUntQuan').val(obj_response[int_cnt]["QTY"]);
                        recalc_add_total();

                        $('#txbPrdCode').selectpicker('refresh');

                        odt_id = obj_response[int_cnt]["ORDER_DTL_ID"];

                        add_po_item(odt_id);

                        txt_response += odt_id+':'+obj_response[int_cnt]["UPDATE_DATE"]+':'+obj_response[int_cnt]["PRODUCT_NAME_EN"]+"\n";
                    }
                });
        }

        function update_poid_order_detail(po_num)
        {
            $.post( "purchase_order_upd_po_dtl.php", { po_num: po_num, pcoid: arr_pcoid })
                .done(function(data)
                {
                    //alert(data);
                });
        }

        function get_pc_order_list()
        {
            $('#pc_order_dtl').html('Loading PC Order, please wait . . .');

            $.post( "purchase_order_get_order_dtl.php", { param1: "1" })
                .done(function(data)
                {
                    $('#pc_order_dtl').html(data);
                });
        }

        function do_report_po(report_type)
        {
            $('#REPORT_TYPE').val(report_type);
            po_id = $('#THE_PO_ID').val();
            document.getElementById("form_report").submit();
        }

        function show_print_po(po_id)
        {
            $('#THE_PO_ID').val(po_id);
            $('#btn_print').css('display','inline-block');
            $('#btn_print2').css('display','inline-block');
        }
    </script>
</head> 

<body onload="start_up();">

<div id="wrap"  class="wrapper">

    <?php require_once("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">PURCHASE ORDER</h1>
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
    <script type="text/javascript">
    <!--

     //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
        <div  class="search-block" id="searching_area" style="padding: 18px; padding-bottom: 0px;" >
            <div class="container">
                <div class="col-md-6 col-md-offset-3">
                    <h2>CONDITION</h2>
                    <div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
                        
                        <div class="sky-form" style="border-style:none">
                            <form class="form-horizontal" role="form"  method="POST" action="" name="form_po">
                                <div class="form-group">
                                    <label for="txbTranNum" class="col-lg-4 control-label">PO Number <span class='txt_red'>*</span> :</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="txbTranNum" id="txbTranNum" placeholder="PO Number" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputTranDate" class="col-lg-4 control-label">Create Date <span class='txt_red'>*</span> :</label>
                                    <section class="col col-lg-4" style="height: 16px">
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" class="form-control" name="txbTranDateStart" id="txbTranDateStart" placeholder="Start date" value="<?=$last_7day;?>">
                                        </label>
                                    </section>

                                    <section class="col col-lg-4" style="height: 16px">
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" class="form-control" name="txbTranDateEnd" id="txbTranDateEnd" placeholder="End date" value="<?=$this_date;?>">
                                        </label>
                                    </section>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-4 col-lg-8">
                                        <button type="button" class="btn-u" onclick="do_searching();"> Search </button> &nbsp;&nbsp;
                                        <button type="button" class="btn-u btn-u-default">Clear</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>    
            
        </div><!--/container--> 

    <!--=== End Search Block Version 2 ===-->

    <!-- start of importing order -->
    <div class="container" id="form_importing" style="display:none;">
        <div class="panel panel-grey margin-bottom-40" style="padding: 18px; margin-top:40px;">
            <h3 style="margin-bottom:20px;">PO Import from PC Order</h3>
            <div id="pc_order_dtl"></div>
        </div>
    </div>
    <!-- end of importing order -->

    <!-- start of adding form -->
    <div class="container" id="form_adding" style="display:none;">
    <div class="panel panel-grey margin-bottom-40" style="padding: 18px; margin-top:40px;">
    <h3 style="margin-bottom:20px;">PO Information</h3>
    <div class="sky-form" style="border-style:none">
    <form class="form-horizontal" role="form"  method="POST" action="" name="form_po_add">
        <div class="form-group">
            <label for="txbPoNum" class="col-lg-3 control-label">PO Number :</label>
            <div class="col-lg-3">
                <input type="text" class="form-control" name="txbPoNum" id="txbPoNum" placeholder="New PO Number" value="" readonly>
            </div>
            <label for="txbPoDate" class="col-lg-2 control-label">PO Date <span class='txt_red'>*</span> :</label>
            <div class="col-lg-3">
                <label class="input">
                    <i class="icon-append fa fa-calendar"></i>
                    <input type="text" class="form-control calendar" name="txbPoDate" id="txbPoDate" placeholder="PO Date" value="<?=$this_date;?>">
                </label>
            </div>
        </div>
        <div class="form-group">
            <label for="txbCusPoNum" class="col-lg-3 control-label">Customer Po No. :</label>
            <div class="col-lg-3">
                <input type="text" class="form-control" name="txbCusPoNum" id="txbCusPoNum" placeholder="Customer Po No." value="">
            </div>
            <label for="txbOrgPoDate" class="col-lg-2 control-label">Original PO Date :</label>
            <div class="col-lg-3">
                <label class="input">
                    <i class="icon-append fa fa-calendar"></i>
                    <input type="text" class="form-control calendar" name="txbOrgPoDate" id="txbOrgPoDate" placeholder="Original PO Date" value="">
                </label>
            </div>
        </div>
        <div class="form-group">
            <label for="rdoPoType" class="col-lg-3 control-label"><div style='display:none;'>Po Type :</div></label>
            <div class="col-lg-3" style="margin:8px 0;">
                <div style='display:none;'>
                    <label>
                        <input type="radio" name="rdoPoType" id="rdoPoTypeDO"  value="DO" checked="checked">
                        for DO
                    </label>
                </div>
                <div style='display:none;'>
                    <label>
                        <input type="radio" name="rdoPoType" id="rdoPoTypeINV" value="Invoice">
                        for Invoice
                    </label>
                </div>
            </div>
            <label for="txbDlvrDate" class="col-lg-2 control-label">Delivery Date :</label>
            <div class="col-lg-3">
                <label class="input">
                    <i class="icon-append fa fa-calendar"></i>
                    <input type="text" class="form-control calendar" name="txbDlvrDate" id="txbDlvrDate" placeholder="Delivery Date" value="">
                </label>
            </div>
        </div>
        <div class="form-group" style="display:none;">
            <label for="fleCusPO" class="col-lg-3 control-label">Upload Customer Po :</label>
            <div class="col-lg-5" style="margin:8px 0;">
                <div id="fileuploader">Upload</div>
                <!--
                <input type="file" class="" name="fleCusPO" id="fleCusPO" placeholder="Upload Customer Po" style="padding:3px; border:1px solid #bbb; width:100%;"  value="">
                -->
            </div>
        </div>
        <div class="form-group">
            <!--
            <label for="selActive" class="col-lg-3 control-label">Active :</label>
            <div class="col-lg-2" style="margin:8px 0;">
                <select class="form-control" name="selActive" id="selActive">
                    <option value='Y'>Yes</option>
                    <option value='N'>No</option>
                </select>
            </div>
            -->
            <input type="HIDDEN" id="selActive" name="selActive" value="Y">
            <label for="txaReasonDtl" class="col-lg-3 control-label">Reason Detail :</label>
            <div class="col-lg-8" style="margin:8px 0;">
                <textarea class="form-control" name="txaReasonDtl" id="txaReasonDtl"></textarea>
            </div>
        </div>
    </form>
    </div>
    </div>

    <!-- Customer Information -->
    <div class="panel panel-grey margin-bottom-40" style="padding: 18px; margin-top:40px;">
    <h3 style="margin-bottom:20px;">Customer Information</h3>
    <div class="sky-form" style="border-style:none">
    <form class="form-horizontal" role="form"  method="POST" action="" name="form_po_add">
        <div class="form-group">
            <label for="selCusID" class="col-lg-3 control-label">Customer Code / Name <span class='txt_red'>*</span> :</label>
            <div class="col-lg-8">
                <select class="form-control" name="selCusID" id="selCusID" onchange="get_customer_detail(this.value);">
                    <option value="0">ระบุลูกค้า</option>
                    <?php echo $amh_customer->get_customer_option(''); ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="txbCusName" class="col-lg-3 control-label">Customer Name :</label>
            <div class="col-lg-8">
                <input type="text" class="form-control" name="txbCusName" id="txbCusName" placeholder="Customer Name" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="txbCntPrsn" class="col-lg-3 control-label">Contact Person :</label>
            <div class="col-lg-8">
                <input type="text" class="form-control" name="txbCntPrsn" id="txbCntPrsn" placeholder="Contact Person" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="txaAddr" class="col-lg-3 control-label">Address <span class='txt_red'>*</span> :</label>
            <div class="col-lg-8">
                <textarea class="form-control" name="txaAddr" id="txaAddr"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="selDistric" class="col-lg-3 control-label">District :</label>
            <div class="col-lg-3">
                <select class="form-control selectpicker" data-live-search="true" name="selDistric" id="selDistric">
                    <option value=''>เลือกจังหวัดก่อน</option>
                </select>
            </div>
            <label for="selProvince" class="col-lg-2 control-label">Province :</label>
            <div class="col-lg-3">
                <select class="form-control selectpicker" data-live-search="true" name="selProvince" id="selProvince" onchange="get_district_option(this.value, '', 'selDistric');">
                    <option value=''>เลือกจังหวัด</option>
                    <?php echo $amh_province->create_province_option(''); ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="txbZip" class="col-lg-3 control-label">Post Code :</label>
            <div class="col-lg-3">
                <input type="text" class="form-control" name="txbZip" id="txbZip" placeholder="Post Code" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="txbTel" class="col-lg-3 control-label">Tel :</label>
            <div class="col-lg-3">
                <input type="text" class="form-control" name="txbTel" id="txbTel" placeholder="Tel" value="">
            </div>
            <label for="txbFax" class="col-lg-2 control-label">Fax :</label>
            <div class="col-lg-3">
                <input type="text" class="form-control" name="txbFax" id="txbFax" placeholder="Fax" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="txbTax" class="col-lg-3 control-label">Tax ID :</label>
            <div class="col-lg-3">
                <input type="text" class="form-control" name="txbTax" id="txbTax" placeholder="Tax ID" value="">
            </div>
        </div>
    </form>
    </div>
    </div>
    <!-- Customer Information -->

    <!-- PO Detail -->
    <div class="panel panel-grey margin-bottom-40" style="padding: 18px; margin-top:40px;">
    <h3 style="margin-bottom:20px;">PO Detail</h3>
    <div class="sky-form" style="border-style:none">
    <form class="form-horizontal" role="form"  method="POST" action="" name="form_po_add">
        <div class="form-group">
            <label for="txbPrdCode" class="col-lg-2">Product Code</label>
            <label for="txbPrdName" class="col-lg-2">Product Name</label>
            <label for="txbUntPrce" class="col-lg-2">Unit Price</label>
            <label for="txbUntQuan" class="col-lg-1 box_qty">Quantity</label>
            <label for="txbTotal"   class="col-lg-2">Total</label>
            <label for="txbDescrip" class="col-lg-2">Description</label>
        </div>
        <div class="form-group">
            <div class="col-lg-2">
                <select class="form-control selectpicker" name="txbPrdCode" id="txbPrdCode" onchange="fill_prd_detail(this.value);">
                <option value="">PRODUCT CODE</option>
                <?php echo $amh_pc->create_product_code_option('',$the_date); ?>
                </select>
            </div>
            <div class="col-lg-2"><input type="text" class="form-control" name="txbPrdName" id="txbPrdName" value=""></div>
    <!--    <div class="col-lg-2"><input type="text" class="form-control" name="txbUntPrce" id="txbUntPrce" value=""  onchange="recalc_add_total();"></div>  -->
            <div class="col-lg-2"><input type="text" class="form-control" name="txbUntPrce" id="txbUntPrce" value=""  readonly></div>
            <div class="col-lg-1"><input type="text" class="form-control box_qty" name="txbUntQuan" id="txbUntQuan" value="1" onchange="recalc_add_total();"></div>
            <div class="col-lg-2"><input type="text" class="form-control" name="txbTotal"   id="txbTotal"   value=""  readonly></div>
            <div class="col-lg-2"><input type="text" class="form-control" name="txbDescrip" id="txbDescrip" value=""></div>
            <div class="col-lg-1"><button type="button" onclick="add_po_item(0);" class="btn-u btn-u-sm">Add</button></div>
        </div>
        <hr class="devider devider-dotted">
        <!-- Item each -->
        <div id="item_detail_area"></div>
        <!-- Summary number area -->
        <div class="form-group">
            <label for="txbPOTotal" class="col-md-offset-5 col-lg-2 control-label">Total</label>
            <div class="col-lg-2">
                <input type="text" class="form-control" name="txbPOTotal" id="txbPOTotal" value="" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="txbDscntPercent" class="col-md-offset-4 col-lg-2 control-label">Discount</label>
            <div class="col-lg-1"><input type="text" class="form-control" name="txbDscntPercent" id="txbDscntPercent" value="0" onchange="calc_grand_total();" style="width:70%; display:inline-block;"> %</div>
            <div class="col-lg-2"><input type="text" class="form-control" name="txbDscntAmount"  id="txbDscntAmount"  value="0" readonly></div>
        </div>
        <div class="form-group">
            <label for="txbVATPercent" class="col-md-offset-4 col-lg-2 control-label">VAT</label>
            <div class="col-lg-1"><input type="text" class="form-control" name="txbVATPercent" id="txbVATPercent" value="7" onchange="calc_grand_total();" style="width:70%; display:inline-block;"> %</div>
            <div class="col-lg-2"><input type="text" class="form-control" name="txbVATAmount"  id="txbVATAmount"  value=""  readonly></div>
        </div>
        <div class="form-group">
            <label for="txbGrandTotal" class="col-md-offset-5 col-lg-2 control-label">Grand Total</label>
            <div class="col-lg-2"><input type="text" class="form-control" name="txbGrandTotal" id="txbGrandTotal" value="" readonly></div>
        </div>
    </form>
    </div>
    </div>
    <!-- PO Detail -->

    <div class="panel panel-grey margin-bottom-40" style="padding: 18px; margin-top:40px;">
    <div class="sky-form" style="border-style:none">
    <form class="form-horizontal" role="form"  method="POST" action="" name="form_po_btn">
        <div class="form-group">
            <div class="col-lg-12">
                <button type="button" onclick="do_save_po();"        class="btn-u" style="width:150px; margin: 0 20px;">Save PO</button>
                <button type="button" onclick="go_ready_stage();"    class="btn-u btn-u-default" style="width:150px; margin: 0 20px;">Clear</button>
                <button type="button" onclick="do_report_po('PO1');" class="btn-u btn-u-default" style="width:150px; margin: 0 20px; display:none;" id='btn_print'>Print PO</button>
                <button type="button" onclick="do_report_po('PO2');" class="btn-u btn-u-default" style="width:150px; margin: 0 20px; display:none;" id='btn_print2'>Print ใบสั่งของ</button>
            </div>
        </div>
    </form>
    </div>
    </div>

    </div>
    <!-- end of adding form -->

    <form action='purchase_order_report.php' target='_BLANK' method='POST' id='form_report'>
    <input type='HIDDEN' id='REPORT_TYPE' name='REPORT_TYPE' value='PO1'>
    <input type='HIDDEN' id='THE_PO_ID'   name='THE_PO_ID'   value=''>
    <input type='HIDDEN' id='THE_media'   name='THE_media'   value='PDF'>
    </form>

    <div id="lower_area" class="container content-sm"  style="padding:30px;">

        <div class="col-lg-offset col-lg-8" style="height:50px">
            <button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#sales_transaction_dtl_form" 
                onclick="show_form_adding();"> <i class="fa fa-plus-square icon-color-white"></i> New Purchase Order </button>
            <button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#sales_transaction_dtl_form" 
                onclick="show_form_import();"> <i class="fa fa-plus-square icon-color-white"></i> Import sale order to PO </button>
        </div>

        <!-- List items Section -->
        <div class="table-search-v2 margin-bottom-30" id="po_list_item"></div>
        <!-- List items Section -->

    </div>
    <?php require("include_footer.php"); ?>

</div><!--/End Wrapepr-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-select.min.js"></script>

<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
<script src="javascripts/jquery.uploadfile.min.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/forms/order.js"></script>
<script type="text/javascript" src="assets/js/forms/review.js"></script>
<script type="text/javascript" src="assets/js/forms/checkout.js"></script>
<script type="text/javascript" src="assets/js/plugins/masking.js"></script>
<script type="text/javascript" src="assets/js/plugins/datepicker.js"></script>
<script type="text/javascript" src="assets/js/plugins/validation.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        OrderForm.initOrderForm();        
        ReviewForm.initReviewForm();        
        CheckoutForm.initCheckoutForm();        
        Masking.initMasking();
        Datepicker.initDatepicker();
        Validation.initValidation();
        });
</script>

<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>
<?php
	ob_start();
	require("connect.php");
	require("config.php");
	session_start();
	if ($_GET["form"] == "frm_pro_mst_dtl")
	{
		$hdMode = $_POST['hdMode'];
		//*** Data Field ****
		
		$txbProCode=$_POST['txbProCode'];
		$txbProEn=$_POST['txbProEn'];
		$txbProTh=$_POST['txbProTh'];
		$hdProId=$_POST['hdProId'];
		$ddlProType = $_POST['ddlProType'];
		$fileImg = $_POST['txbImgFile'];
		
		$query_str="";
		
		if ($hdMode=="new")
		{
			$sql = sprintf("INSERT INTO mst_product(
								   PRODUCT_CODE
								  ,PRODUCT_NAME_TH
								  ,PRODUCT_NAME_EN
								  ,PRODUCT_TYPE_ID
								  ,PRD_IMG
								  ,CREATED_BY
								  ,CREATED_DATE
								  ,UPDATE_DATE
								) VALUES (
								  '%s'  -- PRODUCT_CODE - IN varchar(20)
								  ,'%s'  -- PRODUCT_NAME_TH - IN varchar(100)
								  ,'%s'  -- PRODUCT_NAME_EN - IN varchar(100)
								  ,%d
								  ,'%s' 
								  ,'%s'  -- CREATED_BY - IN varchar(20)
								  ,NOW()  -- CREATED_DATE - IN datetime
								  ,NOW()   -- UPDATE_DATE - IN datetime
								) "
				, mysql_real_escape_string($txbProCode)
				, mysql_real_escape_string($txbProTh)
				, mysql_real_escape_string($txbProEn)
				, mysql_real_escape_string($ddlProType)
				, mysql_real_escape_string($fileImg)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				);
				
			if($result=mysql_query($sql))
			{
				$query_str="?focus=".$txbProCode;
				echo "Insert OK ^.^";
			}else
			{
				$query_str="?error_ins=".$txbProCode;
				echo "SQL Error = ".$sql."<br/>";
			}
		}else if ($hdMode=="edit")
		{
			
			$sql = sprintf("UPDATE mst_product
									SET
										PRODUCT_NAME_EN = '%s' 
										,PRODUCT_NAME_TH = '%s' 
										,PRODUCT_TYPE_ID = %d
										,PRD_IMG = '%s' 
										,UPDATE_BY = '%s' 
										,UPDATE_DATE = Now() 
									WHERE PRODUCT_ID = %d ;"
				
				, mysql_real_escape_string($txbProEn)
				, mysql_real_escape_string($txbProTh)
				, mysql_real_escape_string($ddlProType)
				, mysql_real_escape_string($fileImg)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($hdProId)
				);
			
			if($result=mysql_query($sql))
			{
				$query_str="?focus=".$txbProCode;
				echo "Update OK ^.^=".$sql."<br/>";
			}else
			{
				$query_str="?error_ins=".$txbProCode;
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
		
		if ($_FILES["fileImg"]["name"] != "")
		{
			echo "\$_SERVER[\"fileImg\"][\"SOCUMENT_ROOT\"] = ". $_SERVER['DOCUMENT_ROOT']."<br/>";
			echo "\$_FILES[\"fileImg\"][\"name\"] = ".$_FILES["fileImg"]["name"]."<br>";
			echo "\$_FILES[\"fileImg\"][\"type\"] = ".$_FILES["fileImg"]["type"]."<br>";
			echo "\$_FILES[\"fileImg\"][\"size\"] = ".$_FILES["fileImg"]["size"]."<br>";
			echo "\$_FILES[\"fileImg\"][\"tmp_name\"] = ".$_FILES["fileImg"]["tmp_name"]."<br>";
			echo "\$_FILES[\"fileImg\"][\"error\"] = ".$_FILES["fileImg"]["error"]."<br>";
			echo "\$_FILES[\"fileImg\"][\"value\"] = ".$_FILES["fileImg"]["value"]."<br>";
			
			//$target_dir = "uploads/";
			$target_file = $pro_img_path . basename($_FILES["fileImg"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])) {
				$check = getimagesize($_FILES["fileImg"]["tmp_name"]);
				if($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
				} else {
					echo "<br/>File is not an image.".$target_dir;
					$uploadOk = 0;
				}
			}
			// Check if file already exists
			
			if (file_exists($target_file)) {
				echo "<br/>File already exists.";
				//$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileImg"]["size"] > 500000) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) 
			{
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				echo "<br/><font color= 'ff0033'>Sorry, your file was not uploaded.</font>";
			// if everything is ok, try to upload file
			} else {
				if (move_uploaded_file($_FILES["fileImg"]["tmp_name"], $target_file)) {
					echo "The file ". basename( $_FILES["fileImg"]["name"]). " has been uploaded.";
				} else {
					echo "<br/><font color= 'ff0033'>Sorry, there was an error uploading your file.</font>";
				}
			}
		}
		

	} else if ($_GET["form"] == "frm_pro_master_tb") 
	{
		$hdDelID = $_POST['hdDelID'];
		$hdCancelID = $_POST['hdCancelID'];
		
		if ($_POST['hdDelID'] != "")
		{
			$sql = "DELETE FROM mst_product
					WHERE PRODUCT_ID = ".$hdDelID."; ";
			
			if($result=mysql_query($sql)){
				echo "Delete OK ^.^";
			}else{
				echo "SQL Error = ".$sql."<br/>";
			}
		} else if ($_POST['hdCancelID'] != "")
		{
			echo "</br>CancelID=".$hdCancelID;
			$sql = sprintf("UPDATE mst_product
									SET ACTIVE_FLAG = 'N' 
										,UPDATE_BY = '%s' -- varchar(20)
										,UPDATE_DATE = Now() -- datetime
									WHERE PRODUCT_ID = %d ; -- int(11)"
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($hdCancelID)
				);
			if($result=mysql_query($sql))
			{
				$query_str="?focus=".$txbProCode;
				echo "Update OK ^.^=".$sql."<br/>";
			}else
			{
				$query_str="?error_ins=".$txbProCode;
				echo "SQL Error = ".$sql."<br/>";
			}
		}
	}
	header("Location: product_master.php".$query_str); 
	ob_end_flush();
?>
<?php 
	require("include_function.php");
	require('validatelogin.php'); 
	require("config.php"); 
 ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | PRODUCT MASTER</title>

	<?php $current_menu = "product"; ?>
	<?php require("include_headtag.php"); ?>
    
</head> 

<body>    

<div id="wrap"  class="wrapper">

	<?php require("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">PRODUCT MASTER</h1>
			
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
	<script type="text/javascript">
    <!--
	function confirmDel(DelID)
	{
		if (confirm("Are you sure you want to delete this item."))
		{
			
			document.getElementById('hdDelID').value = DelID;
			document.getElementById('hdCancelID').value  = "";
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 function confirmCancel(CancelID)
	{
		if (confirm("Are you sure you want to cancel this item."))
		{
			document.getElementById('hdDelID').value = "";
			document.getElementById('hdCancelID').value = CancelID;
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
		<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		<?php 
		
			// **** Gen Product sales code
			$sql = "SELECT mp.PRODUCT_ID, mp.PRODUCT_CODE ,mp.PRODUCT_NAME_TH, mp.PRODUCT_NAME_EN, concat(mp.PRODUCT_CODE,  ' : ', mp.PRODUCT_NAME_TH) as PCODE_PNAME
							,mpt.TYPE_NAME_TH, mp.ACTIVE_FLAG, mpt.ICON
							,IFNULL((SELECT mpc.PRODUCT_ID FROM mst_product_pc AS mpc WHERE mpc.PRODUCT_ID = mp.PRODUCT_ID LIMIT 1), 0) as IS_DEL
						FROM mst_product AS mp 
							LEFT JOIN mst_product_type AS mpt ON mp.PRODUCT_TYPE_ID = mpt.PRODUCT_TYPE_ID ";
			$order = " ORDER BY mp.PRODUCT_TYPE_ID, PRODUCT_CODE;  ";
			$result_pro_code =  mysql_query($sql.$order);
			
			
			$where = "WHERE mp.ACTIVE_FLAG = 'Y'  ";
			
			
			if ($_POST['ddlProCodeSearch']!="")
			{
				//if ($where == "") $where .= "WHERE mp.ACTIVE_FLAG = 'Y' "; else $where .=" AND ";
				$where .= "AND mp.PRODUCT_CODE = '".
					mysql_real_escape_string($_POST['ddlProCodeSearch'])
					."' ";
			}
			
			$sql = $sql.$where.$order;
			$result_pro_lish = mysql_query($sql);

			?>
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_condtion" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Product Code / Product Name :</label>
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
												<select name="ddlProCodeSearch" id="ddlProCodeSearch" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
														<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_pro_code)) 
													{
														echo "<option value='".$row['PRODUCT_CODE']."' ";
														if ($row['PRODUCT_CODE'] == $_POST['ddlProCodeSearch'])
														{
															echo " selected ";
														}
														echo ">".$row['PCODE_PNAME']."</option>";
													}
													mysql_data_seek ($result_pro_code , 0 );
													?>
												</select>
												
										</label>
									</div>
								</div>
									
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button type="submit" class="btn-u"> Search </button> &nbsp;&nbsp;
										<!-- <button type="button" class="btn-u btn-u-default">Clear</button> -->
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

	




    <!--=== End Search Block Version 2 ===-->

    <div class="container content-sm"  style="padding:30px;">


		<div class="col-lg-offset col-lg-8" style="height:50px">
			<button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#product_master_dtl_form" 
				onclick="ClearForm();"> <i class="fa fa-plus-square icon-color-white"></i> New Product Master </button> 
		
			<?php  require("product_master_dtl_form.php");  ?>
		</div>

		
		<div class="table-search-v2 margin-bottom-30">
			<div class="table-responsive">
				<form method="POST" enctype="multipart/form-data" action="product_master_model.php?form=frm_pro_master_tb" name="frmProMasterTb" onSubmit="return validate_form(this)"
					id="frmSalesTranTb" >
					
					<input type="hidden" name="hdDelID" id="hdDelID"  value="">
					<input type="hidden" name="hdCancelID" id="hdCancelID"  value="">
					<table class="table table-bordered table-striped">
						<thead>
							<tr valign="middle">
								<th> &nbsp; </th>
								<th>Product Code</th>
								<th>Product Thai name </th>
								<th class="hidden-sm">Product English name </th>
								<th>Product Type </th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<?php 
								while($row=mysql_fetch_array($result_pro_lish)) 
								{
									if($row["PRODUCT_CODE"] == $_REQUEST['focus'])
									{
										echo '<tr style = "background-color: #ffffbb">';
										echo '<script> history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'" ); </script>';
									}else
									{
										echo '<tr>';
									}
									
									echo '<td width="82px">
												<ul class="list-inline table-buttons">
													<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#product_master_dtl_form" onclick="getDataAjax('.$row["PRODUCT_ID"].')"><i class="fa fa-edit"></i> Edit</button></li>
												</ul>
											</td>';
									echo '<td width="120px"> <p>'.$row["PRODUCT_CODE"].'</p> </td>';
									echo '<td> <p>'.$row["PRODUCT_NAME_TH"].'</p> </td>';
									echo '<td> <p>'.$row["PRODUCT_NAME_EN"].'</p> </td>';
									echo '<td width="90px"> <p>'.$row["ICON"].$row["TYPE_NAME_TH"].'</p> </td>';
									
									
									echo '<td  width="88px">
													<ul class="list-inline table-buttons">
														<li>';
									if ($row["IS_DEL"] == 0)
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["PRODUCT_ID"] .')"> 
															<i class="fa fa-trash-o"></i> Delete </button>';
										
									}else if ($row["ACTIVE_FLAG"] == 'Y')
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-orange" onclick ="return confirmCancel('.$row["PRODUCT_ID"] .')"> 
															<i class="fa fa-times-circle"></i> Cancel</button>';
									} else 
									{
										echo '			<button type="submit" class="btn-u btn-u-sm btn-u-green" onclick ="return confirmCancel('.$row["PRODUCT_ID"] .')"> 
															<i class="fa fa-check-circle"></i> Enable</button>';
									}
									echo '			</li>
													</ul>
												</td>
											</tr>';
								}
							?>
						
						</tbody>
					</table>
				</form>
				
				
				<!-- End Test Form -->
			</div>    
		</div>    
        <!-- End Table Search v2 -->
		


        
    </div>
	<?php require("include_footer.php"); ?>
     
</div><!--/End Wrapepr-->

<?php require("include_js.php"); ?>

</body>
</html> 
<?php
/* Class name
   AMH_DB
   use for connect to amsel_healthdb
*/

class AMH_DB
{
    public $mysqli, $dbconnected;

    public function __construct()
    {
        $db_server   = "localhost";
        $db_username = "devadmin";
        $db_password = "amSel@123";
        $db_database = "amsel_healthdb";

        $this->mysqli = new mysqli($db_server, $db_username, $db_password, $db_database);
        $this->mysqli->set_charset('utf8');

        /* check connection */
        if (mysqli_connect_errno())
        {
            return("Connect failed: %s\n");
            $this->dbconnected = false;
        }
        else
        {
            $this->dbconnected = true;
        }
    }
}
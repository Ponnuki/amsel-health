<?php 
	require_once("include_function.php");
	require_once("class_tax_invoice.php");
	require_once("class_amh_province.php");
	require_once("class_amh_district.php");
	require_once("validatelogin.php");
	
	$amh_inv = new AMH_inv();
	$amh_province = new AMH_Province();
	$amh_district = new AMH_District();
	
	$inv_list = $amh_inv->inv_main();
	$ddl_customer = $amh_inv->get_ddl_customer();
	$ddl_po = $amh_inv->get_ddl_po();
	$ddl_province = $amh_province->mst_province();
	
 ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | TAX INVOICE</title>
	<?php $current_menu = "tax_invoice"; ?>
	<?php require("include_headtag.php"); ?>
    
	<script type="text/javascript">
	
		function delInvMain(invId, elmNo)
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp2.onreadystatechange=function()
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200)
				{
					result_msg= xmlhttp2.responseText.replace("\r\n\t", "");
					if (result_msg != "")
					{
						alert("Function [delInvMain] Error :" + result_msg);
					}
					
				}
			}
			
			mode = document.getElementById('hdMode' ).value
			params_dtl  = 'operate=del_inv_main';
			params_dtl += '&p_inv_id=' + invId;
			
			posting = true;
			
			if (posting)
			{
				xmlhttp2.open('POST','tax_invoice_ajax.php',true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params_dtl);
			}
		}
		
		function confirmDel(DelID,elmNo)
		{
			if (confirm("Are you sure you want to delete this item."))
			{
				delInvMain(DelID, elmNo);
				getInvMain();
			}
		 
		}
		
		function getInvMain(invNo, sDate, eDate)
		{
			posting = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp2.onreadystatechange=function() 
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200) 
				{
					var str = xmlhttp2.responseText.replace("\r\n\t", "");
					document.getElementById("divGridInvMain").innerHTML = str;
					//$('.selectpicker').selectpicker('refresh');
					
				}
			}
			
			params_dtl  = 'operate=grid_inv_main';
			if (typeof(invNo) !="undefined" || invNo == "")  
			{
				params_dtl += '&p_inv_no=' + invNo;
			}else
			{
				params_dtl += '&p_inv_no=' + document.getElementById("txbCondInvNo").value;
			}
			
			if (typeof(sDate) !='undefined' || sDate == "") 
			{
				params_dtl += '&p_s_date=' + sDate;
			}else
			{
				params_dtl += '&p_s_date=' + document.getElementById("txbStart").value;
			}
			
			if (typeof(eDate) !='undefined' || eDate == "") 
			{ 
				params_dtl += '&p_e_date=' + eDate;
			}else
			{
				params_dtl += '&p_e_date=' + document.getElementById("txbFinish").value;
			}
			/*
			if (typeof(invNo) !="undefined" || invNo == "")  params_dtl += '&p_inv_no=' + invNo;
			if (typeof(sDate) !='undefined' || sDate == "") params_dtl += '&p_s_date=' + sDate;
			if (typeof(eDate) !='undefined' || eDate == "") params_dtl += '&p_e_date=' + eDate;
			*/
			posting = true;
			
			if (posting)
			{
				xmlhttp2.open('POST','tax_invoice_ajax.php',true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params_dtl);
			}
			
		}
		
		function todayDMY(addDate)
		{
			//if (typeof(addDate) !="undefined" || addDate == "")  addDate = 0;
			var inputday = new Date();
			inputday.setDate(inputday.getDate() + addDate);
			var dd = inputday.getDate();
			var mm = inputday.getMonth()+1; //January is 0!
			var yyyy = inputday.getFullYear();
			if(dd<10){
				dd='0'+dd
			} 
			if(mm<10){
				mm='0'+mm
			} 
			inputday = dd+'-'+mm+'-'+yyyy;
			return inputday;
		}
		
		function resetCondition()
		{
			document.getElementById('txbCondInvNo').value = "";
			document.getElementById('txbStart').value = todayDMY(-7);
			document.getElementById("txbFinish").value = todayDMY(0);
			
		}
		function submit_search()
		{
			getInvMain(document.getElementById('txbCondInvNo').value, document.getElementById('txbStart').value, document.getElementById('txbFinish').value );
		}
	 
	</script>
	<!--=== Search Block Version 2 ===-->
</head> 

<body>    

	<div  id="wrap" class="wrapper">

		<?php require("include_header.php"); ?>

		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs breadcrumbs-dark">
			<div class="container">
				<h1 class="pull-left">TAX INVOICE</h1>
				
			</div>
		</div>
		
	   
		<div id="divSearch"  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		
		

			
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_condtion" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Tax Invoice No. :</label>
									<div class="col-lg-8" style="height: 35px">
										<input type="text" class="form-control" name="txbCondInvNo" id="txbCondInvNo"  placeholder="All Tax Invoice number" value="">
									</div>
								</div>
								
								<div class="form-group">
									<label for="inputTranDate" class="col-lg-4 control-label">Tax Invoice Date :</label>
									<section class="col col-lg-4" style="height: 16px">
										<label class="input">
											<i class="icon-append fa fa-calendar"></i>
											<input type="text" class="form-control;calendarStart" name="txbStart" id="txbStart" placeholder="Start date" value="">
										</label>
									</section>

									<section class="col col-lg-4" style="height: 16px">
										<label class="input">
											<i class="icon-append fa fa-calendar"></i>
											<input type="text" class="form-control;calendarFinish" name="txbFinish" id="txbFinish" placeholder="End date" value="">
										</label>
									</section>
								</div>
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button type="button" class="btn-u" onclick="submit_search()"> Search </button> &nbsp;&nbsp;
										
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

		<!--=== End Search Block Version 2 ===-->
		
		<div id="divTbResult" class="container content-sm"  style="padding:30px;">

			<div class="col-lg-offset col-lg-8" style="height:50px">
				<button type="button" class="btn-u btn-u-green"  data-toggle="modal" 
					onclick="showDtlForm('new');"> <i class="fa fa-plus-square icon-color-white"></i> New Tax Invoice </button> 
			
			</div>

			
			<div class="table-search-v2 margin-bottom-30">
				<div class="table-responsive">
					<form method="POST" enctype="multipart/form-data" action="user_account_model.php?form=frmUsAccTb" name="frmUsAccTb" onSubmit="return validate_form(this)"
						id="frmUsAccTb" >
						<input type="hidden" name="hdDelID" id="hdDelID"  value="">
						<input type="hidden" name="hdCancelID" id="hdCancelID"  value="">
						
						<div id="divGridInvMain">
							<script type="text/javascript">
								resetCondition();
								getInvMain("", document.getElementById('txbStart').value, document.getElementById("txbFinish").value );
							</script>
							<table class="table table-bordered table-striped">
								<thead>
									<tr valign="middle">
										<th> &nbsp; </th>
										<th>Tax Invoice Date</th>
										<th class="hidden-sm">Invoice No. </th>
										<th>PO No.</th>
										<th> Customer Name </th>
										<th> Total Amount </th>
									</tr>
								</thead>
								
							</table>
						</div>
						
					</form>
					
					
					<!-- End Test Form -->
				</div>    
			</div>    
			<!-- End Table Search v2 -->
			


			
		</div>
		<?php  require("tax_invoice_dtl_form.php");  ?>
		
		<?php require("include_footer.php"); ?>
		 
	</div><!--/End Wrapepr-->

	<?php require("include_js.php"); ?>


</body>
</html> 
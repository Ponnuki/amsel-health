<?php 
	require_once("include_function.php");
	require_once("class_tax_invoice.php");
	require_once("class_amh_province.php");
	require_once("class_amh_district.php");
	require_once("validatelogin.php");
	
	$amh_inv = new AMH_inv();
	$amh_province = new AMH_Province();
	$amh_district = new AMH_District();
	
	$inv_list = $amh_inv->inv_main();
	$ddl_customer = $amh_inv->get_ddl_customer();
	$ddl_po = $amh_inv->get_ddl_po();
	$ddl_province = $amh_province->mst_province();
	
 ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | CONSIGNMENT</title>
	<?php $current_menu = "consignment"; ?>
	<?php require("include_headtag.php"); ?>
    
	<script type="text/javascript">
	
		function delInvMain(invId, elmNo)
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp2.onreadystatechange=function()
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200)
				{
					result_msg= xmlhttp2.responseText.replace("\r\n\t", "");
					if (result_msg != "")
					{
						alert("Function [delInvMain] Error :" + result_msg);
					}
					
				}
			}
			
			mode = document.getElementById('hdMode' ).value
			params_dtl  = 'operate=del_inv_main';
			params_dtl += '&p_inv_id=' + invId;
			
			posting = true;
			
			if (posting)
			{
				xmlhttp2.open('POST','tax_invoice_ajax.php',true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params_dtl);
			}
		}
		
		function confirmDel(DelID,elmNo)
		{
			if (confirm("Are you sure you want to delete this item."))
			{
				delInvMain(DelID, elmNo);
				getInvMain();
			}
		 
		}
		
		function getInvMain(invNo, sDate, eDate)
		{
			posting = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp2=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp2.onreadystatechange=function() 
			{
				if (xmlhttp2.readyState==4 && xmlhttp2.status==200) 
				{
					var str = xmlhttp2.responseText.replace("\r\n\t", "");
					document.getElementById("divGridInvMain").innerHTML = str;
					//$('.selectpicker').selectpicker('refresh');
					
				}
			}
			
			params_dtl  = 'operate=grid_inv_main';
			if (typeof(invNo) !="undefined" || invNo == "")  params_dtl += '&p_inv_no=' + invNo;
			if (typeof(sDate) !='undefined' || sDate == "") params_dtl += '&p_s_date=' + sDate;
			if (typeof(eDate) !='undefined' || eDate == "") params_dtl += '&p_e_date=' + eDate;
			
			posting = true;
			
			if (posting)
			{
				xmlhttp2.open('POST','tax_invoice_ajax.php',true);
				xmlhttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp2.send(params_dtl);
			}
			
		}
		
		function todayDMY(addDate)
		{
			//if (typeof(addDate) !="undefined" || addDate == "")  addDate = 0;
			var inputday = new Date();
			inputday.setDate(inputday.getDate() + addDate);
			var dd = inputday.getDate();
			var mm = inputday.getMonth()+1; //January is 0!
			var yyyy = inputday.getFullYear();
			if(dd<10){
				dd='0'+dd
			} 
			if(mm<10){
				mm='0'+mm
			} 
			inputday = dd+'-'+mm+'-'+yyyy;
			return inputday;
		}
		
		function resetCondition()
		{
			document.getElementById('txbCondInvNo').value = "";
			document.getElementById('txbStart').value = todayDMY(-7);
			document.getElementById("txbFinish").value = todayDMY(0);
			
		}
		function submit_search()
		{
			getInvMain(document.getElementById('txbCondInvNo').value, document.getElementById('txbStart').value, document.getElementById('txbFinish').value );
		}
	 
	</script>
	<!--=== Search Block Version 2 ===-->
</head> 

<body>    

	<div  id="wrap" class="wrapper">

		<?php require("include_header.php"); ?>

		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs breadcrumbs-dark">
			<div class="container">
				<h1 class="pull-left">CONSIGNMENT</h1>
				
			</div>
		</div>
		
	   
		<div id="divSearch"  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		
		

			
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_condtion" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Consignee :</label>
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
											<select name="ddlCondConsign" id="ddlCondConsign" class="selectpicker form-control" 
											data-live-search="true" title="Please select ..." onchange="getDdlPO(this.value);">
												<option value="" selected> - Please Select - </option>
												<option value="" > CS-WS001 / ร้านวัตสัน เดอะมอลล์รามฯ </option>
												<option value="" > CS-WS002 / ร้านวัตสัน จอมทอง </option>
												<option value="" > CS-WS003 / ร้านวัตสัน ซีคอน บางแค </option>
												<?php
												/*
													foreach ($ddl_customer as $row)
													{
														echo "<option value='".$row["CUS_ID"]."' > ".$row["CUS_NAME"]."</option>";
													}
												*/
												?>
												
											</select>
											
										</label>
									</div>
								</div>
								
								<div class="form-group">
									<label for="inputTranDate" class="col-lg-4 control-label">Create Date :</label>
									<section class="col col-lg-4" style="height: 16px">
										<label class="select" >
											
											<select name="ddlCondMonth" id="ddlCondMonth" class="selectpicker form-control" 
											data-live-search="true" title="Please select ..." onchange="getDdlPO(this.value);">
												<option value="" selected> - Month - </option>
												<option value="" > July </option>
												<option value="" > August </option>
												<option value="" > September </option>
												<option value="" > Octorber </option>
												<option value="" > November </option>
												<option value="" > December </option>
												<?php
												
												?>
												
											</select>
											
										</label>
									</section>

									<section class="col col-lg-4" style="height: 16px">
										<label class="select" >
											
											<select name="ddlCondYear" id="ddlCondYear" class="selectpicker form-control" 
											data-live-search="true" title="Please select ..." onchange="getDdlPO(this.value);">
												<option value="" selected> - Year - </option>
												<option value="" > 2014 </option>
												<option value="" > 2015 </option>
												
												<?php
												
												?>
												
											</select>
											
										</label>
									</section>
								</div>
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button type="button" class="btn-u" onclick="submit_search()"> Search </button> &nbsp;&nbsp;
										
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

		<!--=== End Search Block Version 2 ===-->
		
		<div id="divTbResult" class="container content-sm"  style="padding:30px;">
			<div class="form-group" style="vertical-align:bottom;">
				<div class="col-lg-offset col-lg-4" style="height:50px; width">
					<button type="button" class="btn-u btn-u-red"  data-toggle="modal"  id="btCredit"
						onclick="showDtlForm('new');"> <i class="fa fa-plus-square icon-color-white"></i> Add Credit </button> 
					&nbsp; &nbsp;
					<button type="button" class="btn-u btn-u-green"  data-toggle="modal" id="btDebit"
						onclick="showDtlForm('new');"> <i class="fa fa-plus-square icon-color-white"></i> Add Debit</button> 
						
				</div>
					
				<div class="col-lg-offset col-lg-2" style="height:50px">
					
				
				</div>
			</div>
			

			
			<div class="table-search-v2 margin-bottom-30">
				<div class="table-responsive">
					<form method="POST" enctype="multipart/form-data" action="user_account_model.php?form=frmUsAccTb" name="frmUsAccTb" onSubmit="return validate_form(this)"
						id="frmUsAccTb" >
						<input type="hidden" name="hdDelID" id="hdDelID"  value="">
						<input type="hidden" name="hdCancelID" id="hdCancelID"  value="">
						
						<div id="divGridInvMain">
							<script type="text/javascript">
								//resetCondition();
								//getInvMain("", document.getElementById('txbStart').value, document.getElementById("txbFinish").value );
							</script>
							<table class="table table-bordered table-striped">
								<thead>
									<tr valign="middle">
										<th width='50px'> &nbsp; </th>
										<th>Create Date</th>
										<th>Account No. </th>
										<th>Account Type</th>
										<th width='124px'> Credit </th>
										<th width='124px'> Debit</th>
										<th width='124px'> Total</th>
										<th width='50px'> &nbsp;</th>
									</tr>
								</thead>
								<tr name='trRow'>
									<td>		</td>
									<td>01/11/2015</td>
									<td>	</td>
									<td>Balance Credit</td>
									<td>	75,255.00</td>
									<td>		</td>
									<td>75,255.00	</td>
									<td>		</td>
								</tr>
								<tr name='trRow'>
									<td>	
										<button class="btn-u btn-u-sm btn-u-blue"  data-toggle="modal" type="button">
										<i class="fa fa-edit"></i>
											Edit
										</button>
									</td>
									<td>02/11/2015</td>
									<td>	DO580825-059</td>
									<td>Delivery Order</td>
									<td>	25,055.00</td>
									<td>		</td>
									<td>	50,200.00</td>
									<td>	
										<button class="btn-u btn-u-sm btn-u-red" type="submit">
										<i class="fa fa-trash-o"></i>
											Delete
										</button>
									</td>
								</tr>
								<tr name='trRow'>
									<td>	
										<button class="btn-u btn-u-sm btn-u-blue"  data-toggle="modal" type="button">
										<i class="fa fa-edit"></i>
											Edit
										</button>
									</td>
									<td>15/11/2015</td>
									<td>	IV58-05959</td>
									<td>Tax Invoice</td>
									<td>	</td>
									<td>	10,000.00	</td>
									<td>	40,200.00</td>
									<td>	
										<button class="btn-u btn-u-sm btn-u-red" type="submit">
										<i class="fa fa-trash-o"></i>
											Delete
										</button>
									</td>
								</tr>
								<tr name='trRow'>
									<td>	
										<button class="btn-u btn-u-sm btn-u-blue"  data-toggle="modal" type="button">
										<i class="fa fa-edit"></i>
											Edit
										</button>
									</td>
									<td>20/11/2015</td>
									<td>	DO580825-099</td>
									<td>Delivery Order</td>
									<td>	12,000.00</td>
									<td>		</td>
									<td>	52,200.00</td>
									<td>	
										<button class="btn-u btn-u-sm btn-u-red" type="submit">
										<i class="fa fa-trash-o"></i>
											Delete
										</button>
									</td>
								</tr>
								<tr name='trRow'>
									<td>	
										<button class="btn-u btn-u-sm btn-u-blue"  data-toggle="modal" type="button">
										<i class="fa fa-edit"></i>
											Edit
										</button>
									</td>
									<td>27/11/2015</td>
									<td>	IV58-05969</td>
									<td>Tax Invoice</td>
									<td>	</td>
									<td>	30,000.00	</td>
									<td>	22,200.00</td>
									<td>	
										<button class="btn-u btn-u-sm btn-u-red" type="submit">
										<i class="fa fa-trash-o"></i>
											Delete
										</button>
									</td>
								</tr>
							</table>
						</div>
						
					</form>
					
					
					<!-- End Test Form -->
				</div>    
			</div>    
			
			<!-- End Table Search v2 -->
			<div class="panel panel-grey margin-bottom-40">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-globe"></i> Stock Summary</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pro. Sale Code</th>
                                    <th class="hidden-sm">Product Name</th>
                                    <th>QTY</th>
                                    <th>Unit Price</th>
									<th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>B001</td>
                                    <td class="hidden-sm">อะมิโนเลซิติก</td>
                                    <td>20</td>
                                    <td>255.00</td>
									<td>5,100.00</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>B001.1</td>
                                    <td class="hidden-sm">อะมิโนเลซิติก</td>
                                    <td>50</td>
                                    <td>199.00</td>
									<td>9,950.00</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>B004</td>
                                    <td class="hidden-sm">แอมเซล อะมิโน บิลเบอร์รี่ สกัด พลัส</td>
                                    <td>10</td>
                                    <td>990.00</td>
									<td>9,900.00</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>B008</td>
                                    <td class="hidden-sm">แอมเซล อะมิโน คอลลาเจน</td>
                                    <td>12</td>
                                    <td>149.00</td>
									<td>1,788.00</td>
                                </tr>
								<tr>
                                    <td></td>
                                    <td></td>
                                    <td class="hidden-sm"></td>
                                    <td></td>
                                    <td><b>Total</b></td>
									<td><b>22,200.00</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>                      
                </div>
                <!--End Table Bordered-->


			
		</div>
		<?php  require("tax_invoice_dtl_form.php");  ?>
		
		<?php require("include_footer.php"); ?>
		 
	</div><!--/End Wrapepr-->

	<?php require("include_js.php"); ?>


</body>
</html> 
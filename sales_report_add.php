<?php 

error_reporting(E_ERROR);
ini_set('display_errors', 1);

require_once('class_amh_db.php');
require_once('class_amh_pc.php');

$amh_pc = new AMH_PC();

$SALE_REPORT_ID = "";
$str_ret        = "";
$arr_pc         = array();

$arr_params = json_decode($_REQUEST["params"], 1);

$arr_aut  = $amh_pc->get_aut_user(" AUT_ID = '".$_REQUEST["aut_id"]."' ");
$aut_name = $arr_aut[0]["AUT_UNAME"];
$the_date = $amh_pc->swap_date($_REQUEST["the_date"]);

$str_del = $amh_pc->del_sale_report_dtl_backend($the_date, $aut_name, $_REQUEST["cus_id"]);

foreach ($arr_params as $num => $params)
{
    $SALE_REPORT_ID = $amh_pc->add_sale_rpt_dtl($aut_name, $params, $the_date, $_REQUEST["cus_id"]);
    $str_ret .= $num." prd_pc_id:".$params["prd_pc_id"]." qty:".$params["qty"]."\n";
}

if ($_REQUEST["adtn_pc"] != "") { $arr_pc = explode(',', $_REQUEST["adtn_pc"]); }

array_unshift($arr_pc, $_REQUEST["aut_id"]);

if ($SALE_REPORT_ID != '')
{
    $amh_pc->del_sale_rpt_rel($SALE_REPORT_ID);

    foreach ($arr_pc as $the_pc)
    {
        $amh_pc->add_sale_rpt_rel($the_pc, $SALE_REPORT_ID, $aut_name);
    }

    $str_error = "";
}
else
{
    $str_error = "(Additional PC cannot add)";
}

echo "Updated ".count($arr_params)." item(s)\n".$aut_name."\n".$the_date."\n".$str_ret."\n".$str_del."\n".$str_error;
	<script type="text/javascript">
		function ClearForm()
		{
			document.getElementById('hdMode').value = "new";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-plus-square'></i>New Product Master";
			document.getElementById("txbProCode").readOnly = false;
			document.getElementById("txbProCode").style.backgroundColor = "";
			document.getElementById('txbProCode').value = "";
			document.getElementById('labProError').innerHTML = "";
			document.getElementById('labProError').className = "";
			document.getElementById("txbProEn").style.backgroundColor = "";
			document.getElementById("txbProEn").value = "";
			document.getElementById("txbProTh").style.backgroundColor = "";
			document.getElementById("txbProTh").value = "";
			$("#ddlProType").selectpicker('val',"");
			document.getElementById("txbImgFile").value = "";
			$('#imgPrd').attr('src', "<?php echo $pro_img_path; ?>no_image.png");
			document.getElementById("btSave").style.display = "none";
			document.getElementById("btSaveNew").style.display = "";
			
		}
		
		function chkDupData()
		{
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp1=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp1=new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp1.onreadystatechange=function() 
			{
				
				if (xmlhttp1.readyState==4 && xmlhttp1.status==200) 
				{
					var dupProCode = "";
					dupProCode = xmlhttp1.responseText.replace("\r\n\t", "");
					
					if (dupProCode == ""|| dupProCode== null)
					{
						document.getElementById("labProError").innerHTML = "";
						document.getElementById("btSubmit").click();
						//document.forms["frmProSalesDtl"].submit();
					}else
					{
						// Check Edit Case
						if (document.getElementById("hdMode").value == "new")
						{
							document.getElementById("labProError").className= "col-lg-8 error";
							document.getElementById("labProError").innerHTML = "This data duplicate. Please chack Product code : ["+ dupProCode+"]";
							
						}else
						{
							document.getElementById("btSubmit").click();
						}
					}
				}
				
			}
			
			var chkprocode = document.getElementById("txbProCode").value;
			params  = 'operate=chk_dup';
			params += "&chkprocode="+chkprocode;
			posting = true;
			if (posting)
			{
				xmlhttp1.open("POST","product_master_ajax.php",true);
				xmlhttp1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp1.send(params);
			}
			
		}
		
		function getDataAjax(product_id) 
		{
			document.getElementById('hdMode').value = "edit";
			document.getElementById('hdProId').value = product_id;
			document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit Product Master";
			document.getElementById("txbProCode").readOnly = true;
			document.getElementById("labProError").className = "";
			document.getElementById("labProError").innerHTML = "";
			document.getElementById("btSave").style.display = "";
			document.getElementById("btSaveNew").style.display = "none";
			
			posting = false;
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200) 
				{
					
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					
					var result = JSON.parse(str);
					//var res = str.split("|");
					//alert("str="+str);
					//document.getElementById("ddlProCode").readOnly = true;
					document.getElementById('txbProCode').value = result[0]["PRODUCT_CODE"];
					document.getElementById('txbProTh').value = result[0]["PRODUCT_NAME_TH"];
					document.getElementById('txbProEn').value = result[0]["PRODUCT_NAME_EN"];
					$("#ddlProType").attr("disabled", false);
					$("#ddlProType").selectpicker("setStyle", "");
					$("#ddlProType").selectpicker('val',result[0]["PRODUCT_TYPE_ID"]);
					document.getElementById('txbImgFile').value = result[0]["PRD_IMG"];
					$('#imgPrd').attr('src', "<?php echo $pro_img_path; ?>"+result[0]["PRD_IMG"]);
				}
			}
			params  = 'operate=get_prd_dtl';
			if (typeof(product_id) !="undefined")  params += "&product_id="+product_id;
			posting = true;
			if (posting)
			{
				xmlhttp.open("POST","product_master_ajax.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(params);
			}
		}
		
		function previewImg(imgObj)
		{
			
			document.getElementById('txbImgFile').value = imgObj.value.replace(/.*[\/\\]/, '');
			if (imgObj.files && imgObj.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#imgPrd').attr('src', e.target.result);
				}
				reader.readAsDataURL(imgObj.files[0]);
			}
		}
    
    </script>
	<?php
		// **** Gen Mater Product 
		$sql = "SELECT PRODUCT_TYPE_ID, concat(TYPE_NAME_TH , ' / ', TYPE_NAME_EN) as TYPE_NAME, ICON
					FROM mst_product_type 
					ORDER BY TYPE_NAME_TH; ";
		$result_pro_type =  mysql_query($sql);
	?>
	
		<form method="POST" enctype="multipart/form-data" action="product_master_model.php?form=frm_pro_mst_dtl" name="frmProMstDtl" 
		 id="frmProMasterDtl"  data-toggle="validator" role="form">
			<input type="hidden" name="hdMode" id="hdMode"  value="">
			<input type="hidden" name="hdProId" id="hdProId"  value="">
		
			<div class="modal fade" id="product_master_dtl_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog">
					<div id="divFormSalesTran" class="modal-content">
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							
							<h4 class="modal-title" id="labMode"><i class="fa fa-plus-square"></i>New Product Master</h4>
						</div>
						<div class="modal-body sky-form"  style="border-style:none;  padding: 10px">
							
							<!-- Product panal -->
							<div class="panel panel-grey margin-bottom-5 padding: 15px">
								<div class="row" style="padding-top: 15px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-4 control-label" style="text-align:right">Product Code <font color=#ff0000>*</font></label>
										
										
										<div class="col-lg-8">
											<input type="text" class="form-control" name="txbProCode" id="txbProCode" placeholder="" required>
										</div>
										
									</div>
									<div class="col-lg-4"></div><label id="labProError"></label>
								</div>
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-4 control-label" style="text-align:right">Product Thai Name <font color=#ff0000>*</font></label>
										<div class="col-lg-8">
											<input type="text" class="form-control" name="txbProTh" id="txbProTh" placeholder="" required>
										</div>
										
									</div>
								</div>
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										
										
										<label class="col-lg-4 control-label" style="text-align:right">Product English Name <font color=#ff0000>*</font></label>
										<div class="col-lg-8">
											<input type="text" class="form-control" name="txbProEn" id="txbProEn" placeholder="" required>
										</div>
										
									</div>
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-4 control-label" style="text-align:right">Product Type <font color=#ff0000>*</font></label>
										<div class="col-lg-8">
											<label class="select" >
												<select name="ddlProType" id="ddlProType" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
														<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_pro_type)) 
													{
														echo "<option value='".$row['PRODUCT_TYPE_ID']."' ";
														echo ">".$row['TYPE_NAME']."</option>";
													}
													mysql_data_seek ($result_pro_code , 0 );
													?>
												</select>
										</label>
										</div>
										
									</div>
								</div>
								
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-4 control-label" style="text-align:right">Image Product <font color=#ff0000>*</font></label>
										<div class="col-lg-8">
											<label for="file" class="input input-file">
												<div class="button"><input type="file" name="fileImg" id="fileImg" onchange="previewImg(this);">Browse</div>
												<input type="text" id="txbImgFile" name="txbImgFile" readonly>
											</label>
										</div>
										
									</div>
								</div>
								
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<div class="col-lg-12" style="text-align: center;"> 
										Please use image size 300 x 300 px.<br/>
											<img name="imgPrd" id="imgPrd"  src="<?php echo $pro_img_path; ?>no_image.png" alt="" height="300" width="300" style="border:1px solid #bbbbbb">
										</div>
										
									</div>
									
								</div>
								
							</div><!-- End Product panal -->
							
							

							
						</div><!-- End "modal-body" -->
						<div class="modal-footer" style=" padding: 10px">
							<button type="button" name="btClose" id="btClose" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
							<button type="button" onclick="chkDupData()" class="btn-u btn-u-primary" name="btSaveNew" id="btSaveNew" >Save New</button>
							<button type="button" onclick="chkDupData()"  class="btn-u btn-u-primary" name="btSave" id="btSave">Save</button>
							<button type="submit" class="btn-u btn-u-primary" name="btSubmit" id="btSubmit" style="display:none">Submit</button>
						</div>
						
					</div><!-- END modal-body -->
				</div>
			</div>

		</form>
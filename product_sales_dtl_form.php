﻿	<script type="text/javascript">
    <!--
		function ClearForm()
		{
			document.getElementById('hdMode').value = "new";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-plus-square'></i>New Product Sales";
			
			$("#ddlProCode").attr("disabled", false);
			$('#ddlProCode').selectpicker("setStyle", "");
			$('#ddlProCode').selectpicker('val','');
			document.getElementById("ddlProCode").style.backgroundColor = "";
			document.getElementById('txbProSalesCode').value = "";
			document.getElementById("txbProSalesCode").style.backgroundColor = "";
			document.getElementById("labProSalesCode").innerHTML = "Product Sales Code <font color='#ff0000'>*</font> :";
			document.getElementById('txbLTPPrice').value = "";
			document.getElementById('txbStickerPrice').value = "";
			document.getElementById('txbCheerRate').value = "";
			document.getElementById('txbLPTComm').value = "";
			document.getElementById('labError').innerHTML = "";
			document.getElementById("btSave").style.display = "none";
			document.getElementById("btSaveNew").style.display = "";
			
		}
		function getNewProCode(pro_id)
		{
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					document.getElementById("txbProSalesCode").value = xmlhttp.responseText.replace("\r\n\t", "");
					document.getElementById("txbProSalesCode").style.backgroundColor = "#ddffdd";
					document.getElementById("labProSalesCode").innerHTML = "New Product Sales Code <font color=#ff0000>*</font>: ";
				}
			}
			xmlhttp.open("GET","product_sales_ajax.php?pro_id="+pro_id ,true);
			xmlhttp.send();
		
		}
		
		function chkDupData()
		{
			//var dupResult;
			//dupResult = false;
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					var dupProSalesCode = "";
					dupProSalesCode = xmlhttp.responseText.replace("\r\n\t", "");
					
					if (dupProSalesCode == ""|| dupProSalesCode== null)
					{
						document.getElementById("labError").innerHTML = "";
						document.getElementById("btSubmit").click();
						
						//document.forms["frmProSalesDtl"].submit();
					}else
					{
						if (document.getElementById("hdMode").value == "new")
						{
							document.getElementById("labError").innerHTML = "This data duplicate. Please chack Product sales code : ["+ dupProSalesCode+"]";
						}else
						{
							document.getElementById("btSubmit").click();
						}
					}
				}
				
			}
			
			procode = $('#ddlProCode').val();
			ltp_price = document.getElementById('txbLTPPrice').value;
			sticker_price = document.getElementById('txbStickerPrice').value
			cheerrate = document.getElementById('txbCheerRate').value 
			ltp_comm= document.getElementById('txbLPTComm').value
			
			xmlhttp.open("GET","product_sales_ajax.php?procode="+procode+"&ltp_price="+ltp_price+"&sticker_price="+sticker_price+"&cheerrate="+cheerrate+"&ltp_comm="+ltp_comm ,true);
			xmlhttp.send();
			
			
		}
		
		function getDataAjax(int) 
		{
			document.getElementById('hdMode').value = "edit";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit Product Sales";
			document.getElementById("labError").innerHTML = "";
			document.getElementById("btSave").style.display = "";
			document.getElementById("btSaveNew").style.display = "none";
			$("#ddlProCode").attr('disabled', true);
			$('#ddlProCode').selectpicker('setStyle', 'btn-inverse');
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					var res = str.split("|");
					//alert("str="+str);
					$('#ddlProCode').selectpicker('val',res[2]);
					//document.getElementById("ddlProCode").readOnly = true;
					document.getElementById('txbProSalesCode').value = res[1];
					document.getElementById('txbLTPPrice').value = res[4];
					document.getElementById('txbStickerPrice').value = res[5];
					document.getElementById('txbCheerRate').value = res[6];
					document.getElementById('txbLPTComm').value = res[7];
					
				}
			}
			
			xmlhttp.open("GET","product_sales_ajax.php?trn_ltp_chart_id="+int,true);
			xmlhttp.send();
		}
    //-->
    </script>
	<?php
		// **** Gen Maser Product 
		$sql = "SELECT PRODUCT_ID, concat(PRODUCT_CODE,  ' : ', mp.PRODUCT_NAME_TH) as PCODE_PNAME
					FROM mst_product AS mp 
					ORDER BY PRODUCT_CODE; ";
		$result_pro =  mysql_query($sql);
	?>
	
		<form method="POST" enctype="multipart/form-data" action="product_sales_model.php?form=pro_sales_dtl" name="frmProSalesDtl" 
		 id="frmProSalesDtl"  data-toggle="validator" role="form">
			<input type="hidden" name="hdMode" id="hdMode"  value="">
		
			<div class="modal fade" id="product_sales_dtl_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog modal-lg">
					<div id="divFormSalesTran" class="modal-content">
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							
							<h4 class="modal-title" id="labMode"><i class="fa fa-plus-square"></i>New Product Sales</h4>
						</div>
						<div class="modal-body sky-form"  style="border-style:none">
							
							<!-- Product panal -->
							<div class="panel panel-grey margin-bottom-15 padding: 15px">
								<div class="row" style="padding-top: 15px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Product Code <font color=#ff0000>*</font>:</label>
										<div class="col-lg-4">
											<label class="select" >
												<select name="ddlProCode" id="ddlProCode" class="selectpicker form-control" 
												data-live-search="true" title="Please select ..." onchange="getNewProCode(this.value);">
													<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_pro)) 
													{
														echo "<option value='".$row['PRODUCT_ID']."' >".$row['PCODE_PNAME']."</option>";
													}
													mysql_data_seek ($result_pro_sale_code , 0 );
													?>
												</select>
												
										</label>
										</div>
										<label id="labProSalesCode" class="col-lg-2 control-label" style="text-align:right">Product Sales Code *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbProSalesCode" id="txbProSalesCode" placeholder="" readonly>
										</div>
									</div>
								</div>
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">LTP Price *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbLTPPrice" id="txbLTPPrice" placeholder="" required>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Sticker Price *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbStickerPrice" id="txbStickerPrice" placeholder="" required>
										</div>
									</div>
								</div>
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Cheer Rate *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbCheerRate" id="txbCheerRate" placeholder="" required>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">LTP for Commission *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbLPTComm" id="txbLPTComm" placeholder="" required>
										</div>
									</div>
								</div>
								
							</div><!-- End Product panal -->
							<label id="labError" class="error"></label>
							

							
						</div><!-- End "modal-body" -->
						<div class="modal-footer">
							<button type="button" name="btClose" id="btClose" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
							<button type="button" onclick="chkDupData('New')" class="btn-u btn-u-primary" name="btSaveNew" id="btSaveNew" >Save New</button>
							<button type="button" onclick="chkDupData('Edit')"  class="btn-u btn-u-primary" name="btSave" id="btSave">Save</button>
							<button type="submit" class="btn-u btn-u-primary" name="btSubmit" id="btSubmit" style="display:none">Submit</button>
						</div>
						
					</div><!-- END modal-body -->
				</div>
			</div>

		</form>
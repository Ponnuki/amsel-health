﻿<?php
	require("connect.php");
	
// ******** Function **********
	function getCurrentQty($shop_id, $sales_emp_id, $product_sale_code,$trn_sales_no)
	{
		$stock_qty = "";
		$sql = sprintf("SELECT ss.STOCK_QTY + IFNULL(tsd.SALES_OUT,0) - IFNULL(tsd.SALES_IN,0) AS STOCK_QTY
								FROM stock_shop AS ss 
								  LEFT JOIN trn_sales_dtl AS tsd 
									ON ss.SHOP_ID = tsd.SHOP_ID 
										AND ss.SALES_EMP_ID = tsd.SALES_EMP_ID 
										AND ss.PRODUCT_SALE_CODE = tsd.PRODUCT_SALE_CODE 
										AND tsd.TRN_SALES_NO = '%s'
								WHERE ss.SHOP_ID = %d AND ss.SALES_EMP_ID =%d AND ss.PRODUCT_SALE_CODE = '%s' ",
					mysql_real_escape_string($trn_sales_no),
					mysql_real_escape_string($shop_id),
					mysql_real_escape_string($sales_emp_id),
					mysql_real_escape_string($product_sale_code)
					);
			$result_qty_stock =  mysql_query($sql);
			$num_rows = mysql_num_rows($result_qty_stock);
			if ($num_rows > 0)
			{
				
				while($row = mysql_fetch_array($result_qty_stock)) 
				{
					$stock_qty = $row['STOCK_QTY'];
					//$stock_qty = "111";
				}
			} else
			{
				$stock_qty = 0.00;
			}
			return $stock_qty;
		
	}
	//**************************
	
	if (isset($_REQUEST['sales_dtl_id']))
	{
			$sql = "SELECT SALES_DTL_ID, TRN_SALES_NO, DATE_FORMAT(TRN_SALES_DATE, '%d-%m-%Y') as TRN_SALES_DATE,  me.EMP_ID, me.EMP_CODE, me.FNAME_TH, 
						  tsd.PRODUCT_SALE_CODE, mp.PRODUCT_NAME_TH, tlc.LTP , tlc.STICKER_PRICE, tlc.CHEER_RATE, tlc.LPT_FOR_COMM, 
						  tsd.SHOP_ID, ms.SHOP_NAME,
						  tsd.SALES_IN, tsd.SALES_OUT
					FROM trn_sales_dtl AS tsd
					  LEFT JOIN mst_employee AS me ON tsd.SALES_EMP_ID = me.EMP_ID
					  LEFT JOIN mst_shop AS ms ON tsd.SHOP_ID = ms.SHOP_ID
					  LEFT JOIN mst_product_sale AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE
					  LEFT JOIN mst_product AS mp ON tlc.PRODUCT_ID = mp.PRODUCT_ID
					WHERE SALES_DTL_ID = ".$_REQUEST['sales_dtl_id'];
			$result_trn_sale_dtl =  mysql_query($sql);
			
			while($row = mysql_fetch_array($result_trn_sale_dtl)) 
			{
				echo $row['SALES_DTL_ID']."|".$row['TRN_SALES_NO']."|".$row['TRN_SALES_DATE']."|".$row['EMP_ID']."|".$row['FNAME_TH']."|".
					$row['PRODUCT_SALE_CODE']."|".$row['PRODUCT_NAME_TH']."|".$row['LTP']."|".$row['STICKER_PRICE']."|".$row['CHEER_RATE']."|".
					$row['LPT_FOR_COMM']."|".$row['SHOP_ID']."|".$row['SHOP_NAME']."|".$row['SALES_IN']."|".$row['SALES_OUT']."|".
					getCurrentQty($row['SHOP_ID'],$row['EMP_ID'],$row['PRODUCT_SALE_CODE'],'') ;

			}
			
	}else if (isset($_REQUEST['sales_id']) && isset($_REQUEST['product_sale_code']) && isset($_REQUEST['shop_id']))
	{
		if (isset($_REQUEST['trn_sales_no']))
		{
			echo getCurrentQty($_REQUEST['shop_id'],$_REQUEST['sales_id'],$_REQUEST['product_sale_code'],$_REQUEST['trn_sales_no']) ;
		}else{
			echo getCurrentQty($_REQUEST['shop_id'],$_REQUEST['sales_id'],$_REQUEST['product_sale_code'],'') ;
			
		}
			
				
		
	}	else if (isset($_REQUEST['sales_id']))
	{
			$sql = "SELECT me.EMP_ID, me.EMP_CODE, CONCAT(me.FNAME_TH, ' ' , IFNULL(LNAME_TH,'') , ' (' , IFNULL(NICK_NAME,'') , ')' )AS SALES_NAME
					FROM mst_employee AS me 
					WHERE me.EMP_ID =".$_REQUEST['sales_id']."; ";
			$result_sales_name =  mysql_query($sql);
			
			while($row = mysql_fetch_array($result_sales_name)) 
			{
				echo $row['SALES_NAME'];
			}
		
	}else if (isset($_REQUEST['product_sale_code']))
	{
			$sql = "SELECT tlc.PRODUCT_SALE_CODE, mp.PRODUCT_NAME_TH, tlc.LTP , tlc.STICKER_PRICE, tlc.CHEER_RATE, tlc.LPT_FOR_COMM
					FROM mst_product_sale AS tlc 
					  LEFT JOIN mst_product AS mp ON tlc.PRODUCT_ID = mp.PRODUCT_ID
					WHERE tlc.PRODUCT_SALE_CODE ='".$_REQUEST['product_sale_code']."'; ";
			$result_sales_name =  mysql_query($sql);
			
			while($row = mysql_fetch_array($result_sales_name)) 
			{
				echo $row['PRODUCT_NAME_TH']."|".$row['LTP']."|".$row['STICKER_PRICE']."|".$row['CHEER_RATE']."|".$row['LPT_FOR_COMM'];
			}
		
	}else if (isset($_REQUEST['shop_id']))
	{
			$sql = "SELECT ms.SHOP_NAME
					FROM mst_shop AS ms 
					WHERE ms.SHOP_ID =".$_REQUEST['shop_id']."; ";
			$result_shop_name =  mysql_query($sql);
			
			while($row = mysql_fetch_array($result_shop_name)) 
			{
				echo $row['SHOP_NAME'];
			}
		
	}
	
?>

	<?php
		break; 					
	?>
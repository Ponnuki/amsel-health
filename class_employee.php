<?php
require_once("class_amh_db.php");
require_once("include_function.php");
//require_once("validatelogin.php");

class AMH_emp extends AMH_DB
{
	public function get_emp_dtl($emp_id = "")
    {
        $arr_ret = array();
		
		$sql = "SELECT EMP_ID, EMP_CODE, FNAME_TH, LNAME_TH, FNAME_EN, LNAME_EN, NICK_NAME, TITLE_ID,
								DATE_FORMAT(START_JOB_DATE, '%d-%m-%Y') as START_JOB_DATE,
								IFNULL(EMAIL,'') as EMAIL, IFNULL(TEL,'') as TEL,  POSITION_ID
							FROM mst_employee
							WHERE EMP_ID = ".$emp_id.";  ";
        $result = $this->mysqli->query($sql) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
        while ($row = $result->fetch_array(MYSQLI_BOTH))
        {
			$arr_ret[] = $row;
        }
        $result->free();
		
        return json_encode($arr_ret);
	}
	
	public function chk_dup($emp_code = "",$fname_en ="", $lname_en = "")
    {
        $arr_ret = array();
		
		$sql = "SELECT IFNULL(EMP_CODE,'') AS EMP_CODE, IFNULL(FNAME_EN,'') AS FNAME_EN, IFNULL(LNAME_EN,'') AS LNAME_EN	
			FROM mst_employee
			WHERE EMP_CODE = '".$emp_code."' OR (FNAME_EN = '".$fname_en."' AND LNAME_EN = '".$lname_en."'); ";
        $result = $this->mysqli->query($sql) OR die("<div style='color:red;'>get_aut_user error:{$sql_sel}</div>");
      
		if ($result->num_rows > 0)
		{
			while ($row = $result->fetch_array(MYSQLI_BOTH))
			{
				echo $row['EMP_CODE'];
			}
		}else{
			echo "";
		}
        $result->free();
		
	}
	
}

	
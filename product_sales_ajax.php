﻿<?php
	require("connect.php");
	
	
	if (isset($_REQUEST['trn_lpt_chart_id']))
	{
		// Select Detail TRN_LPT_CHART_ID
		$sql = sprintf("SELECT tlc.LPT_CHART_ID, PRODUCT_SALE_CODE, tlc.PRODUCT_ID, mp.PRODUCT_NAME_TH, tlc.LTP,
					  tlc.STICKER_PRICE, tlc.CHEER_RATE, tlc.LPT_FOR_COMM
					FROM trn_lpt_chart AS tlc 
						LEFT JOIN mst_product AS mp ON tlc.PRODUCT_ID = mp.PRODUCT_ID
					WHERE tlc.LPT_CHART_ID = %d; "
					, mysql_real_escape_string($_REQUEST['trn_lpt_chart_id'])
					);
		$result_trn_lpt_chart_dtl =  mysql_query($sql);
		
		while($row = mysql_fetch_array($result_trn_lpt_chart_dtl)) 
		{
			echo $row['LPT_CHART_ID']."|".$row['PRODUCT_SALE_CODE']."|".$row['PRODUCT_ID']."|".$row['PRODUCT_NAME_TH']."|".
				$row['LTP']."|".$row['STICKER_PRICE']."|".$row['CHEER_RATE']."|".$row['LPT_FOR_COMM'];
		}
			
	}else if (isset($_REQUEST['pro_id']))
	{//Get New PRODUCT_SALE_CODE
		$sql = "SELECT IFNULL(
									CASE WHEN INSTR(MAX(PRODUCT_SALE_CODE), '.') > 0 THEN
										CONCAT( mp.PRODUCT_CODE, '.', 
										SUBSTRING(MAX(PRODUCT_SALE_CODE), INSTR(MAX(PRODUCT_SALE_CODE), '.')+1
										, 3) + 1) 
									ELSE
										CONCAT(MAX(PRODUCT_SALE_CODE),'.1') END, mp.PRODUCT_CODE
								)	AS NEW_SALE_CODE
					FROM trn_lpt_chart AS tlc
						LEFT JOIN mst_product AS mp ON tlc.PRODUCT_ID = mp.PRODUCT_ID
					WHERE  mp.PRODUCT_ID =".$_REQUEST['pro_id']."; ";
		$result_sales_name =  mysql_query($sql);
		
		while($row = mysql_fetch_array($result_sales_name)) 
		{
			echo $row['NEW_SALE_CODE'];
		}
		
	}
	else if (isset($_REQUEST['procode']) && isset($_REQUEST['lpt_price']) && isset($_REQUEST['sticker_price']) ) 
	{ // Check Duplicate 
		$sql=sprintf("SELECT *	FROM trn_lpt_chart
			WHERE PRODUCT_ID = %d AND LTP = %f AND STICKER_PRICE = %f AND CHEER_RATE = %f AND LPT_FOR_COMM = %f "
			, mysql_real_escape_string($_REQUEST['procode'])
			, mysql_real_escape_string($_REQUEST['lpt_price'])
			, mysql_real_escape_string($_REQUEST['sticker_price'])
			, mysql_real_escape_string($_REQUEST['cheerrate'])
			, mysql_real_escape_string($_REQUEST['lpt_comm'])
			);
		$result_dup=  mysql_query($sql);
		//mysql_num_rows($result_dup)
		
		if (mysql_num_rows($result_dup) > 0)
		{
			while($row = mysql_fetch_array($result_dup)) 
			{
				echo $row['PRODUCT_SALE_CODE'];
			}
			//echo mysql_num_rows($result_dup);
		}else{
			echo "";
		}
		
	}else if (isset($_REQUEST['shop_id']))
	{
			$sql = "SELECT ms.SHOP_NAME
					FROM mst_shop AS ms 
					WHERE ms.SHOP_ID =".$_REQUEST['shop_id']."; ";
			$result_shop_name =  mysql_query($sql);
			
			while($row = mysql_fetch_array($result_shop_name)) 
			{
				echo $row['SHOP_NAME'];
			}
		
	}
	
?>

	<?php
		break; 					
	?>
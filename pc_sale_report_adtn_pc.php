<?php  session_start();

    error_reporting(E_ERROR);
    ini_set('display_errors', 1);

$user_name = $_SESSION['aut_uname'];

if ($user_name != '')
{
    require_once('config.php');
    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');

    $str_ret   = "";
    $user_name = $_REQUEST["user_name"];
    $cus_id    = $_REQUEST["cus_id"];
    $s_date    = $_REQUEST["s_date"];

    $amh_pc = new AMH_PC();

    $str_where = " AUT_UNAME = '".$user_name."' ";
    $arr_aut   = $amh_pc->get_aut_user($str_where);
    $aut_id    = $arr_aut[0]["AUT_ID"];

    $arr_exist = array();
    $arr_exist = $amh_pc->chk_exist_report($user_name, $cus_id, $s_date);

    if ($arr_exist["SALE_REPORT_ID"] == "")
    {
        $amh_pc->add_report_main($user_name, $cus_id, $s_date);
        $arr_exist = $amh_pc->chk_exist_report($user_name, $cus_id, $s_date);
        $amh_pc->add_sale_rpt_rel($aut_id, $arr_exist["SALE_REPORT_ID"], $user_name);
    }

    if ($_REQUEST["action"] == "add")
    {
        $adtn_id   = $_REQUEST["adtn_id"];
        $adtn_name = $_REQUEST["adtn_name"];

        $amh_pc->add_sale_rpt_rel($adtn_id, $arr_exist["SALE_REPORT_ID"], $adtn_name);
    }

    $str_del_response = "";

    if ($_REQUEST["action"] == "del")
    {
        $str_del_response = $amh_pc->remove_sale_rpt_rel($_REQUEST["adtn_id"], $arr_exist["SALE_REPORT_ID"]);
    }

    $arr_pc = array();
    $arr_pc = $amh_pc->get_sale_rpt_rel($arr_exist["SALE_REPORT_ID"], $aut_id);

    foreach ($arr_pc as $the_pc)
    {
        $str_ret .= "<div class='pc_tag' id='aut_".$the_pc["AUT_ID"]."' onclick='loading_pc(\"del\", \"".$the_pc["AUT_UNAME"]."\", \"".$the_pc["AUT_ID"]."\")'>".$the_pc["AUT_UNAME"]."</div>\n";
    }

    echo $str_ret;
}
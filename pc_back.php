<?php 

    error_reporting(E_ERROR);
    ini_set('display_errors', 1);

    require_once("include_function.php");
    require_once("connect.php");
    require_once('validatelogin.php');

    require_once('class_amh_db.php');
    require_once('class_amh_pc.php');
    require_once('class_amh_customer.php');

    $this_date    = Date('d-m-Y');
    $last_7day    = Date('d-m-Y', strtotime("-7 days"));

    $amh_pc       = new AMH_PC();
    $amh_customer = new AMH_Customer();

    $str_pc_opt   = $amh_pc->create_pc_option();

    $str_cus_opt  = "<option value=''> - Please Select - </option>".$amh_customer->get_customer_option('');

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<title>AMSEL HEALTH SELECT | PC BACKEND</title>
<?php
    $current_menu = "pc_backend";
    require("include_headtag.php");
?>
<style type='text/css'>
.box_qty
{
    text-align: center;
}

input:-moz-read-only /* For Firefox */
{
    background-color: #bbb;
}

input:read-only
{
    background-color: #bbb;
}

.ptype_title
{
    /* border-bottom:1px solid #aaa; */
    font-size: 16px;
    font-weight: bold;
    margin:40px 0 10px;
    text-align: left;
}

.txt_red
{
    color: #e22;
}

.txt_bold
{
    font-weight: bold;
}

.waiting_text
{
    color: #3B3;
    font-size: 24px;
    font-weight: bold;
    text-align: center;
}
</style>

<script type="text/javascript">

function start_up()
{
    //loading_pc_order();
}

function checking_requirement()
{
    aut_id   = $('#aut_id'  ).val();
    cus_id   = $('#cus_id'  ).val();
    the_date = $('#the_date').val();

    if (aut_id   == '') { return 'Please Select PC!'; }
    if (cus_id   == '') { return 'Please Select Customer!'; }
    if (the_date == '') { return 'Date Missing!'; }

    return '';
}

function loading_pc_order()
{
    $('#btn_group_1').css('display','none');
    $('#btn_group_2').css('display','none');

    $('#product_pc_list_item').html('Loading, please wait . . .');

    check_req = checking_requirement();

    if (check_req != '')
    {
        $('#product_pc_list_item').html('<div style="color:#F00; font-size:14px; font-weight:bold;">'+check_req+'</div>');
    }
    else
    {
        aut_id   = $('#aut_id'  ).val();
        cus_id   = $('#cus_id'  ).val();
        the_date = $('#the_date').val();

        $.post( "pc_back_order_list.php", { the_date: the_date, cus_id: cus_id, aut_id: aut_id })
            .done(function(data)
            {
                $('#btn_group_1').css('display','block');
                $('#btn_group_2').css('display','block');
                $('#product_pc_list_item').html(data);

                loading_pc_order_existing(the_date, cus_id, aut_id);
            });
    }
}

function loading_pc_order_existing(the_date, cus_id, aut_id)
{
    $.post( "pc_back_order_load.php", { the_date: the_date, cus_id: cus_id, aut_id: aut_id })
        .done(function(data)
        {
            var str_shw       = "";
            var arr_order_dtl = JSON.parse(data);

            num_items = arr_order_dtl.length;

            for (i_cnt = 0; i_cnt < num_items; i_cnt++)
            {
                the_qty = arr_order_dtl[i_cnt].QTY * 1;
                num_qty = the_qty.toFixed(0);

                prd_pc_id = arr_order_dtl[i_cnt].PRODUCT_PC_ID;
                po_no     = arr_order_dtl[i_cnt].PO_NO;

                str_shw = prd_pc_id + " - " + po_no +" : QTY - " + num_qty + "\n";

                put_data_to_list(prd_pc_id, num_qty, po_no);
            }
        });
}

function put_data_to_list(prd_pc_id, num_qty, po_no)
{
    if ($('#num_all_item').length > 0)
    {
        item_num = $('#num_all_item').val();

        int_pls = 0;

        str_txt = "";

        for (j_cnt = 1; j_cnt <= item_num; j_cnt++)
        {
            this_val = document.getElementById("prd_pc_id_"+j_cnt).value;

            if (this_val == prd_pc_id)
            {
                document.getElementById("chk_"+j_cnt).checked = true;
                document.getElementById("QTY_"+j_cnt).value   = num_qty;
                if (po_no == null)
                {
                    swap_readonly(j_cnt);
                    document.getElementById("PO_NO_"+j_cnt).innerHTML = "";
                }
                else
                {
                    document.getElementById("chk_"+j_cnt).disabled = true;
                    document.getElementById("PO_NO_"+j_cnt).innerHTML = " ออก PO แล้ว ("+po_no+")";
                }
            }
        }
    }
}

function close_pc_order()
{
    $('#btn_group_1').css('display','none');
    $('#btn_group_2').css('display','none');
    $('#product_pc_list_item').html('');
}

function upd_pc_order()
{
    /*
        var employees = [
            {"firstName":"John", "lastName":"Doe"},
            {"firstName":"Anna", "lastName":"Smith"},
            {"firstName":"Peter","lastName": "Jones"}
        ];
    */

    the_param = "";
    num_param = 0;

    var arr_param = [];

    if ($('#num_all_item').length > 0)
    {
        item_num = $('#num_all_item').val();

        for (i_cnt = 1; i_cnt <= item_num; i_cnt++)
        {
            if (document.getElementById("chk_"+i_cnt).checked)
            {
                num_param++;
                if (num_param > 1) { the_param += ",\n"; }
                prm_id  = document.getElementById("prd_pc_id_"+i_cnt).value;
                prm_qty = document.getElementById("QTY_"+i_cnt).value;
                the_param += '{"prd_pc_id":"'+prm_id+'", "qty":"'+prm_qty+'"}';
            }
        }
        the_param  = "[\n"+the_param+"\n]";

        aut_id     = $('#aut_id').val();
        cus_id     = $('#cus_id').val();
        the_date   = $('#the_date').val();

        $.post( "pc_back_order_add.php", { params: the_param, aut_id: aut_id, cus_id: cus_id, the_date: the_date })
            .done(function(data)
            {
                alert("Updating completed.");
                /*
                $('#product_pc_list_item').prepend('<div id="status_tag1" style="margin:10px; color:#0E0; font-weight:bold; font-size:24px; text-align:center;">PC Order Updated.</div>');
                $('#product_pc_list_item').append('<div id="status_tag2" style="margin:10px; color:#0E0; font-weight:bold; font-size:24px; text-align:center;">PC Order Updated.</div>');
                setInterval(function(){ $('#status_tag1').remove(); $('#status_tag2').remove(); }, 4000);
                */
            });
    }
}

function choose_pc_id(cus_id)
{
    $.post( "pc_back_get_pc_id.php", { cus_id: cus_id })
        .done(function(data)
        {
            $('#aut_id').val(data);
            $('#aut_id').selectpicker('refresh');
        });
}

function swap_readonly(the_id)
{
    if (document.getElementById("chk_"+the_id).checked)
    { document.getElementById("QTY_"+the_id).readOnly = false; }
    else
    { document.getElementById("QTY_"+the_id).readOnly = true; }
}
</script>
</head> 

<body onload="start_up();">

<div id="wrap"  class="wrapper">

    <?php require_once("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">ORDER FROM PC</h1>
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
    <script type="text/javascript">
    <!--

     //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
        <div  class="search-block" id="searching_area" style="padding: 18px; padding-bottom: 0px;" >
            <div class="container">
                <div class="col-md-6 col-md-offset-3">
                    <h2>CONDITION</h2>
                    <div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
                        
                        <div class="sky-form" style="border-style:none">
                            <form class="form-horizontal" role="form"  method="POST" action="" name="form_po">
                                <div class="form-group">
                                    <label for="cus_id" class="col-lg-4 control-label">Customer <span class='txt_red'>*</span> :</label>
                                    <div class="col-lg-8">
                                        <select class="selectpicker form-control" id="cus_id" name="cus_id" data-live-search="true" onchange="choose_pc_id(this.value);"><?=$str_cus_opt;?></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="aut_id" class="col-lg-4 control-label">PC <span class='txt_red'>*</span> :</label>
                                    <div class="col-lg-8">
                                        <select class="selectpicker form-control" id="aut_id" name="aut_id" data-live-search="true"><?=$str_pc_opt;?></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="the_date" class="col-lg-4 control-label">Order Date <span class='txt_red'>*</span> :</label>
                                    <section class="col col-lg-4" style="height: 16px">
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" class="form-control" name="the_date" id="the_date" placeholder="Order Date" value="<?=$this_date;?>">
                                        </label>
                                    </section>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-4 col-lg-8">
                                        <button type="button" class="btn-u" onclick="loading_pc_order();">Loading PC Order</button> &nbsp;&nbsp;
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>    
            
        </div><!--/container--> 

    <!--=== End Search Block Version 2 ===-->

    <div id="lower_area" class="container content-sm" style="padding:30px;">

        <div id='btn_group_1' class="col-lg-offset col-lg-8" style="height:50px; display: none;">
            <button type="button" class="btn-u btn-u-green" onclick="upd_pc_order();"> <i class="fa fa-plus-square icon-color-white"></i> Update PC Order </button>
            <button type="button" class="btn-u btn-u-green" onclick="close_pc_order();"> <i class="fa fa-plus-square icon-color-white"></i> Close </button>
        </div>

        <!-- List items Section -->
        <div class="table-search-v2 margin-bottom-30" id="product_pc_list_item"></div>
        <!-- List items Section -->

        <div id='btn_group_2' class="col-lg-offset col-lg-8" style="height:50px; display: none;">
            <button type="button" class="btn-u btn-u-green" onclick="upd_pc_order();"> <i class="fa fa-plus-square icon-color-white"></i> Update PC Order </button>
            <button type="button" class="btn-u btn-u-green" onclick="close_pc_order();"> <i class="fa fa-plus-square icon-color-white"></i> Close </button>
        </div>

    </div>
    <?php require("include_footer.php"); ?>

</div><!--/End Wrapepr-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-select.min.js"></script>

<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
<script src="javascripts/jquery.uploadfile.min.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/forms/order.js"></script>
<script type="text/javascript" src="assets/js/forms/review.js"></script>
<script type="text/javascript" src="assets/js/forms/checkout.js"></script>
<script type="text/javascript" src="assets/js/plugins/masking.js"></script>
<script type="text/javascript" src="assets/js/plugins/datepicker.js"></script>
<script type="text/javascript" src="assets/js/plugins/validation.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        OrderForm.initOrderForm();        
        ReviewForm.initReviewForm();        
        CheckoutForm.initCheckoutForm();        
        Masking.initMasking();
        Datepicker.initDatepicker();
        Validation.initValidation();
        });

    $('#the_date').datepicker({
        dateFormat: 'dd-mm-yy',
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>'
        /*
        onSelect: function( selectedDate )
        {
            $('#txbTranDateEnd').datepicker('option', 'minDate', selectedDate);
        }
        */
    });
</script>

<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>
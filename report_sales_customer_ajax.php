<?php
	require_once("class_report_sales_customer.php");
	require_once("include_function.php");

	$amh_rp_sale = new AMH_rp_sale_cus();
	
	
	if ($_POST["operate"] == "get_ddl_customer")
	{
		echo $amh_rp_sale->get_ddl_customer();
	}
	else if ($_POST["operate"] == "get_prd_type")
	{
		echo $amh_rp_sale->get_prd_type();
	}
	else if ($_POST["operate"] == "get_data_customer")
	{
		if ($_POST["data_type"] == "sale")
		{
			echo $amh_rp_sale->get_data_sale($_POST["cus_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"], $temp_excel_path, $pro_img_path);
			
		}else if ($_POST["data_type"] == "order")
		{
			echo $amh_rp_sale->get_data_order($_POST["cus_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"], $temp_excel_path, $pro_img_path);
			
		}
		else if ($_POST["data_type"] == "stock")
		{
			echo $amh_rp_sale->get_data_stock($_POST["cus_id"], $_POST["prd_type"], $temp_excel_path, $pro_img_path);
		}
		
		
	}
	else if ($_POST["operate"] == "get_data_customer_daily")
	{
		if ($_POST["data_type"] == "sale")
		{
			echo $amh_rp_sale->get_data_sale_daily($_POST["cus_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"], $temp_excel_path, $pro_img_path);
			
		}else if ($_POST["data_type"] == "order")
		{
			echo $amh_rp_sale->get_data_order_daily($_POST["cus_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"], $temp_excel_path, $pro_img_path);
			
		}
		/*
		else if ($_POST["data_type"] == "stock")
		{
			echo $amh_rp_sale->get_data_stock_daily($_POST["cus_id"],$_POST["prd_type"],$_POST["start_date"],$_POST["end_date"]);
		}
		*/
		
	}
	
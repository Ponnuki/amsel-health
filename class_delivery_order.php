<?php
/* Class name
   AMH_Customer

   public function 
   mst_customer($str_search) return array of customer info (example: )
*/
require_once("class_amh_db.php");
require_once("include_function.php");
//require_once("validatelogin.php");

class AMH_do extends AMH_DB
{
    public function do_main($do_no="", $start_date ="", $end_date = "")
    {
        $arr_ret = array();

        $str_where = "1";
        if ($do_no != "")
        {
            $str_where  .= " AND  (DO_NO   LIKE '%".$do_no."%') ";
        }
        if ($start_date !="")
        {
            //if ($str_where !="1" ) $str_where .= " AND ";
            $str_where .= " AND (DO_DATE BETWEEN '".printDate($start_date)."' AND '".printDate($end_date)."') ";
        }

        $arr_ret = array();
        
        $sql_sel = " SELECT dom.DO_ID, dom.DO_NO, DATE_FORMAT(dom.DO_DATE, '%d-%m-%Y') AS DO_DATE,
                            dom.PO_ID, dom.DO_TYPE_ID, dom.CREDIT_TERM, dom.NOTE, dom.CUS_ID, dom.CUS_NAME, dom.CUS_CONTACT, dom.CUS_ADDRESS,
                            dom.DISTRICT_CODE, dom.PROVINCE_CODE, dom.CUS_ZIP, dom.CUS_TEL, dom.CUS_FAX, dom.TAX_NUMBER, dom.DO_TOTAL, dom.DISCOUNT_PER, 
                            dom.VAT_PER, dom.GRAND_TOTAL, pm.PO_NO
                        FROM do_main AS dom 
                            LEFT JOIN po_main AS pm ON dom.PO_ID = pm.PO_ID 
                        WHERE ".$str_where." ORDER BY dom.DO_NO DESC; ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>do_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();
        
        return json_encode($arr_ret);
    }

    public function do_main($do_no="", $start_date ="", $end_date = "")
    {
        $arr_ret = array();

        $str_where = "1";
        if ($do_no != "")
        {
            $str_where  .= " AND  (DO_NO   LIKE '%".$do_no."%') ";
        }
        if ($start_date !="")
        {
            //if ($str_where !="1" ) $str_where .= " AND ";
            $str_where .= " AND (DO_DATE BETWEEN '".printDate($start_date)."' AND '".printDate($end_date)."') ";
        }

        $arr_ret = array();
        
        $sql_sel = " SELECT dom.DO_ID, dom.DO_NO, DATE_FORMAT(dom.DO_DATE, '%d-%m-%Y') AS DO_DATE,
                            dom.PO_ID, dom.DO_TYPE_ID, dom.CREDIT_TERM, dom.NOTE, dom.CUS_ID, dom.CUS_NAME, dom.CUS_CONTACT, dom.CUS_ADDRESS,
                            dom.DISTRICT_CODE, dom.PROVINCE_CODE, dom.CUS_ZIP, dom.CUS_TEL, dom.CUS_FAX, dom.TAX_NUMBER, dom.DO_TOTAL, dom.DISCOUNT_PER, 
                            dom.VAT_PER, dom.GRAND_TOTAL, pm.PO_NO
                        FROM do_main AS dom 
                            LEFT JOIN po_main AS pm ON dom.PO_ID = pm.PO_ID 
                        WHERE ".$str_where." ORDER BY dom.DO_NO DESC; ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>do_main error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();
        
        
        return json_encode($arr_ret);
    }
    
    public function get_ddl_customer()
    {
        $arr_ret = array();

        $sql_sel = "SELECT CUS_ID, CUS_NAME 
                        FROM mst_customer
                        WHERE ACTIVE_FLAG = 'Y'
                        ORDER BY CUS_NAME ;  ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }
    
    public function get_ddl_po($cus_id="")
    {
        
        $arr_ret = array();
        $where = "";
        if ($cus_id != "") 
        {
            $where = " AND pm.CUS_ID =".$cus_id;
        }
            
        //*** Select กรณี PO ที่ยังไม่ถูกออก DO หมดแล้วเท่านั้น
        $sql_sel = "SELECT po_sum.PO_ID, po_sum.PO_NO, po_sum.PO_QTY, IFNULL(do_sum.DO_QTY,0) AS DO_QTY
                        FROM
                            (SELECT pm.PO_ID, pm.PO_NO, SUM(pd.QTY) AS PO_QTY
                            FROM po_main AS pm
                                INNER JOIN po_dtl AS pd ON pm.PO_ID = pd.PO_ID
                            WHERE pm.ACTIVE_FLAG = 'Y' AND pm.OBJ_ID = 2 
                            GROUP BY pm.PO_ID, pm.PO_NO) po_sum
                            LEFT JOIN 
                                (SELECT do.PO_ID, SUM(dd.QTY) AS DO_QTY
                                FROM do_main AS do
                                    INNER JOIN do_dtl AS dd ON do.DO_ID = dd.DO_ID
                                WHERE do.ACTIVE_FLAG = 'Y' 
                                GROUP BY do.PO_ID ) do_sum ON
                                po_sum.PO_ID = do_sum.PO_ID 
                        WHERE po_sum.PO_QTY > IFNULL(do_sum.DO_QTY,0) ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }
    
    public function get_customer_dtl($cus_id)
    {
        $arr_ret = array();
        $where = "";
        if ($cus_id != "") 
        {
            $where = " AND CUS_ID =".$cus_id;
        }
        
        $sql_sel = "SELECT *
                        FROM mst_customer 
                        WHERE ACTIVE_FLAG = 'Y' ".$where;
                        
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }
    public function get_do_dtl_from_po_id($po_id)
    {
        $arr_ret = array();

        $sql_sel = "SELECT po_sum.PO_ID, po_sum.PO_NO, po_sum.CUS_ID,
                            po_sum.CUS_NAME    ,po_sum.CUS_CONTACT,po_sum.CUS_ADDRESS,po_sum.PROVINCE_CODE,po_sum.DISTRICT_CODE,po_sum.CUS_ZIP,po_sum.CUS_TEL,po_sum.CUS_FAX,po_sum.TAX_NUMBER,
                            po_sum.QNO, po_sum.PRODUCT_ID, po_sum.PRODUCT_SALE_CODE, po_sum.PRODUCT_NAME, po_sum.DESCRIPTION, 
                            po_sum.UNIT_PRICE, po_sum.PO_QTY - IFNULL(do_sum.DO_QTY,0) AS DO_QTY, 
                            po_sum.UNIT_PRICE * (po_sum.PO_QTY - IFNULL(do_sum.DO_QTY,0) ) AS AMOUNT,
                            po_sum.DISCOUNT_PER, po_sum.VAT_PER
                        FROM
                            (SELECT pm.PO_ID, pm.PO_NO, pm.CUS_ID
                                ,pm.CUS_NAME    ,pm.CUS_CONTACT,pm.CUS_ADDRESS,pm.PROVINCE_CODE,pm.DISTRICT_CODE,pm.CUS_ZIP,pm.CUS_TEL,pm.CUS_FAX,pm.TAX_NUMBER
                                ,pd.QNO, pd.PRODUCT_ID, pd.PRODUCT_SALE_CODE, pd.PRODUCT_NAME,pd.DESCRIPTION, 
                                pd.UNIT_PRICE, SUM(pd.QTY) AS PO_QTY, pm.DISCOUNT_PER, pm.VAT_PER
                            FROM po_main AS pm
                                INNER JOIN po_dtl AS pd ON pm.PO_ID = pd.PO_ID
                            WHERE pm.ACTIVE_FLAG = 'Y' AND pm.OBJ_ID = 2 AND pm.PO_ID = ".$po_id."
                            GROUP BY pm.PO_ID, pm.PO_NO, pm.CUS_ID,
                                pm.CUS_NAME    ,pm.CUS_CONTACT,pm.CUS_ADDRESS,pm.PROVINCE_CODE,pm.DISTRICT_CODE,pm.CUS_ZIP,pm.CUS_TEL,pm.CUS_FAX,pm.TAX_NUMBER,
                                pd.QNO, pd.PRODUCT_ID, pd.PRODUCT_SALE_CODE, pd.PRODUCT_NAME, pd.DESCRIPTION, pd.UNIT_PRICE, pm.DISCOUNT_PER, pm.VAT_PER
                            ) po_sum
                            LEFT JOIN 
                                (SELECT do.PO_ID, dd.PRODUCT_ID, dd.PRODUCT_SALE_CODE, SUM(dd.QTY) AS DO_QTY
                                FROM do_main AS do
                                    INNER JOIN do_dtl AS dd ON do.DO_ID = dd.DO_ID
                                WHERE do.ACTIVE_FLAG = 'Y'  AND do.PO_ID = ".$po_id."
                                GROUP BY do.PO_ID, dd.PRODUCT_ID, dd.PRODUCT_SALE_CODE ) do_sum ON
                                po_sum.PO_ID = do_sum.PO_ID and po_sum.PRODUCT_SALE_CODE = do_sum.PRODUCT_SALE_CODE
                        ORDER BY QNO;  ";
                        
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
        
    }
    
    
    private function get_max_do_no()
    {
        
        $arr_ret = array();

        $sql_sel = "SELECT CONCAT('DO',(DATE_FORMAT(DATE_ADD(Now(),INTERVAL 543 YEAR),'%y%m%d')),'-',
                                                    LPAD((SUBSTR(IFNULL(MAX(DO_NO),'DOYYMMDD-000') ,10,3)+1),3,'0') ) AS NEW_DO_NO
                        FROM do_main 
                        WHERE DO_NO LIKE CONCAT('DO',DATE_FORMAT(DATE_ADD(Now(),INTERVAL 543 YEAR),'%y%m%d'),'%'); ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret[0]["NEW_DO_NO"];

    }
    
    public function get_do_id($do_no)
    {
        $arr_ret = array();

        $sql_sel = "SELECT DO_ID
                        FROM do_main 
                        WHERE DO_NO = '".$do_no."'; ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret[0]["DO_ID"];
        
    }
    
    public function add_do_dtl($posting)
    {
        
        $sql_add  = " INSERT INTO do_dtl SET   ";
        $sql_add .= "      DO_ID = ".$posting["p_do_id"];
        $sql_add .= "      ,QNO = ".$posting["p_qno"];
        $sql_add .= "      ,PRODUCT_ID = ".$posting["p_pro_id"]."  ";
        $sql_add .= "      ,PRODUCT_SALE_CODE = '".$posting["p_pro_sale_code"]."'  ";
        $sql_add .= "      ,PRODUCT_NAME = '".$posting["p_pro_name"]."'  ";
        $sql_add .= "      ,DESCRIPTION = '".$posting["p_pro_desc"]."'  ";
        $sql_add .= "      ,UNIT_PRICE = ".$posting["p_unit_price"];
        $sql_add .= "      ,QTY = ".$posting["p_do_qty"];
        $sql_add .= "      ,AMOUNT = ".$posting["p_amount"];
        $sql_add .= "      ,CREATED_BY = '".$_SESSION["aut_uname"]."'  ";
        $sql_add .= "      ,CREATED_DATE = Now()  ";
        $sql_add .= "      ,UPDATE_DATE = Now() ";


        $this->mysqli->query($sql_add) OR die("Function add_do_dtl error:{$sql_add}");
        return "";
        
    }
    
    public function add_do_main($posting)
    {
        
        $new_do_no = $this ->get_max_do_no();
        $sql_add  = " INSERT INTO do_main SET   ";
        $sql_add .= "   DO_NO  = '".$new_do_no."' ,";
        $sql_add .= "   DO_DATE = '".printDate($posting["p_do_date"])."' ,";
        $sql_add .= "   PO_ID = ".$posting["p_po_id"]." ,";
        $sql_add .= "   DO_TYPE_ID = ".$posting["p_do_type_id"]." ,";
        $sql_add .= "   CREDIT_TERM = ".$posting["p_credit_term"]." ,";
        $sql_add .= "   NOTE = '".$posting["p_note"]."' ,";
        $sql_add .= "   CUS_ID = ".$posting["p_cus_id"]." ,";
        $sql_add .= "   CUS_NAME = '".$posting["p_cus_name"]."' ,";
        $sql_add .= "   CUS_CONTACT = '".$posting["p_cus_contact"]."' ,";
        $sql_add .= "   CUS_ADDRESS = '".$posting["p_cus_address"]."' ,";
        $sql_add .= "   DISTRICT_CODE = ".$posting["p_district_code"].", ";
        $sql_add .= "   PROVINCE_CODE = ".$posting["p_province_code"].", ";
        $sql_add .= "   CUS_ZIP = '".$posting["p_cus_zip"]."', ";
        $sql_add .= "   CUS_TEL = '".$posting["p_cus_tel"]."', ";
        $sql_add .= "   CUS_FAX = '".$posting["p_cus_fax"]."', ";
        $sql_add .= "   TAX_NUMBER = '".$posting["p_tex_number"]."', ";
        $sql_add .= "   DO_TOTAL =".$posting["p_do_total"].", ";
        $sql_add .= "   DISCOUNT_PER =".$posting["p_discount_per"].", ";
        $sql_add .= "   VAT_PER =".$posting["p_vat_per"].", ";
        $sql_add .= "   GRAND_TOTAL =".$posting["p_grand_total"].",";
        $sql_add .= "   CREATED_BY ='".$_SESSION["aut_uname"]."', ";
        $sql_add .= "   CREATED_DATE =Now(),";
        $sql_add .= "   UPDATE_DATE = Now() ";
        
        
        //echo " SQL = ".$sql_add;
        $this->mysqli->query($sql_add) OR die("<div style='display:none;'>add_do_main error:{$sql_add}</div>");
        
        return $new_do_no;
    }
    public function get_do_dtl($do_id)
    {
        $arr_ret = array();

        $sql_sel = "SELECT DO_DTL_ID, DO_ID, QNO, PRODUCT_ID, PRODUCT_SALE_CODE, PRODUCT_NAME, DESCRIPTION, 
                            UNIT_PRICE, QTY, AMOUNT
                        FROM do_dtl
                        WHERE DO_ID = ".$do_id."; ";
                        
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='color:red;'>SQL error : {$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
        
    }
    public function edit_do_main($posting)
    {
        
        $sql_add  = " UPDATE do_main SET   ";
        $sql_add .= "   DO_DATE = '".printDate($posting["p_do_date"])."', ";
        $sql_add .= "   DO_TYPE_ID = ".$posting["p_do_type_id"].", ";
        $sql_add .= "   CREDIT_TERM = ".$posting["p_credit_term"].", ";
        $sql_add .= "   NOTE = '".$posting["p_note"]."', ";
        $sql_add .= "   CUS_NAME = '".$posting["p_cus_name"]."', ";
        $sql_add .= "   CUS_CONTACT = '".$posting["p_cus_contact"]."', ";
        $sql_add .= "   CUS_ADDRESS = '".$posting["p_cus_address"]."' ,";
        $sql_add .= "   DISTRICT_CODE = ".$posting["p_district_code"].", ";
        $sql_add .= "   PROVINCE_CODE = ".$posting["p_province_code"].", ";
        $sql_add .= "   CUS_ZIP = '".$posting["p_cus_zip"]."', ";
        $sql_add .= "   CUS_TEL = '".$posting["p_cus_tel"]."', ";
        $sql_add .= "   CUS_FAX = '".$posting["p_cus_fax"]."', ";
        $sql_add .= "   TAX_NUMBER = '".$posting["p_tex_number"]."', ";
        $sql_add .= "   DO_TOTAL =".$posting["p_do_total"].", ";
        $sql_add .= "   DISCOUNT_PER =".$posting["p_discount_per"].", ";
        $sql_add .= "   VAT_PER =".$posting["p_vat_per"].", ";
        $sql_add .= "   GRAND_TOTAL =".$posting["p_grand_total"].", ";
        $sql_add .= "   UPDATE_BY ='".$_SESSION["aut_uname"]."', ";
        $sql_add .= "   UPDATE_DATE = Now() ";
        $sql_add .= "WHERE DO_ID = ".$posting["p_do_id"]." ;";
        //echo " SQL = ".$sql_add;
        $this->mysqli->query($sql_add) OR die("<div style='display:none;'>edit_do_main error:{$sql_add}</div>");
        
        return $posting["p_do_no"];
    }
    public function edit_do_dtl($posting)
    {
        
        $sql_add  = " UPDATE do_dtl SET   ";
        $sql_add .= "      QNO = ".$posting["p_qno"];
        $sql_add .= "      ,QTY = ".$posting["p_do_qty"];
        $sql_add .= "      ,AMOUNT = ".$posting["p_amount"];
        $sql_add .= "      ,UPDATE_BY = '".$_SESSION["aut_uname"]."'  ";
        $sql_add .= "      ,UPDATE_DATE = Now() ";
        $sql_add .= " WHERE DO_DTL_ID = ".$posting["p_do_dtl_id"]." ;";
        //echo " SQL = ".$sql_add;
        $this->mysqli->query($sql_add) OR die("<div style='display:none;'>edit_do_main error:{$sql_add}</div>");
        
        return $posting["p_do_no"];
    }
    public function del_do_dtl($posting)
    {
        
        $sql_add  = " DELETE FROM do_dtl   ";
        $sql_add .= " WHERE DO_DTL_ID = ".$posting["p_do_dtl_id"]." ;";
        
        $this->mysqli->query($sql_add) OR die("del_do_dtl error:{$sql_add}");
        return "";
    }
    public function del_do_main($posting)
    {
        //$this->mysqli->autocommit(false);
        
        $sql_add  = " DELETE FROM do_dtl   ";
        $sql_add .= " WHERE DO_ID = ".$posting["p_do_id"]." ;";
        $this->mysqli->query($sql_add) OR die("del_do_main error:{$sql_add}");
        
        $sql_add = " DELETE FROM do_main   ";
        $sql_add .= " WHERE DO_ID = ".$posting["p_do_id"]." ;";
        $this->mysqli->query($sql_add) OR die("del_do_main error:{$sql_add}");
        
        //if (!$mysqli->commit()) 
        //{
            //return ("Transaction commit failed\n");
            //exit();
        //}
        //$this->mysqli->autocommit(true);
        
        return "";
    }
}
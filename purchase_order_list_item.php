<?php 

require_once('class_amh_po.php');

$po_num   = $_REQUEST["po_num"];
$str_date = $_REQUEST["str_date"];
$end_date = $_REQUEST["end_date"];

if ($str_date == '') { $str_date = date('d-m-Y', strtotime("-7 days")); }

$amh_po   = new AMH_PO();

$str_date = $amh_po->convert_date($str_date);
$end_date = $amh_po->convert_date($end_date);
$order_by = "CREATED_DATE DESC, ";
$asc_desc = "PO_NO DESC";

$arr_po   = $amh_po->po_main_like($po_num, $str_date, $end_date, $order_by, $asc_desc);

$str_body  = "";

foreach ($arr_po as $po_main)
{
    $created_date = Date('j/m/Y',strtotime($po_main["CREATED_DATE"]));

        if ($po_main["OBJ_ID"] == '1')
        { $po_type = 'For Invoice'; }
    elseif ($po_main["OBJ_ID"] == '2')
        { $po_type = 'For DO'; }
    else
        { $po_type = 'Unknown'; }

    $exist_in_do = $amh_po->exist_in_do($po_main["PO_ID"]);

    if ($exist_in_do)
    {
        $str_del_btn = "DO";
    }
    else
    {
        $str_del_btn = "
        <ul class='list-inline table-buttons'>
        <li><button type='button' class='btn-u btn-u-sm btn-u-red' onclick='delete_po(\"{$po_main["PO_ID"]}\",\"{$po_main["PO_NO"]}\");'><i class='fa fa-trash-o'></i> Delete</button></li>
        </ul>";
    }

    $str_body .= "
    <tr>
        <td>
        <ul class='list-inline table-buttons'>
        <li><button type='button' data-target='#sales_transaction_dtl_form' data-toggle='modal' class='btn-u btn-u-sm btn-u-blue' onclick='edit_po(\"{$po_main["PO_ID"]}\");'><i class='fa fa-edit'></i> Edit</button></li>
        </ul>
        </td>
        <td>".$created_date         ."</td>
        <td>".$po_main["PO_NO"]     ."</td>
        <td>".$po_main["CUS_NAME"]  ."</td>
        <td>".$po_main["GRAD_TOTAL"]."</td>
        <td>{$po_type}</td>
        <td>{$str_del_btn}</td>
    </tr>
    ";
}
?>
<div class='table-responsive'>
    <input type='hidden' value='' id='hdDelID' name='hdDelID'>
    <table class='table table-bordered table-striped'>
        <thead>
            <tr>
                <th width='42px'> &nbsp; </th>
                <th width='100px'>Create Date</th>
                <th width='120px'>PO NO</th>
                <th>Customer Name</th>
                <th width='120px'>Total Amount</th>
                <th width='80px'>Type</th>
                <th width='42px'> &nbsp; </th>
            </tr>
        </thead>
        <tbody>
        <?=$str_body;?>
        </tbody>
    </table>
</div>
﻿<?php 
	
	require("include_function.php");
	require("connect.php");
	require('validatelogin.php'); 
	//echo "session =".$_SESSION['aut_uname'];
 ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMESEL HEALTH SELECT | SALES AMOUNT & STOCK INDIVIDUAL REPORT</title>
	<?php $current_menu = "report"; ?>
	<?php require("include_headtag.php"); ?>
    
</head> 

<body>    

<div id="wrap"  class="wrapper">

	<?php require("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">SALES AMOUNT & STOCK INDIVIDUAL REPORT</h1>
			
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
	<script type="text/javascript">
    <!--
	function confirmDel(DelID)
	{
		if (confirm("Are you sure you want to delete this item."))
		{
			document.getElementById('hdDelID').value = DelID;
			return true;
		}else{
			return false;
		}
	 
	}
	function validate_form(obj)
	{
		result = true;
		if (document.getElementById('ddlSalesCodeSearch').value == "")
		{
			document.getElementById('labVddlSalesCodeSearch').innerHTML = "Please Select Sales Code.";
			result = false;
		}else
		{
			document.getElementById('labVddlSalesCodeSearch').innerHTML = "";
			result = true;
		}
		if (document.getElementById('ddlProName').value == "")
		{
			document.getElementById('labVddlProName').innerHTML = "Please Select Product name.";
			result = false;
		}else
		{
			document.getElementById('labVddlProName').innerHTML = "";
			result = true;
		}
		
		if (document.getElementById('ddlMonth').value == "" || document.getElementById('ddlYear').value == "")
		{
			document.getElementById('labVddlMonthYear').innerHTML = "<label style='width:188px'> </label> Please Select Month and Year.";
			result = false;
		}else
		{
			document.getElementById('labVddlMonthYear').innerHTML = "";
			result = true;
		}
		
		return result;
		
	}
	 
	 //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
		<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		<?php 
			// **** Gen Sales Code
			$sql = "SELECT EMP_ID, EMP_CODE, CONCAT(EMP_CODE,' : ',FNAME_TH,' ',LNAME_TH,'(',NICK_NAME,')') AS SALES_NAME 
						FROM mst_employee WHERE POSITION_ID = 11 ORDER BY EMP_CODE; ";
			//$_GET['result_emp_code']=  mysql_query($sql);
			$result_emp_code = mysql_query($sql);
			
			// **** Gen PRODUCT MASTER
			$sql = "SELECT PRODUCT_ID, CONCAT (PRODUCT_CODE, ':', PRODUCT_NAME_TH ) AS PRODUCT_NAME FROM mst_product 
						ORDER BY PRODUCT_CODE;";
			//$_GET['result_pro_mst']=  mysql_query($sql);
			$result_pro_mst = mysql_query($sql);
			
			$sql = "SELECT YEAR(TRN_SALES_DATE)  as HAVE_YEAR
						FROM trn_sales_dtl 
						WHERE TRN_SALES_DATE IS NOT NULL
						GROUP BY YEAR(TRN_SALES_DATE) ; ";
			$result_year = mysql_query($sql);
			
			//*** Start Default ***//
			$default = true;
			
			//***** End Default *****//
			
			if (ISSET($_POST['btSubmit']) or $default)
			{
				if ($default && !ISSET($_POST['btSubmit']))
				{
					$_POST['ddlSalesCodeSearch'] = 2;
					$_POST['ddlProName'] = 3;
					$_POST['ddlMonth'] = date('n');
					$_POST['ddlYear'] = date('Y');
				}
				
				if ($_POST['ddlSalesCodeSearch'] != "")
				{
					$sales_id = $_POST['ddlSalesCodeSearch'];
				}
				if ($_POST['ddlProName'] != "")
				{
					$pro_id = $_POST['ddlProName'];
				}
				if ($_POST['ddlMonth'] != "")
				{
					$month_no = $_POST['ddlMonth'];
				}
				if ($_POST['ddlYear'] != "")
				{
					$year_no = $_POST['ddlYear'];
				}
				
			}
			
			
			// **** Get Sales Amount Chart Data
			$set_variable = mysql_query("SET @month_no := ".$month_no." , @year_no := ".$year_no.", @pro_id := ".$pro_id.", @sales_id := ".$sales_id.";");
			
			$sql = " SELECT '0' AS SEQ_WEEK, SUM(tsd.SALES_IN * tlc.LTP) - SUM(tsd.SALES_OUT * tlc.LTP) AS QUOTED_AMOUNT, tsd.PRODUCT_ID, tsd.SALES_EMP_ID,
						  SUM(CASE WHEN DATE_FORMAT(tsd.TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')-1 THEN tsd.SALES_OUT * tlc.LTP ELSE 0 END) AS W_SALES_OUT_AMOUNT
						FROM trn_sales_dtl  AS tsd
						  LEFT JOIN trn_lpt_chart AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE AND tsd.PRODUCT_ID = tlc.PRODUCT_ID
						WHERE tsd.TRN_SALES_DATE < CONCAT(@year_no,'-',@month_no+1,'-1')
							AND DATE_FORMAT(tsd.TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') 
							AND tsd.PRODUCT_ID = @pro_id
							AND tsd.SALES_EMP_ID = @sales_id
						GROUP BY tsd.PRODUCT_ID, tsd.SALES_EMP_ID
							  UNION
						SELECT '1' AS SEQ_WEEK,  SUM(SALES_IN* tlc.LTP ) - SUM(SALES_OUT* tlc.LTP ) AS QUOTED_AMOUNT, tsd.PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') THEN SALES_OUT * tlc.LTP ELSE 0 END) AS W_SALES_OUT_AMOUNT
						FROM trn_sales_dtl AS tsd
						  LEFT JOIN trn_lpt_chart AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE AND tsd.PRODUCT_ID = tlc.PRODUCT_ID
						WHERE tsd.TRN_SALES_DATE < CONCAT(@year_no,'-',@month_no+1,'-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +1
							AND tsd.PRODUCT_ID = @pro_id
							AND tsd.SALES_EMP_ID = @sales_id
						GROUP BY tsd.PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT '2' AS SEQ_WEEK , SUM(SALES_IN* tlc.LTP ) - SUM(SALES_OUT* tlc.LTP ) AS QUOTED_AMOUNT, tsd.PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+1  THEN SALES_OUT * tlc.LTP ELSE 0 END) AS W_SALES_OUT_AMOUNT
						FROM trn_sales_dtl  AS tsd
						  LEFT JOIN trn_lpt_chart AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE AND tsd.PRODUCT_ID = tlc.PRODUCT_ID
						WHERE tsd.TRN_SALES_DATE < CONCAT(@year_no,'-',@month_no+1,'-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +2
							AND tsd.PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY tsd.PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT  '3' AS SEQ_WEEK , SUM(SALES_IN* tlc.LTP ) - SUM(SALES_OUT* tlc.LTP ) AS QUOTED_AMOUNT, tsd.PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+2  THEN SALES_OUT * tlc.LTP ELSE 0 END) AS W_SALES_OUT_AMOUNT
						FROM trn_sales_dtl  AS tsd
						  LEFT JOIN trn_lpt_chart AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE AND tsd.PRODUCT_ID = tlc.PRODUCT_ID
						WHERE tsd.TRN_SALES_DATE < CONCAT(@year_no,'-',@month_no+1,'-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +3
							AND tsd.PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY tsd.PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT  '4' AS SEQ_WEEK , SUM(SALES_IN* tlc.LTP ) - SUM(SALES_OUT* tlc.LTP ) AS QUOTED_AMOUNT, tsd.PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+3  THEN SALES_OUT * tlc.LTP ELSE 0 END) AS W_SALES_OUT_AMOUNT
						FROM trn_sales_dtl  AS tsd
						  LEFT JOIN trn_lpt_chart AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE AND tsd.PRODUCT_ID = tlc.PRODUCT_ID
						WHERE tsd.TRN_SALES_DATE < CONCAT(@year_no,'-',@month_no+1,'-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +4
							AND tsd.PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY tsd.PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT  '5' AS SEQ_WEEK , SUM(SALES_IN* tlc.LTP ) - SUM(SALES_OUT* tlc.LTP ) AS QUOTED_AMOUNT, tsd.PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+4  THEN SALES_OUT * tlc.LTP ELSE 0 END) AS W_SALES_OUT_AMOUNT
						FROM trn_sales_dtl  AS tsd
						  LEFT JOIN trn_lpt_chart AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE AND tsd.PRODUCT_ID = tlc.PRODUCT_ID
						WHERE tsd.TRN_SALES_DATE < CONCAT(@year_no,'-',@month_no+1,'-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +5
						   AND TRN_SALES_DATE <= LAST_DAY(CONCAT(@year_no,'-',@month_no,'-1'))
						   AND DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +4  <=  DATE_FORMAT(LAST_DAY(CONCAT(@year_no,'-',@month_no,'-1')),'%U')
							AND tsd.PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY tsd.PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT  '6' AS SEQ_WEEK , SUM(SALES_IN* tlc.LTP ) - SUM(SALES_OUT* tlc.LTP ) AS QUOTED_AMOUNT, tsd.PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+5  THEN SALES_OUT * tlc.LTP ELSE 0 END) AS W_SALES_OUT_AMOUNT
						FROM trn_sales_dtl  AS tsd
						  LEFT JOIN trn_lpt_chart AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE AND tsd.PRODUCT_ID = tlc.PRODUCT_ID
						WHERE tsd.TRN_SALES_DATE < CONCAT(@year_no,'-',@month_no+1,'-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +6 
							AND DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +5  <=  DATE_FORMAT(LAST_DAY(CONCAT(@year_no,'-',@month_no,'-1')),'%U')
							AND tsd.PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY tsd.PRODUCT_ID, SALES_EMP_ID      ";
			
			$where = " ";
			$order = " ORDER BY SEQ_WEEK; ";
			
			//$sql = $sql.$where.$order;
			$result_amount_chart = mysql_query($sql);
			
			// **** Get Stock Chart Data
			//$set_variable = mysql_query("SET @month_no := 6 , @year_no := 2015, @pro_id := 3, @sales_id := 2;");
			$sql = " SELECT '0' AS SEQ_WEEK, SUM(SALES_IN) - SUM(SALES_OUT) AS QUOTED, PRODUCT_ID, SALES_EMP_ID,
						  SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')-1 THEN SALES_OUT ELSE 0 END) AS W_SALES_OUT
						FROM trn_sales_dtl  
						WHERE  TRN_SALES_DATE < CONCAT(@year_no,'-', @month_no + 1, '-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') 
							AND PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY PRODUCT_ID, SALES_EMP_ID
							  UNION
						SELECT '1' AS SEQ_WEEK,  SUM(SALES_IN) - SUM(SALES_OUT) AS QUOTED, PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') THEN SALES_OUT ELSE 0 END) AS W_SALES_OUT
						FROM trn_sales_dtl  
						WHERE TRN_SALES_DATE < CONCAT(@year_no,'-', @month_no + 1, '-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +1
							AND PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT '2' AS SEQ_WEEK , SUM(SALES_IN) - SUM(SALES_OUT) AS QUOTED, PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+1 THEN SALES_OUT ELSE 0 END) AS W_SALES_OUT
						FROM trn_sales_dtl  
						WHERE TRN_SALES_DATE < CONCAT(@year_no,'-', @month_no + 1, '-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +2
							AND PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT  '3' AS SEQ_WEEK , SUM(SALES_IN) - SUM(SALES_OUT) AS QUOTED, PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+2 THEN SALES_OUT ELSE 0 END) AS W_SALES_OUT
						FROM trn_sales_dtl  
						WHERE TRN_SALES_DATE < CONCAT(@year_no,'-', @month_no + 1, '-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +3
							AND PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT  '4' AS SEQ_WEEK , SUM(SALES_IN) - SUM(SALES_OUT) AS QUOTED, PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+3 THEN SALES_OUT ELSE 0 END) AS W_SALES_OUT
						FROM trn_sales_dtl  
						WHERE TRN_SALES_DATE < CONCAT(@year_no,'-', @month_no + 1, '-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +4
							AND PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT  '5' AS SEQ_WEEK , SUM(SALES_IN) - SUM(SALES_OUT) AS QUOTED, PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+4 THEN SALES_OUT ELSE 0 END) AS W_SALES_OUT
						FROM trn_sales_dtl  
						WHERE TRN_SALES_DATE < CONCAT(@year_no,'-', @month_no + 1, '-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +5
						   AND TRN_SALES_DATE <= LAST_DAY(CONCAT(@year_no,'-',@month_no,'-1'))
						   AND DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +4  <=  DATE_FORMAT(LAST_DAY(CONCAT(@year_no,'-',@month_no,'-1')),'%U')
							AND PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY PRODUCT_ID, SALES_EMP_ID  
							  UNION
						SELECT  '6' AS SEQ_WEEK , SUM(SALES_IN) - SUM(SALES_OUT) AS QUOTED, PRODUCT_ID, SALES_EMP_ID,
							SUM(CASE WHEN DATE_FORMAT(TRN_SALES_DATE,'%U') >= DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U')+5 THEN SALES_OUT ELSE 0 END) AS W_SALES_OUT
						FROM trn_sales_dtl  
						WHERE TRN_SALES_DATE < CONCAT(@year_no,'-', @month_no + 1, '-1')
							AND DATE_FORMAT(TRN_SALES_DATE,'%U') < DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +6 
							AND DATE_FORMAT(CONCAT(@year_no,'-',@month_no,'-1'),'%U') +5  <=  DATE_FORMAT(LAST_DAY(CONCAT(@year_no,'-',@month_no,'-1')),'%U')
							AND PRODUCT_ID = @pro_id
							AND SALES_EMP_ID = @sales_id
						GROUP BY PRODUCT_ID, SALES_EMP_ID    ";
			
			$where = " ";
			$order = " ORDER BY SEQ_WEEK; ";
			
			
			$result_chart_data = mysql_query($sql);
			
			$sql = "SELECT CONCAT(me.EMP_CODE,'(', me.NICK_NAME, ')') AS SALES_CODE, tlc.PRODUCT_SALE_CODE,
							SUM(tsd.SALES_IN) - SUM(tsd.SALES_OUT) AS QTY, tlc.LTP, (SUM(tsd.SALES_IN) - SUM(tsd.SALES_OUT)) * tlc.LTP AS AMOUNT , ms.SHOP_NAME
						FROM trn_sales_dtl AS tsd
							LEFT JOIN trn_lpt_chart AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE AND tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE
							LEFT JOIN mst_employee AS me ON tsd.SALES_EMP_ID = me.EMP_ID
							LEFT JOIN mst_shop AS ms ON tsd.SHOP_ID = ms.SHOP_ID
						WHERE tsd.TRN_SALES_DATE < CONCAT(@year_no,'-', @month_no + 1, '-1') 
							AND tsd.PRODUCT_ID = @pro_id 
							AND tsd.SALES_EMP_ID = @sales_id
						GROUP BY me.EMP_CODE, me.NICK_NAME,tlc.PRODUCT_SALE_CODE, tlc.LTP, ms.SHOP_NAME; ";
			$result_sales_dtl = mysql_query($sql);
			
			
			
			?>
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="frm_rep_amount_stock" 
								id="frm_rep_amount_stock" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Sales Code <font color="#ff0000">*</font></label>
									
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
												<select name="ddlSalesCodeSearch" id="ddlSalesCodeSearch"  class=" form-control selectpicker"  
												data-live-search="true">
														<option value="" selected disabled> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_emp_code)) 
													{
														echo "<option value='".$row['EMP_ID']."' ";
														
														if ($row['EMP_ID'] == $_POST['ddlSalesCodeSearch'])
														{
															echo " selected ";
														}
														echo ">".$row['SALES_NAME']."</option>";
													}
													
													mysql_data_seek ($result_emp_code , 0 );
													?>
												</select >
												<label class ="error" id="labVddlSalesCodeSearch"></label>
										</label>
										
									</div>
									
								</div>

								<div class="form-group">
									<label for="inputProductCode" class="col-lg-4 control-label">Product Sale Code <font color="#ff0000">*</font></label>
									<div class="col-lg-8">
										<label class="select">
											<select name="ddlProName" id="ddlProName" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
												<option value="" selected disabled> - Please Select - </option>
												<?php 
												while($row=mysql_fetch_array($result_pro_mst)) 
												{
													echo "<option value='".$row['PRODUCT_ID']."' ";
													if ($row['PRODUCT_ID'] == $_POST['ddlProName']  )
													{
														echo " selected ";
													}
													echo">".$row['PRODUCT_NAME']."</option>";
												}
												mysql_data_seek ($result_pro_mst , 0 );
												?>
											</select>
											<label class="error" id="labVddlProName"></label>
										</label>
									</div>
									
								</div>
								
								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Month <font color="#ff0000">*</font> / Year <font color="#ff0000">*</font></label>
									
									<div class="col-lg-4">
										<div class="select">
												<select  name="ddlMonth" id="ddlMonth" class="selectpicker form-control" >
														<option value="" <?php if (!ISSET($_POST['ddlMonth'])) echo 'selected';  ?> disabled> - Month - </option>
														<?php
														for ($i = 1; $i <= 12; $i++)
														{
															echo '<option value="'.$i.'"';
															if ($i == $_POST['ddlMonth'])
															{
																	echo  ' selected';
															}
															/*** get the month ***/
															$mon = date("F", mktime(0, 0, 0, $i+1, 0, 0, 0));
															echo '>'.$mon.'</option>';
														}
														?>
														
												</select>
												
										</div>
									</div>
									<div class="col-lg-4" style="height: 35px">
										<label class="select" >
												<select  name="ddlYear" id="ddlYear" class="selectpicker form-control" 
												  title="Please select ...">
													<option value="" selected disabled> - Year - </option>
													<?php
														while($row=mysql_fetch_array($result_year)) 
														{
															echo "<option value='".$row['HAVE_YEAR']."' ";
															if ($row['HAVE_YEAR'] == $_POST['ddlYear'])
															{
																echo " selected ";
															}
															echo">".$row['HAVE_YEAR']."</option>";
														}
														mysql_data_seek ($result_year , 0 );
													?>
												</select>
												
										</label>
									</div>
									
									<label class="error" id="labVddlMonthYear"></label>
								</div>
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button name="btSubmit" id="btSubmit" type="submit" class="btn-u"> Search </button> &nbsp;&nbsp;
										<button type="button" class="btn-u btn-u-default">Clear</button>
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

	




    <!--=== End Search Block Version 2 ===-->

    <div class="container content-sm"  style="padding:30px;">


		
		<div class="table-search-v2 margin-bottom-30">
			<div class="table-responsive">
				<form method="POST" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" name="formSalesTranTable" onSubmit=""
					id="formSalesTranTable" >
					
					
					<div class="col-sm-12">
						<div class="box">
							<div class="box-header">
								<h2><i class="fa fa-btc"></i><span class="break"></span>Sales Amount Chart</h2>
								<div class="box-icon">
									<i class="fa fa-line-chart"></i> <a href="<?php echo $_SERVER['PHP_SELF']; ?>#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
								</div>
							</div>
							
							<div class="box-content">
								<div id="chtSaleAmount" class="center" style="height: 300px; padding: 0px; position: relative;">
									<canvas class="flot-base" width="1074" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1074px; height: 300px;">
									</canvas>
									<div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
										<div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 22px; text-align: center;">0</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 99px; text-align: center;">1</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 176px; text-align: center;">2</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 253px; text-align: center;">3</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 330px; text-align: center;">4</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 408px; text-align: center;">5</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 485px; text-align: center;">6</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 562px; text-align: center;">7</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 639px; text-align: center;">8</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 716px; text-align: center;">9</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 790px; text-align: center;">10</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 867px; text-align: center;">11</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 944px; text-align: center;">12</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 1021px; text-align: center;">13</div>
										</div>
										<div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 247px; left: 0px; text-align: right;">-1.0</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 191px; left: 0px; text-align: right;">-0.5</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 135px; left: 4px; text-align: right;">0.0</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 79px; left: 4px; text-align: right;">0.5</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 23px; left: 4px; text-align: right;">1.0</div>
										</div>
									</div>
									<canvas class="flot-overlay" width="1074" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1074px; height: 300px;">
									</canvas>
									<div class="legend">
										<div style="position: absolute; width: 53px; height: 34px; top: 14px; right: 13px; opacity: 0.85; background-color: rgb(255, 255, 255);"> 
										</div>
										<table style="position:absolute;top:14px;right:13px;;font-size:smaller;color:#545454">
											<tbody>
												<tr>
													<td class="legendColorBox">
														<div style="border:1px solid #ccc;padding:1px">
															<div style="width:4px;height:0;border:5px solid rgb(250,88,51);overflow:hidden"></div>
														</div>
													</td>
													<td class="legendLabel">sin(x)/x</td>
												</tr>
												<tr>
													<td class="legendColorBox">
														<div style="border:1px solid #ccc;padding:1px">
															<div style="width:4px;height:0;border:5px solid rgb(47,171,233);overflow:hidden"></div>
														</div>
													</td>
													<td class="legendLabel">cos(x)</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								
							</div>
							<br/><br/>
							<div class="box-header">
								<h2><i class="fa fa-cubes"></i><span class="break"></span>Sales Stock Chart</h2>
								<div class="box-icon">
									<i class="fa fa-line-chart"></i> 
									<a href="http://bootstrapmaster.com/live/simpliq2/charts-flot.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content">
								<div id="chtStock" class="center" style="height: 300px; padding: 0px; position: relative;">
									<canvas class="flot-base" width="1074" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1074px; height: 300px;">
									</canvas>
									<div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
										<div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 22px; text-align: center;">0</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 99px; text-align: center;">1</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 176px; text-align: center;">2</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 253px; text-align: center;">3</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 330px; text-align: center;">4</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 408px; text-align: center;">5</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 485px; text-align: center;">6</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 562px; text-align: center;">7</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 639px; text-align: center;">8</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 716px; text-align: center;">9</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 790px; text-align: center;">10</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 867px; text-align: center;">11</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 944px; text-align: center;">12</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; max-width: 71px; top: 283px; left: 1021px; text-align: center;">13</div>
										</div>
										<div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 247px; left: 0px; text-align: right;">-1.0</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 191px; left: 0px; text-align: right;">-0.5</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 135px; left: 4px; text-align: right;">0.0</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 79px; left: 4px; text-align: right;">0.5</div>
											<div class="flot-tick-label tickLabel" style="position: absolute; top: 23px; left: 4px; text-align: right;">1.0</div>
										</div>
									</div>
									<canvas class="flot-overlay" width="1074" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1074px; height: 300px;">
									</canvas>
									<div class="legend">
										<div style="position: absolute; width: 53px; height: 34px; top: 14px; right: 13px; opacity: 0.85; background-color: rgb(255, 255, 255);"> 
										</div>
										<table style="position:absolute;top:14px;right:13px;;font-size:smaller;color:#545454">
											<tbody>
												<tr>
													<td class="legendColorBox">
														<div style="border:1px solid #ccc;padding:1px">
															<div style="width:4px;height:0;border:5px solid rgb(250,88,51);overflow:hidden"></div>
														</div>
													</td>
													<td class="legendLabel">sin(x)/x</td>
												</tr>
												<tr>
													<td class="legendColorBox">
														<div style="border:1px solid #ccc;padding:1px">
															<div style="width:4px;height:0;border:5px solid rgb(47,171,233);overflow:hidden"></div>
														</div>
													</td>
													<td class="legendLabel">cos(x)</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<p id="hoverdata2">Mouse position at (<span id="x2">17.04</span>, <span id="y2">8475.84</span>). <span id="clickdata2"></span></p>
							</div>
							<br/>
							<!-- Sales Detail Table -->
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										
										<th>Sales Code / Sales Name</th>
										<th class="hidden-sm">Product Sales Code</th>
										<th>QTY.</th>
										<th>LTP.</th>
										<th>Amount Baht</th>
										<th>Shop name</th>
									
										
									</tr>
								</thead>
								<tbody>
									<?php 
										while($row=mysql_fetch_array($result_sales_dtl)) 
										{
											
											echo '<tr>';
											
											echo '<td> <p>'.$row["SALES_CODE"].'</p> </td>';
											echo '<td width="120px"> <p>'.$row["PRODUCT_SALE_CODE"].'</p> </td>';
											echo '<td width="100px"> <p>'.number_format($row["QTY"], 2).'</p> </td>';
											echo '<td width="100px"> <p>'.number_format($row["LTP"], 2).'</p> </td>';
											echo '<td width="120px">'. number_format($row["AMOUNT"], 2).'</td>';
											echo '<td> <p>'.$row["SHOP_NAME"].'</p> </td>';
											
											echo '</tr>';
										}
									?>
								
								</tbody>
							</table>
							
							
							<!-- END Sales Detail Table -->
							
							
						</div><!--/col-->
				
					</div>
					
				</form>
			</div>    
		</div>    
        <!-- End Table Search v2 -->


        
    </div>
	<?php require("include_footer.php"); ?>
     
</div><!--/End Wrapepr-->



<!-- JS Global Compulsory -->           
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-select.min.js"></script>

<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/bootstrap-chart/jquery-migrate-1.2.1.min.js"></script>

<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>

<!-- JS Plugis Chart -->
	<script src="assets/plugins/bootstrap-chart/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="assets/plugins/bootstrap-chart/jquery.sparkline.min.js"></script>
	<script src="assets/plugins/bootstrap-chart/jquery.flot.min.js"></script>
	<script src="assets/plugins/bootstrap-chart/jquery.flot.pie.min.js"></script>
	<script src="assets/plugins/bootstrap-chart/jquery.flot.stack.min.js"></script>
	<script src="assets/plugins/bootstrap-chart/jquery.flot.resize.min.js"></script>
	<script src="assets/plugins/bootstrap-chart/jquery.flot.time.min.js"></script>
	<!-- theme scripts -->
	<script src="assets/plugins/bootstrap-chart/custom.min.js"></script>
	<script src="assets/plugins/bootstrap-chart/core.min.js"></script>
	<!-- inline scripts related to this page 
	<script src="assets/plugins/bootstrap-chart/charts-flot.js"></script>-->
	
	<!-- Gen Data pot chart -->
	<script type="text/javascript">
	 <!--
		/* ---------- My Chart with points ---------- */
		
		$(document).ready(function(){
					
			
			if($("#chtSaleAmount").length)
			{
				var val1 = [], val2 = [];
				<?php 
				$max_value = 100;
				while($row=mysql_fetch_array($result_amount_chart)) 
				{
					echo "val1.push([".$row["SEQ_WEEK"].", ".$row["W_SALES_OUT_AMOUNT"]."]);";
					echo "val2.push([".$row["SEQ_WEEK"].", ".$row["QUOTED_AMOUNT"]."]);";
					if ( $row["W_SALES_OUT_AMOUNT"] >= $max_value) $max_value = round($row["W_SALES_OUT_AMOUNT"],-2) + 100;
					//if ( $row["QUOTED_AMOUNT"] >= $max_value) $max_value = round($row["QUOTED_AMOUNT"],-2) + 100;
					
					if ( $row["QUOTED_AMOUNT"] >= $max_value) 
					{
						$n = strlen(strval($max_value)) - 1;
						$max_value = round($row["QUOTED_AMOUNT"],$n*-1) + pow(10,$n) ;
					}
					//Math.pow(10,(round($row["QUOTED_AMOUNT"],0).toString().length - 1)) ;
					
					//$max_value = 400000;
				}
				
				?>
				
				var plot = $.plot($("#chtSaleAmount"),
					   [ { data: val1, label: ": Amount Sales Out"}, { data: val2, label: ": Amount Stock" } ], {
						   series: {
							   lines: { show: true,
										lineWidth: 2,
									 },
							   points: { show: true },
							   shadowSize: 2
						   },
						   xaxis: {
								ticks: [0,[1,"Week 1"], [2,"Week 2"],[3,"Week 3"], [4,"Week 4"],[5,"Week 5"], [6,"Week 6"]]
							},
						   grid: { hoverable: true, 
								   clickable: true, 
								   tickColor: "#dddddd",
								   borderWidth: 0 
								 },
						   yaxis: { min: 0, max: <?php echo $max_value; ?> },
						   colors: ["#ff66ff", "#2FABE9"]
						 });

				function showTooltipMe(x, y, contents) {
					$('<div id="tooltip">' + contents + '</div>').css( {
						position: 'absolute',
						display: 'none',
						top: y + 5,
						left: x + 5,
						border: '1px solid #fdd',
						padding: '2px',
						'background-color': '#dfeffc',
						opacity: 0.80
					}).appendTo("body").fadeIn(200);
				}

				var previousPoint = null;
				$("#chtSaleAmount").bind("plothover", function (event, pos, item) {
					$("#x2").text(pos.x.toFixed(2));
					$("#y2").text(pos.y.toFixed(2));

						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;

								$("#tooltip").remove();
								var x = item.datapoint[0].toFixed(0),
									y = item.datapoint[1].toFixed(2);

								showTooltipMe(item.pageX, item.pageY,
											item.series.label + " of [Week" + x + "] = " + y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Baht.");
							}
						}
						else {
							$("#tooltip").remove();
							previousPoint = null;
						}
				});
				


				$("#chtSaleAmount").bind("plotclick", function (event, pos, item) {
					if (item) {
						$("#clickdata2").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
						plot.highlight(item.series, item.datapoint);
					}
				});
			}		

			
			if($("#chtStock").length)
			{
				var val1 = [], val2 = [];
				<?php 
				$max_value = 100;
				while($row=mysql_fetch_array($result_chart_data)) 
				{
					echo "val1.push([".$row["SEQ_WEEK"].", ".$row["W_SALES_OUT"]."]);";
					echo "val2.push([".$row["SEQ_WEEK"].", ".$row["QUOTED"]."]);";
					if ( $row["W_SALES_OUT"] >= $max_value) $max_value = round($row["W_SALES_OUT"],-2) + 100;
					
					if ( $row["QUOTED"] >= $max_value) 
					{
						$n = strlen(strval($max_value)) - 1;
						$max_value = round($row["QUOTED"],$n*-1) + pow(10,$n) ;
					}
					
					//$max_value = 1500;
				}
				
				?>
				
				var plot = $.plot($("#chtStock"),
					   [ { data: val1, label: ": Sales Out"}, { data: val2, label: ": Stock" } ], {
						   series: {
							   lines: { show: true,
										lineWidth: 2,
									 },
							   points: { show: true },
							   shadowSize: 2
						   },
						   xaxis: {
								ticks: [0,[1,"Week 1"], [2,"Week 2"],[3,"Week 3"], [4,"Week 4"],[5,"Week 5"], [6,"Week 6"]]
							},
						   grid: { hoverable: true, 
								   clickable: true, 
								   tickColor: "#dddddd",
								   borderWidth: 0 
								 },
						   yaxis: { min: 0, max: <?php echo $max_value; ?> },
						   colors: ["#ff9900", "#009933"]
						 });

				function showTooltipMe(x, y, contents) {
					$('<div id="tooltip">' + contents + '</div>').css( {
						position: 'absolute',
						display: 'none',
						top: y + 5,
						left: x + 5,
						border: '1px solid #fdd',
						padding: '2px',
						'background-color': '#dfeffc',
						opacity: 0.80
					}).appendTo("body").fadeIn(200);
				}

				var previousPoint = null;
				$("#chtStock").bind("plothover", function (event, pos, item) {
					$("#x2").text(pos.x.toFixed(2));
					$("#y2").text(pos.y.toFixed(2));

						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;

								$("#tooltip").remove();
								var x = item.datapoint[0].toFixed(0),
									y = item.datapoint[1].toFixed(2);

								showTooltipMe(item.pageX, item.pageY,
											item.series.label + " of [Week" + x + "] = " + y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Items.");
							}
						}
						else {
							$("#tooltip").remove();
							previousPoint = null;
						}
				});
				


				$("#chtStock").bind("plotclick", function (event, pos, item) {
					if (item) {
						$("#clickdata2").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
						plot.highlight(item.series, item.datapoint);
					}
				});
			}
		});
		
	 //-->
	 </script>
	
<!-- *** END Plugins Chart ***-->

<?php require("include_js.php"); ?>

</body>
</html> 
<?php
/* Class name
   AMH_District

   public function 
   mst_district($str_search) return array of district info, relate by PROVINCE_ID (example: )
*/
require_once('class_amh_db.php');

class AMH_District extends AMH_DB
{
    public function mst_district($str_search)
    {
        $arr_ret = array();

        $str_where = "1";
        if ($str_search != '')
        {
           $str_where  = " (PROVINCE_CODE LIKE '%".$str_search."%') ";
        }

        $sql_sel = " SELECT * FROM mst_district WHERE ".$str_where." ORDER BY DISTRICT_CODE ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>mst_district error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function create_district_option($district_code, $province_code)
    {
        $str_retn = "";
        $arr_dist = $this->mst_district($province_code);

        foreach ($arr_dist as $district)
        {
            $selected  = "";
            if ($district_code == $district["DISTRICT_CODE"]) { $selected  = " selected"; }
            $str_retn .= "<option value='".$district["DISTRICT_CODE"]."'".$selected.">".$district["DISTRICT_NAME"]."</option>";
        }

        return $str_retn;
    }
}
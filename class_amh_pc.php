<?php
/* Class name
   AMH_DB
   use for connect to amsel_healthdb
*/

class AMH_PC extends AMH_DB
{
    public function get_sale_rpt($user_name, $date_fr, $date_to, $cus_id = "")
    {
        $arr_ret = array();

        if ($cus_id != "")
        {
            $str_cus_id = " AND (SRM.CUS_ID = '".$cus_id."') ";
        }
        else
        {
            $str_cus_id = "";
        }

        $sql_sel  = " SELECT PRD.PRODUCT_NAME_TH, PRD.PRODUCT_NAME_EN, PRD.PRD_IMG, CUS.CUS_NAME, CST.CUS_TYPE_NAME, SRM.*, SRD.*, DATE(SRD.UPDATE_DATE) as UPD_DATE ";
        $sql_sel .= " FROM sale_report_main SRM  ";
        $sql_sel .= " LEFT JOIN sale_report_dtl SRD ON SRM.SALE_REPORT_ID = SRD.SALE_REPORT_ID  ";
        $sql_sel .= " LEFT JOIN mst_product_pc PPC ON SRD.PRODUCT_PC_ID   = PPC.PRODUCT_PC_ID   ";
        $sql_sel .= " LEFT JOIN customer_type CST ON CST.CUS_TYPE_ID      = PPC.CUS_TYPE_ID   ";
        $sql_sel .= " LEFT JOIN mst_product PRD ON PPC.PRODUCT_ID         = PRD.PRODUCT_ID      ";
        $sql_sel .= " LEFT JOIN mst_customer CUS ON CUS.CUS_ID            = SRM.CUS_ID      ";
        $sql_sel .= " WHERE (SRD.CREATED_BY = '{$user_name}') AND (SRM.SALE_REPORT_DATE BETWEEN '{$date_fr}' AND '{$date_to}') AND (SRD.ACTIVE_FLAG = 'Y') ";
        $sql_sel .= $str_cus_id." ORDER BY SRD.UPDATE_DATE DESC ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_sale_rpt error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_order_dtl($user_name, $date_fr, $date_to, $cus_id = "")
    {
        $arr_ret = array();

        if ($cus_id != "")
        {
            $str_cus_id = " AND (ODT.CUS_ID = '".$cus_id."') ";
        }
        else
        {
            $str_cus_id = "";
        }

        $sql_sel  = " SELECT PRD.PRODUCT_NAME_TH, PRD.PRODUCT_NAME_EN, PRD.PRD_IMG, POM.PO_NO, CUS.CUS_NAME, CST.CUS_TYPE_NAME, ODT.* FROM order_dtl ODT ";
        $sql_sel .= " LEFT JOIN mst_product_pc PPC ON ODT.PRODUCT_PC_ID = PPC.PRODUCT_PC_ID ";
        $sql_sel .= " LEFT JOIN customer_type CST ON CST.CUS_TYPE_ID    = PPC.CUS_TYPE_ID   ";
        $sql_sel .= " LEFT JOIN mst_product PRD ON PPC.PRODUCT_ID       = PRD.PRODUCT_ID    ";
        $sql_sel .= " LEFT JOIN po_main POM ON POM.PO_ID = ODT.PO_ID ";
        $sql_sel .= " LEFT JOIN mst_customer CUS ON CUS.CUS_ID = ODT.CUS_ID ";
        $sql_sel .= " WHERE (ODT.CREATED_BY = '{$user_name}') AND (ODT.UPDATE_DATE BETWEEN '{$date_fr} 00:00' AND '{$date_to} 23:59') AND (ODT.ACTIVE_FLAG = 'Y') ";
        $sql_sel .= $str_cus_id." ORDER BY ODT.CUS_ID, ODT.UPDATE_DATE DESC ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_order_dtl error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_order_dtl_all()
    {
        $arr_ret = array();

        $sql_sel  = " SELECT PRD.PRODUCT_NAME_TH, PRD.PRODUCT_NAME_EN, PRD.PRD_IMG, ODT.* FROM order_dtl ODT  ";
        $sql_sel .= " LEFT JOIN mst_product_pc PPC ON ODT.PRODUCT_PC_ID = PPC.PRODUCT_PC_ID       ";
        $sql_sel .= " LEFT JOIN mst_product PRD ON PPC.PRODUCT_ID       = PRD.PRODUCT_ID          ";
        $sql_sel .= " WHERE ((ODT.PO_ID IS NULL) OR (ODT.PO_ID = '')) AND (ODT.ACTIVE_FLAG = 'Y')  ";
        $sql_sel .= " ORDER BY ODT.UPDATE_BY, ODT.UPDATE_DATE DESC   ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_order_dtl error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_order_dtl_max()
    {
        $arr_ret = array();

        $sql_sel  = " SELECT MAX(ODT.UPDATE_DATE) as UPD_DATE, ODT.UPDATE_BY, ODT.CUS_ID, CUS.CUS_NAME ";
        $sql_sel .= " FROM order_dtl ODT LEFT JOIN mst_customer CUS ON CUS.CUS_ID = ODT.CUS_ID ";
        $sql_sel .= " WHERE ((ODT.PO_ID IS NULL) OR (ODT.PO_ID = '')) AND (ODT.ACTIVE_FLAG = 'Y')  ";
        $sql_sel .= " GROUP BY ODT.UPDATE_BY, ODT.CUS_ID ORDER BY ODT.UPDATE_DATE ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_order_dtl error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_order_dtl_no_po($user_name, $cus_id)
    {
        $arr_ret = array();

        $sql_sel  = " SELECT PRD.PRODUCT_NAME_TH, PRD.PRODUCT_NAME_EN, PRD.PRD_IMG, PRD.PRODUCT_ID, ODT.* FROM order_dtl ODT  ";
        $sql_sel .= " LEFT JOIN mst_product_pc PPC ON ODT.PRODUCT_PC_ID = PPC.PRODUCT_PC_ID       ";
        $sql_sel .= " LEFT JOIN mst_product PRD ON PPC.PRODUCT_ID       = PRD.PRODUCT_ID          ";
        $sql_sel .= " WHERE ((ODT.PO_ID IS NULL) OR (ODT.PO_ID = '')) AND (ODT.ACTIVE_FLAG = 'Y') ";
        $sql_sel .= "   AND (ODT.CREATED_BY = '{$user_name}') AND (ODT.CUS_ID = '{$cus_id}')      ";
        $sql_sel .= " ORDER BY ODT.UPDATE_DATE ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_order_dtl_no_po error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function chk_exist_order($user_name, $arr_params, $cus_id)
    {
        $arr_ret = array();

        $sql_sel  = " SELECT * FROM order_dtl ";
        $sql_sel .= " WHERE (CREATED_BY    = '{$user_name}') ";
        $sql_sel .= "   AND (CUS_ID        = '{$cus_id}')    ";
        $sql_sel .= "   AND (PRODUCT_PC_ID = '{$arr_params['prd_pc_id']}') ";
        $sql_sel .= "   AND (PO_ID IS NULL) AND (ACTIVE_FLAG = 'Y') ";
        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>chk_exist_order error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret = $row_sel;
        }

        return $arr_ret;
    }

    public function chk_exist_report($user_name, $cus_id, $the_date = "")
    {
        $arr_ret   = array();

        if ($the_date == "")
        {
            $this_date = date('Y-m-d');
        }
        else
        {
            $this_date = $the_date;
        }

        $sql_sel  = " SELECT * FROM sale_report_main ";
        $sql_sel .= " WHERE (CREATED_BY       = '{$user_name}') ";
        $sql_sel .= "   AND (CUS_ID           = '{$cus_id}')    ";
        $sql_sel .= "   AND (SALE_REPORT_DATE BETWEEN '{$this_date} 00:00' AND '{$this_date} 23:59') ";
//        $sql_sel .= "   AND (SALE_REPORT_DATE = '{$this_date}') "; //
        $sql_sel .= "   AND (ACTIVE_FLAG      = 'Y') ";
        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>chk_exist_report error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret = $row_sel;
        }

        //echo "<div>".$sql_sel."</div>\n";

        return $arr_ret;
    }

    public function del_order_dtl_backend($the_date, $aut_name, $cus_id)
    {
        $sql_del = " DELETE FROM order_dtl WHERE (CREATED_BY = '{$aut_name}') AND (CUS_ID = '{$cus_id}') AND (CREATED_DATE >= '{$the_date} 00:00:00') AND (CREATED_DATE <= '{$the_date} 23:59:59') ";
        $this->mysqli->query($sql_del) OR die("<div style='display:none;'>del_order_dtl error:{$sql_del}</div>");

        return $sql_del;
    }

    public function del_sale_report_dtl_backend($the_date, $aut_name, $cus_id)
    {
        $sql_del = " DELETE FROM sale_report_dtl WHERE (CREATED_BY = '{$aut_name}') AND (CUS_ID = '{$cus_id}') AND (CREATED_DATE >= '{$the_date} 00:00:00') AND (CREATED_DATE <= '{$the_date} 23:59:59') ";
        $this->mysqli->query($sql_del) OR die("<div style='display:none;'>del_sale_report_dtl_backend error:{$sql_del}</div>");

        return $sql_del;
    }

    public function add_order_dtl($user_name, $arr_params, $get_time = "", $no_add = "", $cus_id = "")
    {
        $str_ret  = "";
        $sql_head = "";
        $sql_body = "";
        $sql_tail = "";

        if ($get_time == "")
        {
            $get_time = date('Y-m-d H:i:s');
        }

        $arr_exist = array();
        $arr_exist = $this->chk_exist_order($user_name, $arr_params, $cus_id);

        if ($arr_exist["ORDER_DTL_ID"] != "")
        {
            $sql_head = " UPDATE order_dtl SET ";

            if ($no_add == "no add")
            {
                $num_new  = ($arr_params['qty'] * 1);
            }
            else
            {
                $num_new  = ($arr_params['qty'] * 1) + ($arr_exist['QTY'] * 1);
            }

            $sql_body .= " QTY = '".$num_new."', ";

            $sql_tail  = " WHERE ORDER_DTL_ID = '".$arr_exist['ORDER_DTL_ID']."' ";
        }
        else
        {
            $sql_head  = " INSERT INTO order_dtl SET ";

            $sql_body .= " PRODUCT_PC_ID = '".$arr_params['prd_pc_id']."', ";
            $sql_body .= "    CUS_ID     = '{$cus_id}'   , ";
            $sql_body .= "    CREATED_BY = '{$user_name}', ";
            $sql_body .= "  CREATED_DATE = '{$get_time}' , ";
            $sql_body .= "      ORDER_ID = '0', ";
            $sql_body .= "           QTY = '".$arr_params['qty']."', ";
        }

        $sql_body .= "     UPDATE_BY = '{$user_name}', ";
        $sql_body .= "   UPDATE_DATE = '{$get_time}'   ";

        $sql_upd = $sql_head.$sql_body.$sql_tail;

        $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>del_order_dtl error:{$sql_upd}</div>");

        $str_ret = $sql_upd;

        return $str_ret;
    }

    public function add_report_main($user_name, $cus_id = "", $the_date = "")
    {
        if ($the_date == "")
        {
            $this_date = date('Y-m-d');
        }
        else
        {
            $this_date = $the_date;
        }

        $the_time = date('H:i:s');
        $get_time = $this_date." ".$the_time;

        $sql_add  = " INSERT INTO sale_report_main SET   ";
        $sql_add .= " SALE_REPORT_DATE = '{$this_date}' ,";
        $sql_add .= " CUS_ID           = '{$cus_id}'    ,";
        $sql_add .= " CREATED_BY       = '{$user_name}' ,";
        $sql_add .= " CREATED_DATE     = '{$get_time}'  ,";
        $sql_add .= " UPDATE_BY        = '{$user_name}' ,";
        $sql_add .= " UPDATE_DATE      = '{$get_time}'   ";

        $this->mysqli->query($sql_add) OR die("<div style='display:none;'>add_report_main error:{$sql_add}</div>");
    }

    public function add_sale_rpt_dtl($user_name, $arr_params, $get_time = "", $cus_id = "")
    {
        if ($get_time == "")
        {
            $get_time = date('Y-m-d H:i:s');
        }

        $the_date  = date('Y-m-d',strtotime($get_time));

        $arr_exist = array();
        $arr_exist = $this->chk_exist_report($user_name, $cus_id, $the_date);

        if ($arr_exist["SALE_REPORT_ID"] == "")
        {
            $this->add_report_main($user_name, $cus_id, $get_time);
            echo "Added\n".$user_name."\n".$get_time."\n";
            $arr_exist = $this->chk_exist_report($user_name, $cus_id, $the_date);
        }

        $sql_upd  = " INSERT INTO sale_report_dtl SET ";
        $sql_upd .= " SALE_REPORT_ID = '".$arr_exist["SALE_REPORT_ID"]."', ";
        $sql_upd .= "  PRODUCT_PC_ID = '".$arr_params['prd_pc_id']    ."', ";
        $sql_upd .= "         CUS_ID = '".$cus_id                     ."', ";
        $sql_upd .= "            QTY = '".$arr_params['qty']          ."', ";
        $sql_upd .= "      SALE_TIME = '{$get_time}' , ";
        $sql_upd .= "    ACTIVE_FLAG = 'Y' , ";
        $sql_upd .= "     CREATED_BY = '{$user_name}', ";
        $sql_upd .= "   CREATED_DATE = '{$get_time}' , ";
        $sql_upd .= "      UPDATE_BY = '{$user_name}', ";
        $sql_upd .= "    UPDATE_DATE = '{$get_time}'   ";

        $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>add_sale_rpt_dtl error:{$sql_upd}</div>");

        $str_ret = $arr_exist["SALE_REPORT_ID"];

        return $str_ret;
    }

    public function get_sale_rpt_rel($sale_report_id, $aut_id)
    {
        $arr_ret = array();

        $sql_sel  = " SELECT USR.AUT_UNAME, SPR.* FROM sale_report_pc_rel SPR ";
        $sql_sel .= " LEFT JOIN aut_user USR ON SPR.AUT_ID = USR.AUT_ID ";
        $sql_sel .= " WHERE (SPR.SALE_REPORT_ID = '".$sale_report_id."') AND (SPR.AUT_ID != '".$aut_id."') ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_sale_rpt_rel error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function del_sale_rpt_rel($sale_report_id)
    {
        $sql_del = " DELETE FROM sale_report_pc_rel WHERE SALE_REPORT_ID = '".$sale_report_id."' ";
        $this->mysqli->query($sql_del) OR die("<div style='display:none;'>del_sale_rpt_rel error:{$sql_del}</div>");
    }

    public function add_sale_rpt_rel($aut_id, $sale_report_id, $upd_name)
    {
        $sql_upd  = " INSERT INTO sale_report_pc_rel SET ";
        $sql_upd .= " SALE_REPORT_ID = '".$sale_report_id."' ,";
        $sql_upd .= "         AUT_ID = '".$aut_id        ."' ,";
        $sql_upd .= "     CREATED_BY = '".$upd_name      ."'  ";

        $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>add_sale_rpt_rel error:{$sql_upd}</div>");
    }

    public function remove_sale_rpt_rel($aut_id, $sale_report_id)
    {
        $sql_del  = " DELETE FROM sale_report_pc_rel WHERE (SALE_REPORT_ID = '".$sale_report_id."') AND (AUT_ID = '".$aut_id."') ";

        $this->mysqli->query($sql_del) OR die("<div style='display:none;'>del_sale_rpt_rel error:{$sql_del}</div>");

        return $sql_del;
    }

    public function del_order_dtl($user_name, $order_dtl_id)
    {
        $str_ret = "";

        $get_time = date('Y-m-d H:i:s');

        $sql_del = " UPDATE order_dtl SET ACTIVE_FLAG = 'N', UPDATE_BY = '{$user_name}', UPDATE_DATE = '{$get_time}' WHERE ORDER_DTL_ID = '{$order_dtl_id}' ";
        $this->mysqli->query($sql_del) OR die("<div style='display:none;'>del_order_dtl error:{$sql_del}</div>");

        return $str_ret;
    }

    public function del_sale_rpt_dtl($user_name, $sale_report_dtl_id)
    {
        $str_ret = "";

        $get_time = date('Y-m-d H:i:s');

        $sql_del = " UPDATE sale_report_dtl SET ACTIVE_FLAG = 'N', UPDATE_BY = '{$user_name}', UPDATE_DATE = '{$get_time}' WHERE SALE_REPORT_DTL_ID = '{$sale_report_dtl_id}' ";
        $this->mysqli->query($sql_del) OR die("<div style='display:none;'>del_sale_rpt_dtl error:{$sql_del}</div>");

        return $str_ret;
    }

    public function upd_order_dtl($user_name, $order_dtl_id, $the_field, $the_value)
    {
        $str_ret = "";

        $get_time = date('Y-m-d H:i:s');

        $sql_upd  = " UPDATE order_dtl SET {$the_field} = '{$the_value}', ";
        
        if ($user_name != '')
        {
            $sql_upd .= " UPDATE_BY = '{$user_name}', ";
        }

        $sql_upd .= " UPDATE_DATE = '{$get_time}' WHERE ORDER_DTL_ID = '{$order_dtl_id}' ";
        $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>del_order_dtl error:{$sql_upd}</div>");

        return $str_ret;
    }

    public function upd_sale_rpt_dtl($user_name, $sale_report_dtl_id, $the_field, $the_value)
    {
        $str_ret = "";

        $get_time = date('Y-m-d H:i:s');

        $sql_upd = " UPDATE sale_report_dtl SET {$the_field} = '{$the_value}', UPDATE_BY = '{$user_name}', UPDATE_DATE = '{$get_time}' WHERE SALE_REPORT_DTL_ID = '{$sale_report_dtl_id}' ";
        $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>upd_sale_rpt_dtl error:{$sql_upd}</div>");

        return $str_ret;
    }

    public function get_product_pc($the_date, $type_id, $cus_type_id = "")
    {
        $arr_ret = array();

        $str_where = " WHERE 1 ";

        if ($type_id     != "") { $str_where  = " WHERE (MPD.PRODUCT_TYPE_ID = '{$type_id}') "; }
        if ($cus_type_id != "") { $str_where .= "   AND (MPC.CUS_TYPE_ID     = '{$cus_type_id}') "; }

        $sql_sel  = " SELECT MPC.*, MPD.*, MPT.TYPE_NAME_EN, MPT.TYPE_NAME_TH, CST.CUS_TYPE_NAME ";
        $sql_sel .= " FROM mst_product_pc AS MPC  ";
        $sql_sel .= " INNER JOIN  ";
        $sql_sel .= "     (SELECT PRODUCT_ID, MAX(effective_date) AS MaxDateTime  "; //PRODUCT_PC_ID
        $sql_sel .= "     FROM mst_product_pc  ";
        $sql_sel .= "     WHERE effective_date <= '{$the_date}' ";
        $sql_sel .= "     GROUP BY PRODUCT_ID) AS group_mpc     ";
        $sql_sel .= " ON (MPC.PRODUCT_ID = group_mpc.PRODUCT_ID) AND (MPC.EFFECTIVE_DATE = group_mpc.MaxDateTime) ";
        $sql_sel .= " LEFT JOIN mst_product AS MPD      ON MPC.PRODUCT_ID      = MPD.PRODUCT_ID ";
        $sql_sel .= " LEFT JOIN mst_product_type AS MPT ON MPT.PRODUCT_TYPE_ID = MPD.PRODUCT_TYPE_ID ";
        $sql_sel .= " LEFT JOIN customer_type    AS CST ON MPC.CUS_TYPE_ID     = CST.CUS_TYPE_ID     ";
        $sql_sel .= "   ".$str_where." ORDER BY MPD.PRODUCT_TYPE_ID, MPD.PRODUCT_NAME_EN, CST.CUS_TYPE_NAME ";

        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_product_pc error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function create_product_code_option($product_pc_id, $the_date)
    {
        $str_ret = "";
        $arr_product = $this->get_product_pc($the_date,'');

        foreach ($arr_product as $product)
        {
            $selected  = "";
            if ($product_pc_id == $product["PRODUCT_PC_ID"]) { $selected  = " selected"; }
            $str_ret .= "<option value='".$product["PRODUCT_PC_ID"]."'".$selected.">".$product["PRODUCT_CODE"]." - ".$product["PRODUCT_NAME_EN"]."</option>";
        }

        return $str_ret;
    }

    public function get_aut_user($str_where)
    {
        $arr_ret = array();

        if ($str_where == "") { $str_where = "1"; }

        $sql_sel  = " SELECT *  ";
        $sql_sel .= " FROM aut_user ";
        $sql_sel .= " WHERE ".$str_where." ORDER BY AUT_UNAME ";

        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_aut_user error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function create_pc_option()
    {
        $str_ret = "<option value=''>- Please Select -</option>\r\n";

        $arr_pc = $this->get_aut_user(" AUT_ROLE_ID = 4 ");
        
        foreach ($arr_pc as $aut_pc)
        {
            $str_ret .= "<option value='".$aut_pc["AUT_ID"]."'>".$aut_pc["AUT_UNAME"]."</option>\r\n";
        }

        return $str_ret;
    }

    public function crate_pc_order_table()
    {
        $str_ret = "";
        $arr_order_dtl = array();

        $arr_order_dtl = $this->get_order_dtl_max();

        $str_thead = "";
        $str_tbody = "";
        $str_ttail = "";

        if (count($arr_order_dtl) > 0)
        {
            $str_thead .= "<table class='table table-bordered table-striped'>";
            $str_thead .= "<thead>";
            $str_thead .= "<tr><th style='width:42px;'></th><th>DATE</th><th>NAME</th><th>CUSTOMER NAME (SHOP)</th></tr>";
            //$str_thead .= "<tr><th>DATE</th><th>NAME</th><th>ITEM</th><th>QTY</th></tr>";
            $str_thead .= "</thead>";

            $str_ttail .= "</table>";
        }

        foreach ($arr_order_dtl as $order_detail)
        {
            $read_date  = date('d/m/Y - H:i', strtotime($order_detail["UPD_DATE"]));

            $the_qty    = round($order_detail["QTY"]);
            $str_tbody .= "<tr>";
            $str_tbody .= "<td>";
            $str_tbody .= "<ul class='list-inline table-buttons'>";
            $str_tbody .= "<li><button onclick='import_pc_order(\"{$order_detail["UPDATE_BY"]}\",\"{$order_detail["CUS_ID"]}\");' class='btn-u btn-u-sm btn-u-blue' data-toggle='modal' data-target='#pc_order_imp_form' type='button'><i class='fa fa-download'></i> Import</button></li>";
            $str_tbody .= "</ul>";
            $str_tbody .= "</td>";
            $str_tbody .= "<td>{$read_date}</td><td>{$order_detail["UPDATE_BY"]}</td><td>{$order_detail["CUS_NAME"]}</td>";
            $str_tbody .= "</tr>";
            //$str_tbody .= "<tr><td>{$order_detail["UPDATE_DATE"]}</td><td>{$order_detail["UPDATE_BY"]}</td><td>{$order_detail["PRODUCT_NAME_TH"]}</td><td>{$the_qty}</td></tr>";
        }

        $str_tbody = "<tbody>".$str_tbody."</tbody>";

        $str_ret = $str_thead.$str_tbody.$str_ttail;

        return $str_ret;
    }

    public function aut_user_customer($user_name, $cus_id)
    {
        $arr_ret = array();

        $sql_sel  = " SELECT CUS.* ";
        $sql_sel .= " FROM aut_user_customer USC ";
        $sql_sel .= " LEFT JOIN aut_user USR ON USR.AUT_ID = USC.AUT_ID ";
        $sql_sel .= " LEFT JOIN mst_customer CUS ON CUS.CUS_ID = USC.CUS_ID ";
        $sql_sel .= " WHERE USC.CUS_ID = '{$cus_id}' ";
        //$sql_sel .= " WHERE USR.AUT_UNAME = '{$user_name}' ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>aut_user_customer error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_aut_id($cus_id)
    {
        $str_ret = "";

        if ($cus_id != "")
        {
            $sql_sel  = " SELECT USR.AUT_ID, USR.AUT_UNAME, CUS.* ";
            $sql_sel .= " FROM aut_user_customer as AUC ";
            $sql_sel .= " LEFT JOIN aut_user     USR ON AUC.AUT_ID = USR.AUT_ID ";
            $sql_sel .= " LEFT JOIN mst_customer CUS ON AUC.CUS_ID = CUS.CUS_ID ";
            $sql_sel .= " WHERE AUC.CUS_ID = '".$cus_id."' ";

            $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_aut_id error:{$sql_sel}</div>");
            if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
            {
                $str_ret = $row_sel["AUT_ID"];
            }
            $res_sel->free();
        }

        return $str_ret;  
    }

    public function get_cus_id($aut_name)
    {
        $str_ret = "";

        if ($aut_name != "")
        {
            $sql_sel  = " SELECT CUS.CUS_ID, CUS.CUS_NAME ";
            $sql_sel .= " FROM aut_user_customer as AUC ";
            $sql_sel .= " LEFT JOIN aut_user     USR ON AUC.AUT_ID = USR.AUT_ID ";
            $sql_sel .= " LEFT JOIN mst_customer CUS ON AUC.CUS_ID = CUS.CUS_ID ";
            $sql_sel .= " WHERE USR.AUT_UNAME = '".$aut_name."' ";

            $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_cus_id error:{$sql_sel}</div>");
            if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
            {
                $str_ret = $row_sel["CUS_ID"];
            }
            $res_sel->free();
        }

        return $str_ret;  
    }

    function get_aut_user_id($aut_uname)
    {
        $str_ret = "";
        $arr_aut = $this->get_aut_user(" AUT_UNAME = '{$aut_uname}' ");
        $str_ret = $arr_aut[0]["AUT_ID"];
        return $str_ret;
    }

    public function swap_date($the_date)
    {
        $arr_date = explode("-", $the_date);

        return $arr_date[2]."-".$arr_date[1]."-".$arr_date[0];
    }

    public function get_sale_report_month($month, $year)
    {
        $arr_ret = array();

        $sql_sel = "
            SELECT SRP.AUT_ID, MPD.PRODUCT_CODE, MPD.PRODUCT_NAME_TH, MPD.PRODUCT_TYPE_ID, SRD.QTY, MPP.*, SRM.*
            FROM sale_report_main SRM
            LEFT JOIN sale_report_pc_rel SRP ON SRP.SALE_REPORT_ID = SRM.SALE_REPORT_ID
            LEFT JOIN sale_report_dtl SRD ON SRD.SALE_REPORT_ID = SRM.SALE_REPORT_ID
            LEFT JOIN mst_product_pc MPP ON MPP.PRODUCT_PC_ID = SRD.PRODUCT_PC_ID
            LEFT JOIN mst_product MPD ON MPD.PRODUCT_ID = MPP.PRODUCT_ID
            WHERE (SRD.SALE_REPORT_DTL_ID IS NOT NULL)
              AND (MONTH(SRM.SALE_REPORT_DATE) = '{$month}') AND (YEAR(SRM.SALE_REPORT_DATE)  = '{$year}')
        ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_sale_report_detail error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_sale_report_detail($aut_id, $month, $year)
    {
        $arr_ret = array();

        $sql_sel = "
            SELECT SRP.AUT_ID, MPD.PRODUCT_CODE, MPD.PRODUCT_NAME_TH, MPD.PRODUCT_TYPE_ID, SRD.QTY, MPP.*, SRM.*
            FROM sale_report_main SRM
            LEFT JOIN sale_report_pc_rel SRP ON SRP.SALE_REPORT_ID = SRM.SALE_REPORT_ID
            LEFT JOIN sale_report_dtl SRD ON SRD.SALE_REPORT_ID = SRM.SALE_REPORT_ID
            LEFT JOIN mst_product_pc MPP ON MPP.PRODUCT_PC_ID = SRD.PRODUCT_PC_ID
            LEFT JOIN mst_product MPD ON MPD.PRODUCT_ID = MPP.PRODUCT_ID
            WHERE (SRP.AUT_ID = '{$aut_id}') AND (SRD.SALE_REPORT_DTL_ID IS NOT NULL)
              AND (MONTH(SRM.SALE_REPORT_DATE) = '{$month}') AND (YEAR(SRM.SALE_REPORT_DATE)  = '{$year}')
        ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_sale_report_detail error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function get_sale_target_detail($aut_id, $month, $year)
    {
        $num_ret = 0;

        $sql_sel  = " SELECT AST.* FROM aut_user_sales_target AST ";
        $sql_sel .= " WHERE (AST.AUT_ID = {$aut_id}) AND (AST.TARGET_MONTH = {$month}) AND (AST.TARGET_YEAR = {$year}) ";

        $res_sel  = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>get_sale_target_detail error:{$sql_sel}</div>");
        if ($row_sel = $res_sel->fetch_array(MYSQLI_ASSOC)) // MYSQLI_BOTH
        {
            $num_ret = $row_sel["SALES_TARGET"];
        }
        $res_sel->free();

        return $num_ret;
    }

    public function set_sale_target_detail($aut_id, $month, $year, $target)
    {
        $target_ret = $this->get_sale_target_detail($aut_id, $month, $year);

        $arr_aut = $this->get_aut_user(" AUT_ID = '{$aut_id}' ");
        $updater = $arr_aut[0]["AUT_UNAME"];

        if ($target_ret == 0)
        {
            $sql_upd  = " INSERT INTO aut_user_sales_target SET \n";
            $sql_upd .= " AUT_ID = '{$aut_id}', TARGET_MONTH = {$month}, TARGET_YEAR = {$year}, SALES_TARGET = {$target}, UPDATE_BY = '{$updater}' \n";
        }

        if ($target_ret != 0)
        {
            $sql_upd  = " UPDATE aut_user_sales_target SET SALES_TARGET = {$target}, UPDATE_BY = '{$updater}' \n";
            $sql_upd .= " WHERE (TARGET_MONTH = {$month}) AND (TARGET_YEAR = {$year}) AND (AUT_ID = '{$aut_id}') \n";
        }

        $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>set_sale_target_detail error:{$sql_upd}</div>");
    }

    public function update_password($user_name, $pass_word)
    {
        $str_ret  = "";

        $str_pwd  = hash('sha256', $pass_word);

        $sql_upd  = " UPDATE aut_user SET AUT_PW = '".$str_pwd."' WHERE AUT_UNAME = '".$user_name."' ";

        $this->mysqli->query($sql_upd) OR die("<div style='display:none;'>update_password error:{$sql_upd}</div>");

        $str_ret  = $user_name." เปลี่ยนพาสเวิร์ดแล้ว";

        return $str_ret;
    }
}
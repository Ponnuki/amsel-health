﻿<?php
	
	echo "Submit Form = ".$_GET["form"]."</br> hdMode=".$_POST['hdMode']."<br/>";
	require("connect.php");
	session_start();
	
	
	if ($_GET["form"] == "pro_sales_dtl")
	{
		$hdMode = $_POST['hdMode'];
		//*** Data Field ****
		
		$ddlProCode=$_POST['ddlProCode'];
		$txbProSalesCode=$_POST['txbProSalesCode'];
		$txbLTPPrice=$_POST['txbLTPPrice'];
		$txbStickerPrice=$_POST['txbStickerPrice'];
		$txbCheerRate=$_POST['txbCheerRate'];
		$txbLPTComm=$_POST['txbLPTComm'];
		
		$query_str="";
		
		if ($hdMode=="new")
		{
			
			$sql = sprintf("INSERT INTO mst_product_sale(
						   PRODUCT_SALE_CODE
						  ,PRODUCT_ID
						  ,LTP
						  ,STICKER_PRICE
						  ,CHEER_RATE
						  ,LPT_FOR_COMM
						  ,CANCEL_FLAG
						  ,CREATED_BY
						  ,CREATE_DATE
						  ,UPDATE_BY
						  ,UPDATE_DATE
						) VALUES (
						  '%s'  -- PRODUCT_SALE_CODE - IN varchar(15)
						  ,%d   -- PRODUCT_ID - IN int(11)
						  ,%f   -- LTP - IN decimal(12,2)
						  ,%f   -- STICKER_PRICE - IN decimal(12,2)
						  ,%f   -- CHEER_RATE - IN decimal(12,2)
						  ,%f   -- LPT_FOR_COMM - IN double
						  ,'N'  -- CANCEL_FLAG - IN varchar(1)
						  ,'%s'  -- CREATED_BY - IN varchar(20)
						  ,NOW()  -- CREATE_DATE - IN datetime
						  ,''  -- UPDATE_BY - IN varchar(20)
						  ,NOW()  -- UPDATE_DATE - IN datetime
						) "
				, mysql_real_escape_string($txbProSalesCode)
				, mysql_real_escape_string($ddlProCode)
				, mysql_real_escape_string($txbLTPPrice)
				, mysql_real_escape_string($txbStickerPrice)
				, mysql_real_escape_string($txbCheerRate)
				, mysql_real_escape_string($txbLPTComm)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				);
				
			if($result=mysql_query($sql)){
				$query_str="?focus=".$txbProSalesCode;
				echo "Insert OK ^.^";
			}else{
				$query_str="?error_ins=".$txbTranNo;
				echo "SQL Error = ".$sql."<br/>";
			}
		}else if ($hdMode=="edit")
		{
			$sql = sprintf("UPDATE mst_product_sale
									SET
									  LTP = %f -- decimal(12,2)
									  ,STICKER_PRICE = %f -- decimal(12,2)
									  ,CHEER_RATE = %f -- decimal(12,2)
									  ,LPT_FOR_COMM = %f -- decimal(10,2)
									  ,UPDATE_BY = '%s' -- varchar(20)
									  ,UPDATE_DATE = NOW() -- datetime
									WHERE PRODUCT_SALE_CODE = '%s' -- int(11)"
				, mysql_real_escape_string($txbLTPPrice)
				, mysql_real_escape_string($txbStickerPrice)
				, mysql_real_escape_string($txbCheerRate)
				, mysql_real_escape_string($txbLPTComm)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				,mysql_real_escape_string($txbProSalesCode)
				);
			
			if($result=mysql_query($sql)){
				$query_str="?focus=".$txbProSalesCode;
				echo "Update OK ^.^=".$sql."<br/>";
			}else{
				$query_str="?error_ins=".$txbTranNo;
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
		

	} else if ($_GET["form"] == "frm_sales_tran_tb") 
	{
		$hdDelID = $_POST['hdDelID'];
		$hdCancelID = $_POST['hdCancelID'];
		
		if ($_POST['hdDelID'] != "")
		{
			$sql = "DELETE FROM mst_product_sale
					WHERE LTP_CHART_ID = ".$hdDelID."; ";
			
			if($result=mysql_query($sql)){
				echo "Delete OK ^.^";
			}else{
				echo "SQL Error = ".$sql."<br/>";
			}
		
		} else if ($_POST['hdCancelID'] != "")
		{
			echo "</br>CancelID=".$hdCancelID;
			$sql = sprintf("UPDATE mst_product_sale
									SET CANCEL_FLAG = CASE WHEN CANCEL_FLAG = 'Y' THEN 'N' ELSE 'Y' END -- varchar(1)
										,UPDATE_BY = '%s' -- varchar(20)
										,UPDATE_DATE = Now() -- datetime
									WHERE LTP_CHART_ID = %d ; "
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($hdCancelID)
				);
			if($result=mysql_query($sql))
			{
				$query_str="?focus=".$txbProSalesCode;
				echo "Update OK ^.^=".$sql."<br/>";
			}else
			{
				$query_str="?error_ins=".$txbProSalesCode;
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
		
	}
	header("Location: product_sales.php".$query_str); 

	
?>
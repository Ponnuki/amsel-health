<?php
	ob_start();
	require_once("config.php");
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL NUTRACEUTICAL SYSTEM | WELCOM</title>
	<!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <!--<link rel="shortcut icon" href="favicon.ico">-->
	<link rel="icon" type="image/png" href="images/favicon.png" />

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-me.css">
	
	<!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-colors/orange.css" /> 

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">

    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="assets/css/pages/page_log_reg_v1.css">    

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">
	
	
	<link href="assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">

      /* Sticky footer styles
      -------------------------------------------------- */

      html,
      body {
        height: 100%;
        /* The html and body elements cannot have any padding or margin. */
      }

      /* Wrapper for page content to push down footer */
      #wrap {
        min-height: 100%;
        height: auto !important;
        height: 100%;
        /* Negative indent footer by it's height */
        margin: 0 auto -85px;
      }
      
    </style>
	
</head> 

<body>    

<div id="wrap" class="wrapper">
	
    <div class="header">
       
    
		<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
            <div class="container">
				
			</div>
		</div>
	</div>
    <!--=== End Header ===-->
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="logo">
                <img src="images/logo.png" alt="Logo">
            </div>
            
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

	
		
    <!--=== Content Part ===-->
    <div class="container content">		
    	<div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <form id="fmLogin" class="reg-page" method="POST" enctype="multipart/form-data" action="login_model.php">
                    <div class="reg-header">            
                        <h2>Login to your account</h2>
                    </div>
					<div >
					<?php 
					
						session_start();
						if(!isset($_SESSION['aut_uname']))
						{
							if ($_GET["msg"] == "1")
							{
								echo "<font size='2' color='#ff6666'>Invalid Username or password. Please try again.</font> <br/><br/>";
							}
							if ($_GET["msg"] == "timeout")
							{
								echo "<font size='2' color='#ff6666'>Login time out. (".$timeout." minutes)</font> <br/><br/>";
							}
							if ($_GET["msg"] == "change_pw")
							{
								echo "<font size='2' color='#008000'>You can change password successfully. Please login again.</font> <br/><br/>";
								
							}
							if ($_GET["msg"] == "session_exp")
							{
								echo "<font size='2' color='#ff6666'>Login session time out.</font> <br/><br/>";
								
							}
						}else
						{
							//echo "<br/><h3>role_id=".$_SESSION['role_id']."<br/>Location: home.php</h3>";
							if ($_SESSION["role_id"] == "4")
							{
								header("Location: pc_main.php");
							}else
							{
								header("Location: home.php");
							}
						}
						ob_end_flush();
					?>
					</div>
                    <div class="input-group margin-bottom-20">
						
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" id="txbUser" name="txbUser"  placeholder="Username" class="form-control" value="<?php echo $_SESSION['keyin_uname']; ?>">
                    </div>                    
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" id="txbPw" name="txbPw" placeholder="Password" class="form-control" >
                    </div>                    

                    <div class="row">
                         <div class="col-md-6 checkbox">
                            <!--<label><input type="checkbox"> Stay signed in</label>   -->                     
                        </div>
                        <div class="col-md-6">
                            <button class="btn-u pull-right" type="submit" style="width:80px">Login</button>                        
                        </div>
                    </div>

                    <hr>

                </form>            
            </div>
        </div><!--/row-->
    </div><!--/container-->		
    <!--=== End Content Part ===-->
<!--=== Footer Version 1 ===-->
    
    <!--=== End Footer Version 1 ===-->
    
</div><!--/wrapper-->

<?php require("include_footer.php"); ?>  
     
	
<!-- JS Global Compulsory -->           
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        });
</script>
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
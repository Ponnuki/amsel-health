<?php
	require_once("class_amh_province.php");
	require_once("class_amh_district.php");
	require_once("class_tax_invoice.php");
	require_once("include_function.php");
	$amh_province = new AMH_Province();
	$amh_district = new AMH_District();
	$amh_inv = new AMH_inv();
	if (isset($_REQUEST['cus_id']))
	{
		$cus_dtl = $amh_inv->get_customer_dtl($_REQUEST['cus_id']);
		
		foreach ($cus_dtl as $row)
		{
			echo $row['CUS_CODE']."|".$row['CUS_NAME']."|".$row['CUS_CONTRACT']."|".$row['CUS_ADDRESS']."|".$row['DISTRICT_CODE']."|".$row['PROVINCE_CODE']."|"
				.$row['CUS_ZIP']."|".$row['CUS_TEL']."|".$row['CUS_FAX']."|".$row['TAX_NUMBER'];
		}
		
	}
	else if (isset($_REQUEST['province_code']) )
	{
		echo $amh_district->create_district_option($_REQUEST['district_code'],$_REQUEST['province_code']);
	}
	else if ($_POST["operate"] == 'new') 
	{ 
		$new_inv_no = $amh_inv->add_inv_main($_POST); 
		$new_inv_id = $amh_inv ->get_inv_id($new_inv_no);
		echo $new_inv_no."|".$new_inv_id;
	}
	else if ($_POST["operate"] == 'new_inv_dtl') 
	{ 
		$result_msg = $amh_inv->add_inv_dtl($_POST); 
		echo $result_msg;
	}
	else if ($_POST["operate"] == 'grid_inv_main') 
	{ 
		
		$inv_list = json_decode($amh_inv->inv_main($_POST["p_inv_no"], $_POST["p_s_date"], $_POST["p_e_date"]),true);
		
		echo '   <table class="table table-bordered table-striped">
						<thead>
							<tr valign="middle">
								<th width="78px"> &nbsp; </th>
								<th width="100px">Tax Inv. Date</th>
								<th width="95px" class="hidden-sm">Tax Inv. No. </th>
								<th width="95px">PO No.</th>
								<th> Customer Name </th>
								<th width="130px"> Total Amount </th>
							</tr>
						</thead>
						<tbody> ';
		$i_no = 1;
		foreach ($inv_list as $row)
		{
			if($row["INV_NO"] == $_POST["focus"])
			{
				echo '<tr style = "background-color: #ffffbb">';
				echo '<script> history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'" ); </script>';
			}else
			{
				echo '<tr>';
			}
			
			echo '<td >
						<ul class="list-inline table-buttons">
							<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#u_acc_dtl_form" onclick="showDtlForm(\'edit\',\''.$row["INV_NO"].'\')"><i class="fa fa-edit"></i> Edit</button></li>
						</ul>
					</td>';
			echo '<td > <p>'.$row["INV_DATE"].'</p> </td>';
			echo '<td > <p>'.$row["INV_NO"].'</p> </td>';
			echo '<td > <p>'.$row["PO_NO"].'</p> </td>';
			echo '<td >'.$row["CUS_NAME"].'</td>';
			
			echo '<td >'.number_format($row["GRAND_TOTAL"],2).'</td>';
			
			
			echo '<td  width="84px">
							<ul class="list-inline table-buttons">
								<li>';

								
			echo '			<button type="button" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["INV_ID"] .','.($i_no-1).')"> 
								<i class="fa fa-trash-o"></i> Delete </button>';

			echo '			</li>
		   
							</ul>
						</td>
					</tr>';
			
				
		}		
		
		
		
	}
	else if ($_POST["operate"] == 'get_inv_data') 
	{ 
		//****** 
		echo $amh_inv->inv_main($_POST["p_inv_no"]);
		
	}
	else if ($_POST["operate"] == 'get_inv_dtl_data') 
	{ 
		
		$inv_dtl = $amh_inv->get_inv_dtl($_POST["p_inv_id"]);
		
		
		echo "<table class='table table-bordered table-striped'>
					<thead>
						<tr valign='middle'>
							<th>  # </th>
							<th>Pro. Sale Code</th>
							<th>Product Name</th>
							<th>Description</th>
							<th>Unit Price</th>
							<th>Qty.</th>
							<th>Total Amount</th>
							<th>   </th>
						</tr>
					</thead>
					<tbody>";
					
		$i_no = 1;
		$total = 0;
		foreach ($inv_dtl as $row)
		{
			echo "	
						<tr name='trRow'>
							<td width='45px'>
								<ul class='list-inline table-buttons'>
								<li><div name='divQno'>".$i_no."</div><input type='hidden' name='hdInvDtlId' value='".$row["INV_DTL_ID"]."'>
								</li>
								</ul>
							</td>
							<td width='90px'>
								<input type='hidden' name='hdProID' value='".$row["PRODUCT_ID"]."'>
								<p><input type='text' class='form-control' name='txbProSaleCode' value='".$row["PRODUCT_SALE_CODE"]."' readonly></p>
							</td>
							<td width='200px'>
								<input type='hidden' name='hdProName' value='".$row["PRODUCT_NAME"]."'>
								<div name='divProName'>".$row["PRODUCT_NAME"]."</div>
							</td>
							<td>
								<input type='hidden' name='hdProDesc' value='".$row["DESCRIPTION"]."'>
								<div name='divDesc'>".$row["DESCRIPTION"]."</div>
							</td>
							<td width='100px'>
								<p><input type='text' class='form-control' name='txbUnitPrice' value='".number_format($row["UNIT_PRICE"],2)."' readonly></p>
							</td>
							<td width='75px'>
								<p><input type='text' class='form-control' name='txbInvQty' value='".number_format($row["QTY"],0)."' onkeyup='reCalDtl(".($i_no-1).");'></p>
							</td>
							<td width='140px'>
								<p><input type='text' class='form-control' name='txbAmount' value='".number_format($row["AMOUNT"],2)."' readonly></p>
							</td>
							<td width='84px'>
								<ul class='list-inline table-buttons'>
								<li>
								<button class='btn-u btn-u-sm btn-u-red' onclick='confirmDtlDel(".($i_no-1).")' type='button'>
									<i class='fa fa-trash-o'></i> Delete
								</button>
								</li>
								</ul>
							</td>
						</tr>
						";
			$total = $total + $row["AMOUNT"];
			$i_no++;
		}						
		
		echo "	</tbody>
				</table>";
		echo "<input type='hidden' name='hdTotal' id='hdTotal'  value='".$total."'>";
		
		
	}
	else if ($_POST["operate"] == 'edit') 
	{ 
		$edit_result = $amh_inv->edit_inv_main($_POST); 
		
		echo $edit_result;
	}
	else if ($_POST["operate"] == 'del_inv_dtl') 
	{ 
		$result_msg = $amh_inv->del_inv_dtl($_POST); 
		echo $result_msg;
	}
	else if ($_POST["operate"] == 'del_inv_main') 
	{ 
		$result_msg = $amh_inv->del_inv_main($_POST); 
		echo $result_msg;
	}
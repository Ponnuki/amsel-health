<?php
	require_once("class_amh_province.php");
	require_once("class_amh_district.php");
	require_once("class_delivery_order.php");
	require_once("include_function.php");
	$amh_province = new AMH_Province();
	$amh_district = new AMH_District();
	$amh_do = new AMH_do();
	if (isset($_REQUEST['cus_id']))
	{
		$cus_dtl = $amh_do->get_customer_dtl($_REQUEST['cus_id']);
		
		foreach ($cus_dtl as $row)
		{
			echo $row['CUS_CODE']."|".$row['CUS_NAME']."|".$row['CUS_CONTRACT']."|".$row['CUS_ADDRESS']."|".$row['DISTRICT_CODE']."|".$row['PROVINCE_CODE']."|"
				.$row['CUS_ZIP']."|".$row['CUS_TEL']."|".$row['CUS_FAX']."|".$row['TAX_NUMBER'];
		}
		
	}
	else if (isset($_REQUEST['province_code']) )
	{
		echo $amh_district->create_district_option($_REQUEST['district_code'],$_REQUEST['province_code']);
	}
	else if ($_POST["operate"] == 'new') 
	{ 
		$new_do_no = $amh_do->add_do_main($_POST); 
		$new_do_id = $amh_do ->get_do_id($new_do_no);
		echo $new_do_no."|".$new_do_id;
	}
	else if ($_POST["operate"] == 'new_do_dtl') 
	{ 
		$result_msg = $amh_do->add_do_dtl($_POST); 
		echo $result_msg;
	}
	else if ($_POST["operate"] == 'grid_do_main') 
	{ 
		
		$do_list = json_decode($amh_do->do_main($_POST["p_do_no"], $_POST["p_s_date"], $_POST["p_e_date"]),true);
		
		echo '   <table class="table table-bordered table-striped">
						<thead>
							<tr valign="middle">
								<th> &nbsp; </th>
								<th>DO Date</th>
								<th class="hidden-sm">DO No. </th>
								<th>PO No.</th>
								<th> Customer Name </th>
								<th> Total Amount </th>
							</tr>
						</thead>
						<tbody> ';
		$i_no = 1;
		foreach ($do_list as $row)
		{
			if($row["DO_NO"] == $_POST["focus"])
			{
				echo '<tr style = "background-color: #ffffbb">';
				echo '<script> history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'" ); </script>';
			}else
			{
				echo '<tr>';
			}
			
			echo '<td width="78px">
						<ul class="list-inline table-buttons">
							<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#u_acc_dtl_form" onclick="showDtlForm(\'edit\',\''.$row["DO_NO"].'\')"><i class="fa fa-edit"></i> Edit</button></li>
						</ul>
					</td>';
			echo '<td width="80px"> <p>'.$row["DO_DATE"].'</p> </td>';
			echo '<td width="80px"> <p>'.$row["DO_NO"].'</p> </td>';
			echo '<td width="80px"> <p>'.$row["PO_NO"].'</p> </td>';
			echo '<td >'.$row["CUS_NAME"].'</td>';
			
			echo '<td width="110px" >'.number_format($row["GRAND_TOTAL"],2).'</td>';
			
			
			echo '<td  width="84px">
							<ul class="list-inline table-buttons">
								<li>';

								//if ($row["IS_DEL"] == 1)
								//{
			echo '			<button type="button" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["DO_ID"] .','.($i_no-1).')"> 
								<i class="fa fa-trash-o"></i> Delete </button>';

								/*}else if ($row["ACTIVE_FLAG"] == 'Y')
								{
									echo '			<button type="submit" class="btn-u btn-u-sm btn-u-orange" onclick ="return confirmCancel('.$row["DO_ID"] .')"> 
														<i class="fa fa-times-circle"></i> Cancel</button>';
								} else 
								{
									echo '			<button type="submit" class="btn-u btn-u-sm btn-u-green" onclick ="return confirmCancel('.$row["DO_ID"] .')"> 
														<i class="fa fa-check-circle"></i> Enable</button>';
								}*/

			echo '			</li>
		   
							</ul>
						</td>
					</tr>';
			
				
		}		
		
		
		
	}
	else if ($_POST["operate"] == 'get_do_data') 
	{ 
		//****** 
		echo $amh_do->do_main($_POST["p_do_no"]);
		
	}
	else if ($_POST["operate"] == 'get_do_dtl_data') 
	{ 
		//****** ทำถึงนี่ รอดึง Data DO_MAIN มา Fill ใส Contron และทำการดึงข้อมูล do_dtl มาใส่ Grid ต่อ
		$do_dtl = $amh_do->get_do_dtl($_POST["p_do_id"]);
		
		//echo "<input type='hidden' name='hdCusID' id='hdCusID'  value='".$do_dtl[0]["CUS_ID"]."'>";
		echo "<table class='table table-bordered table-striped'>
					<thead>
						<tr valign='middle'>
							<th>  # </th>
							<th>Pro. Sale Code</th>
							<th>Product Name</th>
							<th>Description</th>
							<th>Unit Price</th>
							<th>Qty.</th>
							<th>Total Amount</th>
							<th>   </th>
						</tr>
					</thead>
					<tbody>";
					
		$i_no = 1;
		$total = 0;
		foreach ($do_dtl as $row)
		{
			echo "	
						<tr name='trRow'>
							<td width='45px'>
								<ul class='list-inline table-buttons'>
								<li><div name='divQno'>".$i_no."</div><input type='hidden' name='hdDoDtlId' value='".$row["DO_DTL_ID"]."'>
								</li>
								</ul>
							</td>
							<td width='90px'>
								<input type='hidden' name='hdProID' value='".$row["PRODUCT_ID"]."'>
								<p><input type='text' class='form-control' name='txbProSaleCode' value='".$row["PRODUCT_SALE_CODE"]."' readonly></p>
							</td>
							<td width='200px'>
								<input type='hidden' name='hdProName' value='".$row["PRODUCT_NAME"]."'>
								<div name='divProName'>".$row["PRODUCT_NAME"]."</div>
							</td>
							<td>
								<input type='hidden' name='hdProDesc' value='".$row["DESCRIPTION"]."'>
								<div name='divDesc'>".$row["DESCRIPTION"]."</div>
							</td>
							<td width='100px'>
								<p><input type='text' class='form-control' name='txbUnitPrice' value='".number_format($row["UNIT_PRICE"],2)."' readonly></p>
							</td>
							<td width='75px'>
								<p><input type='text' class='form-control' name='txbDoQty' value='".number_format($row["QTY"],0)."' onkeyup='reCalDtl(".($i_no-1).");'></p>
							</td>
							<td width='140px'>
								<p><input type='text' class='form-control' name='txbAmount' value='".number_format($row["AMOUNT"],2)."' readonly></p>
							</td>
							<td width='84px'>
								<ul class='list-inline table-buttons'>
								<li>
								<button class='btn-u btn-u-sm btn-u-red' onclick='confirmDtlDel(".($i_no-1).")' type='button'>
									<i class='fa fa-trash-o'></i> Delete
								</button>
								</li>
								</ul>
							</td>
						</tr>
						";
			$total = $total + $row["AMOUNT"];
			$i_no++;
		}						
		
		echo "	</tbody>
				</table>";
		echo "<input type='hidden' name='hdTotal' id='hdTotal'  value='".$total."'>";
		//echo "<input type='hidden' name='hdDiscPer' id='hdDiscPer'  value='".$do_dtl[0]["DISCOUNT_PER"]."'>";
		//echo "<input type='hidden' name='hdVatPer' id='hdVatPer'  value='".$do_dtl[0]["VAT_PER"]."'>";
		
	}
	else if ($_POST["operate"] == 'edit') 
	{ 
		$edit_result = $amh_do->edit_do_main($_POST); 
		//$new_do_id = $amh_do ->get_do_id($new_do_no);
		echo $edit_result;
	}
	else if ($_POST["operate"] == 'del_do_dtl') 
	{ 
		$result_msg = $amh_do->del_do_dtl($_POST); 
		echo $result_msg;
	}
	else if ($_POST["operate"] == 'del_do_main') 
	{ 
		$result_msg = $amh_do->del_do_main($_POST); 
		echo $result_msg;
	}
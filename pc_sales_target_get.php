<?php

$aut_id     = $_REQUEST["aut_id"];
$this_month = $_REQUEST["the_month"];
$this_year  = $_REQUEST["the_year"];

require_once('config.php');
require_once('class_amh_db.php');
require_once('class_amh_pc.php');

$amh_pc = new AMH_PC();

$sales_target = $amh_pc->get_sale_target_detail($aut_id, $this_month, $this_year);

echo $sales_target;
﻿	<script type="text/javascript">
    <!--
		function ClearSalesTranForm()
		{
			document.getElementById('hdMode').value = "new";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-plus-square'></i>New Sales Transaction";
			
			document.getElementById('txbTranNo').value = "";
			document.getElementById('txbTranDate').value = todayDDMMYYYY();
			document.getElementById('ddlSalesCode').value = "";
			document.getElementById('txbSalesName').value = "";
			document.getElementById('ddlProSaleCode').value = "";
			document.getElementById('txbProName').value = "";
			document.getElementById('txbLTPPrice').value = "";
			document.getElementById('txbStickerPrice').value = "";
			document.getElementById('txbCheerRate').value = "";
			document.getElementById('txbLPTComm').value = "";
			document.getElementById('ddlShopCode').value = "";
			document.getElementById('txbShopName').value = "";
			document.getElementById('txbSalesIn').value = "0.00";
			document.getElementById('txbSalesOut').value = "0.00";
			document.getElementById('txbQty').value = "";
			
		}
		
		function getSalesName(sales_id)
		{
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					document.getElementById("txbSalesName").value = xmlhttp.responseText.replace("\r\n\t", "");
				}
			}
			xmlhttp.open("GET","sales_transaction_ajax.php?sales_id="+sales_id ,true);
			xmlhttp.send();
		}
		
		function getTrnLptChartDtl(product_sale_code)
		{
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					var res = str.split("|");
					document.getElementById("txbProName").value = res[0];
					document.getElementById('txbLTPPrice').value = res[1];
					document.getElementById('txbStickerPrice').value = res[2];
					document.getElementById('txbCheerRate').value = res[3];
					document.getElementById('txbLPTComm').value = res[4];
				}
			}
			xmlhttp.open("GET","sales_transaction_ajax.php?product_sale_code="+product_sale_code ,true);
			xmlhttp.send();
		}
		
		function getShopName(shop_id)
		{
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					document.getElementById("txbShopName").value = xmlhttp.responseText.replace("\r\n\t", "");
				}
			}
			xmlhttp.open("GET","sales_transaction_ajax.php?shop_id="+shop_id ,true);
			xmlhttp.send();
		}
		
		function getDataAjax(int) 
		{
			document.getElementById('hdMode').value = "edit";
			document.getElementById('labMode').innerHTML = "<i class='fa fa-pencil-square-o'></i>Edit Sales Transaction";
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					var str = xmlhttp.responseText.replace("\r\n\t", "");
					var res = str.split("|");
					document.getElementById("txbTranNo").value = res[1];
					document.getElementById('txbTranDate').value = res[2];
					document.getElementById('ddlSalesCode').value = res[3];
					document.getElementById('txbSalesName').value = res[4];
					document.getElementById('ddlProSaleCode').value = res[5];
					document.getElementById('txbProName').value = res[6];
					document.getElementById('txbLTPPrice').value = res[7];
					document.getElementById('txbStickerPrice').value = res[8];
					document.getElementById('txbCheerRate').value = res[9];
					document.getElementById('txbLPTComm').value = res[10];
					document.getElementById('ddlShopCode').value = res[11];
					document.getElementById('txbShopName').value = res[12];
					document.getElementById('txbSalesIn').value = res[13];
					document.getElementById('txbSalesOut').value = res[14];
					document.getElementById('txbQty').value = res[15];
					document.getElementById('hdTempQty').value =  parseFloat(res[15])+parseFloat(res[14])-parseFloat(res[13]);
					
				}
			}
			
			xmlhttp.open("GET","sales_transaction_ajax.php?sales_dtl_id="+int,true);
			xmlhttp.send();
		}
		
		function getNowQty(obj)
		{
			//alert("test");
			
			if (window.XMLHttpRequest) 
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else 
			{  // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() 
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==500) 
				{
					//document.getElementById("hdTempQty").value = xmlhttp.responseText.replace("\r\n\t", "");
					 temQty = parseFloat(xmlhttp.responseText.replace("\r\n\t", ""));
					
					if (obj.name=="txbSalesIn")
					{
						document.getElementById("txbSalesOut").value = "0.00";
						document.getElementById("txbQty").value  = (temQty + parseFloat(obj.value)).toFixed(2);
					}else {
						document.getElementById("txbSalesIn").value = "0.00";
						document.getElementById("txbQty").value  = (temQty - parseFloat(obj.value)).toFixed(2);
					}
					
				}
			}
			shop_id = document.getElementById('ddlShopCode').value;
			pro_sale_code = document.getElementById('ddlProSaleCode').value;
			sales_id = document.getElementById('ddlSalesCode').value;
			trn_sales_no = "";
			if (document.getElementById('txbTranNo').value != "")
			{
				trn_sales_no = "&trn_sales_no=" + document.getElementById('txbTranNo').value;
			}
			
			xmlhttp.open("GET","sales_transaction_ajax.php?shop_id="+shop_id+"&sales_id="+sales_id+"&product_sale_code=" +pro_sale_code + trn_sales_no,true);
			xmlhttp.send();
		}
		
    //-->
    </script>
	
		<form method="POST" enctype="multipart/form-data" action="sales_transaction_model.php?form=sales_tran_dtl" name="formSalesTranDtl" onSubmit="return validate_form(this)"
		 id="formSalesTranDtl" >
			<input type="hidden" name="hdMode" id="hdMode"  value="">
		
			<div class="modal fade" id="sales_transaction_dtl_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog modal-lg">
					<div id="divFormSalesTran" class="modal-content">
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							
							<h4 class="modal-title" id="labMode"><i class="fa fa-plus-square"></i>New Sales Transaction</h4>
						</div>
						<div class="modal-body sky-form"  style="border-style:none">
							
							<div class="row" style="padding-bottom:15px;">
								<div class="form-group " >
									<label for="labTranNo" class="col-lg-2 control-label" style="text-align:right">Tran. No. <font color=#ff0000> *</font>:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" name="txbTranNo" id="txbTranNo" placeholder="Transaction Number" readonly>
									</div>
									<section >
										<label for="labTranDate" class="col-lg-2 control-label" style="text-align:right">Tran. Date *:</label>
										<div class="col-lg-4">
											<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" name="txbTranDate" id="txbTranDate" required>
											</label>
										</div>
									</section>
								</div>
							</div>
						

							<div class="row" style="padding-bottom:15px;">

								<div class="form-group" style="vertical-align:bottom;">
									<label class="col-lg-2 control-label" style="text-align:right">Sales Code *:</label>
									<div class="col-lg-4">
										<label class="select" >
												<select id="ddlSalesCode" name="ddlSalesCode" onchange="getSalesName(this.value);" required>
													<option value="" selected> - Please Select - </option>
												<?php 
												while($row=mysql_fetch_array($result_emp_code)) 
												{
													echo "<option value='".$row['EMP_ID']."'>".$row['EMP_CODE']."</option>";
												}
												?>
												</select>
												<i></i>
										</label>
									</div>
									<label class="col-lg-2 control-label" style="text-align:right">Sales Name *:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" name="txbSalesName" id="txbSalesName" placeholder="Sales Name" readonly>
									</div>
								</div>
							</div>
							<!-- Product panal -->
							<div class="panel panel-grey margin-bottom-15 padding: 15px">
								<div class="row" style="padding-top: 15px; padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Product Sales Code *:</label>
										<div class="col-lg-4">
											<label class="select" >
												<select name="ddlProSaleCode" id="ddlProSaleCode" onchange="getTrnLptChartDtl(this.value)" required>
														<option value="" selected> - Please Select - </option>
													<?php 
													while($row=mysql_fetch_array($result_pro_sale_code)) 
													{
														echo "<option value='".$row['PRODUCT_SALE_CODE']."'>".$row['PRODUCT_SALE_CODE']."</option>";
													}
													?>
												</select>
												<i></i>
											</label>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Product Name *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbProName" id="txbProName" placeholder="" readonly>
										</div>
									</div>
								</div>
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">LTP Price *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbLTPPrice" id="txbLTPPrice" placeholder="" readonly>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">Sticker Price *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbStickerPrice" id="txbStickerPrice" placeholder="" readonly>
										</div>
									</div>
								</div>
								<div class="row" style="padding-bottom:15px;  padding-bottom:15px; padding-right:20px;padding-left:20px;">
									<div class="form-group" style="vertical-align:bottom;">
										
										<label class="col-lg-2 control-label" style="text-align:right">Cheer Rate *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbCheerRate" id="txbCheerRate" placeholder="" readonly>
										</div>
										<label class="col-lg-2 control-label" style="text-align:right">LPT for Commission *:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="txbLPTComm" id="txbLPTComm" placeholder="" readonly>
										</div>
									</div>
								</div>
							</div><!-- End Product panal -->

							<div class="row" style="padding-bottom:15px;">
								<div class="form-group" style="vertical-align:bottom;">
									<label class="col-lg-2 control-label" style="text-align:right">Shop Code *:</label>
									<div class="col-lg-4">
										<label class="select" >
												<select name="ddlShopCode" id="ddlShopCode" onchange="getShopName(this.value)" required>
														<option value="" selected> - Please Select - </option>
													<?php 
													while($row=mysql_fetch_array($result_shop_code)) 
													{
														echo "<option value='".$row['SHOP_ID']."'>".$row['SHOP_CODE']."</option>";
													}
													?>
												</select>
												<i></i>
										</label>
									</div>
									<label class="col-lg-2 control-label" style="text-align:right">Shop Name *:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" name="txbShopName" id="txbShopName" placeholder="Shop Name" readonly>
									</div>
								</div>
							</div>

							<div class="row"  style="padding-bottom:15px;">
								<div class="form-group" style="vertical-align:bottom;">
									<label  class="col-lg-2 control-label" style="text-align:right">Sales In  :</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" name="txbSalesIn" id="txbSalesIn"  onkeyup="getNowQty(this);" autocomplete="off" placeholder="">
									</div>
									<label  class="col-lg-2 control-label" style="text-align:right">Sales Out  :</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" name="txbSalesOut" id="txbSalesOut" onkeyup="getNowQty(this);" autocomplete="off" placeholder="">
									</div>
								</div>
							</div>

							<div class="row"  style="padding-bottom:15px;">
								<div class="form-group" style="vertical-align:bottom;">
									<label  class="col-lg-2 control-label" style="text-align:right">Current Qty  :</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" name="txbQty" id="txbQty" placeholder="Current Qty.(at SHOP)" style="background-color:#ffffa0;" readonly>
										<input type="hidden" name="hdTempQty" id="hdTempQty"  value="0">
									</div>

									<div class="col-lg-6">
										&nbsp;
									</div>
								</div>
							</div>
						</div><!-- End "modal-body" -->
						<div class="modal-footer">
							<button type="button" name="btClose" id="btClose" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn-u btn-u-primary" name="btSubmit" id="btSubmit">Save New</button>
						</div>
						
					</div><!-- END modal-body -->
				</div>
			</div>

		</form>
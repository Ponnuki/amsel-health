<?php
/* Class name
   AMH_Province

   public function 
   mst_province($str_search) return array of province info (example: )
*/
require_once('class_amh_db.php');

class AMH_Province extends AMH_DB
{
    public function mst_province($str_search)
    {
        $arr_ret = array();

        $str_where = "1";
        if ($str_search != '')
        {
           $str_where  = " (PROVINCE_NAME LIKE '%".$str_search."%') ";
        }

        $sql_sel = " SELECT * FROM mst_province WHERE ".$str_where." ORDER BY PROVINCE_NAME ";
        $res_sel = $this->mysqli->query($sql_sel) OR die("<div style='display:none;'>mst_province error:{$sql_sel}</div>");
        while ($row_sel = $res_sel->fetch_array(MYSQLI_BOTH))
        {
          $arr_ret[] = $row_sel;
        }
        $res_sel->free();

        return $arr_ret;
    }

    public function create_province_option($province_code)
    {
        $str_retn = "";
        $arr_prov = $this->mst_province('');
        
        foreach ($arr_prov as $province)
        {
            $selected  = "";
            if ($province_code == $province["PROVINCE_CODE"]) { $selected  = " selected"; }
            $str_retn .= "<option value='".$province["PROVINCE_CODE"]."'".$selected.">".$province["PROVINCE_NAME"]."</option>";
        }

        return $str_retn;
    }
}
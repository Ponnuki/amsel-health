<?php
	ob_start();
	require("connect.php");
	require_once("include_function.php");
	session_start();
	
	
	if ($_GET["form"] == "pro_pc_dtl")
	{
		$hdMode = $_POST['hdMode'];
		$hdProPcId = $_POST['hdProPcId'];
		//*** Data Field ****
		
		$ddlProCode = $_POST['ddlProCode'];
		$txbDate = $_POST['txbDate'];
		$ddlCusType = $_POST['ddlCusType'];
		$txbLTPPrice = $_POST['txbLTPPrice'];
		$txbLTPPer = $_POST['txbLTPPer']; 
		$txbStickerPrice = $_POST['txbStickerPrice'];
		$txbCheerRate = $_POST['txbCheerRate'];
		$txbLPTComm = $_POST['txbLPTComm'];
		
		$query_str="";
		
		if ($hdMode=="new")
		{
			
			$sql = sprintf("INSERT INTO mst_product_pc(
									EFFECTIVE_DATE
									,PRODUCT_ID
									,CUS_TYPE_ID
									,LTP_PER
									,LTP_PRICE
									,STICKER_PRICE
									,CHEER_RATE
									,LTP_FOR_COMM
									,CREATED_BY
									,CREATED_DATE
									,UPDATE_DATE
								) VALUES (
								  '%s'  
								  ,%d 
								  ,%d
								  ,%f   
								  ,%f 
								  ,%f 
								  ,%f 
								  ,%f 
								  ,'%s' 
								  ,NOW() 
								  ,NOW() 
						) "
				, mysql_real_escape_string(printDate($txbDate))
				, mysql_real_escape_string($ddlProCode)
				, mysql_real_escape_string($ddlCusType)
				, mysql_real_escape_string($txbLTPPer)
				, mysql_real_escape_string($txbLTPPrice)
				, mysql_real_escape_string($txbStickerPrice)
				, mysql_real_escape_string($txbCheerRate)
				, mysql_real_escape_string($txbLPTComm)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				);
				
			if($result=mysql_query($sql)){
				$query_str="?focus_d=".$txbDate."&focus_p=".$ddlProCode;
				echo "Insert OK ^.^";
			}else{
				$query_str="?error_ins=".$txbTranNo;
				echo "SQL Error = ".$sql."<br/>";
			}
		}else if ($hdMode=="edit")
		{
			$sql = sprintf("UPDATE mst_product_pc
									SET
										EFFECTIVE_DATE = '%s'
									  ,CUS_TYPE_ID = %d
									  ,LTP_PER = %f -- decimal(12,2)
									  ,LTP_PRICE = %f -- decimal(12,2)
									  ,STICKER_PRICE = %f -- decimal(12,2)
									  ,CHEER_RATE = %f -- decimal(12,2)
									  ,LTP_FOR_COMM = %f -- decimal(10,2)
									  ,UPDATE_BY = '%s' -- varchar(20)
									  ,UPDATE_DATE = NOW()
									WHERE PRODUCT_PC_ID = %d; "
				, mysql_real_escape_string(printDate($txbDate))
				, mysql_real_escape_string($ddlCusType)
				, mysql_real_escape_string($txbLTPPer)
				, mysql_real_escape_string($txbLTPPrice)
				, mysql_real_escape_string($txbStickerPrice)
				, mysql_real_escape_string($txbCheerRate)
				, mysql_real_escape_string($txbLPTComm)
				, mysql_real_escape_string($_SESSION['aut_uname'])
				,mysql_real_escape_string($hdProPcId)
				);
			
			if($result=mysql_query($sql)){
				$query_str="?focus=".$hdProPcId;
				echo "Update OK ^.^=".$sql."<br/>";
			}else{
				$query_str="?error_ins=".$txbTranNo;
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
		

	} else if ($_GET["form"] == "frm_pc_tran_tb") 
	{
		$hdDelID = $_POST['hdDelID'];
		$hdCancelID = $_POST['hdCancelID'];
		
		if ($_POST['hdDelID'] != "")
		{
			$sql = "DELETE FROM mst_product_pc
					WHERE PRODUCT_PC_ID = ".$hdDelID."; ";
			
			if($result=mysql_query($sql)){
				echo "Delete OK ^.^";
			}else{
				echo "SQL Error = ".$sql."<br/>";
			}
		
		} else if ($_POST['hdCancelID'] != "")
		{
			echo "</br>CancelID=".$hdCancelID;
			$sql = sprintf("UPDATE mst_product_sale
									SET CANCEL_FLAG = CASE WHEN CANCEL_FLAG = 'Y' THEN 'N' ELSE 'Y' END -- varchar(1)
										,UPDATE_BY = '%s' -- varchar(20)
										,UPDATE_DATE = Now() -- datetime
									WHERE LTP_CHART_ID = %d ; "
				, mysql_real_escape_string($_SESSION['aut_uname'])
				, mysql_real_escape_string($hdCancelID)
				);
			if($result=mysql_query($sql))
			{
				$query_str="?focus=".$txbProSalesCode;
				echo "Update OK ^.^=".$sql."<br/>";
			}else
			{
				$query_str="?error_ins=".$txbProSalesCode;
				echo "SQL Error = ".$sql."<br/>";
			}
			
		}
	}
	header("Location: product_pc.php".$query_str); 
	ob_end_flush();
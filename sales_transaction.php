﻿<?php 
	
	require("include_function.php");
	require("connect.php");
	require('validatelogin.php'); 
	//echo "session =".$_SESSION['aut_uname'];
 ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>AMSEL HEALTH SELECT | SALES TRANSACTION</title>

	<?php $current_menu = "sales_transaction"; ?>
	<?php require("include_headtag.php"); ?>
    
</head> 

<body>    

<div id="wrap"  class="wrapper">

	<?php require("include_header.php"); ?>

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs breadcrumbs-dark">
        <div class="container">
            <h1 class="pull-left">SALES TRANSACTION</h1>
			
        </div>
    </div>
    <!--=== End Breadcrumbs ===-->
	<script type="text/javascript">
    <!--
	function confirmDel(DelID)
	{
		if (confirm("Are you sure you want to delete this item."))
		{
			document.getElementById('hdDelID').value = DelID;
			return true;
		}else{
			return false;
		}
	 
	}
	 
	 //-->
    </script>
    <!--=== Search Block Version 2 ===-->
   
		<div  class="search-block"  style = "padding: 18px; padding-bottom: 0px; " >
		<?php 
			// **** Gen Sales Code
			$sql = "SELECT EMP_ID, EMP_CODE FROM mst_employee WHERE POSITION_ID = 11 ORDER BY EMP_CODE; ";
			$_GET['result_emp_code']=  mysql_query($sql);
			$result_emp_code = mysql_query($sql);

			// **** Gen Product sales code
			$sql = "SELECT tlc.LTP_CHART_ID, tlc.PRODUCT_SALE_CODE,  tlc.PRODUCT_ID , mp.PRODUCT_NAME_TH
						FROM mst_product_sale AS tlc
						  LEFT JOIN mst_product AS mp ON tlc.PRODUCT_ID = mp.PRODUCT_ID 
						ORDER BY PRODUCT_SALE_CODE; ";
			$result_pro_sale_code =  mysql_query($sql);
			
			// **** Gen Shop code
			$sql = "SELECT SHOP_ID, SHOP_CODE, SHOP_NAME
						FROM mst_shop ";
			$result_shop_code =  mysql_query($sql);
			
			// **** Gen Sales Teansaction
			$sql = "SELECT tsd.SALES_DTL_ID, tsd.TRN_SALES_NO, DATE_FORMAT(TRN_SALES_DATE, '%d-%m-%Y') as TRN_DATE,  me.EMP_CODE, tsd.PRODUCT_SALE_CODE,ms.SHOP_NAME, tlc.STICKER_PRICE,
						  tsd.SALES_IN, tsd.SALES_OUT
						FROM trn_sales_dtl AS tsd
						  LEFT JOIN mst_employee AS me ON tsd.SALES_EMP_ID = me.EMP_ID
						  LEFT JOIN mst_shop AS ms ON tsd.SHOP_ID = ms.SHOP_ID
						  LEFT JOIN mst_product_sale AS tlc ON tsd.PRODUCT_SALE_CODE = tlc.PRODUCT_SALE_CODE ";
			
			$where = "";
			
			if ($_POST['txbTranNum'] != "")
			{
				if ($where == "") $where .= "WHERE ";
				$where .= " tsd.TRN_SALES_NO LIKE '%".
					mysql_real_escape_string($_POST['txbTranNum'])
					."%' ";
			}
			
			if ($_POST['txbTranDateStart'] != "" && $_POST['txbTranDateEnd'] != "")
			{
				if ($where == "") $where .= "WHERE "; else $where .=" and ";
				$where .= " tsd.TRN_SALES_DATE BETWEEN '".
					mysql_real_escape_string(printDate($_POST['txbTranDateStart']))
					."' AND '".
					mysql_real_escape_string(printDate($_POST['txbTranDateEnd']))
					."'  ";
			}
			if ($_POST['ddlSalesCodeSearch']!= "")
			{
				if ($where == "") $where .= "WHERE "; else $where .=" and ";
				$where .= " tsd.SALES_EMP_ID = ".
					mysql_real_escape_string($_POST['ddlSalesCodeSearch'])
					." ";
			}
			if ($_POST['ddlProSaleCodeSearch']!="")
			{
				if ($where == "") $where .= "WHERE "; else $where .=" and ";
				$where .= " tsd.PRODUCT_SALE_CODE = '".
					mysql_real_escape_string($_POST['ddlProSaleCodeSearch'])
					."' ";
			}
			
			$order = "	ORDER BY TRN_SALES_DATE DESC ; ";
			$sql = $sql.$where.$order;
			$result_sales_tran = mysql_query($sql);

			?>
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<h2>CONDITION</h2>
					<div class="panel panel-grey margin-bottom-40" style="padding: 18px;">
						
						<div class="sky-form" style="border-style:none">                                                      
							<form class="form-horizontal" role="form"  method="POST" enctype="multipart/form-data" action="<? echo $_SERVER['PHP_SELF']; ?>" name="form_sales_tran_dtl" onSubmit="return validate_form(this)" >
								<div class="form-group">
									<label for="inputTranNo" class="col-lg-4 control-label">Transaction Number :</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" name="txbTranNum" id="txbTranNum" placeholder="Transaction Number" value="<?php echo $_POST['txbTranNum']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="inputTranDate" class="col-lg-4 control-label">Transaction Date :</label>
									<section class="col col-lg-4" style="height: 16px">
										<label class="input">
											<i class="icon-append fa fa-calendar"></i>
											<input type="text" class="form-control" name="txbTranDateStart" id="txbTranDateStart" placeholder="Start date" value="<?php echo $_POST['txbTranDateStart']; ?>">
										</label>
									</section>

									<section class="col col-lg-4" style="height: 16px">
										<label class="input">
											<i class="icon-append fa fa-calendar"></i>
											<input type="text" class="form-control" name="txbTranDateEnd" id="txbTranDateEnd" placeholder="End date" value="<?php echo $_POST['txbTranDateEnd']; ?>">
										</label>
									</section>
								</div>

								<div class="form-group">
									<label for="inputSalesCode" class="col-lg-4 control-label">Sales Code :</label>
									<div class="col-lg-8" style="height: 35px">
										<label class="select" >
												<select name="ddlSalesCodeSearch" id="ddlSalesCodeSearch" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
														<option value="" selected> - Please Select - </option>
													<?php 
													while($row = mysql_fetch_array($result_emp_code)) 
													{
														echo "<option value='".$row['EMP_ID']."' ";
														if ($row['EMP_ID'] == $_POST['ddlSalesCodeSearch'])
														{
															echo " selected ";
														}
														echo ">".$row['EMP_CODE']."</option>";
													}
													mysql_data_seek ($result_emp_code , 0 );
													?>
												</select>
												
										</label>
									</div>
								</div>

								<div class="form-group">
									<label for="inputProductCode" class="col-lg-4 control-label">Product Sale Code :</label>
									<div class="col-lg-8">
										<label class="select">
											<select name="ddlProSaleCodeSearch" id="ddlProSaleCodeSearch" class="selectpicker form-control" 
												data-live-search="true" title="Please select ...">
													<option value="" selected> - Please Select - </option>
												<?php 
												while($row=mysql_fetch_array($result_pro_sale_code)) 
												{
													echo "<option value='".$row['PRODUCT_SALE_CODE']."' ";
													if ($row['PRODUCT_SALE_CODE'] == $_POST['ddlProSaleCodeSearch'])
													{
														echo " selected ";
													}
													echo">".$row['PRODUCT_SALE_CODE']."</option>";
												}
												mysql_data_seek ($result_pro_sale_code , 0 );
												?>
											</select>
											
										</label>
									</div>
								</div>
									
								 
								<div class="form-group">
									<div class="col-lg-offset-4 col-lg-8">
										<button type="submit" class="btn-u"> Search </button> &nbsp;&nbsp;
										<button type="button" class="btn-u btn-u-default">Clear</button>
									</div>
								</div>

							</form>

						</div>
					</div>

				</div>
			</div>    
			
		</div><!--/container--> 

	




    <!--=== End Search Block Version 2 ===-->

    <div class="container content-sm"  style="padding:30px;">


		<div class="col-lg-offset col-lg-8" style="height:50px">
			<button type="button" class="btn-u btn-u-green"  data-toggle="modal" data-target="#sales_transaction_dtl_form" 
				onclick="ClearSalesTranForm();"> <i class="fa fa-plus-square icon-color-white"></i> New Sales Transaction </button> 
		
			<?php  require("sales_transaction_dtl_form.php");  ?>
		</div>

		
		<div class="table-search-v2 margin-bottom-30">
			<div class="table-responsive">
				<form method="POST" enctype="multipart/form-data" action="sales_transaction_model.php?form=sales_tran_table" name="formSalesTranTable" onSubmit="return validate_form(this)"
					id="formSalesTranTable" >
					<input type="hidden" name="hdDelID" id="hdDelID"  value="">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th> &nbsp; </th>
								<th>Transaction No.</th>
								<th class="hidden-sm">Transaction Date</th>
								<th>Sales Code</th>
								<th>Product Code</th>
								<th>Sticker Price</th>
								<th>Shop name</th>
								<th>Sale in</th>
								<th>Sale Out</th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<?php 
								while($row=mysql_fetch_array($result_sales_tran)) 
								{
									if ($_REQUEST['focus'] == $row["TRN_SALES_NO"])
									{
										echo '<tr style="background-color:#ffffbb">';
										if (isset($_REQUEST['new'])){
											echo "<script>alert('New Transaction Number :'+".$_REQUEST['focus'].");</script>";
										}
										//*** Clear URL Query string
										echo '<script>history.pushState({},"URL Rewrite Example","'.$_SERVER['PHP_SELF'].'")</script>';
									}else
									{
										echo '<tr>';
									}
									echo '<td width="74px">
												<ul class="list-inline table-buttons">
													<li><button type="button" class="btn-u btn-u-sm btn-u-blue"data-toggle="modal" data-target="#sales_transaction_dtl_form" onclick="getDataAjax('.$row["SALES_DTL_ID"].')"><i class="fa fa-edit"></i> Edit</button></li>
												</ul>
											</td>';
									echo '<td width="100px"> <p>'.$row["TRN_SALES_NO"].'</p> </td>';
									echo '<td width="100px"> <p>'.$row["TRN_DATE"].'</p> </td>';
									echo '<td width="120px"> <p>'.$row["EMP_CODE"].'</p> </td>';
									echo '<td width="120px">'.$row["PRODUCT_SALE_CODE"].'</td>';
									echo '<td width="80px"> <p>'.number_format($row["STICKER_PRICE"],2).'</p> </td>';
									echo '<td> <p>'.$row["SHOP_NAME"].'</p> </td>';
									echo '<td width="85px"> <p>'.number_format($row["SALES_IN"],2).'</p> </td>';
									echo '<td width="85px"> <p>'.number_format($row["SALES_OUT"],2).'</p> </td>';
									
									echo '<td  width="42px">
													<ul class="list-inline table-buttons">
														<li><button type="submit" class="btn-u btn-u-sm btn-u-red" onclick ="return confirmDel('.$row["SALES_DTL_ID"] .')"><i class="fa fa-trash-o"></i> Delete</button></li>
													</ul>
												</td>
											</tr>';
								}
							?>
						
						</tbody>
					</table>
				</form>
			</div>    
		</div>    
        <!-- End Table Search v2 -->


        
    </div>
	<?php require("include_footer.php"); ?>
     
</div><!--/End Wrapepr-->

<?php require("include_js.php"); ?>

</body>
</html> 